/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.exceptions.SpringServiceNotInitializedException;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.SpellBook;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static pl.etp.creatures.spells.SpellStatistic.TargetType.ALLY;
import static pl.etp.spells.TestSpellFactory.createMagicArrow;
import static pl.etp.spells.TestSpellFactory.createMagicArrow2ElectricBoogaloo;

class GameEngineTest
{
	GameEngineTestUtil util;

	@BeforeEach
	void init()
	{
		util = new GameEngineTestUtil();
	}

	@Test
	void creatureShouldNotMoveTwoTimesAtTheSameRound()
	{
		//given
		Creature creature = new Creature.Builder().build();
		GameEngine engine = new GameEngine( new Hero( List.of( creature ) ), new Hero( List.of() ) );
		Point firstTargetPoint = engine.getBoardManager()
		                               .getPoint( 0, 2 );
		Point secondTargetPoint = engine.getBoardManager()
		                                .getPoint( 0, 3 );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( firstTargetPoint ) );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( secondTargetPoint ) );
		Creature positionAfterFirstMove = engine.getBoardManager()
		                                        .getCreatureByPoint( firstTargetPoint );
		Creature positionAfterSecondMove = engine.getBoardManager()
		                                         .getCreatureByPoint( secondTargetPoint );

		//then
		assertEquals( creature, positionAfterFirstMove );
		assertNull( positionAfterSecondMove );
	}

	@Test
	void shouldRecognizeFriendlyCreatureAndDoNotAttackHer()
	{
		Creature attacker = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );
		Creature defender = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );

		List<Creature> l1 = List.of( attacker, defender );
		List<Creature> l2 = List.of();

		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( l1 ) ), new Hero( new ArrayList<>( l2 ) ) );
		assertFalse( engine.canAttack( engine.getBoardManager()
		                                     .getPoint( 0, 1 ) ) );
	}

	@Test
	void shouldCorrectlyChangeActiveCreatureAfterPass()
	{
		Creature creature = new Creature.Builder().build();
		Creature creature2 = new Creature.Builder().build();
		GameEngine engine = new GameEngine( new Hero( List.of( creature, creature2 ) ), new Hero( List.of() ) );

		assertEquals( creature, engine.getActiveCreature() );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertEquals( creature2, engine.getActiveCreature() );

	}

	@Test
	void createMoveDoesntImpactOnOthersMoveCounter()
	{

		//given
		Creature creature = new Creature.Builder().build();
		Creature creature2 = new Creature.Builder().build();
		GameEngine engine = new GameEngine( new Hero( List.of( creature, creature2 ) ), new Hero( List.of() ) );
		Point firstTargetPoint = engine.getBoardManager()
		                               .getPoint( 0, 2 );
		Point secondTargetPoint = engine.getBoardManager()
		                                .getPoint( 0, 4 );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( firstTargetPoint ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( secondTargetPoint ) );

		//then
		assertEquals( creature, engine.getBoardManager()
		                              .getCreatureByPoint( firstTargetPoint ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( engine.getBoardManager()
		                                             .getPoint( 0, 1 ) ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( engine.getBoardManager()
		                                             .getPoint( 0, 3 ) ) );
	}


	@Test
	void createAttackDoesntImpactOnOthersAttackCounter()
	{

		//given
		Creature creature = new Creature.Builder().moveRange( 100 )
		                                          .armor( 100 )
		                                          .maxHp( 100 )
		                                          .build();
		Creature creature2 = new Creature.Builder().moveRange( 100 )
		                                           .armor( 100 )
		                                           .build();
		GameEngine engine = new GameEngine( new Hero( List.of( creature ) ), new Hero( List.of( creature2 ) ) );
		Point firstTargetPoint = engine.getBoardManager()
		                               .getPoint( 18, 1 );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( firstTargetPoint ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		engine.attack( firstTargetPoint );

		//then
		assertEquals( 1, creature2.getAttackContext()
		                          .getTurnAttackCounter() );
		assertEquals( 0, creature.getAttackContext()
		                         .getTurnAttackCounter() );
	}

	@Test
	void shouldReturnAllPossibleMoveDestinations()
	{
		//given
		List<Creature> creatures = util.generateCreatures();
		GameEngine engine = new GameEngine( new Hero( creatures ), new Hero( List.of() ) );

		//when
		List<Point> destinations = util.generateMoveDest( engine.getBoardManager() );
		List<Point> result = engine.getAvailableMoves();

		//then
		assertEquals( 59, result.size() );
		assertTrue( util.compare( result, destinations ) );
	}

	@Test
	void shouldReturnAllPossibleAttackDestinations()
	{
		//given
		List<Creature> creatures = util.generateCreatures();
		Creature attacker = new Creature.Builder().moveRange( 100 )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( creatures ), new Hero( List.of( attacker ) ) );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 0, 2 ) ) );
		for ( int i = 0; i <= creatures.size(); i++ )
		{
			assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		}
		assertEquals( attacker, engine.getActiveCreature() );

		//then
		List<Point> destinations = util.generateAttackDest( engine.getBoardManager() );
		List<Point> result = engine.getAvailableAttacks();
		assertEquals( 2, result.size() );
		assertTrue( util.compare( result, destinations ) );
	}

	@Test
	void shouldReturnAllPossibleMoveDestinationsAfterFirstPlayerMoveAndPass()
	{

		//given
		List<Creature> heroOneCreatures = List.of( new Creature.Builder().moveRange( 10 )
		                                                                 .build() );
		List<Creature> heroTwoCreatures = List.of( new Creature.Builder().moveRange( 10 )
		                                                                 .build() );
		GameEngine engine = new GameEngine( new Hero( heroOneCreatures ), new Hero( heroTwoCreatures ) );

		//FIRST PLAYER
		//when
		List<Point> destinations = util.generateMoveDest( engine.getBoardManager() );
		List<Point> result = engine.getAvailableMoves();

		//then
		assertEquals( 75, result.size() );

		//first player move and pass
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 0, 0 ) ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		//SECOND PLAYER
		//when
		List<Point> result2 = engine.getAvailableMoves();
		assertEquals( 75, result2.size() );
	}

	@Test
	void shouldReturnAllPossibleCastDestinationsForEnemyTypeSpell()
	{
		//given
		List<Creature> creatures = util.generateCreatures();
		Creature attacker = new Creature.Builder().moveRange( 100 )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( creatures ), new Hero( List.of( attacker ), new SpellBook( 20, List.of( createMagicArrow() ) ) ) );

		//when
		AbstractSpell spellToCast = engine.getActiveHero()
		                                  .getSpellBook()
		                                  .getSpells()
		                                  .get( 0 );
		List<Point> destinations = util.generateCastDest( engine.getBoardManager() );
		List<Point> result = engine.getAvailableCasts( spellToCast );

		//then
		assertEquals( 7, result.size() );
		assertTrue( util.compare( result, destinations ) );
	}

	@Test
	void shouldReturnAllPossibleCastDestinationsForAllyTypeSpell()
	{
		//given
		List<Creature> creatures = util.generateCreatures();
		Creature attacker = new Creature.Builder().moveRange( 100 )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( creatures ), new Hero( List.of( attacker ), new SpellBook( 20, List.of( createMagicArrow( ALLY ) ) ) ) );

		//when
		AbstractSpell spellToCast = engine.getActiveHero()
		                                  .getSpellBook()
		                                  .getSpells()
		                                  .get( 0 );
		List<Point> destinations = List.of( engine.getBoardManager()
		                                          .getPoint( 19, 1 ) );
		List<Point> result = engine.getAvailableCasts( spellToCast );

		//then
		assertEquals( 1, result.size() );
		assertTrue( util.compare( result, destinations ) );
	}

	@Test
	void shouldAllowToCastSecondSpellAfterEndOfTurn()
	{
		GameEngine engine = new GameEngine( new Hero( List.of( CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 7 ) ) ), new Hero( List.of( CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 7 ) ) ) );

		//hero1 is active
		assertTrue( engine.canCastSpellInThisTurn() );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( createMagicArrow(), engine.getBoardManager()
		                                                                                                            .getPoint( 0, 1 ) ) );
		assertFalse( engine.canCastSpellInThisTurn() );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		//hero2 is active
		assertTrue( engine.canCastSpellInThisTurn() );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( createMagicArrow(), engine.getBoardManager()
		                                                                                                            .getPoint( 19, 1 ) ) );
		assertFalse( engine.canCastSpellInThisTurn() );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		//hero1 is active
		assertTrue( engine.canCastSpellInThisTurn() );
	}

	@Test
	void shouldReturnCorrectSpellByNameFromListOfAvailableSpells()
	{
		List<Creature> creatures = util.generateCreatures();
		Creature attacker = new Creature.Builder().moveRange( 100 )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( creatures ), new Hero( List.of( attacker ), new SpellBook( 20, List.of( createMagicArrow(), createMagicArrow2ElectricBoogaloo() ) ) ) );


		AbstractSpell result = engine.getSpellFromAvailableSpells( "Tests Spell Magic Arrow 2: Electric Boogaloo" );

		assertEquals( "Tests Spell Magic Arrow 2: Electric Boogaloo", result.getName() );
	}

	@Test
	void shouldCanAttackBetween2MovesInOneTurn()
	{
		//given
		Creature moving2TimesCreature = CreatureFactory.create( CreatureStatistic.TEST_MOVING_TWO_TIMES, 1 );
		Creature defender = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( moving2TimesCreature ) ) ), new Hero( new ArrayList<>( List.of( defender ) ) ) );
		Point firstMoveTargetPoint = engine.getBoardManager()
		                                   .getPoint( 18, 1 );
		Point secondMoveTargetPoint = engine.getBoardManager()
		                                    .getPoint( 10, 1 );
		Point attackTargetPoint = engine.getBoardManager()
		                                .getPointByTile( defender );
		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( firstMoveTargetPoint ) );
		engine.attack( attackTargetPoint );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( secondMoveTargetPoint ) );

		//then
		Creature creatureAfterSecondMove = ( Creature ) engine.getBoardManager()
		                                                      .getTileByPoint( secondMoveTargetPoint );
		Creature creatureFromAttackTarget = ( Creature ) engine.getBoardManager()
		                                                       .getTileByPoint( attackTargetPoint );

		assertEquals( moving2TimesCreature, creatureAfterSecondMove );
		assertNull( creatureFromAttackTarget );
	}

	@Test
	void shouldResetCanCastAfterPass()
	{
		//given
		Creature defender = new Creature.Builder().maxHp( 1000 )
		                                          .build();
		Creature attacker1 = new Creature.Builder().moveRange( 100 )
		                                           .build();
		Creature attacker2 = new Creature.Builder().moveRange( 100 )
		                                           .build();
		Hero caster = new Hero( new ArrayList<>( List.of( attacker1, attacker2 ) ), new SpellBook( 20, List.of( createMagicArrow() ) ) );
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( defender ) ) ), caster );

		//when
		AbstractSpell spellToCast = engine.getActiveHero()
		                                  .getSpellBook()
		                                  .getSpells()
		                                  .get( 0 );
		Point pointToCast = engine.getBoardManager()
		                          .getPointByTile( defender );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( spellToCast, pointToCast ) );

		//then
		assertEquals( caster, engine.getActiveHero() );
		assertFalse( engine.canCastSpellInThisTurn() );

		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		assertEquals( caster, engine.getActiveHero() );
		assertTrue( engine.canCastSpellInThisTurn() );
	}
}