/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.spells.SpellStatistic;
import pl.etp.creatures.statistic.CreatureStatistic;


import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SpellCastingRulesManagerTest
{
	BoardManager boardManager;

	@BeforeEach
	void init()
	{
		boardManager = new BoardManager();
	}

	@Test
	void shouldNotAttackWhileBoardIsEmptyInTargetedPoint()
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		GameEngine gameEngine = mock( GameEngine.class );
		when( gameEngine.isAllyCreature( any() ) ).thenReturn( true );
		when( gameEngine.isEnemyCreature( any() ) ).thenReturn( false );
		when( gameEngine.getBoardManager() ).thenReturn( boardManager );

		Set<Point> result = spellCastingRulesManager.calculateCastTargetPoints( TestSpellFactory.createMagicArrow(), boardManager.getPoint( 10, 10 ), boardManager, gameEngine::isEnemyCreature );

		assertTrue( result.isEmpty() );
	}

	@Test
	void shouldCastSpellOnlyForTargetPlace()
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		GameEngine gameEngine = mock( GameEngine.class );
		when( gameEngine.isAllyCreature( any() ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( any() ) ).thenReturn( true );
		when( gameEngine.getBoardManager() ).thenReturn( boardManager );
		boardManager.putOnBoard( boardManager.getPoint( 10, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 11, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );

		Set<Point> result = spellCastingRulesManager.calculateCastTargetPoints( TestSpellFactory.createMagicArrow(), boardManager.getPoint( 10, 10 ), boardManager, gameEngine::isEnemyCreature );

		assertEquals( 1, result.size() );
		assertTrue( result.contains( boardManager.getPoint( 10, 10 ) ) );
	}

	@Test
	void shouldCastSpellForAllExistsCreature()
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		boardManager.putOnBoard( boardManager.getPoint( 10, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 9, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 10, 9 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 11, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 10, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 9, 9 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 11, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 9, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 11, 9 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		//shouldn't be attacked
		boardManager.putOnBoard( boardManager.getPoint( 11, 12 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 12, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 9, 8 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 8, 9 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		GameEngine gameEngine = mock( GameEngine.class );
		when( gameEngine.isAllyCreature( any() ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( any() ) ).thenReturn( true );
		when( gameEngine.getBoardManager() ).thenReturn( boardManager );

		Set<Point> result = spellCastingRulesManager.calculateCastTargetPoints( TestSpellFactory.createMagicArrowWithSplash( 1 ), boardManager.getPoint( 10, 10 ), boardManager, gameEngine::isEnemyCreature );

		assertEquals( 9, result.size() );
	}

	@Test
	void shouldCastSpellOnlyForAllies()
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		boardManager.putOnBoard( boardManager.getPoint( 10, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 9, 9 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 11, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		GameEngine gameEngine = mock( GameEngine.class );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 11, 11 ) ) ).thenReturn( true );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 10, 10 ) ) ).thenReturn( true );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 9, 9 ) ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 11, 11 ) ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 10, 10 ) ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 9, 9 ) ) ).thenReturn( true );
		when( gameEngine.getBoardManager() ).thenReturn( boardManager );

		Set<Point> result = spellCastingRulesManager.calculateCastTargetPoints( TestSpellFactory.createMagicArrowWithSplashAndTargetType( 1, SpellStatistic.TargetType.ALLY ), boardManager.getPoint( 10, 10 ), boardManager, gameEngine::isEnemyCreature );

		assertEquals( 2, result.size() );
		assertTrue( result.contains( boardManager.getPoint( 10, 10 ) ) );
		assertTrue( result.contains( boardManager.getPoint( 11, 11 ) ) );
	}

	@Test
	void shouldCastSpellOnlyForEnemies()
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		boardManager.putOnBoard( boardManager.getPoint( 10, 10 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		boardManager.putOnBoard( boardManager.getPoint( 10, 11 ), CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) );
		GameEngine gameEngine = mock( GameEngine.class );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 11, 11 ) ) ).thenReturn( true );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 10, 10 ) ) ).thenReturn( false );
		when( gameEngine.isAllyCreature( boardManager.getPoint( 9, 9 ) ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 11, 11 ) ) ).thenReturn( false );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 10, 10 ) ) ).thenReturn( true );
		when( gameEngine.isEnemyCreature( boardManager.getPoint( 9, 9 ) ) ).thenReturn( true );
		when( gameEngine.getBoardManager() ).thenReturn( boardManager );

		Set<Point> result = spellCastingRulesManager.calculateCastTargetPoints( TestSpellFactory.createMagicArrowWithSplash( 1 ), boardManager.getPoint( 10, 10 ), boardManager, gameEngine::isEnemyCreature );

		assertEquals( 1, result.size() );
		assertTrue( result.contains( boardManager.getPoint( 10, 10 ) ) );
	}
}