/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.creatures.Creature;
import pl.etp.exceptions.SpringServiceNotInitializedException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatusEffectSpellTest
{
	@Test
	void shouldApplyBuff()
	{
		//given
		Creature creature = new Creature.Builder().moveRange( 5 )
		                                          .build();
		AbstractSpell buff = TestSpellFactory.createBuff();

		//when
		buff.cast( creature );

		//then
		assertEquals( 10, creature.getMoveRange() );
	}

	@Test
	void shouldRemoveBuffAfter2Rounds()
	{
		//given
		Creature creature = new Creature.Builder().moveRange( 5 )
		                                          .build();
		AbstractSpell buff = TestSpellFactory.createBuff();

		GameEngine engine = new GameEngine( new Hero( List.of( creature ), new SpellBook( 10, List.of( buff ) ) ), new Hero( List.of( new Creature.Builder().build() ) ) );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( buff, engine.getBoardManager()
		                              .getPointByTile( creature ) ) );

		//then
		assertEquals( 10, creature.getMoveRange() );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertEquals( 10, creature.getMoveRange() );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertEquals( 5, creature.getMoveRange() );
	}

	@Test
	void shouldApplyMoreThanOneBuffAndRemoveInGoodTime()
	{
		//given
		Creature creature = new Creature.Builder().moveRange( 5 )
		                                          .maxHp( 5 )
		                                          .build();
		AbstractSpell buff1 = TestSpellFactory.createBuff();
		AbstractSpell buff2 = TestSpellFactory.createHpBuff();

		GameEngine engine = new GameEngine( new Hero( List.of( creature ), new SpellBook( 10, List.of( buff1, buff2 ) ) ), new Hero( List.of( new Creature.Builder().build() ) ) );

		//when && then
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( buff1, engine.getBoardManager()
		                               .getPointByTile( creature ) ) );

		assertEquals( 10, creature.getMoveRange() );
		assertEquals( 5, creature.getDefenceContext()
		                         .getDefenceStatistic()
		                         .getMaxHp() );

		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( buff2, engine.getBoardManager()
		                               .getPointByTile( creature ) ) );

		assertEquals( 10, creature.getMoveRange() );
		assertEquals( 10, creature.getDefenceContext()
		                          .getDefenceStatistic()
		                          .getMaxHp() );

		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		assertEquals( 5, creature.getMoveRange() );
		assertEquals( 10, creature.getDefenceContext()
		                          .getDefenceStatistic()
		                          .getMaxHp() );

		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		assertEquals( 5, creature.getMoveRange() );
		assertEquals( 5, creature.getDefenceContext()
		                         .getDefenceStatistic()
		                         .getMaxHp() );
	}

}