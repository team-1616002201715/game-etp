/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.Creature;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TurnQueueTest
{
	private Creature creature1;
	private Creature creature2;
	private Creature creature3;

	@BeforeEach
	void init()
	{
		creature1 = new Creature.Builder().moveRange( 3 )
		                                  .build();
		creature2 = new Creature.Builder().moveRange( 4 )
		                                  .build();
		creature3 = new Creature.Builder().moveRange( 5 )
		                                  .build();
	}

	@Test
	void shouldChangeActiveCreature()
	{
		TurnQueue creatureTurnQueue = new TurnQueue( new Hero( List.of( creature1 ) ), new Hero( List.of( creature2 ) ) );

		assertEquals( creature2, creatureTurnQueue.getActiveCreature() );
		creatureTurnQueue.next();

		assertEquals( creature1, creatureTurnQueue.getActiveCreature() );
	}

	@Test
	void shouldCorrectlyRollQueue()
	{
		TurnQueue creatureTurnQueue = new TurnQueue( new Hero( List.of( creature1, creature2 ) ), new Hero( List.of( creature3 ) ) );

		assertEquals( creature3, creatureTurnQueue.getActiveCreature() );
		creatureTurnQueue.next();

		assertEquals( creature2, creatureTurnQueue.getActiveCreature() );
		creatureTurnQueue.next();

		assertEquals( creature1, creatureTurnQueue.getActiveCreature() );
		creatureTurnQueue.next();

		assertEquals( creature3, creatureTurnQueue.getActiveCreature() );
	}
}