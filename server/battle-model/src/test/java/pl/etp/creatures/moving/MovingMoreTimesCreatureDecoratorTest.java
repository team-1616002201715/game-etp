/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import org.junit.jupiter.api.Test;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;

import static org.junit.jupiter.api.Assertions.*;

class MovingMoreTimesCreatureDecoratorTest
{
	@Test
	void shouldCanMove2TimesInOneTurn()
	{
		//given
		BoardManager boardManager = new BoardManager();
		MoveEngine moveEngine = new MoveEngine( boardManager );
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point firstTargetPoint = boardManager.getPoint( 0, 1 );
		Point secondTargetPoint = boardManager.getPoint( 0, 2 );
		Creature moving2TimesCreature = CreatureFactory.create( CreatureStatistic.TEST_MOVING_TWO_TIMES, 1 );

		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), moving2TimesCreature );

		//when
		moveEngine.move( moving2TimesCreature, firstTargetPoint );
		moveEngine.move( moving2TimesCreature, secondTargetPoint );

		//then
		Creature creatureFromBoard = boardManager.getCreatureByPoint( secondTargetPoint );
		assertEquals( moving2TimesCreature, creatureFromBoard );
		assertFalse( moveEngine.canMove( moving2TimesCreature, boardManager.getPoint( 0, 4 ) ) );
		assertNull( boardManager.getCreatureByPoint( sourcePoint ) );
		assertNull( boardManager.getCreatureByPoint( firstTargetPoint ) );
	}
}