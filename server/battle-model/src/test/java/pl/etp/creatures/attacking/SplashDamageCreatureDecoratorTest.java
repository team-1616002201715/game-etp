/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.board.BoardManager;
import pl.etp.creatures.Creature;
import pl.etp.creatures.defending.DefenceContextFactory;
import pl.etp.creatures.defending.DefenceContextIf;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class SplashDamageCreatureDecoratorTest
{
	@Test
	void checkSplashDamage()
	{
		Creature splashCreature = CreatureFactory.create( CreatureStatistic.RAILGUN, 1 );

		DefenceContextIf defenderDef = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock1Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock2Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock3Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock4Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock5Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock6Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock7Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf splashMock8Def = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );
		DefenceContextIf notSplashMockDef = spy( ( DefenceContextFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 ) ) );

		Creature defender = new Creature.Builder().defenceContext( defenderDef )
		                                          .build();
		Creature splashMock1 = new Creature.Builder().defenceContext( splashMock1Def )
		                                             .build();
		Creature splashMock2 = new Creature.Builder().defenceContext( splashMock2Def )
		                                             .build();
		Creature splashMock3 = new Creature.Builder().defenceContext( splashMock3Def )
		                                             .build();
		Creature splashMock4 = new Creature.Builder().defenceContext( splashMock4Def )
		                                             .build();
		Creature splashMock5 = new Creature.Builder().defenceContext( splashMock5Def )
		                                             .build();
		Creature splashMock6 = new Creature.Builder().defenceContext( splashMock6Def )
		                                             .build();
		Creature splashMock7 = new Creature.Builder().defenceContext( splashMock7Def )
		                                             .build();
		Creature splashMock8 = new Creature.Builder().defenceContext( splashMock8Def )
		                                             .build();
		Creature notSplashMock = new Creature.Builder().defenceContext( notSplashMockDef )
		                                               .build();


		BoardManager board = new BoardManager();
		board.putOnBoard( board.getPoint( 10, 10 ), defender );
		board.putOnBoard( board.getPoint( 10, 9 ), splashMock1 );
		board.putOnBoard( board.getPoint( 10, 11 ), splashMock2 );
		board.putOnBoard( board.getPoint( 9, 10 ), splashMock3 );
		board.putOnBoard( board.getPoint( 11, 10 ), splashMock4 );
		board.putOnBoard( board.getPoint( 11, 11 ), splashMock5 );
		board.putOnBoard( board.getPoint( 9, 9 ), splashMock6 );
		board.putOnBoard( board.getPoint( 11, 9 ), splashMock7 );
		board.putOnBoard( board.getPoint( 9, 11 ), splashMock8 );
		board.putOnBoard( board.getPoint( 0, 4 ), notSplashMock );

		GameEngine gameEngine = new GameEngine( new Hero( List.of( splashCreature ) ), new Hero( Collections.emptyList() ), board );
		gameEngine.attack( board.getPoint( 10, 10 ) );

		verify( defenderDef, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock1Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock2Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock3Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock4Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock5Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock6Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock7Def, times( 1 ) ).applyDamage( anyInt() );
		verify( splashMock8Def, times( 1 ) ).applyDamage( anyInt() );
		verify( notSplashMockDef, never() ).applyDamage( anyInt() );
	}

}