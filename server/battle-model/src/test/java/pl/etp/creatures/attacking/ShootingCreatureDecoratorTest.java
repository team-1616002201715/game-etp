/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.exceptions.SpringServiceNotInitializedException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static pl.etp.creatures.statistic.CreatureStatistic.SPECTRE;
import static pl.etp.creatures.statistic.CreatureStatistic.TEST_SHOOTING_CREATURE;

class ShootingCreatureDecoratorTest
{

	@Test
	void onlyShooterShouldAttackOnDistance()
	{
		//given
		Creature shootingCreature = CreatureFactory.create( TEST_SHOOTING_CREATURE, 1 );
		Creature defender = new Creature.Builder().moveRange( 1 )
		                                          .maxHp( 300 )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( List.of( shootingCreature ) ), new Hero( List.of( defender ) ) );

		//when && then
		assertTrue( engine.canAttack( engine.getBoardManager()
		                                    .getPoint( GameEngine.BOARD_WIDTH - 1, 1 ) ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertFalse( engine.canAttack( engine.getBoardManager()
		                                     .getPoint( 0, 1 ) ) );
	}

	@Test
	void defenderShouldNotCounterAttackShooter()
	{
		//given
		Creature shootingCreature = CreatureFactory.create( TEST_SHOOTING_CREATURE, 1 );
		Creature defender = new Creature.Builder().moveRange( 1 )
		                                          .maxHp( 300 )
		                                          .build();
		AttackEngine attackEngine = spy( new AttackEngine() );

		//when
		attackEngine.attack( shootingCreature, defender );

		//then
		verify( attackEngine, never() ).attack( defender.getAttackContext(), shootingCreature.getDefenceContext() );
		assertEquals( 0, defender.getRetaliationContext()
		                         .getRetaliationCounter() );
	}

}