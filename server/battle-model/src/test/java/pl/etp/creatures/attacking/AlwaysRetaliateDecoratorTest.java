/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import org.junit.jupiter.api.Test;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static pl.etp.creatures.statistic.CreatureStatistic.TEST_ALWAYS_RETALIATE;
import static pl.etp.creatures.statistic.CreatureStatistic.TEST_MOVING_TWO_TIMES;

class AlwaysRetaliateDecoratorTest
{

	@Test
	void shouldRetaliateAlways()
	{
		//given
		Creature alwaysRetaliate = CreatureFactory.create( TEST_ALWAYS_RETALIATE, 1 );
		Creature attacker1 = CreatureFactory.create( TEST_MOVING_TWO_TIMES, 1 );
		Creature attacker2 = CreatureFactory.create( TEST_MOVING_TWO_TIMES, 1 );
		AttackEngine attackEngine = spy( new AttackEngine() );

		//when
		attackEngine.attack( attacker1, alwaysRetaliate );
		attackEngine.attack( attacker2, alwaysRetaliate );

		//then
		verify( attackEngine, times( 1 ) ).attack( alwaysRetaliate.getAttackContext(), attacker1.getDefenceContext() );
		verify( attackEngine, times( 1 ) ).attack( alwaysRetaliate.getAttackContext(), attacker2.getDefenceContext() );
		assertEquals( 2, alwaysRetaliate.getRetaliationContext()
		                                .getRetaliationCounter() );
		assertTrue( alwaysRetaliate.getRetaliationContext()
		                           .canRetaliate() );
	}
}