/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.exceptions.SpringServiceNotInitializedException;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.SpellBook;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static pl.etp.spells.TestSpellFactory.createMagicArrow;
import static pl.etp.spells.TestSpellFactory.createMagicArrowWithSplash;

class DeathTest
{
	GameEngine engine;
	Creature attacker;
	Creature defender;

	@BeforeEach
	void init()
	{
		attacker = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 10 );
		defender = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );
		engine = new GameEngine( new Hero( new ArrayList<>( List.of( attacker ) ) ), new Hero( new ArrayList<>( List.of( defender ) ) ) );
	}

	@Test
	void creatureShouldBeRemovedFromBoardAfterDeath()
	{
		//given
		Point attackPoint = engine.getBoardManager()
		                          .getPointByTile( defender );

		//when
		engine.attack( attackPoint );

		//then
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( attackPoint ) );
	}

	@Test
	void itShouldNotBePossibleToControlCreatureAfterItDies()
	{
		//given
		Point attackPoint = engine.getBoardManager()
		                          .getPointByTile( defender );

		//when
		engine.attack( attackPoint );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );

		//then
		assertEquals( engine.getActiveCreature(), attacker );
	}

	@Test
	void creatureShouldNotCounterAttackAfterDeath()
	{
		//given
		AttackEngine attackEngine = spy( new AttackEngine() );

		//when
		attackEngine.attack( attacker, defender );

		//then
		verify( attackEngine, never() ).attack( defender.getAttackContext(), attacker.getDefenceContext() );
	}

	@Test
	void creatureShouldBeRemovedFromBoardAfterDeathBySpell()
	{
		//given
		Creature defender = new Creature.Builder().maxHp( 1 )
		                                          .build();
		Creature attacker1 = new Creature.Builder().moveRange( 100 )
		                                           .build();

		Hero caster = new Hero( new ArrayList<>( List.of( attacker1 ) ), new SpellBook( 20, List.of( createMagicArrow() ) ) );
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( defender ) ) ), caster );

		//when
		AbstractSpell spellToCast = engine.getActiveHero()
		                                  .getSpellBook()
		                                  .getSpells()
		                                  .get( 0 );
		Point pointToCast = engine.getBoardManager()
		                          .getPointByTile( defender );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( spellToCast, pointToCast ) );

		//then
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( pointToCast ) );
	}

	@Test
	void creatureShouldBeRemovedFromBoardAfterDeathBySplashSpell()
	{
		//given
		Creature targetCreature = new Creature.Builder().maxHp( 1 )
		                                                .build();
		Creature splash1 = new Creature.Builder().maxHp( 1 )
		                                         .build();
		Creature splash2 = new Creature.Builder().maxHp( 1 )
		                                         .build();
		Creature splash3 = new Creature.Builder().maxHp( 1 )
		                                         .build();
		Creature attacker1 = new Creature.Builder().moveRange( 100 )
		                                           .build();

		BoardManager boardManager = new BoardManager();

		boardManager.putOnBoard( boardManager.getPoint( 10, 10 ), targetCreature );
		boardManager.putOnBoard( boardManager.getPoint( 10, 9 ), splash1 );
		boardManager.putOnBoard( boardManager.getPoint( 10, 11 ), splash2 );
		boardManager.putOnBoard( boardManager.getPoint( 9, 10 ), splash3 );

		Hero caster = new Hero( new ArrayList<>( List.of( attacker1 ) ), new SpellBook( 20, List.of( createMagicArrowWithSplash( 9 ) ) ) );
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( targetCreature ) ) ), caster, boardManager );

		//when
		AbstractSpell spellToCast = engine.getActiveHero()
		                                  .getSpellBook()
		                                  .getSpells()
		                                  .get( 0 );
		Point pointToCast = engine.getBoardManager()
		                          .getPointByTile( targetCreature );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.castSpell( spellToCast, pointToCast ) );

		//then
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( boardManager.getPoint( 10, 10 ) ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( boardManager.getPoint( 10, 9 ) ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( boardManager.getPoint( 10, 11 ) ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( boardManager.getPoint( 9, 10 ) ) );
	}
}