/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.etp.creatures.Creature;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DefaultRetaliationContextTest
{

	@Test
	void creatureShouldCounterAttack()
	{
		Creature attacker = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .build();
		Creature defender = new Creature.Builder().armor( 10 )
		                                          .maxHp( 100 )
		                                          .damage( Range.closed( 10, 10 ) )
		                                          .build();

		AttackEngine attackEngine = spy( new AttackEngine() );
		attackEngine.attack( attacker, defender );

		verify( attackEngine, Mockito.times( 1 ) ).attack( defender.getAttackContext(), attacker.getDefenceContext() );
		assertEquals( 95, attacker.getCurrentHp() );
	}

	@Test
	void creatureShouldCounterAttackOnlyOnceAtTurn()
	{
		Creature attacker = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .build();
		Creature attacker2 = new Creature.Builder().armor( 5 )
		                                           .maxHp( 100 )
		                                           .build();
		Creature defender = new Creature.Builder().armor( 10 )
		                                          .maxHp( 100 )
		                                          .damage( Range.closed( 10, 10 ) )
		                                          .build();

		AttackEngine attackEngine = spy( new AttackEngine() );
		attackEngine.attack( attacker, defender );
		attackEngine.attack( attacker2, defender );

		assertEquals( 95, attacker.getCurrentHp() );
		assertEquals( 100, attacker2.getCurrentHp() );
		verify( attackEngine, Mockito.times( 1 ) ).attack( defender.getAttackContext(), attacker.getDefenceContext() );
		verify( attackEngine, never() ).attack( defender.getAttackContext(), attacker2.getDefenceContext() );
	}
}