/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;

import static org.junit.jupiter.api.Assertions.*;

class MoveEngineTest
{
	private MoveEngine moveEngine;
	private BoardManager boardManager;
	private Creature groundCreature;
	private Creature flyingCreature;
	private Creature teleportCreature;

	@BeforeEach
	void init()
	{
		boardManager = new BoardManager();
		moveEngine = new MoveEngine( boardManager );
		groundCreature = new Creature.Builder().moveRange( 4 )
		                                       .moveStrategy( MovementType.GROUND )
		                                       .build();
		flyingCreature = new Creature.Builder().moveRange( 9 )
		                                       .moveStrategy( MovementType.FLYING )
		                                       .build();
		teleportCreature = new Creature.Builder().moveRange( 4 )
		                                         .moveStrategy( MovementType.TELEPORT )
		                                         .build();
	}

	@Test
	void shouldReturnTrueWhenCreatureHasEnoughMoveRangeToReachTarget()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 1 );
		boardManager.putOnBoard( sourcePoint, teleportCreature );

		//when
		boolean result = moveEngine.canMove( teleportCreature, targetPoint );

		//then
		assertTrue( result );
	}

	@Test
	void shouldReturnFalseWhenCreatureHasNotEnoughMoveRangeToReachTarget()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 5 );
		boardManager.putOnBoard( sourcePoint, teleportCreature );

		//when
		boolean result = moveEngine.canMove( teleportCreature, targetPoint );

		//then
		assertFalse( result );
	}

	@Test
	void shouldReturnFalseWhenCreatureTryToMoveOnOccupiedTile()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 5 );
		boardManager.putOnBoard( sourcePoint, teleportCreature );
		Creature blocker = new Creature.Builder().moveRange( 4 )
		                                         .build();
		boardManager.putOnBoard( targetPoint, blocker );

		//when
		boolean result = moveEngine.canMove( teleportCreature, targetPoint );

		//then
		assertFalse( result );
	}

	@Test
	void teleportCreatureShouldMoveCorrectly()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 1 );
		boardManager.putOnBoard( sourcePoint, teleportCreature );

		//when
		moveEngine.move( teleportCreature, targetPoint );
		Creature creatureFromBoard = boardManager.getCreatureByPoint( targetPoint );

		//then
		assertEquals( teleportCreature, creatureFromBoard );
		assertNull( boardManager.getCreatureByPoint( sourcePoint ) );
	}

	@Test
	void groundCreatureShouldMoveCorrectly()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 1 );
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), groundCreature );

		//when
		moveEngine.move( groundCreature, targetPoint );
		Creature creatureFromBoard = boardManager.getCreatureByPoint( targetPoint );

		//then
		assertEquals( groundCreature, creatureFromBoard );
		assertNull( boardManager.getCreatureByPoint( sourcePoint ) );
	}

	@Test
	void flyingCreatureShouldMoveCorrectly()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 0, 1 );
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), flyingCreature );

		//when
		moveEngine.move( flyingCreature, targetPoint );
		Creature creatureFromBoard = boardManager.getCreatureByPoint( targetPoint );

		//then
		assertEquals( flyingCreature, creatureFromBoard );
		assertNull( boardManager.getCreatureByPoint( sourcePoint ) );
	}
}