/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking.damageCalculator;

import com.google.common.collect.Range;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.Creature;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IgnoreArmorDamageCalculatorTest
{
	private Random randomizer;

	@BeforeEach
	void init()
	{
		randomizer = mock( Random.class );
		when( randomizer.nextInt( anyInt() ) ).thenReturn( 15 );
	}


	@Test
	void shouldIgnoreDefenderArmor()
	{
		Creature attacker = new Creature.Builder().name( "Attacker" )
		                                          .damage( Range.closed( 20, 20 ) )
		                                          .build();
		Creature defender = new Creature.Builder().name( "Defender" )
		                                          .armor( 10 )
		                                          .build();

		IgnoreArmorDamageCalculator defaultDamageCalculator = new IgnoreArmorDamageCalculator();
		int damageToDeal = defaultDamageCalculator.calculateDamage( attacker.getAttackContext()
		                                                                    .getAttackerStatistic(), defender.getArmor(), defender.getAmount() );
		assertEquals( 20, damageToDeal );
	}
}