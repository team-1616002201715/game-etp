/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PathFinderTest
{
	private BoardManager boardManager;
	PathFinder groundPathFinder;
	PathFinder flyingPathFinder;

	@BeforeEach
	void init()
	{
		int maxMoveRange = Integer.MAX_VALUE;
		groundPathFinder = new PathFinder( MovementType.GROUND, maxMoveRange );
		flyingPathFinder = new PathFinder( MovementType.FLYING, maxMoveRange );
		boardManager = new BoardManager();
	}

	@Test
	void manhattanHeuristicShouldReturnCorrectValue()
	{
		ManhattanDistanceHeuristic h = new ManhattanDistanceHeuristic();

		assertEquals( 3, h.getCost( boardManager.getPoint( 0, 0 ), boardManager.getPoint( 1, 2 ) ) );

	}

	@Test
	void euclideanHeuristicShouldReturnCorrectValue()
	{
		EuclideanDistanceHeuristic h = new EuclideanDistanceHeuristic();

		assertEquals( 10, ( int ) h.getCost( boardManager.getPoint( 0, 0 ), boardManager.getPoint( 1, 10 ) ) );

	}

	@Test
	void pathShouldContainCorrectPoints()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0, 0 );
		Point targetPoint = boardManager.getPoint( 8, 0 );

		//when
		List<Point> path = groundPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                   .getSteps();

		//then
		assertTrue( path.contains( boardManager.getPoint( 1, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 2, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 3, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 4, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 5, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 6, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 7, 0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 8, 0 ) ) );
		assertEquals( 8, path.size() );

	}

	@Test
	void pathFinderShouldReturnEmptyPathWhenMoveRangeIsLowerThanDistance()
	{
		//given
		PathFinder specificPathFinder = new PathFinder( MovementType.GROUND, 1 );
		Point sourcePoint = boardManager.getPoint( 0,0 );
		Point targetPoint = boardManager.getPoint( 3,0 );

		//then
		assertEquals( 0, specificPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                   .getSteps()
		                                   .size() );

	}

	@Test
	void pathFinderShouldReturnEmptyPathWhenTargetPointIsNotStandable()
	{
		//given
		Point sourcePoint = boardManager.getPoint( 0,0 );
		Point targetPoint = boardManager.getPoint( 3,0 );
		Creature creature = new Creature.Builder().moveRange( 4 ).build();

		//when
		boardManager.putOnBoard( targetPoint, creature );

		//then
		assertEquals( 0, groundPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                 .getSteps()
		                                 .size() );

	}

	@Test
	void pathFinderShouldReturnEmptyPathWhenCreatureCannotReachTarget()
	{
		// M - mover, MoveType.GROUND, C - Creature (isMovable return false for MoveType.GROUND), T - Target point
		//    0 1 2 3 4
		//  0 M C x x x
		//  1 C T x x x
		//  2 x x x x x
		//  3 x x x x x
		//  4 x x x x x

		Point sourcePoint = boardManager.getPoint( 0,0 );
		Point targetPoint = boardManager.getPoint( 1, 1);
		Point obstacle1 = boardManager.getPoint( 0,1 );
		Point obstacle2 = boardManager.getPoint( 1,0 );
		boardManager.putOnBoard( obstacle1, new Creature.Builder().moveRange( 4 ).build() );
		boardManager.putOnBoard( obstacle2, new Creature.Builder().moveRange( 4 ).build() );

		assertEquals( 0, groundPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                 .getSteps()
		                                 .size() );

	}

	@Test
	void groundStrategyCanAvoidObstacle()
	{
		// M - mover, MoveType.GROUND, C - Creature (isMovable return false for MoveType.GROUND), T - Target point
		//    0 1 2 3 4
		//  0 M x x x x
		//  1 x C x x x
		//  2 x C x x x
		//  3 C x x x x
		//  4 T x x x x

		//given
		Point sourcePoint = boardManager.getPoint( 0,0 );
		Point targetPoint = boardManager.getPoint( 0,4 );
		Point obstacle1 = boardManager.getPoint( 1,1 );
		Point obstacle2 = boardManager.getPoint( 1,2 );
		Point obstacle3 = boardManager.getPoint( 0,3 );
		boardManager.putOnBoard( obstacle1, new Creature.Builder().moveRange( 4 ).build() );
		boardManager.putOnBoard( obstacle2, new Creature.Builder().moveRange( 4 ).build() );
		boardManager.putOnBoard( obstacle3, new Creature.Builder().moveRange( 4 ).build() );

		//when
		List<Point> path = groundPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                   .getSteps();

		//then
		assertEquals( 8, path.size() );
		assertTrue( path.contains( boardManager.getPoint( 1,0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 2,0 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 2,1 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 2,2 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 2,3 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 1,3 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 1,4 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 0,4 ) ) );
	}

	@Test
	void flyingStrategyCanOverflyCreatureObstacle()
	{
		// M - mover, MoveType.FLYING, C - Creature (isMovable return true for MoveType.FLYING), T - Target point
		//    0 1 2 3 4
		//  0 M C x x x
		//  1 C T x x x
		//  2 x x x x x
		//  3 x x x x x
		//  4 x x x x x

		//given
		Point sourcePoint = boardManager.getPoint( 0,0 );
		Point targetPoint = boardManager.getPoint( 1,1 );
		Point obstacle1 = boardManager.getPoint( 0,1 );
		Point obstacle2 = boardManager.getPoint( 1,0 );
		boardManager.putOnBoard( obstacle1, new Creature.Builder().moveRange( 4 ).build() );
		boardManager.putOnBoard( obstacle2, new Creature.Builder().moveRange( 4 ).build() );

		//when
		List<Point> path = flyingPathFinder.getPath( boardManager, sourcePoint, targetPoint )
		                                   .getSteps();

		//then
		assertEquals( 2, path.size() );
		assertTrue( path.contains( boardManager.getPoint( 0,1 ) ) );
		assertTrue( path.contains( boardManager.getPoint( 1,1 ) ) );

	}
}