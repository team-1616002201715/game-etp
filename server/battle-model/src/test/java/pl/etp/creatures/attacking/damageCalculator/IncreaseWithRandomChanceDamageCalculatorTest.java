/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking.damageCalculator;

import com.google.common.collect.Range;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.attacking.AttackStatistic;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IncreaseWithRandomChanceDamageCalculatorTest
{
	private int DEFENDER_DEFENCE = 10;
	private int DEFENDER_AMOUNT = 1;
	private AttackStatistic attackerStats;
	private Random rand;

	@BeforeEach
	void init()
	{
		attackerStats = AttackStatistic.builder()
		                               .amount( 20 )
		                               .damage( Range.closed( 5, 5 ) )
		                               .build();
		rand = mock( Random.class );
		when( rand.nextInt( anyInt() ) ).thenReturn( 0 );
	}

	@Test
	void shouldDealDoubleDamageIfRandomizationPositive()
	{
		when( rand.nextDouble() ).thenReturn( 0.19 );
		AbstractDamageCalculator calc = new IncreaseWithRandomChanceDamageCalculator( 0.2, 2.0, rand );

		int result = calc.calculateDamage( attackerStats, DEFENDER_DEFENCE, DEFENDER_AMOUNT );

		assertEquals( 180, result );
	}

	@Test
	void shouldDealNormalDamageIfRandomizationNegative()
	{
		when( rand.nextDouble() ).thenReturn( 0.21 );
		AbstractDamageCalculator calc = new IncreaseWithRandomChanceDamageCalculator( 0.2, 2.0, rand );

		int result = calc.calculateDamage( attackerStats, DEFENDER_DEFENCE, DEFENDER_AMOUNT );

		assertEquals( 90, result );
	}
}