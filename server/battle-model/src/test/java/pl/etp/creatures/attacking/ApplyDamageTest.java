/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.Creature;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplyDamageTest
{

	@Test
	void shouldLostOneCreatureFromStackAndHasFullHp()
	{
		Creature attacker = new Creature.Builder().damage( Range.closed( 200, 200 ) )
		                                          .build();

		Creature defender = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .amount( 10 )
		                                          .build();

		AttackEngine attackEngine = new AttackEngine();
		attackEngine.attack( attacker, defender );

		assertEquals( 9, defender.getAmount() );
		assertEquals( 100, defender.getCurrentHp() );
	}

	@Test
	void shouldLostTwoCreatureFromStack()
	{
		Creature attacker = new Creature.Builder().damage( Range.closed( 200, 200 ) )
		                                          .build();

		Creature defender = new Creature.Builder().armor( 5 )
		                                          .maxHp( 50 )
		                                          .amount( 10 )
		                                          .build();

		AttackEngine attackEngine = new AttackEngine();
		attackEngine.attack( attacker, defender );

		assertEquals( 8, defender.getAmount() );
		assertEquals( 50, defender.getCurrentHp() );
	}

	@Test
	void shouldLost99HpButWithoutCreatureFromStack()
	{
		Creature attacker = new Creature.Builder()

				.damage( Range.closed( 100, 100 ) )
				.build();
		Creature defender = new Creature.Builder().armor( 1 )
		                                          .maxHp( 100 )
		                                          .amount( 1 )
		                                          .build();

		AttackEngine attackEngine = new AttackEngine();
		attackEngine.attack( attacker, defender );

		assertEquals( 1, defender.getAmount() );
		assertEquals( 1, defender.getCurrentHp() );
	}
}
