/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.creatures.moving.MovementType;

import java.util.ArrayList;
import java.util.List;

class GameEngineTestUtil
{
	private List<Creature> generatedCreatures;

	boolean compare( List<Point> aResult, List<Point> aDestinations )
	{
		boolean result = true;
		for ( Point p : aResult )
		{
			if ( !aDestinations.contains( p ) )
			{
				result = false;
				break;
			}
		}
		return result;
	}

	List<Point> generateAttackDest( BoardManager aBoardManager )
	{
		Point point1 = aBoardManager.getPoint( 0, 1 );
		Point point2 = aBoardManager.getPoint( 0, 3 );

		return List.of(point1, point2);
	}

	List<Creature> generateCreatures()
	{
		Creature mover = new Creature.Builder().moveRange( 9 )
		                                       .moveStrategy( MovementType.FLYING )
		                                       .build();
		Creature creature1 = new Creature.Builder().moveRange( 8 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		Creature creature2 = new Creature.Builder().moveRange( 7 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		Creature creature3 = new Creature.Builder().moveRange( 6 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		Creature creature4 = new Creature.Builder().moveRange( 5 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		Creature creature5 = new Creature.Builder().moveRange( 4 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		Creature creature6 = new Creature.Builder().moveRange( 3 )
		                                           .moveStrategy( MovementType.FLYING )
		                                           .build();
		generatedCreatures =  List.of( mover, creature1, creature2, creature3, creature4, creature5, creature6 );

		return generatedCreatures;
	}

	List<Point> generateMoveDest( BoardManager aBoardManager )
	{
		Point point1 = aBoardManager.getPoint( 0, 0 );
		Point point2 = aBoardManager.getPoint( 1, 0 );
		Point point3 = aBoardManager.getPoint( 2, 0 );
		Point point4 = aBoardManager.getPoint( 3, 0 );
		Point point5 = aBoardManager.getPoint( 4, 0 );
		Point point6 = aBoardManager.getPoint( 5, 0 );
		Point point7 = aBoardManager.getPoint( 6, 0 );
		Point point8 = aBoardManager.getPoint( 7, 0 );
		Point point9 = aBoardManager.getPoint( 8, 0 );
		Point point10 = aBoardManager.getPoint( 1, 1 );
		Point point11 = aBoardManager.getPoint( 2, 1 );
		Point point12 = aBoardManager.getPoint( 3, 1 );
		Point point13 = aBoardManager.getPoint( 4, 1 );
		Point point14 = aBoardManager.getPoint( 5, 1 );
		Point point15 = aBoardManager.getPoint( 6, 1 );
		Point point16 = aBoardManager.getPoint( 7, 1 );
		Point point17 = aBoardManager.getPoint( 8, 1 );
		Point point18 = aBoardManager.getPoint( 9, 1 );
		Point point19 = aBoardManager.getPoint( 0, 2 );
		Point point20 = aBoardManager.getPoint( 1, 2 );
		Point point21 = aBoardManager.getPoint( 2, 2 );
		Point point22 = aBoardManager.getPoint( 3, 2 );
		Point point23 = aBoardManager.getPoint( 4, 2 );
		Point point24 = aBoardManager.getPoint( 5, 2 );
		Point point25 = aBoardManager.getPoint( 6, 2 );
		Point point26 = aBoardManager.getPoint( 7, 2 );
		Point point27 = aBoardManager.getPoint( 8, 2 );
		Point point28 = aBoardManager.getPoint( 1, 3 );
		Point point29 = aBoardManager.getPoint( 2, 3 );
		Point point30 = aBoardManager.getPoint( 3, 3 );
		Point point31 = aBoardManager.getPoint( 4, 3 );
		Point point32 = aBoardManager.getPoint( 5, 3 );
		Point point33 = aBoardManager.getPoint( 6, 3 );
		Point point34 = aBoardManager.getPoint( 7, 3 );
		Point point35 = aBoardManager.getPoint( 0, 4 );
		Point point36 = aBoardManager.getPoint( 1, 4 );
		Point point37 = aBoardManager.getPoint( 2, 4 );
		Point point38 = aBoardManager.getPoint( 3, 4 );
		Point point39 = aBoardManager.getPoint( 4, 4 );
		Point point40 = aBoardManager.getPoint( 5, 4 );
		Point point41 = aBoardManager.getPoint( 6, 4 );
		Point point42 = aBoardManager.getPoint( 1, 5 );
		Point point43 = aBoardManager.getPoint( 2, 5 );
		Point point44 = aBoardManager.getPoint( 3, 5 );
		Point point45 = aBoardManager.getPoint( 4, 5 );
		Point point46 = aBoardManager.getPoint( 5, 5 );
		Point point48 = aBoardManager.getPoint( 0, 6 );
		Point point49 = aBoardManager.getPoint( 1, 6 );
		Point point50 = aBoardManager.getPoint( 2, 6 );
		Point point51 = aBoardManager.getPoint( 3, 6 );
		Point point52 = aBoardManager.getPoint( 4, 6 );
		Point point53 = aBoardManager.getPoint( 1, 7 );
		Point point54 = aBoardManager.getPoint( 2, 7 );
		Point point55 = aBoardManager.getPoint( 3, 7 );
		Point point56 = aBoardManager.getPoint( 0, 8 );
		Point point57 = aBoardManager.getPoint( 1, 8 );
		Point point58 = aBoardManager.getPoint( 2, 8 );
		Point point59 = aBoardManager.getPoint( 1, 9 );
		Point point60 = aBoardManager.getPoint( 0, 10 );

		return List.of( point1, point2, point3, point4, point5, point6, point7, point8, point9, point10, point11, point12, point13, point14, point15, point16, point17, point18, point19, point20, point21, point22, point23, point24, point25, point26, point27, point28, point29, point30, point31, point32, point33, point34, point35, point36, point37, point38, point39, point40, point41, point42, point43, point44, point45, point46, point48, point49, point50, point51, point52, point53, point54, point55, point56, point57, point58, point59, point60 );
	}

	List<Point> generateCastDest( BoardManager aBoardManager )
	{
		List<Point> castDestinations = new ArrayList<>();

		generatedCreatures.forEach( c -> castDestinations.add( aBoardManager.getPointByTile( c ) ) );
		return castDestinations;
	}
}
