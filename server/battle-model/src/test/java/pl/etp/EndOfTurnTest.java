/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.exceptions.SpringServiceNotInitializedException;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EndOfTurnTest
{
	@Test
	void shouldCallPropertyChangeAfterEndOfTurn()
	{
		Creature attacker = spy( new Creature.Builder().moveRange( 4 )
		                                               .build() );
		Creature defender = new Creature.Builder().build();
		GameEngine engine = new GameEngine( new Hero( List.of( attacker ) ), new Hero( List.of( defender ) ) );

		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		verify( attacker, atLeastOnce() ).propertyChange( any( PropertyChangeEvent.class ) );
	}

	@Test
	void shouldResetMoveCounterAfterEndOfTurn()
	{
		//given
		Creature mover = new Creature.Builder().build();
		Creature notMover = new Creature.Builder().build();
		GameEngine engine = new GameEngine( new Hero( List.of( mover ) ), new Hero( List.of( notMover ) ) );
		Point startPosition = engine.getBoardManager()
		                            .getPoint( 0, 0 );
		Point expectedPositionAfter1stMove = engine.getBoardManager()
		                                           .getPoint( 0, 1 );
		Point expectedPositionAfter2ndMove = engine.getBoardManager()
		                                           .getPoint( 0, 2 );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 0, 1 ) ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 0, 2 ) ) );

		//then
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( startPosition ) );
		assertNull( engine.getBoardManager()
		                  .getCreatureByPoint( expectedPositionAfter1stMove ) );
		assertEquals( mover, engine.getBoardManager()
		                           .getCreatureByPoint( expectedPositionAfter2ndMove ) );
	}

	@Test
	void shouldCanAttackOnlyOncePerTurn()
	{
		//given
		Creature attacker = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .moveRange( 100 )
		                                          .build();
		Creature defender = new Creature.Builder().armor( 10 )
		                                          .maxHp( 100 )
		                                          .damage( Range.closed( 10, 10 ) )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( attacker ) ) ), new Hero( new ArrayList<>( List.of( defender ) ) ) );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 18, 1 ) ) );
		engine.attack( engine.getBoardManager().getPoint( 19, 1 ) );

		//then
		assertFalse( engine.canAttack( engine.getBoardManager()
		                                     .getPoint( 19, 1 ) ) );
	}

	@Test
	void shouldResetAttackCounterAfterEndOfTurn()
	{
		//given
		Creature attacker = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .moveRange( 100 )
		                                          .build();
		Creature defender = new Creature.Builder().armor( 10 )
		                                          .maxHp( 100 )
		                                          .damage( Range.closed( 10, 10 ) )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( attacker ) ) ), new Hero( new ArrayList<>( List.of( defender ) ) ) );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 18, 1 ) ) );
		engine.attack( engine.getBoardManager().getPoint( 19, 1 ) );

		//then
		assertFalse( engine.canAttack( engine.getBoardManager()
		                                     .getPoint( 19, 1 ) ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertTrue( engine.canAttack( engine.getBoardManager()
		                                    .getPoint( 19, 1 ) ) );
	}

	@Test
	void shouldCanNotMoveAfterAttack()
	{
		//given
		Creature attacker = new Creature.Builder().armor( 5 )
		                                          .maxHp( 100 )
		                                          .moveRange( 100 )
		                                          .build();
		Creature defender = new Creature.Builder().armor( 10 )
		                                          .maxHp( 100 )
		                                          .damage( Range.closed( 10, 10 ) )
		                                          .build();
		GameEngine engine = new GameEngine( new Hero( new ArrayList<>( List.of( attacker ) ) ), new Hero( new ArrayList<>( List.of( defender ) ) ) );

		//when
		assertThrows( SpringServiceNotInitializedException.class, () -> engine.move( engine.getBoardManager()
		                                                                                   .getPoint( 18, 1 ) ) );
		engine.attack( engine.getBoardManager().getPoint( 19, 1 ) );

		//then
		assertFalse( engine.canAttack( engine.getBoardManager()
		                                     .getPoint( 19, 1 ) ) );
		assertFalse( engine.canMove( engine.getBoardManager()
		                                   .getPoint( 17, 1 ) ) );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertThrows( SpringServiceNotInitializedException.class, engine::pass );
		assertTrue( engine.canMove( engine.getBoardManager()
		                                  .getPoint( 17, 1 ) ) );
	}
}
