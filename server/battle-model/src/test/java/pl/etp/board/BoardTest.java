/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board;

import fields.FieldType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.board.fields.FieldFactory;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;

import java.util.AbstractMap;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BoardTest
{
	private Board board;

	@BeforeEach
	void prepareBoard()
	{
		board = new Board();
	}

	@Test
	void shouldThrowExceptionIfTileIsTaken()
	{
		// given
		Creature creature1 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );
		Creature creature2 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );

		// when
		board.add( new Point( 1, 1 ), creature1 );

		// then
		assertThrows( IllegalArgumentException.class, () -> board.add( new Point( 1, 1 ), creature2 ) );
	}

	@Test
	void shouldThrowExceptionWhenIsOutsideMap()
	{
		Creature creature1 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );

		assertThrows( IllegalArgumentException.class, () -> board.add( new Point( 420, 69 ), creature1 ) );
	}

	@Test
	void shoulrReturnBoardPointsWithTiles()
	{
		// given
		Creature creature1 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );
		Creature creature2 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 2 );
		TileIf lava = FieldFactory.create( FieldType.LAVA );
		TileIf swamp = FieldFactory.create( FieldType.SWAMP );

		// when
		board.add( new Point( 1, 1 ), creature1 );
		board.add( new Point( 1, 2 ), creature2 );
		board.add( new Point( 1, 3 ), lava );
		board.add( new Point( 1, 4 ), swamp );

		// then
		assertEquals( Set.of( new Point( 1, 1 ), new Point( 1, 2 ), new Point( 1, 3 ), new Point( 1, 4 ) ), board.getBoardPointsWithTiles() );
	}

	@Test
	void shouldReturnBoardPointsWithCreatures()
	{
		// given
		Creature creature1 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );
		Creature creature2 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 2 );
		TileIf lava = FieldFactory.create( FieldType.LAVA );
		TileIf swamp = FieldFactory.create( FieldType.SWAMP );

		// when
		board.add( new Point( 1, 1 ), creature1 );
		board.add( new Point( 1, 2 ), creature2 );
		board.add( new Point( 1, 3 ), lava );
		board.add( new Point( 1, 4 ), swamp );

		// then
		assertEquals( Set.of( new AbstractMap.SimpleEntry<Point, TileIf>( new Point( 1, 1 ), creature1 ), new AbstractMap.SimpleEntry<Point, TileIf>( new Point( 1, 2 ), creature2 ) ), board.getBoardPointsWithCreatures() );
	}

	@Test
	void shouldReturnBoardPointsWithSpecialTiles()
	{
		// given
		Creature creature1 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 1 );
		Creature creature2 = CreatureFactory.create( CreatureStatistic.TEST_1_HP_CREATURE, 2 );
		TileIf lava = FieldFactory.create( FieldType.LAVA );
		TileIf swamp = FieldFactory.create( FieldType.SWAMP );

		// when
		board.add( new Point( 1, 1 ), creature1 );
		board.add( new Point( 1, 2 ), creature2 );
		board.add( new Point( 1, 3 ), lava );
		board.add( new Point( 1, 4 ), swamp );

		// then
		assertEquals( Set.of( new AbstractMap.SimpleEntry<Point, String>( new Point( 1, 3 ), lava.getClass()
		                                                                                         .getSimpleName() ), new AbstractMap.SimpleEntry<Point, String>( new Point( 1, 4 ), swamp.getClass()
		                                                                                                                                                                                 .getSimpleName() ) ), board.getBoardPointsWithSpecialTiles() );
	}
}