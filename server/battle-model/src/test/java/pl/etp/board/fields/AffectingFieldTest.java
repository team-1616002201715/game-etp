/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board.fields;

import fields.FieldType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.board.TileIf;
import pl.etp.creatures.Creature;
import pl.etp.creatures.moving.MoveEngine;
import pl.etp.creatures.moving.MovementType;

import static org.junit.jupiter.api.Assertions.*;

class AffectingFieldTest
{
	private MoveEngine moveEngine;
	private BoardManager boardManager;
	private Creature groundCreature;
	Creature flyingCreature;
	Creature teleportCreature;

	@BeforeEach
	void init()
	{
		boardManager = new BoardManager();
		moveEngine = new MoveEngine( boardManager );
		groundCreature = new Creature.Builder().moveRange( 4 )
		                                       .maxHp( 100 )
		                                       .moveStrategy( MovementType.GROUND )
		                                       .build();
		flyingCreature = new Creature.Builder().moveRange( 9 )
		                                       .maxHp( 100 )
		                                       .armor( 10 )
		                                       .moveStrategy( MovementType.FLYING )
		                                       .build();
		teleportCreature = new Creature.Builder().moveRange( 4 )
		                                         .maxHp( 100 )
		                                         .moveStrategy( MovementType.TELEPORT )
		                                         .build();
	}

	@Test
	void swampShouldDecreaseMoveRangeOfGroundCreature()
	{
		// M - mover, MoveType.GROUND, S - Swamp, T - Target point
		//    0 1 2 3 4
		//  0 M S T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), groundCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.SWAMP ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( groundCreature, targetPoint );

		//then
		assertEquals( 2, groundCreature.getMoveRange() );
	}

	@Test
	void swampShouldNotAffectToFlyingCreature()
	{
		// M - mover, MoveType.FLYING, S - Swamp, T - Target point
		//    0 1 2 3 4
		//  0 M S T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), flyingCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.SWAMP ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( flyingCreature, targetPoint );

		//then
		assertEquals( 9, flyingCreature.getMoveRange() );
	}

	@Test
	void lavaShouldDealDamageToGroundCreature()
	{
		// M - mover, MoveType.GROUND, L - Lava, T - Target point
		//    0 1 2 3 4
		//  0 M L T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), groundCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.LAVA ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( groundCreature, targetPoint );

		//then
		assertEquals( 1, groundCreature.getAmount() );
		assertEquals( 75, groundCreature.getCurrentHp() );

	}

	@Test
	void lavaShouldNotAffectOnFlyingCreature()
	{
		// M - mover, MoveType.GROUND, L - Lava, T - Target point
		//    0 1 2 3 4
		//  0 M L T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), flyingCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.LAVA ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( flyingCreature, targetPoint );

		//then
		assertEquals( 1, flyingCreature.getAmount() );
		assertEquals( 100, flyingCreature.getCurrentHp() );
	}

	@Test
	void lavaShouldNotAffectOnTeleportCreature()
	{
		// M - mover, MoveType.GROUND, L - Lava, T - Target point
		//    0 1 2 3 4
		//  0 M L T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), teleportCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.LAVA ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( teleportCreature, targetPoint );

		//then
		assertEquals( 1, teleportCreature.getAmount() );
		assertEquals( 100, teleportCreature.getCurrentHp() );
	}

	@Test
	void gasGeyserShouldDealDamageToFlyingCreatureAndDecreaseArmor()
	{
		// M - mover, MoveType.GROUND, G - Gas Geyser, T - Target point
		//    0 1 2 3 4
		//  0 M G T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), flyingCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.GAS_GEYSER ) );
		Point targetPoint = boardManager.getPoint( 2, 0 );

		//when
		moveEngine.move( flyingCreature, targetPoint );

		//then
		assertEquals( 1, flyingCreature.getAmount() );
		assertEquals( 85, flyingCreature.getCurrentHp() );
		assertEquals( 8, flyingCreature.getArmor() );
	}

	@Test
	void groundCreatureShouldBeAbleToMoveToTileWithMagicBalloon()
	{
		// M - mover, MoveType.GROUND, S - Balloon, T - Target point
		//    0 1 2 3 4
		//  0 M S T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), groundCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.MAGIC_BALLOON ) );
		Point targetPointGround = boardManager.getPoint( 1, 0 );

		//when
		boolean resultForGround = moveEngine.canMove( groundCreature, boardManager.getPoint( targetPointGround.getX(), targetPointGround.getY() ) );

		//then
		assertTrue( resultForGround );
	}

	@Test
	void flyingCreatureShouldNotBeAbleToMoveToTileWithMagicBalloon()
	{
		// M - mover, MoveType.FLYING, S - Balloon, T - Target point
		//    0 1 2 3 4
		//  0 M S T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 2, 0 ), flyingCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.MAGIC_BALLOON ) );
		Point targetPointFlying = boardManager.getPoint( 1, 0 );

		//when
		boolean resultForFlying = moveEngine.canMove( flyingCreature, boardManager.getPoint( targetPointFlying.getX(), targetPointFlying.getY() ) );

		//then
		assertFalse( resultForFlying );
	}

	@Test
	void groundCreatureShouldBeAbleToStandOnTileWithMagicBalloon()
	{
		// M - mover, MoveType.GROUND, S - Swamp, T - Target point
		//    0 1 2 3 4
		//  0 M S T x x

		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), groundCreature );
		boardManager.putOnBoard( boardManager.getPoint( 1, 0 ), FieldFactory.create( FieldType.MAGIC_BALLOON ) );
		Point targetPoint = boardManager.getPoint( 1, 0 );

		//when
		moveEngine.move( groundCreature, targetPoint );

		//then
		TileIf creatureFromBoard = boardManager.getTileByPoint( targetPoint );
		assertEquals( groundCreature, creatureFromBoard );
	}
}