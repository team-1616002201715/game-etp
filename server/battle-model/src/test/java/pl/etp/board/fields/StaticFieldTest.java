/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board.fields;

import com.google.common.collect.Range;
import fields.FieldType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.board.TileIf;
import pl.etp.creatures.Creature;
import pl.etp.creatures.attacking.AttackEngine;
import pl.etp.exceptions.SpringServiceNotInitializedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.GameEngine.BOARD_HEIGHT;

class StaticFieldTest
{
	public static final int NOT_IMPORTANT = 5;
	Creature attacker;

	@BeforeEach
	void init()
	{
		attacker = new Creature.Builder().name( "Attacker" )
		                                 .armor( NOT_IMPORTANT )
		                                 .maxHp( 100 )
		                                 .moveRange( NOT_IMPORTANT )
		                                 .damage( Range.closed( 30, 30 ) )
		                                 .build();
	}

	@Test
	void creatureCanAttackStaticField()
	{
		BoardManager boardManager = new BoardManager();
		AttackEngine attackEngine = new AttackEngine();
		TileIf wallField = FieldFactory.create( FieldType.WALL );
		boardManager.putOnBoard( boardManager.getPoint( 0, 2 ), wallField );

		attackEngine.attack( attacker, wallField );
		assertEquals( 10, wallField.getDefenceContext()
		                           .getDefenceStatistic()
		                           .getCurrentHp() );
	}

	@Test
	void onlyAfterDestroyOneOfWallGroundCreatureCanCrossOverWall()
	{
		//given
		GameEngine gameEngine = new GameEngine( new Hero( new ArrayList<>( List.of( attacker ) ) ), new Hero( List.of() ) );
		Point targetPoint = gameEngine.getBoardManager()
		                              .getPoint( 2, 1 );
		Point wallToDestroy = gameEngine.getBoardManager()
		                                .getPoint( 1, 1 );

		for ( int i = 0; i < BOARD_HEIGHT; i++ )
		{
			gameEngine.getBoardManager()
			          .putOnBoard( gameEngine.getBoardManager()
			                                 .getPoint( 1, i ), FieldFactory.create( FieldType.WALL ) );
		}

		//first attack - wall still exists
		assertFalse( gameEngine.canMove( targetPoint ) );
		TileIf wallField = gameEngine.getBoardManager()
		                             .getTileByPoint( wallToDestroy );

		gameEngine.attack( wallToDestroy );
		assertEquals( 10, wallField.getDefenceContext()
		                           .getDefenceStatistic()
		                           .getCurrentHp() );
		assertFalse( gameEngine.canMove( targetPoint ) );

		//second attack - wall disappeared
		assertThrows( SpringServiceNotInitializedException.class, gameEngine::pass );
		gameEngine.attack( wallToDestroy );
		assertThrows( SpringServiceNotInitializedException.class, gameEngine::pass );
		assertTrue( gameEngine.canMove( targetPoint ) );
	}
}