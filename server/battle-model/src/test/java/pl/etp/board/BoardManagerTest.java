/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.Creature;

import static org.junit.jupiter.api.Assertions.*;

class BoardManagerTest
{
	private BoardManager boardManager;
	private Creature creature;

	@BeforeEach
	void init()
	{
		boardManager = new BoardManager();
		creature = new Creature.Builder().build();
	}

	@Test
	void shouldAddCreature()
	{
		//when
		boardManager.putOnBoard( new Point( 0, 0 ), creature );
		Creature creatureFromBoard = boardManager.getCreatureByPoint( boardManager.getPoint( 0, 0 ) );

		//then
		assertEquals( creature, creatureFromBoard );
	}

	@Test
	void shouldReturnNullWhenFiledIsEmpty()
	{
		//when
		Creature creatureFromBoard = boardManager.getCreatureByPoint( boardManager.getPoint( 0, 0 ) );

		//then
		assertNull( creatureFromBoard );
	}

	@Test
	void shouldThrowIllegalArgumentExceptionWenYouTryAddCreatureToNotEmptyField()
	{
		//given
		boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), creature );
		Creature creature2 = new Creature.Builder().build();

		//when
		assertThrows( IllegalArgumentException.class, () -> boardManager.putOnBoard( boardManager.getPoint( 0, 0 ), creature2 ) );

		//then
		Creature creatureFromBoard = boardManager.getCreatureByPoint( boardManager.getPoint( 0, 0 ) );
		assertEquals( creature, creatureFromBoard );
	}

	@Test
	void shouldReturnCorrectLocationForByCreature()
	{
		//given
		boardManager.putOnBoard( boardManager.getPoint( 5, 5 ), creature );

		//when
		Point result = boardManager.getPointByTile( creature );

		//then
		assertEquals( boardManager.getPoint( 5, 5 ), result );
	}

	@Test
	void shouldReturnFalseWhenTryToStandOnCreature()
	{
		//given
		Point targetPoint = boardManager.getPoint( 5, 5 );
		boardManager.putOnBoard( targetPoint, creature );

		//when
		boolean result = boardManager.canStand( targetPoint );

		//then
		assertFalse( result );
	}

	@Test
	void shouldThrowIllegalArgumentExceptionWenYouTryAddCreatureOutsideTheMap()
	{
		//given
		boardManager.putOnBoard( new Point( 0, 0 ), creature );

		//when
		assertThrows( IllegalArgumentException.class, () -> boardManager.putOnBoard( new Point( 0, 25 ), creature ) );

		//then
		Creature creatureFromBoard = ( Creature ) boardManager.getCreatureByPoint( new Point( 0, 0 ) );
		assertEquals( creature, creatureFromBoard );
	}
}