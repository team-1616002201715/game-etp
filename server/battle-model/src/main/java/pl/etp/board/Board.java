/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board;

import pl.etp.board.fields.AbstractAffectingField;
import pl.etp.board.fields.AbstractStaticField;
import pl.etp.creatures.Creature;

import java.util.*;
import java.util.stream.Collectors;

import static pl.etp.GameEngine.BOARD_HEIGHT;
import static pl.etp.GameEngine.BOARD_WIDTH;

class Board
{
	private final Map<Point, TileIf> map;
	private final String TILE_TAKEN = "Tile isn't empty";
	private final String OUTSIDE_MAP = "You are trying to works outside the map";
	;

	Board()
	{
		map = new HashMap<>();
	}

	TileIf get( Point aPoint )
	{
		return map.get( aPoint );
	}

	Point get( TileIf aTileIf )
	{
		return map.keySet()
		          .stream()
		          .filter( p -> map.get( p )
		                           .equals( aTileIf ) )
		          .findAny()
		          .orElse( null );
	}

	void add( Point aPoint, TileIf aTile )
	{
		throwExceptionIfTileIsTaken( aPoint );
		throwExceptionWhenIsOutsideMap( aPoint );
		map.put( aPoint, aTile );
	}

	void remove( Point aSourcePoint )
	{
		map.remove( aSourcePoint );
	}

	void throwExceptionIfTileIsTaken( Point aPoint )
	{
		if ( isTileTaken( aPoint ) )
		{
			throw new IllegalArgumentException( TILE_TAKEN );
		}
	}

	private boolean isTileTaken( Point aPoint )
	{
		if ( map.get( aPoint ) != null )
		{
			return map.get( aPoint )
			          .getClass()
			          .equals( Creature.class );
		}
		return false;
	}

	void throwExceptionWhenIsOutsideMap( Point aPoint )
	{
		if ( aPoint.getX() < 0 || aPoint.getX() >= BOARD_WIDTH || aPoint.getY() < 0 || aPoint.getY() >= BOARD_HEIGHT )
		{
			throw new IllegalArgumentException( OUTSIDE_MAP );
		}
	}

	Set<Point> getBoardPointsWithTiles()
	{
		return map.keySet();
	}

	Set<Map.Entry<Point, TileIf>> getBoardPointsWithCreatures()
	{
		return ( map.entrySet()
		            .stream()
		            .filter( tile -> tile.getValue()
		                                 .getClass()
		                                 .equals( Creature.class ) )
		            .collect( Collectors.toSet() ) );
	}

	Set<Map.Entry<Point, String>> getBoardPointsWithSpecialTiles()
	{
		return ( map.entrySet()
		            .stream()
		            .filter( tile -> AbstractAffectingField.class.isAssignableFrom( tile.getValue()
		                                                                                .getClass() ) | AbstractStaticField.class.isAssignableFrom( tile.getValue()
		                                                                                                                                                .getClass() ) )
		            .map( entry -> new AbstractMap.SimpleEntry<Point, String>( entry.getKey(), entry.getValue()
		                                                                                            .getClass()
		                                                                                            .getSimpleName() ) ) ).collect( Collectors.toSet() );
	}

	List<Point> getAllBoardPoints()
	{
		List<Point> allPoints = new ArrayList<>();
		for ( int x = 0; x < BOARD_WIDTH; x++ )
		{
			for ( int y = 0; y < BOARD_HEIGHT; y++ )
			{
				allPoints.add( new Point( x, y ) );
			}
		}

		return allPoints;
	}

	public Map<Point, TileIf> getCurrentMap()
	{
		return new HashMap<>( map );
	}
}
