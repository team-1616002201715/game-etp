/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board;

import fields.FieldType;
import pl.etp.board.fields.AbstractAffectingField;
import pl.etp.board.fields.FieldFactory;
import pl.etp.creatures.Creature;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static pl.etp.GameEngine.BOARD_WIDTH;

public class BoardManager
{
	Board board = new Board();

	public void putCreaturesOnBoard( List<Creature> aHeroOneCreatures, List<Creature> aHeroTwoCreatures )
	{
		putCreaturesFromOneSideToBoard( aHeroOneCreatures, 0 );
		putCreaturesFromOneSideToBoard( aHeroTwoCreatures, BOARD_WIDTH - 1 );
	}

	private void putCreaturesFromOneSideToBoard( List<Creature> aCreatures, int aBoardWidth )
	{
		for ( int i = 0; i < aCreatures.size(); i++ )
		{
			putOnBoard( new Point( aBoardWidth, i * 2 + 1 ), aCreatures.get( i ) );
		}
	}

	public void checkPreconditions( Point aTargetPoint )
	{
		board.throwExceptionIfTileIsTaken( aTargetPoint );
		board.throwExceptionWhenIsOutsideMap( aTargetPoint );
	}

	public void putOnBoard( Point aPoint, TileIf aCreature )
	{
		checkPreconditions( aPoint );
		board.add( aPoint, aCreature );
	}

	public void removeFromBoard( Point aSourcePoint )
	{
		board.remove( aSourcePoint );
	}

	public Point getPoint( int aX, int aY )
	{
		return new Point( aX, aY );
	}

	public boolean canStand( Point aTargetPoint )
	{
		if ( getCreatureByPoint( aTargetPoint ) != null )
		{
			return getCreatureByPoint( aTargetPoint ).isStandable();
		}

		return true;
	}

	public Point getPointByTile( TileIf aTileIf )
	{
		return board.get( aTileIf );
	}

	public TileIf getTileByPoint( Point aP )
	{
		return board.get( aP );
	}

	public Creature getCreatureByPoint( Point aPoint )
	{
		if ( board.get( aPoint ) instanceof Creature )
		{
			return ( Creature ) board.get( aPoint );

		}
		else
		{
			return null;
		}

	}

	public List<Point> getBoardPointsWithTiles()
	{
		return new ArrayList<>( board.getBoardPointsWithTiles() );
	}

	public List<Point> getAllBoardPoints()
	{
		return new ArrayList<>( board.getAllBoardPoints() );
	}

	public Set<Map.Entry<Point, TileIf>> getCurrentBoard()
	{
		return board.getCurrentMap()
		            .entrySet();
	}

	public Set<Map.Entry<Point, TileIf>> getBoardPointsWithCreatures()
	{
		return board.getBoardPointsWithCreatures();
	}

	public Set<Map.Entry<Point, String>> getBoardPointsWithSpecialTiles()
	{
		return board.getBoardPointsWithSpecialTiles();
	}

	public List<AbstractAffectingField> getAffectingFields( List<Point> aPointsList )
	{
		ArrayList<AbstractAffectingField> affectingFields = new ArrayList<>();

		aPointsList.stream()
		           .map( p -> board.get( p ) )
		           .forEach( tile ->
		           {
			           if ( tile instanceof AbstractAffectingField )
			           {
				           affectingFields.add( ( AbstractAffectingField ) tile );
			           }
		           } );

		return affectingFields;
	}

	public void createSample()
	{
		TileIf wallField1 = FieldFactory.create( FieldType.WALL );
		TileIf wallField2 = FieldFactory.create( FieldType.WALL );
		TileIf wallField3 = FieldFactory.create( FieldType.WALL );
		TileIf wallField4 = FieldFactory.create( FieldType.WALL );
		TileIf wallField5 = FieldFactory.create( FieldType.WALL );
		putOnBoard( getPoint( 3, 5 ), wallField1 );
		putOnBoard( getPoint( 6, 10 ), wallField2 );
		putOnBoard( getPoint( 8, 13 ), wallField3 );
		putOnBoard( getPoint( 10, 11 ), wallField4 );
		putOnBoard( getPoint( 13, 3 ), wallField5 );
	}
}
