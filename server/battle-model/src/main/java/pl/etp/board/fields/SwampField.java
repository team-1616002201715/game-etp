/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board.fields;

import pl.etp.creatures.Creature;
import pl.etp.creatures.attacking.AttackContextIf;
import pl.etp.creatures.defending.DefenceContextIf;
import pl.etp.creatures.moving.MovementType;
import pl.etp.creatures.retaliating.RetaliationContextIf;
import pl.etp.creatures.spells.SpellStatistic;
import pl.etp.spells.EffectStats;
import pl.etp.spells.StatusEffectSpell;

class SwampField extends AbstractAffectingField
{
	@Override
	public void apply( Creature aCreature )
	{
		if ( aCreature.getMoveContext()
		              .getMoveStrategy()
		              .getMovementType()
		              .equals( MovementType.GROUND ) )
		{
			EffectStats stats = EffectStats.builder()
			                               .moveRangePercentage( -0.5 )
			                               .build();
			StatusEffectSpell swampDebuff = new StatusEffectSpell( 3, stats, 0, SpellStatistic.TargetType.ENEMY, SpellStatistic.SpellElement.EARTH, "SWAMP" );

			swampDebuff.cast( aCreature );
		}
	}

	@Override
	public DefenceContextIf getDefenceContext()
	{
		return null;
	}

	@Override
	public RetaliationContextIf getRetaliationContext()
	{
		return null;
	}

	@Override
	public AttackContextIf getAttackContext()
	{
		return null;
	}
}
