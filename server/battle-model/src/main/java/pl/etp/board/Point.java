/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.board;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(description = "Coordinates object")
public class Point
{
	@ApiModelProperty ( notes = "X coordinate" )
	private final int x;
	@ApiModelProperty ( notes = "Y coordinate" )
	private final int y;

	Point( int aX, int aY )
	{
		x = aX;
		y = aY;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	@Override
	public boolean equals( Object aO )
	{
		if ( this == aO )
		{
			return true;
		}
		if ( aO == null || getClass() != aO.getClass() )
		{
			return false;
		}
		Point point = ( Point ) aO;
		return x == point.x && y == point.y;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash( x, y );
	}

	@Override
	public String toString()
	{
		return "Point{" + "x=" + x + ", y=" + y + '}';
	}

	public double distance( Point aPoint )
	{
		return Math.sqrt( ( aPoint.y - y ) * ( aPoint.y - y ) + ( aPoint.x - x ) * ( aPoint.x - x ) );
	}
}
