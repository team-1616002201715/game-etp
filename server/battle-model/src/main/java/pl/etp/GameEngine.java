/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.board.TileIf;
import pl.etp.creatures.Creature;
import pl.etp.creatures.attacking.AttackEngine;
import pl.etp.creatures.moving.MoveEngine;
import pl.etp.exceptions.SpringServiceNotInitializedException;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.ManaPool;
import pl.etp.spells.SpellCastingRulesManager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class GameEngine implements PropertyChangeListener
{
	public final static int BOARD_WIDTH = 20;
	public final static int BOARD_HEIGHT = 15;

	public static final String CREATURE_KILLED = "CREATURE_KILLED";
	public static final String MANA_CHANGED = "MANA CHANGED";
	public static final String END_OF_TURN = "END_OF_TURN";
	public final String gameId;
	private final BoardManager boardManager;
	private final MoveEngine moveEngine;
	private final AttackEngine attackEngine;
	private final TurnQueue queue;
	private final List<Creature> heroOneCreature;
	private final List<Creature> heroTwoCreature;

	public enum ACTION
	{
		SPELL, ATTACK, PASS, MOVE
	}

	public GameEngine( Hero aHeroOne, Hero aHeroTwo )
	{
		this( aHeroOne, aHeroTwo, new BoardManager() );
	}

	public GameEngine( Hero aHeroOne, Hero aHeroTwo, String aGameId )
	{
		this( aHeroOne, aHeroTwo, new BoardManager(), aGameId );
	}


	public GameEngine( Hero aHeroOne, Hero aHeroTwo, BoardManager aBoard )
	{
		this( aHeroOne, aHeroTwo, aBoard, UUID.randomUUID()
		                                      .toString() );
	}

	public GameEngine( Hero aHeroOne, Hero aHeroTwo, BoardManager aBoard, String aGameId )
	{
		gameId = aGameId;
		boardManager = aBoard;
		attackEngine = new AttackEngine();
		moveEngine = new MoveEngine( boardManager );
		heroOneCreature = aHeroOne.getCreatures();
		heroTwoCreature = aHeroTwo.getCreatures();
		boardManager.putCreaturesOnBoard( heroOneCreature, heroTwoCreature );
		queue = new TurnQueue( aHeroOne, aHeroTwo );
		aHeroOne.getSpellBook()
		        .getManaPool()
		        .addObserver( MANA_CHANGED, this );
		aHeroTwo.getSpellBook()
		        .getManaPool()
		        .addObserver( MANA_CHANGED, this );
		queue.addObserver( CREATURE_KILLED, this );
	}

	public BoardManager getBoardManager()
	{
		return boardManager;
	}

	public void move( Point aTargetPoint )
	{
		List<Point> path = moveEngine.move( queue.getActiveCreature(), aTargetPoint );
		GameEventsService.broadcastPath( this, path );
		GameEventsService.broadcastLastAction( this, ACTION.MOVE );
		GameEventsService.broadcastCurrentBoard( this, boardManager );
		if ( getActiveCreature().getAttackContext()
		                        .canAttack() || getActiveCreature().getMoveContext()
		                                                           .canMove() )
		{
			provideInfoAboutActiveCreature( getActiveCreature() );
		}
	}

	public boolean canMove( Point aTargetPoint )
	{
		return moveEngine.canMove( getActiveCreature(), boardManager.getPoint( aTargetPoint.getX(), aTargetPoint.getY() ) );
	}

	public void pass()
	{
		queue.next();
		provideInfoAboutActiveCreature( getActiveCreature() );
		GameEventsService.broadcastCurrentBoard( this, boardManager );
		GameEventsService.broadcastLastAction( this, ACTION.PASS );
		GameEventsService.broadcastAvailableSpells( this, getCurrentlyAvailableSpellsForCasting() );
		GameEventsService.broadcastCurrentMana( this, this.getActiveHero()
		                                                  .getSpellBook()
		                                                  .getManaPool() );
	}

	private void provideInfoAboutActiveCreature( Creature activeCreature )
	{
		GameEventsService.broadcastActiveCreature( this, activeCreature );
		GameEventsService.broadcastAvailableTiles( this, getAvailableMoves(), getAvailableAttacks() );
	}


	public void castSpell( AbstractSpell aSpell, Point aPoint )
	{
		innerCastSpell( aSpell, aPoint );
		queue.getActiveHero()
		     .castSpell( aSpell );
		GameEventsService.broadcastCurrentBoard( this, boardManager );
		GameEventsService.broadcastLastAction( this, ACTION.SPELL );
	}

	private void innerCastSpell( AbstractSpell aSpell, Point aPoint )
	{
		SpellCastingRulesManager spellCastingRulesManager = new SpellCastingRulesManager();
		Set<Point> targetPoints = spellCastingRulesManager.calculateCastTargetPoints( aSpell, aPoint, boardManager, this::isEnemyCreature );
		targetPoints.forEach( p ->
		{
			Creature pointContent = boardManager.getCreatureByPoint( p );
			aSpell.cast( pointContent );
			deathCheck( pointContent );
		} );
	}

	/**
	 * Check if there is a need to disable the Spellbook button for the current player
	 */
	public boolean canCastSpellInThisTurn()
	{
		return queue.getActiveHero()
		            .canCastSpellInThisTurn();
	}

	public boolean canAttack( Point aTargetPoint )
	{
		return isEnemyCreature( aTargetPoint ) && isInAttackRange( aTargetPoint ) && getActiveCreature().getAttackContext()
		                                                                                                .canAttack();
	}

	private boolean isInAttackRange( Point aTargetPoint )
	{
		return boardManager.getPointByTile( getActiveCreature() )
		                   .distance( aTargetPoint ) <= getActiveCreature().getAttackContext()
		                                                                   .getAttackerStatistic()
		                                                                   .getAttackRange();
	}

	public boolean isAllyCreature( Point aP )
	{
		return boardManager.getCreatureByPoint( aP )
		                   .getClass()
		                   .equals( Creature.class ) && !isEnemyCreature( aP );
	}

	public boolean isEnemyCreature( Point aP )
	{
		return boardManager.getCreatureByPoint( aP ) != null && !queue.getActiveHero()
		                                                              .getCreatures()
		                                                              .contains( boardManager.getCreatureByPoint( aP ) );
	}

	public Creature getActiveCreature()
	{
		return queue.getActiveCreature();
	}

	public String getGameId()
	{
		return gameId;
	}

	public void attack( Point aTargetPoint )
	{
		Creature activeCreature = queue.getActiveCreature();
		boolean[][] splashRange = activeCreature.getAttackContext()
		                                        .getSplashRange()
		                                        .getSplashAreaOfEffect();

		for ( int x = 0; x < splashRange.length; x++ )
		{
			for ( int y = 0; y < splashRange.length; y++ )
			{
				if ( splashRange[x][y] )
				{
					int pointX = aTargetPoint.getX() + x - 1;
					int pointY = aTargetPoint.getY() + y - 1;

					TileIf defender = boardManager.getTileByPoint( boardManager.getPoint( pointX, pointY ) );
					if ( defender != null )
					{
						attackEngine.attack( activeCreature, defender );
						deathCheck( defender );
						try
						{
							GameEventsService.broadcastCurrentBoard( this, boardManager );
							GameEventsService.broadcastLastAction( this, ACTION.ATTACK );
						}
						catch ( SpringServiceNotInitializedException aException )
						{

						}
					}
				}
			}
		}
	}

	private void deathCheck( TileIf aDefender )
	{
		if ( aDefender.getDefenceContext()
		              .getDefenceStatistic()
		              .getCurrentHp() <= 0 )
		{
			removeFromGame( aDefender );
		}
	}

	public void castRange( AbstractSpell aSpell )
	{
		GameEventsService.broadcastCastRange( this, getAvailableCasts( aSpell ) );
	}

	private void removeFromGame( TileIf aTileIf )
	{
		Point tilePoint = boardManager.getPointByTile( aTileIf );

		if ( boardManager.getCreatureByPoint( tilePoint ) != null )
		{
			if ( heroOneCreature.contains( ( Creature ) aTileIf ) )
			{
				heroOneCreature.remove( aTileIf );
			}
			else
			{
				heroTwoCreature.remove( aTileIf );
			}

			queue.removeObserver( ( Creature ) aTileIf );
			queue.removeFromQueue( ( Creature ) aTileIf );
		}

		boardManager.removeFromBoard( tilePoint );
	}

	List<Point> getAvailableMoves()
	{
		List<Point> availableMoves = new ArrayList<>();
		boardManager.getAllBoardPoints()
		            .forEach( p ->
		            {
			            if ( canMove( p ) )
			            {
				            availableMoves.add( p );
			            }
		            } );
		return availableMoves;
	}

	List<Point> getAvailableAttacks()
	{
		List<Point> availableAttacks = new ArrayList<>();
		boardManager.getBoardPointsWithTiles()
		            .forEach( p ->
		            {
			            if ( canAttack( p ) )
			            {
				            availableAttacks.add( p );
			            }
		            } );
		return availableAttacks;
	}

	/**
	 * Should be use to highlight tiles on which spell can be casted
	 */
	List<Point> getAvailableCasts( AbstractSpell aSpell )
	{
		SpellCastingRulesManager calc = new SpellCastingRulesManager();
		List<Point> availableCasts = new ArrayList<>();

		boardManager.getBoardPointsWithTiles()
		            .forEach( p ->
		            {
			            if ( calc.canCast( aSpell, p, this ) )
			            {
				            availableCasts.add( p );
			            }
		            } );
		return availableCasts;
	}

	public List<AbstractSpell> getCurrentlyAvailableSpellsForCasting()
	{
		return queue.getActiveHero()
		            .getSpellBook()
		            .getCurrentlyAvailableSpells();
	}

	public AbstractSpell getSpellFromAvailableSpells( String aName )
	{
		return getCurrentlyAvailableSpellsForCasting().stream()
		                                              .filter( spell -> spell.getName()
		                                                                     .equals( aName ) )
		                                              .findFirst()
		                                              .orElse( null );
	}

	public void refreshClientInfo()
	{
		provideInfoAboutActiveCreature( getActiveCreature() );
		GameEventsService.broadcastCurrentMana( this, this.getActiveHero()
		                                                  .getSpellBook()
		                                                  .getManaPool() );
		GameEventsService.broadcastCurrentBoard( this, boardManager );
		GameEventsService.broadcastAvailableSpells( this, getCurrentlyAvailableSpellsForCasting() );
	}

	public Hero getActiveHero()
	{
		return queue.getActiveHero();
	}

	public boolean isHero1Turn()
	{
		return queue.isHero1Turn();
	}

	@Override
	public void propertyChange( PropertyChangeEvent evt )
	{
		try
		{
			if ( MANA_CHANGED.equals( evt.getPropertyName() ) )
			{
				GameEventsService.broadcastCurrentMana( this, ( ManaPool ) evt.getNewValue() );
				GameEventsService.broadcastAvailableSpells( this, getCurrentlyAvailableSpellsForCasting() );
			}
			if ( CREATURE_KILLED.equals( evt.getPropertyName() ) )
			{
				if ( heroOneCreature.isEmpty() )
				{
					GameEventsService.broadcastWinner( this, 2 );
				}
				else if ( heroTwoCreature.isEmpty() )
				{
					GameEventsService.broadcastWinner( this, 1 );
				}
			}
		}
		catch ( SpringServiceNotInitializedException aE )
		{

		}
	}
}