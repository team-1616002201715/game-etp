/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking.damageCalculator;

import pl.etp.creatures.attacking.AttackStatistic;

import java.util.Random;

public abstract class AbstractDamageCalculator
{

	protected final Random rand;

	AbstractDamageCalculator( Random aRand )
	{
		rand = aRand;
	}

	protected Random getRand()
	{
		return rand;
	}

	public int calculateDamage( AttackStatistic aAttackerStats, int aDefenderArmor, int aDefenderAmount )
	{
		int randomDamageValue = calculateRandomDamageValue( aAttackerStats );
		int wholeStackDamageToDeal = calculateWholeStackDamageToDeal( randomDamageValue, aAttackerStats.getAmount() );
		int wholeStackArmor = calculateWholeStackArmor( aDefenderArmor, aDefenderAmount );
		int damageToDeal = wholeStackDamageToDeal - wholeStackArmor;

		if ( damageToDeal < 0 )
		{
			damageToDeal = randomDamageValue;
		}
		else if ( damageToDeal > 100 )
		{
			damageToDeal = 100;
		}


		return changeDamageAfter( damageToDeal );

	}

	private int calculateRandomDamageValue( AttackStatistic aAttackerStats )
	{
		return rand.nextInt( aAttackerStats.getDamage()
		                                   .upperEndpoint() - aAttackerStats.getDamage()
		                                                                    .lowerEndpoint() + 1 ) + aAttackerStats.getDamage()
		                                                                                                           .lowerEndpoint();
	}

	private int calculateWholeStackDamageToDeal( int aRandomDamageValue, int aAmount )
	{
		return aRandomDamageValue * aAmount;
	}

	protected int calculateWholeStackArmor( int aDefenderArmor, int aDefenderAmount )
	{
		return aDefenderArmor * aDefenderAmount;
	}

	abstract int changeDamageAfter( int aWholeStackDamageToDeal );
}
