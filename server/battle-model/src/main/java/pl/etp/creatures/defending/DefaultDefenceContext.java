/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.defending;

import pl.etp.creatures.attacking.CreatureLifeStats;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import static pl.etp.creatures.Creature.LIFE_CHANGED;

class DefaultDefenceContext implements DefenceContextIf
{
	private final DefenceStatistic stats;
	private DefaultDamageApplier damageApplier;
	private PropertyChangeSupport obsSupport = new PropertyChangeSupport( this );

	DefaultDefenceContext( DefaultDamageApplier aDamageApplier, int aArmor, int aMaxAmount, int aMaxHp )
	{
		damageApplier = aDamageApplier;
		stats = new DefenceStatistic( aArmor, aMaxAmount, aMaxHp );
	}

	@Override
	public void applyDamage( int aDamageToDeal )
	{
		CreatureLifeStats creatureLifeStats = damageApplier.countDamageToApply( this, aDamageToDeal );
		obsSupport.firePropertyChange( LIFE_CHANGED, new CreatureLifeStats( stats.getCurrentHp(), stats.getAmount() ), creatureLifeStats );
		stats.setAmount( creatureLifeStats.getAmount() );
		stats.setCurrentHp( creatureLifeStats.getHp() );
	}

	@Override
	public DefenceStatistic getDefenceStatistic()
	{
		return stats;
	}

	@Override
	public boolean canYouCounterAttackMe()
	{
		return true;
	}

	public void addObserver( String aEventType, PropertyChangeListener aObs )
	{
		obsSupport.addPropertyChangeListener( aEventType, aObs );
		obsSupport.firePropertyChange( LIFE_CHANGED, null, new CreatureLifeStats( stats.getCurrentHp(), stats.getAmount() ) );
	}
}
