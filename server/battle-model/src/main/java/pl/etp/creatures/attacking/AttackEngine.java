/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import pl.etp.board.TileIf;
import pl.etp.creatures.Creature;
import pl.etp.creatures.defending.DefenceContextIf;
import pl.etp.creatures.retaliating.RetaliationContextIf;

public class AttackEngine
{
	public void attack( TileIf aAttacker, TileIf aDefender )
	{
		if ( aAttacker.getAttackContext() instanceof SplashDamageCreatureDecorator )
		{
			attack( aAttacker.getAttackContext(), aDefender.getDefenceContext() );
		}
		else
		{
			while ( aAttacker.getAttackContext()
			                 .canAttack() )
			{
				attack( aAttacker.getAttackContext(), aDefender.getDefenceContext() );
				if ( aAttacker instanceof Creature )
				{
					updateActionCounter( ( Creature ) aAttacker );
				}
				if ( aDefender.getDefenceContext()
				              .getDefenceStatistic()
				              .getCurrentHp() > 0 )
				{
					counterAttack( aDefender.getAttackContext(), aAttacker.getDefenceContext(), aDefender.getRetaliationContext() );
				}
			}
		}
	}

	void attack( AttackContextIf aAttackContext, DefenceContextIf aDefenceContext )
	{
		int damageToDeal = aAttackContext.getDamageCalculator()
		                                 .calculateDamage( aAttackContext.getAttackerStatistic(), aDefenceContext.getDefenceStatistic()
		                                                                                                         .getArmor(), aDefenceContext.getDefenceStatistic()
		                                                                                                                                     .getAmount() );
		aDefenceContext.applyDamage( damageToDeal );
	}

	private void counterAttack( AttackContextIf aAttackContext, DefenceContextIf aDefenceContext, RetaliationContextIf aRetaliationContext )
	{
		if ( aRetaliationContext != null )
		{
			if ( aRetaliationContext.canRetaliate() && aDefenceContext.canYouCounterAttackMe() )
			{
				aRetaliationContext.updateRetaliateCounter();
				attack( aAttackContext, aDefenceContext );
			}
		}
	}

	private void updateActionCounter( Creature aAttacker )
	{
		aAttacker.getMoveContext()
		         .updateMoveCounterByAttack();
		aAttacker.getAttackContext()
		         .updateAttackCounter();
	}

}
