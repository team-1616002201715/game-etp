/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.defending;

import pl.etp.creatures.attacking.CreatureLifeStats;

class DefaultDamageApplier
{
	CreatureLifeStats countDamageToApply( DefaultDefenceContext aDefaultDefenceContext, int aDamageToDeal )
	{
		int amount = aDefaultDefenceContext.getDefenceStatistic()
		                                   .getAmount();
		int currentHp = aDefaultDefenceContext.getDefenceStatistic()
		                                      .getCurrentHp();
		int maxHp = aDefaultDefenceContext.getDefenceStatistic()
		                                  .getMaxHp();

		if ( aDamageToDeal < 0 )
		{
			return new CreatureLifeStats( healCurrentHp( aDamageToDeal, currentHp, maxHp ), amount );
		}
		else
		{

			int fullCurrentHp = ( maxHp * ( amount - 1 ) ) + currentHp - aDamageToDeal;
			if ( fullCurrentHp <= 0 )
			{
				amount = 0;
				currentHp = 0;
			}
			else
			{
				if ( fullCurrentHp % maxHp == 0 )
				{
					currentHp = maxHp;
					amount = fullCurrentHp / maxHp;
				}
				else
				{
					currentHp = fullCurrentHp % maxHp;
					amount = ( fullCurrentHp / maxHp ) + 1;
				}
			}
			return new CreatureLifeStats( currentHp, amount );
		}
	}

	private int healCurrentHp( int aDamageToDeal, int aCurrentHp, int aMaxHp )
	{
		aCurrentHp = aCurrentHp - aDamageToDeal;
		if ( aCurrentHp > aMaxHp )
		{
			aCurrentHp = aMaxHp;
		}
		return aCurrentHp;
	}
}
