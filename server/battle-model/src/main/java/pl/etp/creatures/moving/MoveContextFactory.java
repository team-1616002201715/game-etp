/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import pl.etp.creatures.statistic.CreatureStatistic;

public class MoveContextFactory
{
	private MoveContextFactory()
	{
	}

	public static MoveContextIf create( CreatureStatistic aCreatureStatistic )
	{
		switch ( aCreatureStatistic )
		{
			case BABA_YAGA:
			case TEST_MOVING_TWO_TIMES:
				return new MovingMoreTimesCreatureDecorator( 2, new DefaultMoveContext( new FlyMoveStrategy( aCreatureStatistic.getMoveRange() ), aCreatureStatistic.getMoveRange() ) );
			default:
				return create( aCreatureStatistic.getMovementType(), aCreatureStatistic.getMoveRange() );
		}
	}

	public static MoveContextIf create( MovementType aMovementType, int aMoveRange )
	{
		if ( aMovementType.equals( MovementType.FLYING ) )
		{
			return new DefaultMoveContext( new FlyMoveStrategy( aMoveRange ), aMoveRange );
		}
		else if ( aMovementType.equals( MovementType.TELEPORT ) )
		{
			return new DefaultMoveContext( new TeleportMoveStrategy( aMoveRange ), aMoveRange );
		}
		else
		{
			return new DefaultMoveContext( new GroundMoveStrategy( aMoveRange ), aMoveRange );
		}
	}
}
