/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import java.beans.PropertyChangeEvent;

class DefaultMoveContext implements MoveContextIf
{
	private final MoveStrategyIf moveStrategyIf;
	private final MoveStatistic moveStatistic;
	int turnMovementCounter;
	int maxMovementOnRound = 1;

	public DefaultMoveContext( MoveStrategyIf aMoveStrategyIf, int aMoveRange )
	{
		moveStrategyIf = aMoveStrategyIf;
		moveStatistic = new MoveStatistic( aMoveRange );
	}

	@Override
	public MoveStrategyIf getMoveStrategy()
	{
		return moveStrategyIf;
	}

	@Override
	public void endTurnEvent( PropertyChangeEvent aPropertyChangeEvent )
	{
		turnMovementCounter = 0;
	}

	@Override
	public void updateMoveCounter()
	{
		turnMovementCounter++;
	}

	@Override
	public void updateMoveCounterByAttack()
	{
		updateMoveCounter();
	}

	@Override
	public boolean canMove()
	{
		return turnMovementCounter < maxMovementOnRound;
	}

	@Override
	public MoveStatistic getMoveStatistic()
	{
		return moveStatistic;
	}

	@Override
	public int getTurnMoveCounter()
	{
		return turnMovementCounter;
	}

}
