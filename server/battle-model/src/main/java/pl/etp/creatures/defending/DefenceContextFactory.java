/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.defending;

import context.DefenceContextStatisticIf;
import fields.StaticFieldStatistic;
import pl.etp.creatures.statistic.CreatureStatistic;

public class DefenceContextFactory
{
	private static String INVALID_DEFENCE_CONTEXT_STATISTIC = "Invalid DefenceContextStatistic";

	public static DefenceContextIf create( DefenceContextStatisticIf aStats, int aMaxAmount )
	{
		switch ( aStats.getImplementation() )
		{
			case "CREATURE":
				switch ( ( CreatureStatistic ) aStats )
				{
					case HEDGEHOG:
					case OFFICER:
					case SPECTRE:
					case MINIGUNNER:
					case PATRIOT:
					case TEST_SPLASH_DAMAGE_9:
					case TEST_BLOCK_COUNTER:
					case TEST_GOLDEN_SHOT_CREATURE:
					case TEST_SHOOTING_CREATURE:
						return new BlockCounterAttackCreatureDecorator( createDefaultContext( aStats, aMaxAmount ) );
					case WEREWOLF:
					case TEST_HEAL_AFTER_GET_ATTACK:
						return new HealAfterGetAttackedDecorator( createDefaultContext( aStats, aMaxAmount ), 0.1 );
					case UNDEAD:
						return new HealAfterGetAttackedDecorator( createDefaultContext( aStats, aMaxAmount ), 0.15 );
					default:
						return createDefaultContext( aStats, aMaxAmount );

				}
			case "FIELD":
				switch ( ( StaticFieldStatistic ) aStats )
				{
					case WALL_STATISTIC:
					case MAGIC_BALLOON_STATISTIC:
						return new BlockCounterAttackCreatureDecorator( createDefaultContext( aStats, aMaxAmount ) );
					default:
						return createDefaultContext( aStats, aMaxAmount );
				}
			default:
				throw new IllegalArgumentException( INVALID_DEFENCE_CONTEXT_STATISTIC );
		}

	}

	public static DefenceContextIf create( Integer aArmor, Integer aAmount, Integer aMaxHp )
	{
		return new DefaultDefenceContext( new DefaultDamageApplier(), aArmor, aAmount, aMaxHp );
	}

	private static DefenceContextIf createDefaultContext( DefenceContextStatisticIf aCreatureStatistic, int aMaxAmount )
	{
		return new DefaultDefenceContext( new DefaultDamageApplier(), aCreatureStatistic.getArmor(), aMaxAmount, aCreatureStatistic.getMaxHp() );
	}
}
