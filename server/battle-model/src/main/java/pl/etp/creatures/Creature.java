/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures;

import com.google.common.collect.Range;
import pl.etp.GameEngine;
import pl.etp.board.TileIf;
import pl.etp.creatures.attacking.AttackContextFactory;
import pl.etp.creatures.attacking.AttackContextIf;
import pl.etp.creatures.attacking.AttackStatistic;
import pl.etp.creatures.attacking.damageCalculator.AbstractDamageCalculator;
import pl.etp.creatures.attacking.damageCalculator.DamageCalculatorFactory;
import pl.etp.creatures.defending.DefenceContextFactory;
import pl.etp.creatures.defending.DefenceContextIf;
import pl.etp.creatures.moving.MoveContextFactory;
import pl.etp.creatures.moving.MoveContextIf;
import pl.etp.creatures.moving.MovementType;
import pl.etp.creatures.retaliating.RetaliationContextFactory;
import pl.etp.creatures.retaliating.RetaliationContextIf;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.spells.*;
import pl.etp.spells.EffectStats;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Creature implements PropertyChangeListener, TileIf
{
	public static final String LIFE_CHANGED = "LIFE_CHANGED";
	private final MoveContextIf moveContext;
	private final DefenceContextIf defenceContext;
	private final AttackContextIf attackContext;
	private final RetaliationContextIf retaliationContext;
	private final MagicResistanceContextIf magicResistanceContext;
	private BuffContainer buffContainer;

	Creature( DefenceContextIf aDefenceContext, AttackContextIf aAttackContext, MoveContextIf aMoveContextIf, MagicResistanceContextIf aMagicResistanceContextIf, RetaliationContextIf aRetaliationContextIf )
	{
		defenceContext = aDefenceContext;
		attackContext = aAttackContext;
		defenceContext.addObserver( LIFE_CHANGED, aAttackContext );
		moveContext = aMoveContextIf;
		retaliationContext = aRetaliationContextIf;
		magicResistanceContext = aMagicResistanceContextIf;

		buffContainer = new BuffContainer(this::upgradeCreatureStatistics );
	}
	public boolean isStandable()
	{
		return false;
	}

	@Override
	public boolean isCrossable( MovementType aMovementType )
	{
		return aMovementType.equals( MovementType.FLYING );
	}

	@Override
	public DefenceContextIf getDefenceContext()
	{
		return defenceContext;
	}

	@Override
	public RetaliationContextIf getRetaliationContext()
	{
		return retaliationContext;
	}

	@Override
	public AttackContextIf getAttackContext()
	{
		return attackContext;
	}

	public MoveContextIf getMoveContext()
	{
		return moveContext;
	}

	public MagicResistanceContextIf getMagicResistanceContext() { return magicResistanceContext; }

	public int getCurrentHp()
	{
		return defenceContext.getDefenceStatistic()
		                     .getCurrentHp();
	}

	public int getArmor()
	{
		return defenceContext.getDefenceStatistic()
		                     .getArmor();
	}

	public int getMoveRange()
	{
		return moveContext.getMoveStatistic()
		                  .getMoveRange();
	}

	public int getAmount()
	{
		return defenceContext.getDefenceStatistic()
		                     .getAmount();
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		if ( GameEngine.END_OF_TURN.equals( aPropertyChangeEvent.getPropertyName() ) )
		{
			moveContext.endTurnEvent( aPropertyChangeEvent );
			attackContext.endTurnEvent( aPropertyChangeEvent );
			retaliationContext.endTurnEvent( aPropertyChangeEvent );
		}
	}

	private void updateAttackCounter()
	{
		attackContext.updateAttackCounter();
	}

	private void updateMoveCounter()
	{
		moveContext.updateMoveCounter();
	}

	public void applyMagicDamage( int aDamage )
	{
		int reducedDamage = magicResistanceContext.reduceMagicDamageDamage( aDamage );
		defenceContext.applyDamage( reducedDamage );
	}

	public void applyStatusEffect( StatusEffectSpell aStatusEffectSpell )
	{
		buffContainer.add( aStatusEffectSpell );
	}

	public void upgradeCreatureStatistics( EffectStats aUpgradeStats )
	{
		moveContext.getMoveStatistic()
		           .setMoveRange( moveContext.getMoveStatistic()
		                                     .getMoveRange() + aUpgradeStats.getMoveRange() );
		moveContext.getMoveStatistic()
		           .setMoveRange( calculateUpgradingStats( moveContext.getMoveStatistic()
		                                                              .getMoveRange(), aUpgradeStats.getMoveRangePercentage() ) );

		defenceContext.getDefenceStatistic()
		              .setArmor( defenceContext.getDefenceStatistic()
		                                       .getArmor() + aUpgradeStats.getArmor() );
		defenceContext.getDefenceStatistic()
		              .setArmor( calculateUpgradingStats( defenceContext.getDefenceStatistic()
		                                                                .getArmor(), aUpgradeStats.getArmorPercentage() ) );

		defenceContext.getDefenceStatistic()
		              .setMaxHp( defenceContext.getDefenceStatistic()
		                                       .getMaxHp() + aUpgradeStats.getMaxHp() );
		defenceContext.getDefenceStatistic()
		              .setMaxHp( calculateUpgradingStats( defenceContext.getDefenceStatistic()
		                                                                .getMaxHp(), aUpgradeStats.getMaxHpPercentage() ) );

		attackContext.getAttackerStatistic()
		             .setAttackRange( attackContext.getAttackerStatistic()
		                                           .getAttackRange() + aUpgradeStats.getAttackRange() );

		attackContext.getAttackerStatistic()
		             .setDamage( Range.closed( attackContext.getAttackerStatistic()
		                                                    .getLowerEndpoint() + aUpgradeStats.getLowerEndpoint(), attackContext.getAttackerStatistic()
		                                                                                                                         .getUpperEndpoint() + aUpgradeStats.getUpperEndpoint() ) );
		attackContext.getAttackerStatistic()
		             .setDamage( Range.closed( calculateUpgradingStats( attackContext.getAttackerStatistic()
		                                                                             .getLowerEndpoint(), aUpgradeStats.getDamagePercentage() ), calculateUpgradingStats( attackContext.getAttackerStatistic()
		                                                                                                                                                                               .getUpperEndpoint(), aUpgradeStats.getDamagePercentage() ) ) );

		magicResistanceContext.setMagicResStatistic( magicResistanceContext.getMagicResStatistic() + aUpgradeStats.getMagicResistancePercentage() );
	}

	private int calculateUpgradingStats( int aCurrent, double aBuffPercentage )
	{
		return aCurrent + ( int ) ( aCurrent * aBuffPercentage );
	}

	public BuffContainer getBuffContainer()
	{
		return buffContainer;
	}

	public static class Builder
	{
		private CreatureStatistic stats;

		private String name;
		private Integer armor;
		private Integer maxHp;
		private Integer moveRange;
		private Range<Integer> damage;
		private Integer amount;
		private AbstractDamageCalculator calcDmgStrategy;
		private MovementType movementType;
		private AttackContextIf attackContextIf;
		private DefenceContextIf defenceContextIf;
		private MagicResistanceContextIf magicResistanceContextIf;
		private MoveContextIf moveContextIf;
		private RetaliationContextIf retaliationContextIf;

		public Builder statistic( CreatureStatistic stats )
		{
			this.stats = stats;
			return this;
		}

		public Builder name( String name )
		{
			this.name = name;
			return this;
		}

		public Builder armor( int armor )
		{
			this.armor = armor;
			return this;
		}

		public Builder maxHp( int maxHp )
		{
			this.maxHp = maxHp;
			return this;
		}

		public Builder moveRange( int moveRange )
		{
			this.moveRange = moveRange;
			return this;
		}

		public Builder damage( Range<Integer> damage )
		{
			this.damage = damage;
			return this;
		}

		public Builder amount( int amount )
		{
			this.amount = amount;
			return this;
		}

		public Builder calcDmgStrategy( AbstractDamageCalculator calcDmgStrategy )
		{
			this.calcDmgStrategy = calcDmgStrategy;
			return this;
		}

		public Builder moveStrategy( MovementType aMovementType )
		{
			this.movementType = aMovementType;
			return this;
		}

		public Builder attackContext( AttackContextIf aAttackContext )
		{
			this.attackContextIf = aAttackContext;
			return this;
		}

		public Builder defenceContext( DefenceContextIf aDefenceContext )
		{
			this.defenceContextIf = aDefenceContext;
			return this;
		}

		public Builder magicResistanceContext( MagicResistanceContextIf aMagicResistanceContext )
		{
			this.magicResistanceContextIf = aMagicResistanceContext;
			return this;
		}

		public Builder moveContext( MoveContextIf aMoveContext )
		{
			this.moveContextIf = aMoveContext;
			return this;
		}

		public Builder retaliationContext( RetaliationContextIf aRetaliationContextIf )
		{
			this.retaliationContextIf = aRetaliationContextIf;
			return this;
		}

		public Creature build()
		{
			preconditions();
			DefenceContextIf tempDefenceContext = prepareDefenceContext();
			AttackContextIf tempAttackContext = prepareAttackContext();
			MoveContextIf tempMoveContext = prepareMoveContext();
			MagicResistanceContextIf tempMagicResistanceContext = prepareMagicResistanceContext();
			RetaliationContextIf tempRetaliationContext = prepareRetaliationContext();
			return new Creature( tempDefenceContext, tempAttackContext, tempMoveContext, tempMagicResistanceContext, tempRetaliationContext );
		}

		private void preconditions()
		{
			if ( amount == null )
			{
				amount = 1;
			}
			if ( armor == null )
			{
				armor = 1;
				if ( stats != null )
				{
					armor = stats.getArmor();
				}
			}
			if ( damage == null )
			{
				damage = Range.closed( 1, 1 );
				if ( stats != null )
				{
					damage = stats.getDamage();
				}
			}
			if ( moveRange == null )
			{
				moveRange = 1;
				if ( stats != null )
				{
					moveRange = stats.getMoveRange();
				}
			}
			if ( movementType == null )
			{
				movementType = MovementType.GROUND;
				if ( stats != null )
				{
					movementType = stats.getMovementType();
				}
			}
			if ( maxHp == null )
			{
				maxHp = 1;
				if ( stats != null )
				{
					maxHp = stats.getMaxHp();
				}
			}
			if ( name == null )
			{
				if ( stats != null )
				{
					name = stats.getName();
				}
			}
		}

		private DefenceContextIf prepareDefenceContext()
		{
			if ( defenceContextIf == null )
			{
				if ( stats != null )
				{
					return DefenceContextFactory.create( stats, amount );
				}
				return DefenceContextFactory.create( this.armor, amount, maxHp );
			}
			return defenceContextIf;
		}

		private AttackContextIf prepareAttackContext()
		{
			if ( stats != null )
			{
				return AttackContextFactory.create( stats );
			}

			//for testing
			if ( calcDmgStrategy == null )
			{
				calcDmgStrategy = DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.DEFAULT );
			}
			return AttackContextFactory.create( AttackStatistic.builder()
			                                                   .amount( this.amount )
			                                                   .attackRange( 1 )
			                                                   .damage( this.damage )
			                                                   .build(), calcDmgStrategy );
		}

		private RetaliationContextIf prepareRetaliationContext()
		{
			if ( retaliationContextIf == null )
			{
				if ( stats != null )
				{
					return RetaliationContextFactory.create( stats );
				}
				else
				{
					return RetaliationContextFactory.createDefaultContext();
				}
			}
			return retaliationContextIf;
		}

		private MoveContextIf prepareMoveContext()
		{
			if ( stats != null )
			{
				return MoveContextFactory.create( stats );
			}
			return MoveContextFactory.create( movementType, moveRange );
		}

		private MagicResistanceContextIf prepareMagicResistanceContext()
		{
			if ( stats != null )
			{
				return MagicResistanceFactory.create( stats.getMagicResistance() );
			}
			else if ( magicResistanceContextIf != null )
			{
				return magicResistanceContextIf;
			}

			return MagicResistanceFactory.create( 0 );
		}
	}
}
