/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import pl.etp.creatures.attacking.damageCalculator.AbstractDamageCalculator;

import java.beans.PropertyChangeEvent;

class DefaultAttackContext implements AttackContextIf
{
	private final AbstractDamageCalculator calculateDamageStrategy;
	private AttackStatistic stats;
	int turnAttackCounter;
	int maxAttackOnRound = 1;

	DefaultAttackContext( AbstractDamageCalculator aCalculateDamageStrategy, AttackStatistic aStats )
	{
		calculateDamageStrategy = aCalculateDamageStrategy;
		stats = aStats;
	}

	@Override
	public SplashRange getSplashRange()
	{
		return new SplashRange();
	}

	@Override
	public AbstractDamageCalculator getDamageCalculator()
	{
		return calculateDamageStrategy;
	}

	@Override
	public AttackStatistic getAttackerStatistic()
	{
		return stats;
	}

	@Override
	public void updateAttackCounter()
	{
		turnAttackCounter++;
	}

	@Override
	public void endTurnEvent( PropertyChangeEvent aPropertyChangeEvent )
	{
		turnAttackCounter = 0;
	}

	@Override
	public boolean canAttack()
	{
		return turnAttackCounter < maxAttackOnRound;
	}

	@Override
	public int getTurnAttackCounter()
	{
		return turnAttackCounter;
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		stats = new AttackStatistic( ( ( CreatureLifeStats ) aPropertyChangeEvent.getNewValue() ).getAmount(), stats );
	}
}
