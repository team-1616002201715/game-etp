/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving.utils;

import pl.etp.board.Point;

import java.util.Objects;

public class Node implements Comparable<Node>
{
	private final Point point;
	private int depth;
	private final Node parent;
	private final float heuristic;
	private final float cost;

	public Node( Point aPoint, float aCost, int aDepth, Node aParent, float aHeuristic )
	{
		point = aPoint;
		cost = aCost;
		depth = aDepth;
		parent = aParent;
		heuristic = aHeuristic;
	}

	@Override
	public int compareTo( Node aNode )
	{

		float f = heuristic + cost;
		float of = aNode.getHeuristic() + aNode.getCost();

		if ( f < of )
		{
			return -1;
		}
		else if ( f > of )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public int increaseDepth( Node aParent )
	{
		if ( aParent != null )
		{
			depth = aParent.getDepth() + 1;
		}

		return depth;
	}

	public Point getPoint()
	{
		return point;
	}

	public Node getParent()
	{
		return parent;
	}

	private int getDepth()
	{
		return depth;
	}

	float getHeuristic()
	{
		return heuristic;
	}

	public float getCost()
	{
		return cost;
	}

	@Override
	public boolean equals( Object aO )
	{
		if ( this == aO )
		{
			return true;
		}
		if ( aO == null || getClass() != aO.getClass() )
		{
			return false;
		}
		Node node = ( Node ) aO;
		return depth == node.depth && Float.compare( node.heuristic, heuristic ) == 0 && Float.compare( node.cost, cost ) == 0 && Objects.equals( point, node.point ) && Objects.equals( parent, node.parent );
	}

	@Override
	public int hashCode()
	{
		return Objects.hash( point, depth, parent, heuristic, cost );
	}

}
