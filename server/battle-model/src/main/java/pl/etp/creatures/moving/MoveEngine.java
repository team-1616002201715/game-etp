/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.board.fields.AbstractAffectingField;
import pl.etp.creatures.Creature;

import java.util.ArrayList;
import java.util.List;

public class MoveEngine
{
	private final BoardManager boardManager;
	List<Point> path;

	public MoveEngine( BoardManager aBoardManager )
	{
		boardManager = aBoardManager;
	}

	public List<Point> move( Creature aMover, Point aTargetPoint )
	{
		Point sourcePoint = boardManager.getPointByTile( aMover );

		if ( canMove( aMover, sourcePoint, aTargetPoint ) )
		{
			boardManager.removeFromBoard( sourcePoint );

			applySpecialFieldsEffect( aMover );

			boardManager.putOnBoard( aTargetPoint, aMover );
			aMover.getMoveContext()
			      .updateMoveCounter();

			return path;
		}

		return path;
	}

	private void applySpecialFieldsEffect( Creature aMover )
	{
		List<AbstractAffectingField> affectingFields = boardManager.getAffectingFields( path );
		affectingFields.forEach( f -> f.apply( aMover ) );
	}

	public boolean canMove( Creature aMover, Point aTargetPoint )
	{
		return aMover.getMoveContext()
		             .canMove() && aMover.getMoveContext()
		                                 .getMoveStrategy()
		                                 .isMovePossible( boardManager.getPointByTile( aMover ), aTargetPoint, boardManager );
	}

	private boolean canMove( Creature aMover, Point aSourcePoint, Point aTargetPoint )
	{
		path = aMover.getMoveContext()
		             .getMoveStrategy()
		             .move( aSourcePoint, aTargetPoint, boardManager );
		return path.contains( aTargetPoint ) && aMover.getMoveContext()
		                                              .canMove();
	}
}
