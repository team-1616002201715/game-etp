/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import pl.etp.creatures.attacking.damageCalculator.AbstractDamageCalculator;
import pl.etp.creatures.attacking.damageCalculator.DamageCalculatorFactory;
import pl.etp.creatures.statistic.CreatureStatistic;

public class AttackContextFactory
{
	public static AttackContextIf create( CreatureStatistic aStats )
	{
		switch ( aStats )
		{
			case HANDYMAN:
			case IVAYLO:
				return new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.GOLDEN_SHOT ), mapStatsToAttackContextStats( aStats ) );
			case PATRIOT:
			case TEST_SHOOTING_CREATURE:
			case MINIGUNNER:
				return new ShootingCreatureDecorator( new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.DEFAULT ), mapStatsToAttackContextStats( aStats ) ) );
			case SPECTRE:
				return new ShootingCreatureDecorator( new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.IGNORE_ARMOR ), mapStatsToAttackContextStats( aStats ) ) );
			case RAILGUN:
			case TEST_SPLASH_DAMAGE_9:
				return new SplashDamageCreatureDecorator( splash9Square(), new ShootingCreatureDecorator( new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.DEFAULT ), mapStatsToAttackContextStats( aStats ) ) ) );
			case NEW_YORKER:
			case TEST_ATTACK_TWO_TIMES:
				return new AttackingMoreTimesCreatureDecorator( 2, new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.DEFAULT ), mapStatsToAttackContextStats( aStats ) ) );
			case TEST_GOLDEN_SHOT_CREATURE:
				return new ShootingCreatureDecorator( new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.GOLDEN_SHOT ), mapStatsToAttackContextStats( aStats ) ) );
			default:
				return new DefaultAttackContext( DamageCalculatorFactory.create( DamageCalculatorFactory.TYPE.DEFAULT ), mapStatsToAttackContextStats( aStats ) );
		}
	}

	public static AttackContextIf create( AttackStatistic aStats, AbstractDamageCalculator aCalculateDamageStrategyIf )
	{
		return new DefaultAttackContext( aCalculateDamageStrategyIf, aStats );
	}

	private static AttackStatistic mapStatsToAttackContextStats( CreatureStatistic aStatistic )
	{
		return AttackStatistic.builder()
		                      .attackRange( 1 )
		                      .damage( aStatistic.getDamage() )
		                      .build();
	}

	private static SplashRange splash9Square()
	{
		return new SplashRange( new boolean[][]{{true, true, true}, {true, true, true}, {true, true, true}} );
	}
}
