/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.moving.utils.Node;
import pl.etp.creatures.moving.utils.Path;
import pl.etp.creatures.moving.utils.SortedList;;

import java.util.ArrayList;

import static pl.etp.GameEngine.BOARD_HEIGHT;
import static pl.etp.GameEngine.BOARD_WIDTH;

class PathFinder
{
	private final MovementType movementType;
	private final int maxSearchDistance;
	private final DistanceHeuristic heuristic;
	private Node[][] nodes;
	private BoardManager boardManager;

	PathFinder( MovementType aMovementType, int aMaxSearchDistance )
	{
		this( aMovementType, aMaxSearchDistance, new ManhattanDistanceHeuristic() );
	}

	private PathFinder( MovementType aMovementType, int aMaxSearchDistance, DistanceHeuristic aDistanceHeuristic )
	{
		movementType = aMovementType;
		maxSearchDistance = aMaxSearchDistance;
		heuristic = aDistanceHeuristic;
	}

	Path getPath( BoardManager aBoardManager, Point aSourcePoint, Point aTargetPoint )
	{
		boardManager = aBoardManager;
		Path path = new Path();
		fillBoardWithNodes();

		int sourceX = aSourcePoint.getX();
		int sourceY = aSourcePoint.getY();
		int targetX = aTargetPoint.getX();
		int targetY = aTargetPoint.getY();

		if ( !boardManager.canStand( aTargetPoint ) )
		{
			return path;
		}
		else
		{
			ArrayList<Node> closed = new ArrayList<>();
			SortedList open = new SortedList();
			int maxDepth = 0;

			open.add( nodes[sourceX][sourceY] );

			while ( maxDepth < maxSearchDistance && open.size() != 0 )
			{
				Node current = open.first();

				if ( current.equals( nodes[targetX][targetY] ) )
				{
					break;
				}

				open.remove( current );
				closed.add( current );

				for ( int x = -1; x < 2; x++ )
				{
					for ( int y = -1; y < 2; y++ )
					{
						if ( ( x == 0 ) || ( y == 0 ) )
						{
							int candidateX = x + current.getPoint()
							                            .getX();
							int candidateY = y + current.getPoint()
							                            .getY();

							if ( isValidLocation( aSourcePoint, boardManager.getPoint( candidateX, candidateY ) ) )
							{
								Node candidateNode = nodes[candidateX][candidateY];

								float nextStepCost = current.getCost() + heuristic.getCost( current.getPoint(), candidateNode.getPoint() );

								if ( nextStepCost < candidateNode.getCost() )
								{
									if ( open.contains( candidateNode ) )
									{
										open.remove( candidateNode );
									}

									closed.remove( candidateNode );

								}

								if ( !open.contains( candidateNode ) && !closed.contains( candidateNode ) )
								{
									float newHeuristic = heuristic.getCost( candidateNode.getPoint(), nodes[targetX][targetY].getPoint() );
									maxDepth = Math.max( maxDepth, candidateNode.increaseDepth( current ) );
									Node newNode = new Node( candidateNode.getPoint(), nextStepCost, maxDepth, current, newHeuristic );

									nodes[candidateX][candidateY] = newNode;
									open.add( newNode );
								}
							}
						}

					}
				}
			}
		}

		if ( nodes[targetX][targetY].getParent() != null )
		{
			Node target = nodes[targetX][targetY];
			while ( !target.equals( nodes[sourceX][sourceY] ) )
			{
				path.prependStep( target.getPoint() );
				target = target.getParent();
			}
		}

		return path;

	}

	private void fillBoardWithNodes()
	{
		nodes = new Node[BOARD_WIDTH][BOARD_HEIGHT];
		for ( int x = 0; x < BOARD_WIDTH; x++ )
		{
			for ( int y = 0; y < BOARD_HEIGHT; y++ )
			{
				nodes[x][y] = new Node( boardManager.getPoint( x, y ), 0.0F, 0, null, 0 );
			}
		}
	}

	private boolean isValidLocation( Point aSourcePoint, Point aNeighbourPoint )
	{
		boolean validation = ( aNeighbourPoint.getX() < 0 ) || ( aNeighbourPoint.getY() < 0 ) || ( aNeighbourPoint.getX() >= BOARD_WIDTH ) || ( aNeighbourPoint.getY() >= BOARD_HEIGHT );
		if ( ( !validation ) && ( ( aSourcePoint.getX() != aNeighbourPoint.getX() ) || ( aSourcePoint.getY() != aNeighbourPoint.getY() ) ) )
		{
			if ( boardManager.getTileByPoint( aNeighbourPoint ) != null )
			{
				validation = !boardManager.getTileByPoint( aNeighbourPoint )
				                          .isCrossable( movementType );
			}
		}
		return !validation;
	}
}
