/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.attacking;

import pl.etp.creatures.attacking.damageCalculator.AbstractDamageCalculator;

import java.beans.PropertyChangeEvent;

class AttackingMoreTimesCreatureDecorator implements AttackContextIf
{
	private final int attacksInSeries;
	private final AttackContextIf decorated;

	AttackingMoreTimesCreatureDecorator( int aAttacksInSeries, AttackContextIf aDecorated )
	{
		decorated = aDecorated;
		attacksInSeries = aAttacksInSeries;
	}

	@Override
	public boolean canAttack()
	{
		return decorated.getTurnAttackCounter() < attacksInSeries;
	}

	@Override
	public SplashRange getSplashRange()
	{
		return decorated.getSplashRange();
	}

	@Override
	public AbstractDamageCalculator getDamageCalculator()
	{
		return decorated.getDamageCalculator();
	}

	@Override
	public AttackStatistic getAttackerStatistic()
	{
		return decorated.getAttackerStatistic();
	}

	@Override
	public void updateAttackCounter()
	{
		decorated.updateAttackCounter();
	}

	@Override
	public void endTurnEvent( PropertyChangeEvent aPropertyChangeEvent )
	{
		decorated.endTurnEvent( aPropertyChangeEvent );
	}

	@Override
	public int getTurnAttackCounter()
	{
		return decorated.getTurnAttackCounter();
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		decorated.propertyChange( aPropertyChangeEvent );
	}
}
