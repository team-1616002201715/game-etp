/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.moving;

import java.beans.PropertyChangeEvent;

class MovingMoreTimesCreatureDecorator implements MoveContextIf
{
	private final int possibleMoves;
	private final MoveContextIf decorated;

	MovingMoreTimesCreatureDecorator( int aPossibleMoves, MoveContextIf aDecorated )
	{
		decorated = aDecorated;
		possibleMoves = aPossibleMoves;
	}

	@Override
	public boolean canMove()
	{
		return decorated.getTurnMoveCounter() < possibleMoves;
	}

	@Override
	public MoveStrategyIf getMoveStrategy()
	{
		return decorated.getMoveStrategy();
	}

	@Override
	public void endTurnEvent( PropertyChangeEvent aPropertyChangeEvent )
	{
		decorated.endTurnEvent( aPropertyChangeEvent );
	}

	@Override
	public void updateMoveCounter()
	{
		decorated.updateMoveCounter();
	}

	@Override
	public void updateMoveCounterByAttack()
	{
		//enables attack between moves
	}

	@Override
	public MoveStatistic getMoveStatistic()
	{
		return decorated.getMoveStatistic();
	}

	@Override
	public int getTurnMoveCounter()
	{
		return decorated.getTurnMoveCounter();
	}
}
