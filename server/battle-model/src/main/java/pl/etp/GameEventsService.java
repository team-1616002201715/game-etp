/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.Fraction;
import pl.etp.exceptions.SpringServiceNotInitializedException;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.ManaPool;

import java.util.List;

@Service
@AllArgsConstructor
public class GameEventsService
{
	private static SimpMessagingTemplate simpMessagingTemplate;

	@Autowired
	public void setTemplate( SimpMessagingTemplate aTemplate )
	{
		simpMessagingTemplate = aTemplate;
	}

	public static void broadcastActiveCreature( GameEngine aGameEngine, Creature activeCreature )
	{
		broadcastActivePlayer( aGameEngine, activeCreature );
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/activeCreature/" + aGameEngine.getGameId(), aGameEngine.getBoardManager()
			                                                                                                     .getPointByTile( activeCreature ) );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastActivePlayer( GameEngine aGameEngine, Creature activeCreature )
	{
		if ( aGameEngine.isHero1Turn() )
		{
			try
			{
				simpMessagingTemplate.convertAndSend( "/topic/activePlayer/" + aGameEngine.getGameId(), 1 );
			}
			catch ( NullPointerException aException )
			{
				throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
			}
		}
		else
		{
			try
			{
				simpMessagingTemplate.convertAndSend( "/topic/activePlayer/" + aGameEngine.getGameId(), 2 );
			}
			catch ( NullPointerException aException )
			{
				throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
			}
		}
	}

	public static void broadcastPath( GameEngine aGameEngine, List<Point> aPath )
	{
		if ( aPath.isEmpty() )
		{
			try
			{
				simpMessagingTemplate.convertAndSend( "/topic/move/" + aGameEngine.getGameId(), "no move" );
			}
			catch ( NullPointerException aException )
			{
				throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );

			}
		}
		else
		{
			try
			{
				simpMessagingTemplate.convertAndSend( "/topic/move/" + aGameEngine.getGameId(), aPath );
			}
			catch ( NullPointerException aException )
			{
				throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
			}
		}
	}

	public static void broadcastAvailableTiles( GameEngine aGameEngine, List<Point> aAvailableMoves, List<Point> aAvailableAttacks )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/availableMoves/" + aGameEngine.getGameId(), aAvailableMoves );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/availableAttacks/" + aGameEngine.getGameId(), aAvailableAttacks );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastCurrentBoard( GameEngine aGameEngine, BoardManager aBoardManager )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/currentBoard/creatures/" + aGameEngine.getGameId(), aBoardManager.getBoardPointsWithCreatures() );
			simpMessagingTemplate.convertAndSend( "/topic/currentBoard/specialTiles/" + aGameEngine.getGameId(), aBoardManager.getBoardPointsWithSpecialTiles() );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastLastAction( GameEngine aGameEngine, Enum aAction )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/lastAction/" + aGameEngine.getGameId(), aAction );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastAvailableSpells( GameEngine aGameEngine, List<AbstractSpell> aAvailableSpells )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/availableSpells/" + aGameEngine.getGameId(), aAvailableSpells );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastCastRange( GameEngine aGameEngine, List<Point> aAvailableCasts )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/castRange/" + aGameEngine.getGameId(), aAvailableCasts );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastCurrentMana( GameEngine aGameEngine, ManaPool aManaPool )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/currentMana/" + aGameEngine.getGameId(), aManaPool );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastWinner( GameEngine aGameEngine, int aVictor )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/victor/" + aGameEngine.getGameId(), aVictor );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastFractions( String aGameId, Fraction[] aValues )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/fractions/" + aGameId, aValues );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastHeroes( String aGameId, String aPlayerSeat, List<String> aAllHeroes )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/heroes/" + aPlayerSeat + "/" + aGameId, aAllHeroes );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastCreatureShop( String aGameId, String aPlayerSeat, List<String> aCurrentCreaturePopulation )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/creatureShop/" + aPlayerSeat + "/" + aGameId, aCurrentCreaturePopulation );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastSpellShop( String aGameId, String aPlayerSeat, List<String> aCurrentSpellPopulation )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/spellShop/" + aPlayerSeat + "/" + aGameId, aCurrentSpellPopulation );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastArtifactShop( String aGameId, String aPlayerSeat, List<String> aCurrentArtifactPopulation )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/artifactShop/" + aPlayerSeat + "/" + aGameId, aCurrentArtifactPopulation );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastPlayerGold( String aGameId, String aPlayerSeat, int aGold )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/currentGold/" + aPlayerSeat + "/" + aGameId, aGold );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

	public static void broadcastOpponentsProgress( String aGameId, String aPlayerSeat, int aOpponentsEcoPhase )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/opponentsEcoPhase/" + aPlayerSeat + "/" + aGameId, aOpponentsEcoPhase );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}

	}

	public static void broadcastPlayersSetup( String aGameId, String aPlayerSeat, List<String> aCreatures, List<String> aArtifacts, List<String> aSpells )
	{
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/playerSetup/" + aPlayerSeat + "/" + aGameId, List.of( aCreatures, aArtifacts, aSpells ) );
		}
		catch ( NullPointerException aException )
		{
			throw new SpringServiceNotInitializedException( "simpMessagingTemplate was not initialized correctly", aException );
		}
	}

}
