/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import pl.etp.creatures.Creature;
import pl.etp.creatures.spells.SpellStatistic;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.EffectStats;
import pl.etp.spells.SpellBook;
import pl.etp.spells.TestSpellFactory;

import java.util.List;

public class Hero
{
	private final List<Creature> creatures;
	private final SpellBook spellBook;

	public Hero( List<Creature> aCreatures )
	{
		this( aCreatures, new SpellBook( 15, List.of( TestSpellFactory.createMagicArrow(), TestSpellFactory.createMagicArrowWithSplashAndTargetType( 2, SpellStatistic.TargetType.ALLY ), TestSpellFactory.createMagicArrowWithSplash( 2 ) ) ) );
	}

	public Hero( List<Creature> aCreatures, SpellBook aSpellBook )
	{
		creatures = aCreatures;
		spellBook = aSpellBook;
	}

	public List<Creature> getCreatures()
	{
		return creatures;
	}

	boolean canCastSpellInThisTurn()
	{
		return spellBook.canCastSpellInThisTurn();
	}

	public int getCurrentMana()
	{
		return spellBook.getCurrentMana();
	}

	public int getMaxMana()
	{
		return spellBook.getMaxMana();
	}


	public SpellBook getSpellBook()
	{
		return spellBook;
	}

	void castSpell( AbstractSpell aSpell )
	{
		spellBook.cast( aSpell );
	}
}
