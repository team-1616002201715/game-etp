/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import pl.etp.creatures.Creature;
import pl.etp.spells.SpellBook;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

import static pl.etp.GameEngine.CREATURE_KILLED;

class TurnQueue
{
	private final ArrayList<Creature> creatures;
	private final Queue<Creature> creaturesQueue;
	private Creature activeCreature;
	private final PropertyChangeSupport observerSupport;
	private final Hero hero1;
	private final Hero hero2;

	TurnQueue( Hero aHero1, Hero aHero2 )
	{
		observerSupport = new PropertyChangeSupport( this );
		creatures = new ArrayList<>();
		creaturesQueue = new LinkedList<>();
		hero1 = aHero1;
		hero2 = aHero2;

		initQueue();
		next();
	}

	private void initQueue()
	{
		creatures.clear();
		registerHero( hero1 );
		registerHero( hero2 );
		creatures.forEach( c ->
		{
			addObserver( GameEngine.END_OF_TURN, c );
			addObserver( GameEngine.END_OF_TURN, c.getBuffContainer() );
		} );

		creaturesQueue.addAll( creatures );
	}

	public void clearQueueAfterVictory()
	{
		creaturesQueue.clear();
	}

	private void registerHero( Hero aHero )
	{
		addObserver( GameEngine.ACTION.PASS.name(), aHero.getSpellBook() );
		creatures.addAll( aHero.getCreatures() );
		creatures.sort( ( c1, c2 ) -> c2.getMoveRange() - c1.getMoveRange() );
	}

	public void addObserver( String aPropertyName, PropertyChangeListener aObserver )
	{
		observerSupport.addPropertyChangeListener( aPropertyName, aObserver );
	}

	void removeObserver( Creature aObserver )
	{
		observerSupport.addPropertyChangeListener( aObserver );
	}

	Creature getActiveCreature()
	{
		return activeCreature;
	}

	Hero getActiveHero()
	{
		if ( hero1.getCreatures()
		          .contains( activeCreature ) )
		{
			return hero1;
		}
		else
		{
			return hero2;
		}
	}

	void next()
	{
		notifyObservers( new PropertyChangeEvent( this, GameEngine.ACTION.PASS.name(), null, null ) );

		if ( creaturesQueue.isEmpty() )
		{
			initQueue();
			notifyObservers( new PropertyChangeEvent( this, GameEngine.END_OF_TURN, null, null ) );
		}

		activeCreature = creaturesQueue.poll();
	}

	private void notifyObservers( PropertyChangeEvent aPropertyChangeEvent )
	{
		observerSupport.firePropertyChange( aPropertyChangeEvent );
	}

	void removeFromQueue( Creature aCreature )
	{
		creaturesQueue.remove( aCreature );
		creatures.remove( aCreature );
		notifyObservers( new PropertyChangeEvent( this, CREATURE_KILLED, null, this ) );
	}

	boolean isHero1Turn()
	{
		return getActiveHero() == hero1;
	}
}
