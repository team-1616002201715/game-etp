/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class BuffContainer extends HashMap<StatusEffectSpell, Integer> implements PropertyChangeListener
{
	Consumer<EffectStats> buffCreatureConsumer;

	public BuffContainer( Consumer<EffectStats> aConsumer )
	{
		buffCreatureConsumer = aConsumer;
	}

	public List<EffectStats> getAllBuffStats()
	{
		return keySet().stream()
		               .map( StatusEffectSpell::getStatusStats )
		               .collect( Collectors.toList() );
	}

	public void add( StatusEffectSpell aBuff )
	{
		boolean shouldBuff = !containsKey( aBuff );
		if ( shouldBuff )
		{
			put( aBuff, aBuff.getDuration() );
			buffCreatureConsumer.accept( aBuff.getStatusStats() );
		}
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		final List<StatusEffectSpell> toRemove = new ArrayList<>();
		keySet().forEach( buff ->
		{
			if ( !get( buff ).equals( 0 ) )
			{
				put( buff, get( buff ) - 1 );
			}
			else
			{
				toRemove.add( buff );
			}
		} );
		toRemove.forEach( b ->
		{
			remove( b );
			buffCreatureConsumer.accept( b.getStatusStats()
			                              .reverse() );
		} );
	}
}
