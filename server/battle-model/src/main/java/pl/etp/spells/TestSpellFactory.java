/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import pl.etp.creatures.spells.SpellStatistic;

public class TestSpellFactory
{
	public static AbstractSpell createMagicArrow()
	{
		return new DamageSpell( 5, SpellStatistic.TargetType.ENEMY, SpellStatistic.SpellElement.AIR, 10, 0, "Tests Spell Magic Arrow" );
	}

	public static AbstractSpell createMagicArrow( SpellStatistic.TargetType aTargetType )
	{
		return new DamageSpell( 20, aTargetType, SpellStatistic.SpellElement.AIR, 10, 0, "Tests Spell Magic Arrow" );
	}

	public static AbstractSpell createMagicArrowWithSplash( int aSplash )
	{
		return new DamageSpell( 5, SpellStatistic.TargetType.ENEMY, SpellStatistic.SpellElement.AIR, 10, aSplash, "Splash: " + aSplash );
	}

	public static AbstractSpell createMagicArrowWithSplashAndTargetType( int aSplash, SpellStatistic.TargetType aTargetType )
	{
		return new DamageSpell( 5, aTargetType, SpellStatistic.SpellElement.AIR, 10, aSplash, "Splash: " + aSplash + " targetType: " + aTargetType );
	}

	public static AbstractSpell createMagicArrow2ElectricBoogaloo()
	{
		return new DamageSpell( 1, SpellStatistic.TargetType.ENEMY, SpellStatistic.SpellElement.AIR, 1, 0, "Tests Spell Magic Arrow 2: Electric Boogaloo" );
	}

	public static AbstractSpell createBuff()
	{
		return new StatusEffectSpell( 2, EffectStats.builder()
		                                            .moveRange( 5 )
		                                            .build(), 5, SpellStatistic.TargetType.ALLY, SpellStatistic.SpellElement.AIR, "Test Move Buff " );
	}

	static AbstractSpell createHpBuff()
	{
		return new StatusEffectSpell( 3, EffectStats.builder()
		                                            .maxHp( 5 )
		                                            .build(), 5, SpellStatistic.TargetType.ALLY, SpellStatistic.SpellElement.AIR, "Tes Hp Buff" );
	}
}
