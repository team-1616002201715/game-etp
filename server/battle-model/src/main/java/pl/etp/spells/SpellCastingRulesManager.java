/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import pl.etp.GameEngine;
import pl.etp.board.BoardManager;
import pl.etp.board.Point;
import pl.etp.creatures.spells.SpellStatistic;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SpellCastingRulesManager
{
	public Set<Point> calculateCastTargetPoints( AbstractSpell aSpell, Point aTargetPoint, BoardManager aBoardManager, Function<Point, Boolean> alliesFunction )
	{
		Set<Point> pointHashSet = new HashSet<>();
		if ( isTileTargetSpell( aTargetPoint ) )
		{
			int splash = aSpell.getSplashRange();
			for ( int x = aTargetPoint.getX() - splash; x <= aTargetPoint.getX() + splash; x++ )
			{
				for ( int y = aTargetPoint.getY() - splash; y <= aTargetPoint.getY() + splash; y++ )
				{
					pointHashSet.add( aBoardManager.getPoint( x, y ) );
				}
			}

			pointHashSet = pointHashSet.stream()
			                           .filter( p -> aBoardManager.getCreatureByPoint( p ) != null )
			                           .collect( Collectors.toSet() );

			if ( shouldCastOnlyForAllyCreatures( aSpell ) )
			{
				pointHashSet = pointHashSet.stream()
				                           .filter( p -> !alliesFunction.apply( p ) )
				                           .collect( Collectors.toSet() );
			}

			if ( shouldCastOnlyForEnemyCreatures( aSpell ) )
			{
				pointHashSet = pointHashSet.stream()
				                           .filter( alliesFunction::apply )
				                           .collect( Collectors.toSet() );
			}
		}

		return pointHashSet;
	}

	public boolean canCast( AbstractSpell aSpell, Point aPoint, GameEngine aGameEngine )
	{
		if ( shouldCastOnlyForEnemyCreatures( aSpell ) )
		{
			return aGameEngine.isEnemyCreature( aPoint );
		}
		if ( shouldCastOnlyForAllyCreatures( aSpell ) )
		{
			return aGameEngine.isAllyCreature( aPoint );
		}

		return false;
	}

	private boolean shouldCastOnlyForEnemyCreatures( AbstractSpell aSpell )
	{
		return aSpell.getTargetType() == SpellStatistic.TargetType.ENEMY || aSpell.getTargetType() == SpellStatistic.TargetType.ALL_ENEMIES;
	}

	private boolean shouldCastOnlyForAllyCreatures( AbstractSpell aSpell )
	{
		return aSpell.getTargetType() == SpellStatistic.TargetType.ALLY || aSpell.getTargetType() == SpellStatistic.TargetType.ALL_ALLIES;
	}

	private boolean isTileTargetSpell( Point aTargetPoint )
	{
		return aTargetPoint != null;
	}
}
