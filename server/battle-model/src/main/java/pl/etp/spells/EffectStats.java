/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import com.google.common.collect.Range;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class EffectStats
{
	private final int moveRange;
	private final int armor;
	private final int maxHp;
	private Range<Integer> damage;
	private final double attackRange;
	private final int spellDuration;
	private final int spellDamage;

	private final double moveRangePercentage;
	private final double attackPercentage;
	private final double armorPercentage;
	private final double maxHpPercentage;
	private final double damagePercentage;
	private final double maxAmountPercentage;
	private final double magicResistancePercentage;

	EffectStats( int aMoveRange, int aArmor, int aMaxHp, Range<Integer> aDamage, double aAttackRange, int aSpellDuration, int aSpellDamage, double aMoveRangePercentage, double aAttackPercentage, double aArmorPercentage, double aMaxHpPercentage, double aDamagePercentage, double aMaxAmountPercentage, double aMagicResistancePercentage )
	{
		moveRange = aMoveRange;
		armor = aArmor;
		maxHp = aMaxHp;
		spellDuration = aSpellDuration;
		spellDamage = aSpellDamage;
		if ( damage == null )
		{
			damage = Range.closed( 0, 0 );
		}
		else
		{
			damage = aDamage;
		}
		attackRange = aAttackRange;
		moveRangePercentage = aMoveRangePercentage;
		attackPercentage = aAttackPercentage;
		armorPercentage = aArmorPercentage;
		maxHpPercentage = aMaxHpPercentage;
		damagePercentage = aDamagePercentage;
		maxAmountPercentage = aMaxAmountPercentage;
		magicResistancePercentage = aMagicResistancePercentage;
	}

	public EffectStats reverse()
	{
		return EffectStats.builder()
		                  .moveRange( -moveRange )
		                  .armor( -armor )
		                  .maxHp( -maxHp )
		                  .damage( Range.closed( -damage.lowerEndpoint(), -damage.upperEndpoint() ) )
		                  .attackRange( -attackRange )
		                  .moveRangePercentage( -moveRangePercentage )
		                  .attackPercentage( -attackPercentage )
		                  .armorPercentage( -armorPercentage )
		                  .maxHpPercentage( -maxHpPercentage )
		                  .damagePercentage( -damagePercentage )
		                  .magicResistancePercentage( -magicResistancePercentage )
		                  .spellDamage( -spellDamage )
		                  .spellDuration( -spellDuration )
		                  .build();
	}

	public int getUpperEndpoint()
	{
		return damage.upperEndpoint();
	}

	public int getLowerEndpoint()
	{
		return damage.lowerEndpoint();
	}
}
