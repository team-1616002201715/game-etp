/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class SpellBook implements PropertyChangeListener
{
	private final ManaPool manaPool;
	private final List<AbstractSpell> spells;
	private boolean spellWasCastedInThisTurn;
	private final String SPELL_EXCEPTION = "You cannot cast more spells in this turn!";

	public SpellBook( int aMana, List<AbstractSpell> aSpells )
	{
		manaPool = new ManaPool( aMana, aMana );
		spells = aSpells;
	}

	public List<AbstractSpell> getSpells()
	{
		return spells;
	}

	public void cast( AbstractSpell aSpell )
	{
		if ( preconditions( aSpell ) )
		{
			return;
		}
		spellWasCastedInThisTurn = true;
		manaPool.setCurrentMana( getCurrentMana() - aSpell.getManaCost() );
	}

	private boolean preconditions( AbstractSpell aSpell )
	{
		if ( !canCastSpellInThisTurn( aSpell ) )
		{
			throw new IllegalStateException( SPELL_EXCEPTION );
		}
		return !spells.contains( aSpell );
	}

	public boolean canCastSpellInThisTurn()
	{
		return !spellWasCastedInThisTurn;
	}

	public boolean canCastSpellInThisTurn( AbstractSpell aSpell )
	{
		return getCurrentMana() >= aSpell.getManaCost() && canCastSpellInThisTurn();
	}

	void endOfTurn()
	{
		spellWasCastedInThisTurn = false;
	}

	@Override
	public void propertyChange( PropertyChangeEvent aPropertyChangeEvent )
	{
		endOfTurn();
	}

	public int getCurrentMana()
	{
		return manaPool.getCurrentMana();
	}

	public int getMaxMana()
	{
		return manaPool.getMaxMana();
	}

	public List<AbstractSpell> getCurrentlyAvailableSpells()
	{
		List<AbstractSpell> availableSpells = new ArrayList<>();
		spells.forEach( s ->
		{
			if ( s.manaCost < getCurrentMana() )
			{
				availableSpells.add( s );
			}
		} );
		return availableSpells;
	}

	public ManaPool getManaPool()
	{
		return manaPool;
	}
}
