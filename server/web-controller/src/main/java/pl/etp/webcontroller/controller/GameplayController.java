/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import pl.etp.webcontroller.controller.dto.MoveOrAttackOnTileRequest;
import pl.etp.webcontroller.controller.dto.SpellOnTileRequest;
import pl.etp.webcontroller.service.GameService;
import pl.etp.webcontroller.storage.GameStorage;

@Controller
@AllArgsConstructor
@RequestMapping ( "/game" )
@CrossOrigin ( origins = "*" )
@Tag ( name = "gameplay-controller", description = "API for gameplay management" )
public class GameplayController
{
	private final Logger logger = LogManager.getLogger( GameplayController.class );
	private final GameService gameService;
	private final SimpMessagingTemplate simpMessagingTemplate;
	private final GameStorage gameStorage;

	@PostMapping ( "/{gameId}/move" )
	@ApiOperation ( value = "Moves currently active creature to provided location", notes = "The location must be within active creature's move range in a form of Tile coordinates" )
	public ResponseEntity<?> moveUnit( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @RequestBody MoveOrAttackOnTileRequest aActionOnTileRequest )
	{
		logger.info( "move request to tile: {}", aActionOnTileRequest.toString() );
		try
		{
			gameService.moveUnit( aActionOnTileRequest, aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		return ResponseEntity.ok()
		                     .build();
	}

	@PostMapping ( "/{gameId}/attack" )
	@ApiOperation ( value = "Performs an attack by the currently active creature on the given tile", notes = "The location must be within active creature's attack range in a form of Tile coordinates" )
	public ResponseEntity<?> attackUnit( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @RequestBody MoveOrAttackOnTileRequest aActionOnTileRequest )
	{
		logger.info( "attack request on tile: " + aActionOnTileRequest.toString() );
		try
		{
			gameService.attackUnit( aActionOnTileRequest, aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		return ResponseEntity.ok()
		                     .build();
	}

	@PostMapping ( "/{gameId}/pass" )
	@ApiOperation ( value = "Informs the game engine that the currently active player has ended its turn" )
	public ResponseEntity<?> pass( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId )
	{
		logger.info( "Player passed" );
		try
		{
			gameService.pass( aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		return ResponseEntity.ok()
		                     .build();
	}

	@PostMapping ( "/{gameId}/cast" )
	@ApiOperation ( value = "Casts a spell selected by the player on the given tile" )
	public ResponseEntity<?> cast( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @RequestBody SpellOnTileRequest aActionOnTileRequest )
	{
		logger.info( "cast request: " + aActionOnTileRequest.toString() );
		try
		{
			gameService.cast( aActionOnTileRequest, aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		return ResponseEntity.ok()
		                     .build();
	}

	@PostMapping ( "/{gameId}/castRange" )
	@ApiOperation ( value = "Asks the game engine for the range of the selected spell" )
	public ResponseEntity<?> castRange( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @RequestBody String aSpellName )
	{
		logger.info( "Client requests range for: " + aSpellName );
		try
		{
			gameService.castRange( aSpellName, aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		return ResponseEntity.ok()
		                     .build();
	}

}
