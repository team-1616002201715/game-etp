/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import pl.etp.EcoController;
import pl.etp.GameEngine;

@Data
@ApiModel ( description = "Details about the game" )
public class Game
{
	@ApiModelProperty ( notes = "Unique ID of the game" )
	private String gameId;
	@ApiModelProperty ( notes = "ID of the player in the player1 slot" )
	private Player player1;
	@ApiModelProperty ( notes = "ID of the player in the player2 slot" )
	private Player player2;
	@ApiModelProperty ( notes = "EcoController assigned to the game" )
	private EcoController ecoController;
	@ApiModelProperty ( notes = "gameEngine assigned to the game" )
	private GameEngine gameEngine;
	@ApiModelProperty ( notes = "Current game status" )
	private GameStatus status;
}
