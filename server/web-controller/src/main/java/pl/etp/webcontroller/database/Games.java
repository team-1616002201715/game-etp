package pl.etp.webcontroller.database;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table ( name = "Games" )
public class Games
{

	@Id
	@Column ( unique = true )
	private String id;

	@CreationTimestamp
	@Column ( nullable = false )
	private java.sql.Timestamp startTime;

	@CreationTimestamp
	@Column ( nullable = false )
	private java.sql.Timestamp endTime;


	public Games()
	{
	}

	public Games( String id, Timestamp startTime, Timestamp endTime )
	{
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Games( Timestamp startTime, Timestamp endTime )
	{
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public Games( String id )
	{
		this.id = id;
	}

	public String getId()
	{
		return id;
	}

	public void setId( String id )
	{
		this.id = id;
	}

	public Timestamp getStartTime()
	{
		return startTime;
	}

	public void setStartTime( Timestamp startTime )
	{
		this.startTime = startTime;
	}

	public Timestamp getEndTime()
	{
		return endTime;
	}

	public void setEndTime( Timestamp endTime )
	{
		this.endTime = endTime;
	}
}
