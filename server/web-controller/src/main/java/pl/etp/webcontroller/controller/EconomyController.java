/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import pl.etp.webcontroller.controller.dto.BuyOrSellRequest;
import pl.etp.webcontroller.service.GameService;
import pl.etp.webcontroller.storage.GameStorage;

@Controller
@AllArgsConstructor
@RequestMapping ( "/economy" )
@CrossOrigin ( origins = "*" )
@Tag ( name = "economy-controller", description = "API for economy phase management" )
public class EconomyController
{
	private final Logger logger = LogManager.getLogger( GameplayController.class );
	private final GameService gameService;
	private final SimpMessagingTemplate simpMessagingTemplate;
	private final GameStorage gameStorage;

	@PostMapping ( "/{gameId}/startEconomy" )
	@ApiOperation ( value = "Begins the economy phase", notes = "The lobby must have 2 players connected and ready" )
	public ResponseEntity<?> startEconomy( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId )
	{
		logger.info( "starting economy phase in game: {}", aGameId );
		try
		{
			return gameService.startEconomy( aGameId );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/pickFraction/{playerSeat}" )
	@ApiOperation ( value = "Sets players fraction and broadcasts available heroes to pick" )
	public ResponseEntity<?> pickFraction( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat, @RequestBody String aFractionName )
	{
		logger.info( "picking fraction {} for player {} in game {}", aFractionName, aPlayerSeat, aGameId );
		try
		{
			return gameService.pickFraction( aGameId, aPlayerSeat, aFractionName );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/pickHero/{playerSeat}" )
	@ApiOperation ( value = "Sets players hero and enters the shopping phase" )
	public ResponseEntity<?> pickHero( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat, @RequestBody String aHeroName )
	{
		logger.info( "picking hero {} for player {} in game {}", aHeroName, aPlayerSeat, aGameId );
		try
		{
			return gameService.pickHero( aGameId, aPlayerSeat, aHeroName );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/buyItem/{playerSeat}" )
	@ApiOperation ( value = "Buys selected item for the player" )
	public ResponseEntity<?> buyItem( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat, @RequestBody BuyOrSellRequest aBuyRequest )
	{
		logger.info( "processing request for purchase of {}", aBuyRequest.toString() );
		try
		{
			return gameService.buyItem( aGameId, aPlayerSeat, aBuyRequest );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/sellItem/{playerSeat}" )
	@ApiOperation ( value = "Sells selected item for the player" )
	public ResponseEntity<?> sellItem( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat, @RequestBody BuyOrSellRequest aBuyRequest )
	{
		logger.info( "processing request for purchase of {}", aBuyRequest.toString() );
		try
		{
			return gameService.sellItem( aGameId, aPlayerSeat, aBuyRequest );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/buyMaxCreature/{playerSeat}" )
	@ApiOperation ( value = "Buys maximum amount of selected creature for the player" )
	public ResponseEntity<?> buyMaxCreatures( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat, @RequestBody String aCreatureName )
	{
		logger.info( "processing request for maximum purchase of {}", aCreatureName );
		try
		{
			return gameService.buyMaxCreatures( aGameId, aPlayerSeat, aCreatureName );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
	}

	@PostMapping ( "/{gameId}/readyForBattle/{playerSeat}" )
	@ApiOperation ( value = "Sets player as ready for battle in the ecoPhase", notes = "The battle will now start if both players are ready" )
	public ResponseEntity<?> readyForBattle( @ApiParam ( value = "ID value for the game lobby in which the action request occured", required = true, format = "UUID received after creating the game lobby" ) @PathVariable ( "gameId" ) String aGameId, @ApiParam ( value = "Player seat", required = true ) @PathVariable ( value = "playerSeat" ) String aPlayerSeat )
	{
		logger.info( "Player {} is now ready", aPlayerSeat );
		try
		{
			simpMessagingTemplate.convertAndSend( "/topic/readyForBattle/" + aGameId, gameService.readyForBattle( aGameId, aPlayerSeat ) );
		}
		catch ( HttpStatusCodeException aException )
		{
			return ResponseEntity.status( aException.getStatusCode() )
			                     .build();
		}
		gameService.checkForBattleStart( aGameId );
		return ResponseEntity.ok()
		                     .build();
	}

}
