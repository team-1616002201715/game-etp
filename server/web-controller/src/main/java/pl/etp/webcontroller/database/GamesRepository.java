package pl.etp.webcontroller.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GamesRepository extends JpaRepository<Games, String>
{
}
