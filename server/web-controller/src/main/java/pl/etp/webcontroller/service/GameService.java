/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.service;

import fields.FieldType;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import pl.etp.EcoController;
import pl.etp.GameEngine;
import pl.etp.GameEventsService;
import pl.etp.Hero;
import pl.etp.board.BoardManager;
import pl.etp.board.TileIf;
import pl.etp.board.fields.FieldFactory;
import pl.etp.converter.EcoBattleConverter;
import pl.etp.creatures.Creature;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;
import pl.etp.webcontroller.controller.dto.AbstractActionOnTileRequest;
import pl.etp.webcontroller.controller.dto.BuyOrSellRequest;
import pl.etp.webcontroller.controller.dto.SpellOnTileRequest;
import pl.etp.webcontroller.model.Game;
import pl.etp.webcontroller.model.Player;
import pl.etp.webcontroller.storage.GameStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static pl.etp.webcontroller.model.GameStatus.*;

@Service
@AllArgsConstructor
public class GameService
{
	private final String GAME_NOT_STARTED_YET = "Status of the Game with the given ID is not IN_PROGRESS";
	private final String GAME_IS_NOT_OPEN = "Status of the Game with the given ID is not OPEN";
	private final String GAME_IS_FULL = "The Game with the given ID is already full";
	private final String INVALID_PLAYER_SEAT = "Invalid player seat indicated";

	private final Logger logger = LogManager.getLogger( GameService.class );
	private final GameStorage gameStorage = GameStorage.getInstance();


	public Game createGame( Player aPlayer )
	{
		Game game = new Game();

		EcoController ecoController = new EcoController();

		game.setGameId( ecoController.getGameId() );
		game.setEcoController( ecoController );
		game.setPlayer1( aPlayer );
		game.setStatus( OPEN );
		gameStorage.setGame( game );
		return game;
	}

	public String connect( Player aPlayer2, String aGameId )
	{
		Game game = getGameById( aGameId );
		try
		{
			if ( game.getPlayer2() != null )
			{
				throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
			}
			else
			{
				game.setPlayer2( aPlayer2 );
			}
		}
		catch ( HttpClientErrorException aE )
		{
			logger.error( GAME_IS_FULL );
			throw new HttpClientErrorException( HttpStatus.NOT_FOUND );
		}
		gameStorage.setGame( game );
		return game.getGameId();
	}

	public String connectToRandomGame( Player aPlayer2 )
	{
		Game game;
		try
		{
			game = gameStorage.getGames()
			                  .values()
			                  .stream()
			                  .filter( it -> it.getStatus()
			                                   .equals( OPEN ) )
			                  .findFirst()
			                  .orElseThrow( () -> new HttpClientErrorException( HttpStatus.NOT_FOUND ) );
		}
		catch ( HttpClientErrorException aE )
		{
			logger.error( "Could not find any OPEN game room." );
			throw new HttpClientErrorException( HttpStatus.NOT_FOUND );
		}
		game.setPlayer2( aPlayer2 );
		gameStorage.setGame( game );
		return game.getGameId();
	}

	public void moveUnit( AbstractActionOnTileRequest aActionOnTileRequest, String aGameId )
	{
		Game game = getGameById( aGameId );

		getGameEngine( game ).move( aActionOnTileRequest.getTargetTile() );
	}

	public void attackUnit( AbstractActionOnTileRequest aActionOnTileRequest, String aGameId )
	{
		Game game = getGameById( aGameId );

		getGameEngine( game ).attack( aActionOnTileRequest.getTargetTile() );
	}

	public void pass( String aGameId )
	{
		Game game = getGameById( aGameId );

		getGameEngine( game ).pass();
	}

	public void cast( SpellOnTileRequest aActionOnTileRequest, String aGameId )
	{
		Game game = getGameById( aGameId );
		GameEngine gameEngine = getGameEngine( game );

		gameEngine.castSpell( gameEngine.getSpellFromAvailableSpells( aActionOnTileRequest.getSpellName() ), aActionOnTileRequest.getTargetTile() );
	}

	public ResponseEntity<?> startEconomy( String aGameId )
	{
		Game game = getGameById( aGameId );
		if ( game.getStatus() == OPEN )
		{
			game.setStatus( IN_PROGRESS );
			game.getEcoController()
			    .provideFractionInformation();
			return ResponseEntity.ok()
			                     .build();
		}
		else
		{
			logger.error( GAME_IS_NOT_OPEN );
			throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
		}
	}

	public ResponseEntity<?> startBattle( String aGameId )
	{
		Game game = getGameById( aGameId );
		pl.etp.player.Player player1 = game.getEcoController()
		                                   .getPlayer1economyEngine()
		                                   .getPlayer();
		pl.etp.player.Player player2 = game.getEcoController()
		                                   .getPlayer2economyEngine()
		                                   .getPlayer();
		GameEventsService.broadcastPlayersSetup( aGameId, "1", getPlayerCreatures( player1 ), getPlayerArtifacts( player1 ), getPlayerSpells( player1 ) );
		GameEventsService.broadcastPlayersSetup( aGameId, "2", getPlayerCreatures( player2 ), getPlayerArtifacts( player2 ), getPlayerSpells( player2 ) );
		BoardManager sampleBoard = new BoardManager();
		sampleBoard.createSample();
		GameEngine gameEngine = new GameEngine( ( EcoBattleConverter.convert( player1 ) ), ( EcoBattleConverter.convert( player2 ) ), sampleBoard, aGameId );
		game.setGameEngine( gameEngine );
		if ( game.getStatus() == IN_PROGRESS )
		{
			getGameEngine( game ).refreshClientInfo();
			return ResponseEntity.ok()
			                     .build();
		}
		else
		{
			logger.error( GAME_NOT_STARTED_YET );
			throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
		}
	}

	private List<String> getPlayerSpells( pl.etp.player.Player aPlayer )
	{
		return aPlayer.getSpells()
		              .stream()
		              .map( EconomySpell::getName )
		              .collect( Collectors.toList() );
	}

	private List<String> getPlayerArtifacts( pl.etp.player.Player aPlayer )
	{
		return aPlayer.getArtifacts()
		              .stream()
		              .map( EconomyArtifact::getName )
		              .collect( Collectors.toList() );
	}

	private List<String> getPlayerCreatures( pl.etp.player.Player aPlayer )
	{
		return aPlayer.getCreatures()
		              .stream()
		              .map( EconomyCreature::getName )
		              .collect( Collectors.toList() );
	}

	public ResponseEntity<?> concludeGame( String aGameId )
	{
		Game game = getGameById( aGameId );
		if ( game.getStatus() == IN_PROGRESS )
		{
			game.setStatus( FINISHED );
			getGameEngine( game ).refreshClientInfo();
			return ResponseEntity.ok()
			                     .build();
		}
		else
		{
			logger.error( GAME_NOT_STARTED_YET );
			throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
		}
	}

	public ResponseEntity<?> rematchGame( String aGameId )
	{
		Game game = getGameById( aGameId );
		if ( game.getStatus() == FINISHED )
		{
			game.setStatus( OPEN );
			game.getPlayer1()
			    .setReadyForBattleStatus( false );
			game.getPlayer2()
			    .setReadyForBattleStatus( false );
			startEconomy( aGameId );
			return ResponseEntity.ok()
			                     .build();
		}
		else
		{
			logger.error( GAME_NOT_STARTED_YET );
			throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
		}
	}

	public ResponseEntity<?> pickFraction( String aGameId, String aPlayerSeat, String aFractionName )
	{
		Game game = getGameById( aGameId );
		EcoController ecoController = game.getEcoController();
		try
		{
			ecoController.pickFraction( aPlayerSeat, aFractionName );
			ecoController.provideAvailableHeroes( aPlayerSeat );
			return ResponseEntity.ok()
			                     .build();
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
	}

	public ResponseEntity<?> pickHero( String aGameId, String aPlayerSeat, String aHeroName )
	{
		Game game = getGameById( aGameId );
		EcoController ecoController = game.getEcoController();
		try
		{
			ecoController.pickHero( aPlayerSeat, aHeroName );
			return ResponseEntity.ok()
			                     .build();
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
	}

	public ResponseEntity<?> buyItem( String aGameId, String aPlayerSeat, BuyOrSellRequest aBuyRequest )
	{
		Game game = getGameById( aGameId );
		EcoController ecoController = game.getEcoController();
		try
		{
			ecoController.buyItem( aPlayerSeat, aBuyRequest.getITEM_TYPE(), aBuyRequest.getItemName(), aBuyRequest.getAmount() );
			return ResponseEntity.ok()
			                     .build();
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
		catch ( IllegalStateException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.CONFLICT );
		}
	}

	public ResponseEntity<?> sellItem( String aGameId, String aPlayerSeat, BuyOrSellRequest aBuyRequest )
	{
		Game game = getGameById( aGameId );
		EcoController ecoController = game.getEcoController();
		try
		{
			ecoController.sellItem( aPlayerSeat, aBuyRequest.getITEM_TYPE(), aBuyRequest.getItemName(), aBuyRequest.getAmount() );
			return ResponseEntity.ok()
			                     .build();
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
		catch ( IllegalStateException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.CONFLICT );
		}
	}

	public ResponseEntity<?> buyMaxCreatures( String aGameId, String aPlayerSeat, String aCreatureName )
	{
		Game game = getGameById( aGameId );
		EcoController ecoController = game.getEcoController();
		try
		{
			ecoController.buyMaxCreatures( aPlayerSeat, aCreatureName );
			return ResponseEntity.ok()
			                     .build();
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
		catch ( IllegalStateException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.CONFLICT );
		}
	}

	public void castRange( String aSpellName, String aGameId )
	{
		Game game = getGameById( aGameId );
		GameEngine gameEngine = getGameEngine( game );

		gameEngine.castRange( gameEngine.getSpellFromAvailableSpells( aSpellName ) );
	}

	public Object readyForEco( String aGameId, String aPlayerSeat )
	{
		Game game = getGameById( aGameId );
		try
		{
			if ( aPlayerSeat.equals( "1" ) )
			{
				game.getPlayer1()
				    .setReadyForEcoStatus( true );
			}
			else if ( aPlayerSeat.equals( "2" ) )
			{
				game.getPlayer2()
				    .setReadyForEcoStatus( true );
			}
			else
			{
				throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
			}
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
		return List.of( game.getPlayer1()
		                    .toString(), game.getPlayer2()
		                                     .toString() );

	}

	public Object readyForBattle( String aGameId, String aPlayerSeat )
	{
		Game game = getGameById( aGameId );
		try
		{
			if ( aPlayerSeat.equals( "1" ) )
			{
				game.getPlayer1()
				    .setReadyForBattleStatus( true );
			}
			else if ( aPlayerSeat.equals( "2" ) )
			{
				game.getPlayer2()
				    .setReadyForBattleStatus( true );
			}
			else
			{
				throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
			}
		}
		catch ( IllegalArgumentException aE )
		{
			logger.error( aE.getMessage() );
			throw new HttpClientErrorException( HttpStatus.BAD_REQUEST );
		}
		return List.of( game.getPlayer1()
		                    .toString(), game.getPlayer2()
		                                     .toString() );

	}

	private Game getGameById( String aGameId )
	{
		Game game;
		game = gameStorage.getGames()
		                  .get( aGameId );
		if ( game == null )
		{
			logger.error( "Could not find a game with a given ID." );
			throw new HttpClientErrorException( HttpStatus.NOT_FOUND );
		}
		return game;
	}

	private GameEngine getGameEngine( Game aGame )
	{
		if ( aGame.getGameEngine() == null )
		{
			logger.error( "GameEngine is not initialized correctly." );
			throw new HttpServerErrorException( HttpStatus.INTERNAL_SERVER_ERROR );
		}
		else
		{
			return aGame.getGameEngine();
		}
	}

	public Game createTestBattle( Player aPlayer )
	{
		Game game = new Game();
		// ultimately economy phase should happen before creating game engine

		Creature unit1 = CreatureFactory.create( CreatureStatistic.TEST_FLYING_CREATURE, 1 );
		Creature unit2 = CreatureFactory.create( CreatureStatistic.TEST_100_HP_CREATURE, 1 );
		Creature unit3 = CreatureFactory.create( CreatureStatistic.TEST_10_HP_CREATURE, 1 );
		Creature unit4 = CreatureFactory.create( CreatureStatistic.TEST_TELEPORT_CREATURE, 1 );
		Creature unit5 = CreatureFactory.create( CreatureStatistic.TEST_MOVING_TWO_TIMES, 1 );

		Creature unit6 = CreatureFactory.create( CreatureStatistic.TEST_BLOCK_COUNTER, 1 );
		Creature unit7 = CreatureFactory.create( CreatureStatistic.TEST_ATTACK_TWO_TIMES, 1 );
		Creature unit8 = CreatureFactory.create( CreatureStatistic.TEST_GOLDEN_SHOT_CREATURE, 1 );
		Creature unit9 = CreatureFactory.create( CreatureStatistic.TEST_HEAL_AFTER_GET_ATTACK, 1 );
		Creature unit10 = CreatureFactory.create( CreatureStatistic.TEST_100_HP_CREATURE, 1 );

		GameEngine gameEngine = new GameEngine( new Hero( new ArrayList<>( List.of( unit1, unit2, unit3, unit4, unit5 ) ) ), new Hero( new ArrayList<>( List.of( unit6, unit7, unit8, unit9, unit10 ) ) ) );

		BoardManager boardManager = gameEngine.getBoardManager();
		TileIf wallField = FieldFactory.create( FieldType.WALL );
		TileIf ballonField = FieldFactory.create( FieldType.MAGIC_BALLOON );
		TileIf swamp = FieldFactory.create( FieldType.SWAMP );
		TileIf gasGeyser = FieldFactory.create( FieldType.GAS_GEYSER );
		TileIf lava = FieldFactory.create( FieldType.LAVA );
		boardManager.putOnBoard( boardManager.getPoint( 3, 4 ), wallField );
		boardManager.putOnBoard( boardManager.getPoint( 3, 5 ), wallField );
		boardManager.putOnBoard( boardManager.getPoint( 3, 6 ), ballonField );
		boardManager.putOnBoard( boardManager.getPoint( 3, 7 ), ballonField );

		boardManager.putOnBoard( boardManager.getPoint( 14, 5 ), swamp );
		boardManager.putOnBoard( boardManager.getPoint( 14, 6 ), gasGeyser );
		boardManager.putOnBoard( boardManager.getPoint( 14, 7 ), lava );


		game.setGameId( gameEngine.getGameId() );
		game.setGameEngine( gameEngine );
		game.setPlayer1( aPlayer );
		game.setStatus( OPEN );
		gameStorage.setGame( game );
		return game;
	}

	public ResponseEntity<?> startTestBattle( String aGameId )
	{
		Game game = getGameById( aGameId );
		if ( game.getStatus() == OPEN )
		{
			game.setStatus( IN_PROGRESS );
			getGameEngine( game ).refreshClientInfo();
			return ResponseEntity.ok()
			                     .build();
		}
		else
		{
			logger.error( GAME_IS_NOT_OPEN );
			throw new HttpClientErrorException( HttpStatus.FORBIDDEN );
		}
	}

	public void checkForBattleStart( String aGameId )
	{
		Game game = getGameById( aGameId );
		if ( game.getPlayer1().readyForBattleStatus && game.getPlayer2().readyForBattleStatus )
		{
			startBattle( aGameId );
		}
	}
}
