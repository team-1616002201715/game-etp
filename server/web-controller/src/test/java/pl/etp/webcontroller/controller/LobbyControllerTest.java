/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.controller;

import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.HttpClientErrorException;
import pl.etp.webcontroller.MockTestGameService;
import pl.etp.webcontroller.model.GameStatus;
import pl.etp.webcontroller.model.Player;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest ( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
@AutoConfigureMockMvc
class LobbyControllerTest
{
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private MockTestGameService service;

	private String gameId;

	@AfterEach
	void cleanup()
	{
		service.cleanUp();
	}

	@Test
	void shouldCreateGameRoom() throws Exception
	{
		Player player = new Player();
		Gson gson = new Gson();

		player.setId( "testID" );
		String json = gson.toJson( player );

		MvcResult result = mockMvc.perform( post( "/lobby/create" ).contentType( MediaType.APPLICATION_JSON )
		                                                           .content( json ) )
		                          .andDo( print() )
		                          .andExpect( status().isOk() )
		                          .andReturn();

		gameId = result.getResponse()
		               .getContentAsString();

		assertNotEquals( null, service.getGameById( gameId ) );
	}

	@Test
	void shouldConnectToRandomGameRoom() throws Exception
	{
		prepareGameRoom();

		Player player = new Player();
		Gson gson = new Gson();

		player.setId( "player2" );
		String json = gson.toJson( player );

		mockMvc.perform( post( "/lobby/connect/random" ).contentType( MediaType.APPLICATION_JSON )
		                                                .content( json ) )
		       .andDo( print() )
		       .andExpect( status().isOk() )
		       .andReturn();

		assertEquals( player, service.getGameById( gameId )
		                             .getPlayer2() );
	}

	@Test
	void shouldReturn400StatusWhenBadRequest() throws Exception
	{
		Gson gson = new Gson();

		String json = gson.toJson( "blablal" );
		mockMvc.perform( post( "/lobby/connect/random" ).contentType( MediaType.APPLICATION_JSON )
		                                                .content( json ) )
		       .andDo( print() )
		       .andExpect( status().isBadRequest() )
		       .andReturn();
	}

	@Test
	void shouldReturn404StatusWhenNoGameIdPassed() throws Exception
	{
		mockMvc.perform( post( "/lobby//start" ) )
		       .andDo( print() )
		       .andExpect( status().isNotFound() )
		       .andReturn();
	}

	@Test
	void shouldReturn404StatusWhenWrongGameIdPassed() throws Exception
	{
		mockMvc.perform( post( "/lobby/thisisntavalidgameid/start" ) )
		       .andDo( print() )
		       .andExpect( status().isNotFound() )
		       .andReturn();
	}

	@Test
	void shouldReturn404StatusWhenNoOpenRoomIsAvailable() throws Exception
	{
		Player player = new Player();
		Gson gson = new Gson();

		player.setId( "player2" );
		String json = gson.toJson( player );

		mockMvc.perform( post( "/lobby/connect/random" ).contentType( MediaType.APPLICATION_JSON )
		                                                .content( json ) )
		       .andDo( print() )
		       .andExpect( status().isNotFound() )
		       .andReturn();
	}

	// FIXME: ETP-368 needs refactoring after inclusion of economy phase in the default start method
	@Disabled
	@Test
	void shouldStartGame() throws Exception
	{
		prepareGameRoom();

		MvcResult response = mockMvc.perform( post( "/lobby/" + gameId + "/start" ) )
		                            .andDo( print() )
		                            .andExpect( status().isOk() )
		                            .andReturn();
		assertEquals( ResponseEntity.ok()
		                            .build()
		                            .getStatusCodeValue(), response.getResponse()
		                                                           .getStatus() );
		assertEquals( GameStatus.IN_PROGRESS, service.getGameById( gameId )
		                                             .getStatus() );
	}

	@Test
	void shouldNotStartAlreadyStartedGameAndReturn403Status() throws Exception
	{
		prepareGameRoom();
		service.getGameById( gameId )
		       .setStatus( GameStatus.IN_PROGRESS );

		assertThrows( HttpClientErrorException.class, () -> service.startTestBattle( gameId ) );
	}

	@Test
	void shouldNotStartNonExistentGameAndReturn404Status() throws Exception
	{
		mockMvc.perform( post( "/lobby/" + "testGameId" + "/start" ) )
		       .andDo( print() )
		       .andExpect( status().isNotFound() )
		       .andReturn();
	}

	private void prepareGameRoom() throws Exception
	{
		Player player = new Player();
		Gson gson = new Gson();
		player.setId( "player1" );
		String json = gson.toJson( player );
		MvcResult result = mockMvc.perform( post( "/lobby/create" ).contentType( MediaType.APPLICATION_JSON )
		                                                           .content( json ) )
		                          .andDo( print() )
		                          .andExpect( status().isOk() )
		                          .andReturn();
		gameId = result.getResponse()
		               .getContentAsString();
	}
}