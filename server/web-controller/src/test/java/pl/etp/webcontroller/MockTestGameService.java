/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller;

import com.google.common.collect.Range;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import pl.etp.GameEngine;
import pl.etp.Hero;
import pl.etp.creatures.Creature;
import pl.etp.spells.SpellBook;
import pl.etp.webcontroller.controller.dto.AbstractActionOnTileRequest;
import pl.etp.webcontroller.model.Game;
import pl.etp.webcontroller.model.Player;
import pl.etp.webcontroller.service.GameService;
import pl.etp.webcontroller.storage.GameStorage;

import java.util.ArrayList;
import java.util.List;

import static pl.etp.creatures.moving.MovementType.GROUND;
import static pl.etp.spells.TestSpellFactory.createMagicArrow;
import static pl.etp.spells.TestSpellFactory.createMagicArrow2ElectricBoogaloo;
import static pl.etp.webcontroller.model.GameStatus.OPEN;

@Service
@AllArgsConstructor
public class MockTestGameService extends GameService
{
	private final GameStorage gameStorage = GameStorage.getInstance();

	public Game createGameWithSpells( Player aPlayer )
	{
		Game game = new Game();
		// ultimately economy phase should happen before creating game engine

		Creature unit1 = new Creature.Builder().damage( Range.closed( 3, 5 ) )
		                                       .moveRange( 7 )
		                                       .moveStrategy( GROUND )
		                                       .build();
		Creature unit2 = new Creature.Builder().damage( Range.closed( 2, 3 ) )
		                                       .moveRange( 10 )
		                                       .moveStrategy( GROUND )
		                                       .build();
		Creature unit3 = new Creature.Builder().damage( Range.closed( 3, 5 ) )
		                                       .moveRange( 10 )
		                                       .moveStrategy( GROUND )
		                                       .build();
		Creature unit4 = new Creature.Builder().damage( Range.closed( 2, 3 ) )
		                                       .moveRange( 10 )
		                                       .moveStrategy( GROUND )
		                                       .build();
		Creature unit5 = new Creature.Builder().damage( Range.closed( 3, 5 ) )
		                                       .moveRange( 10 )
		                                       .moveStrategy( GROUND )
		                                       .build();

		GameEngine gameEngine = new GameEngine( new Hero( new ArrayList<>( List.of( unit1, unit2, unit3 ) ), new SpellBook( 20, List.of( createMagicArrow(), createMagicArrow2ElectricBoogaloo() ) ) ), new Hero( new ArrayList<>( List.of( unit4, unit5 ) ), new SpellBook( 20, List.of( createMagicArrow(), createMagicArrow2ElectricBoogaloo() ) ) ) );

		game.setGameId( gameEngine.getGameId() );
		game.setGameEngine( gameEngine );
		game.setPlayer1( aPlayer );
		game.setStatus( OPEN );
		gameStorage.setGame( game );
		return game;
	}

	public Game getGameById( String aGameId )
	{
		Game game;
		try
		{
			game = gameStorage.getGames()
			                  .get( aGameId );
			if ( game == null )
			{
				throw new HttpServerErrorException( HttpStatus.NOT_FOUND );
			}
		}
		catch ( NullPointerException aException )
		{
			throw new HttpServerErrorException( HttpStatus.INTERNAL_SERVER_ERROR );
		}
		return game;
	}

	public void putUnitWith50HpAndNoArmorOnTile( AbstractActionOnTileRequest aActionOnTileRequest, String aGameId )
	{
		Game game = getGameById( aGameId );

		Creature creatureToPut = new Creature.Builder().armor( 0 )
		                                               .maxHp( 50 )
		                                               .amount( 1 )
		                                               .build();

		game.getGameEngine()
		    .getBoardManager()
		    .putOnBoard( aActionOnTileRequest.getTargetTile(), creatureToPut );

	}

	public void cleanUp()
	{
		gameStorage.getGames()
		           .clear();
	}
}
