/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.webcontroller.controller;

import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import pl.etp.GameEngine;
import pl.etp.board.BoardManager;
import pl.etp.webcontroller.MockTestGameService;
import pl.etp.webcontroller.controller.dto.AbstractActionOnTileRequest;
import pl.etp.webcontroller.controller.dto.MoveOrAttackOnTileRequest;
import pl.etp.webcontroller.model.Player;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// FIXME: ETP-368 needs refactoring after inclusion of economy phase in the default start method
@Disabled
@SpringBootTest ( webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT )
@AutoConfigureMockMvc
@TestPropertySource ( "classpath:application-test.properties" )
class GameplayControllerSupportTest
{
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private MockTestGameService service;

	BlockingQueue<String> blockingQueue;

	WebSocketStompClient stompClient;

	private String gameId;

	private final Gson gson = new Gson();

	private final DefaultStompFrameHandler defaultStompFrameHandler = new DefaultStompFrameHandler()
	{
	};

	final String WEBSOCKET_URI = "ws://localhost:6789/gameplay";

	class DefaultStompFrameHandler extends StompSessionHandlerAdapter
	{
		@Override
		public Type getPayloadType( StompHeaders stompHeaders )
		{
			return byte[].class;
		}

		@Override
		public void handleFrame( StompHeaders stompHeaders, Object o )
		{
			blockingQueue.offer( new String( ( byte[] ) o ) );
		}

	}

	@BeforeEach
	void setUp() throws Exception
	{
		prepareGameRoom();
		addSecondPlayerAndStartGame();
		blockingQueue = new LinkedBlockingDeque<>();
		stompClient = new WebSocketStompClient( new SockJsClient( List.of( new WebSocketTransport( new StandardWebSocketClient() ) ) ) );
	}

	@AfterEach
	void cleanUp()
	{
		service.cleanUp();
		blockingQueue.clear();
		stompClient.stop();
	}

	@Disabled //TODO: ETP-239
	@Test
	void shouldBroadcastCorrectPlayerAfterPass() throws Exception
	{
		StompSession session = stompClient.connect( WEBSOCKET_URI, new StompSessionHandlerAdapter()
		                                  {
		                                  } )
		                                  .get( 1, TimeUnit.SECONDS );
		session.subscribe( "/topic/activePlayer/" + gameId, defaultStompFrameHandler );

		service.pass( gameId );

		String response = blockingQueue.poll( 10, TimeUnit.SECONDS );

		assertEquals( "1", response );
		session.disconnect();
	}

	@Test
	void shouldBroadcastCorrectActiveCreatureAfterPass() throws Exception
	{
		StompSession session = stompClient.connect( WEBSOCKET_URI, new StompSessionHandlerAdapter()
		                                  {
		                                  } )
		                                  .get( 1, TimeUnit.SECONDS );
		session.subscribe( "/topic/lastAction/" + gameId, defaultStompFrameHandler );

		AbstractActionOnTileRequest tileToMoveTo = new MoveOrAttackOnTileRequest();
		tileToMoveTo.setTargetTile( new BoardManager().getPoint( 2, 2 ) );
		service.moveUnit( tileToMoveTo, gameId );
		String response = blockingQueue.poll( 10, TimeUnit.SECONDS );

		assertEquals( "\"" + GameEngine.ACTION.MOVE + "\"", response );
		session.disconnect();
	}

	private void prepareGameRoom() throws Exception
	{
		Player player = new Player();
		Gson gson = new Gson();
		player.setId( "player1" );
		String json = gson.toJson( player );
		MvcResult result = mockMvc.perform( post( "/lobby/create" ).contentType( MediaType.APPLICATION_JSON )
		                                                           .content( json ) )
		                          .andDo( print() )
		                          .andExpect( status().isOk() )
		                          .andReturn();
		gameId = result.getResponse()
		               .getContentAsString();
	}

	private void addSecondPlayerAndStartGame() throws Exception
	{
		Player player = new Player();
		Gson gson = new Gson();
		player.setId( "player2" );
		String json = gson.toJson( player );
		mockMvc.perform( post( "/lobby/connect/random" ).contentType( MediaType.APPLICATION_JSON )
		                                                .content( json ) )
		       .andDo( print() )
		       .andExpect( status().isOk() )
		       .andReturn();
		mockMvc.perform( post( "/lobby/" + gameId + "/start" ) )
		       .andDo( print() )
		       .andExpect( status().isOk() )
		       .andReturn();
	}
}