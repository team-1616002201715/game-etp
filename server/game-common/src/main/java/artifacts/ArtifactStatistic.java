/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package artifacts;

import static artifacts.ArtifactSlot.*;
import static artifacts.ArtifactStatistic.TargetType.CREATURE;
import static artifacts.ArtifactStatistic.TargetType.SPELL;

public enum ArtifactStatistic
{
	TEST_RING_OF_VITALITY( "Ring Of Vitality", "Increases health of all your units by 1", NECK, 5000, CREATURE ),
	TEST_SURCOAT_OF_COUNTERPOISE( "Surcoat of Counterpoise", "+10% to magic resistance", NECK, 4000, CREATURE ),
	TEST_ORB_OF_TEMPESTOUS_FIRE( "Orb of Tempestuous Fire", "Hero's fire spells to extra 50% damage", HEAD, 6000, SPELL ),
	TEST_PENDANT_OF_LIFE( "Pendant of Life", "Renders your units immune to the death ripple spell", NECK, 2500, SPELL ),
	TEST_BLACKSHARD_OF_THE_DEAD_KNIGHT( "Blackshard of the Dead Knight", "+3 attack skill", RIGHT_HAND, 3000, SPELL ),
	TEST_SHIELD_OF_THE_YAWNING_DEAD( "Shield of the Yawning Dead", "+3 defence skill", LEFT_HAND, 3000, CREATURE ),
	TEST_ARMOR_OF_WONDER( "Armor of Wonder", "+1 to all 4 primary skills", TORSO, 4000, SPELL ),
	TEST_PENDANT_OF_COURAGE( "Pendant of Courage", "+3 luck and morale", NECK, 7000, CREATURE ),
	TEST_GOLDEN_BOW( "Golden Bow", "Your troops can shoot at any distance through any obstacle without penalty", HEAD, 8000, CREATURE ),
	TEST_COLLAR_OF_CONJURING( "Collar of Conjuring", "Increases duration of all your spells by 1", NECK, 500, SPELL ),
	TEST_STATEMANS_MEDAL( "Stateman's Medal", "reduces the cost of surrendering", NECK, 5000, SPELL ),
	TEST_RING_OF_THE_WAYFARER( "Ring of the Wayfarer", "Increases the combat speed of all your units by 1", FINGERS, 5000, CREATURE ),
	TEST_TOME_OF_AIR_MAGIC( "Tome of Air Magic", "All air spells are available to hero when equipped", HEAD, 20000, SPELL ),

	GOLDEN_BOW( "Golden Bow", "Increases creature damage by 30%", HEAD, 2000, CREATURE ),
	ORB_OF_TEMPESTOUS_FIRE( "Orb of Tempestuous Fire", "Increases creature attack by 30%", HEAD, 2000, CREATURE ),
	COLLAR_OF_CONJURING( "Collar of Conjuring", "Increases spell damage by 30%", NECK, 2000, SPELL ),
	PENDANT_OF_COURAGE( "Pendant of Courage", "Increases creature armor by 20%", NECK, 2000, CREATURE ),
	STATEMANS_MEDAL( "Stateman's Medal", "Increases spell duration by 2 turns", NECK, 2000, SPELL ),
	SURCOAT_OF_COUNTERPOISE( "Surcoat of Counterpoise", "Increases creature magic resistance by 10%", SHOULDERS, 2000, CREATURE ),
	ARMOR_OF_WONDER( "Armor of Wonder", "Increases creature armor by 20%", TORSO, 2000, CREATURE ),
	SHIELD_OF_THE_YAWNING_DEAD( "Shield of the Yawning Dead", "Increases creature armor by 2", LEFT_HAND, 2000, CREATURE ),
	BLACKSHARD_OF_THE_DEAD_KNIGHT( "Blackshard of the Dead Knight", "Increases spell damage by 3", RIGHT_HAND, 2000, SPELL ),
	TOME_OF_AIR_MAGIC( "Tome of Air Magic", "Increases spell duration by 1 turns", RIGHT_HAND, 2000, SPELL ),
	RING_OF_VITALITY( "Ring Of Vitality", "Increases creature health by 1", FINGERS, 2000, CREATURE ),
	PENDANT_OF_LIFE( "Pendant of Life", "Increases creature health by 20%", FINGERS, 2000, CREATURE ),
	RING_OF_THE_WAYFARER( "Ring of the Wayfarer", "Increases creature move range by 1", FINGERS, 2000, CREATURE ),
	SHOES_OF_BLIGHTS( "Shoes of Blights", "Increases spell damage by 20%", FEET, 2000, SPELL ),
	LIFES_BOOTS( "Life's Boots", "Increases creature health by 2", FEET, 2000, CREATURE );

	public enum TargetType
	{
		CREATURE,
		SPELL
	}

	ArtifactStatistic( String aName, String aDescription, ArtifactSlot aArtifactSlot, int aGoldCost, TargetType aTargetType )
	{
		name = aName;
		description = aDescription;
		artifactSlot = aArtifactSlot;
		goldCost = aGoldCost;
		targetType = aTargetType;
	}

	private final String name;
	private final String description;
	private final ArtifactSlot artifactSlot;
	private final int goldCost;
	private final TargetType targetType;

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public ArtifactSlot getArtifactSlot()
	{
		return artifactSlot;
	}

	public int getGoldCost()
	{
		return goldCost;
	}

	public TargetType getTargetType()
	{
		return targetType;
	}

	@Override
	public String toString()
	{
		return "{\"name\"=\"" + name + "\", \"artifactSlot\"=\"" + artifactSlot + "\", \"targetType\"=\"" + targetType + "\"}";
	}
}
