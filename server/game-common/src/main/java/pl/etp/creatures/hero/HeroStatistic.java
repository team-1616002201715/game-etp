/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.hero;

public enum HeroStatistic
{
	ALEKSANDR_GOLOVIN( "Aleksandr Golovin", 2, 3, 2, 30 ),
	ARTEM_DZYUBA( "Artem Dzyuba", 4, 2, 1, 30 ),
	STANISLAV_CHERCHESOV( "Stanislav Cherchesov", 1, 1, 3, 30 ),
	EMILE_HESKEY( "Emile Heskey", 5, 1, 1, 30 ),
	JOHN_TERRY( "John Terry", 1, 5, 1, 50 ),
	ADEBAYO_AKINFENWA( "Adebayo Akinfenwa", 2, 2, 3, 40 );


	private final int attack;
	private final int defence;
	private final int power;

	private final int mana;
	private final String name;

	HeroStatistic( String aName, int aAttack, int aDefence, int aPower, int aMana )
	{
		name = aName;
		attack = aAttack;
		defence = aDefence;
		power = aPower;
		mana = aMana;
	}

	public int getPower()
	{
		return power;
	}

	public int getDefence()
	{
		return defence;
	}

	public int getAttack()
	{
		return attack;
	}

	public String getName()
	{
		return name();
	}

	public int getMana()
	{
		return mana;
	}
}
