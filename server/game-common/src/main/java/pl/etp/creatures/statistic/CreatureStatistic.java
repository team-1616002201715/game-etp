/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.statistic;

import com.google.common.collect.Range;
import context.DefenceContextStatisticIf;
import pl.etp.creatures.moving.MovementType;

public enum CreatureStatistic implements CreatureStatisticIf, DefenceContextStatisticIf
{
	//Federal State
	HEDGEHOG( "Hedgehog", 1, 10, 5, Range.closed( 2, 2 ), 0, 12, MovementType.FLYING, "Blocks counter-attack. ", 1 ),
	RAILGUN( "Railgun", 1, 8, 0, Range.closed( 2, 3 ), 0, 8, MovementType.GROUND, "Shooting creature. Splash damage 3x3", 2 ),
	SPECTRE( "Spectre", 1, 14, 3, Range.closed( 2, 4 ), 0, 7, MovementType.GROUND, "Shooting creature. Ignores the opponent's armor", 3 ),
	NEW_YORKER( "New Yorker", 1, 14, 5, Range.closed( 3, 5 ), 0, 4, MovementType.GROUND, "Attacks twice in a row", 4 ),
	HANDYMAN( "Handyman", 1, 20, 5, Range.closed( 5, 5 ), 0, 3, MovementType.GROUND, "Has a 50% chance of his attack dealing twice as much damage", 5 ),
	TROOP_CARRIER( "Troop Carrier", 2, 30, 7, Range.closed( 3, 4 ), 30, 2, MovementType.FLYING, "Without special powers", 6 ),
	MARAUDER( "Marauder", 3, 30, 8, Range.closed( 7, 7 ), 0, 1, MovementType.GROUND, "Without special powers", 7 ),
	PATRIOT( "Patriot", 4, 30, 3, Range.closed( 5, 12 ), 0, 1, MovementType.GROUND, "Shooting creature.", 8 ),

	//Soveja
	IVAYLO( "Ivaylo", 0, 12, 5, Range.closed( 2, 4 ), 0, 12, MovementType.GROUND, "Has a 50% chance of his attack dealing twice as much damage", 1 ),
	OFFICER( "Officer", 0, 12, 5, Range.closed( 2, 3 ), 0, 8, MovementType.GROUND, "Blocks counter-attack.", 2 ),
	UNDEAD( "Undead", 0, 20, 5, Range.closed( 3, 5 ), 0, 7, MovementType.GROUND, "After being attacked it heals 15% of its current HP", 3 ),
	WEREWOLF( "Werewolf", 0, 20, 9, Range.closed( 4, 5 ), 0, 4, MovementType.GROUND, "After being attacked it heals 10% of its current HP", 4 ),
	TROGLODYTE( "Troglodyte", 0, 30, 7, Range.closed( 3, 7 ), 0, 3, MovementType.GROUND, "Without special powers", 5 ),
	MINIGUNNER( "Minigunner", 1, 25, 3, Range.closed( 7, 7 ), 0, 2, MovementType.GROUND, "Shooting creature.", 6 ),
	BABA_YAGA( "Baba Yaga", 2, 25, 8, Range.closed( 6, 9 ), 0, 1, MovementType.FLYING, "Can move twice in a turn", 7 ),
	MATUSHKA( "Matushka", 3, 40, 5, Range.closed( 7, 12 ), 3, 1, MovementType.TELEPORT, "Without special powers", 8 ),

	//Testing
	TEST_NORMAL_GROUND_UNIT( "Patriot", 5, 15, 7, Range.closed( 3, 5 ), 0, 1, MovementType.GROUND, "orci. Vivamus pellentesque libero ut cursus placerat. Nullam convallis commodo felis, et sagittis sem dignissim nec. Maecenas iaculis, ", 1 ),
	TEST_MOVING_TWO_TIMES( "Baba Yaga", 17, 200, 20, Range.closed( 25, 50 ), 0, 1, MovementType.FLYING, "Sed euismod sapien enim, ut efficitur neque finibus at. Vivamus hendrerit vehicula erat. Ut lobortis non diam in placerat. Aenean at elit ", 2 ),
	TEST_BLOCK_COUNTER( "Hedgehog", 0, 20, 5, Range.closed( 10, 14 ), 0, 12, MovementType.FLYING, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non facilisis eros. Phasellus orci dui, vulputate a urna sollicitudin", 3 ),
	TEST_ATTACK_TWO_TIMES( "New Yorker", 10, 40, 9, Range.closed( 10, 10 ), 0, 4, MovementType.GROUND, "Donec molestie tortor eget quam blandit, et aliquam turpis lacinia. Sed metus dolor, ultrices a pellentesque id, porta sed augue. Mauris ", 4 ),
	TEST_SPLASH_DAMAGE_9( "Railgun", 0, 100, 5, Range.closed( 2, 3 ), 0, 8, MovementType.GROUND, "tincidunt pulvinar ex. Maecenas vel efficitur augue. Donec mollis interdum massa et finibus. Nulla bibendum interdum quam id porttitor. ", 5 ),
	TEST_ALWAYS_RETALIATE( "Test Ivaylo", 100, 100, 10, Range.closed( 5, 5 ), 0, 12, MovementType.GROUND, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non facilisis eros. Phasellus orci dui, vulputate a urna sollicitudin", 6 ),
	TEST_HEAL_AFTER_GET_ATTACK( "Werewolf", 0, 40, 9, Range.closed( 5, 8 ), 0, 4, MovementType.GROUND, "Donec molestie tortor eget quam blandit, et aliquam turpis lacinia. Sed metus dolor, ultrices a pellentesque id, porta sed augue. Mauris ", 7 ),
	TEST_1_HP_CREATURE( "1HP creature", 0, 1, 5, Range.closed( 1, 2 ), 0, 4, MovementType.GROUND, "creature that has 1 hp", 8 ),
	TEST_10_HP_CREATURE( "10HP creature", 0, 10, 10, Range.closed( 1, 2 ), 0, 4, MovementType.GROUND, "creature that has 10 hp", 9 ),
	TEST_100_HP_CREATURE( "100HP creature", 0, 100, 100, Range.closed( 5, 5 ), 0, 4, MovementType.GROUND, "creature that has 100 hp", 10 ),
	TEST_TELEPORT_CREATURE( "Teleport creature", 0, 10, 5, Range.closed( 1, 2 ), 0, 0, MovementType.TELEPORT, "Creature with teleport movement type", 11 ),
	TEST_FLYING_CREATURE( "Flying creature", 0, 10, 3, Range.closed( 1, 2 ), 0, 0, MovementType.FLYING, "Creature with flying movement type", 12 ),
	TEST_GOLDEN_SHOT_CREATURE( "Creature with golden shot", 0, 100, 5, Range.closed( 5, 10 ), 0, 2, MovementType.GROUND, "Creature with random chance to deal bonus damage", 13 ),
	TEST_SHOOTING_CREATURE( "Creature with golden shot", 0, 100, 5, Range.closed( 5, 10 ), 0, 2, MovementType.GROUND, "Creature with random chance to deal bonus damage", 13 );

	private final String name;
	private final int armor;
	private final int maxHp;
	private final int moveRange;
	private final Range<Integer> damage;
	private final String description;
	private final int growth;
	private final MovementType movementType;
	private final int magicResistance;
	public final String IMPLEMENTATION = "CREATURE";
	private int tier;

	CreatureStatistic( String aName, int aArmor, int aMaxHp, int aMoveRange, Range<Integer> aDamage, int aMagicResistance, int aGrowth, MovementType aMovementType, String aDescription, int aTier )
	{
		name = aName;
		armor = aArmor;
		maxHp = aMaxHp;
		moveRange = aMoveRange;
		damage = aDamage;
		description = aDescription;
		growth = aGrowth;
		movementType = aMovementType;
		magicResistance = aMagicResistance;
		tier = aTier;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public Range<Integer> getDamage()
	{
		return damage;
	}

	public int getMoveRange()
	{
		return moveRange;
	}

	public int getArmor()
	{
		return armor;
	}

	public int getMaxHp()
	{
		return maxHp;
	}

	@Override
	public String getImplementation()
	{
		return IMPLEMENTATION;
	}

	public MovementType getMovementType()
	{
		return movementType;
	}

	public int getGrowth()
	{
		return growth;
	}

	public int getMagicResistance()
	{
		return magicResistance;
	}

	public int getTier()
	{
		return tier;
	}

	@Override
	public String toString()
	{
		return "{name='" + name + '\'' + ", armor=" + armor + ", maxHp=" + maxHp + ", moveRange=" + moveRange + ", damage=" + damage + '\'' + ", movementType=" + movementType + ", magicResistance=" + magicResistance + '\'' + ", tier=" + tier + '}';
	}

	public String toStringEconomy()
	{
		return "{\"name\"=\"" + name + '\"' + ", \"armor\"=" + armor + ", \"maxHp\"=" + maxHp + ", \"moveRange\"=" + moveRange + ", \"damage\"=\"" + damage + "\", \"movementType\"=\"" + movementType + "\", \"magicResistance\"=" + magicResistance + ", \"tier\"=" + tier + '}';
	}
}
