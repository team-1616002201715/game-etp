/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures.spells;

public enum SpellStatistic
{
	TEST_HASTE( "Haste", "Increases the speed of the selected unit.", SpellElement.AIR, SpellType.BUFF, TargetType.ALLY, 6, 100 ),
	TEST_SUMMON_AIR_ELEMENTAL( "Summon Air Elemental", "Allows you to summon elementals. Once cast, no other elemental types may be summoned.", SpellElement.AIR, SpellType.SUMMON, TargetType.MAP, 6, 200 ),
	TEST_DISPEL( "Dispel", "Protects the selected unit from all low level spells.", SpellElement.WATER, SpellType.SPECIAL, TargetType.ALLY, 5, 300 ),
	TEST_TELEPORT( "Teleport", "Teleports any friendly unit to any unoccupied spot on the battlefield.", SpellElement.WATER, SpellType.SPECIAL, TargetType.ALLY, 15, 1100 ),
	TEST_FIRE_BALL( "Fire Ball", "Causes the selected target to burst into flames, inflicting fire damage to the target and any adjacent units.", SpellElement.FIRE, SpellType.DAMAGE, TargetType.SPLASH_MAP, 15, 500 ),
	TEST_IMPLOSION( "Implosion", "Inflicts massive damage to a single creature stack.", SpellElement.EARTH, SpellType.DAMAGE, TargetType.ENEMY, 30, 600 ),
	TEST_SLOW( "Slow", "Reduces the speed of the selected enemy unit.", SpellElement.EARTH, SpellType.DEBUFF, TargetType.ENEMY, 6, 700 ),
	TEST_DEATH_RIPPLE( "Death Ripple", "Sends a wave of death across the battlefield which damages all non-undead units.", SpellElement.EARTH, SpellType.DAMAGE, TargetType.SPECIAL, 10, 800 ),
	TEST_MAGIC_ARROW( "Magic Arrow", "Causes a bolt of magical energy to strike the selected unit.", SpellElement.ALL, SpellType.DAMAGE, TargetType.ENEMY, 5, 200 ),

	HASTE( "Haste", "Increases the move range of the selected unit by 4 for 3 turns.", SpellElement.AIR, SpellType.BUFF, TargetType.ALLY, 6, 100 ),
	BLOODLUST( "Bloodlust", "Increases armor of the selected unit by 20% for 2 turns.", SpellElement.AIR, SpellType.BUFF, TargetType.ALLY, 6, 200 ),
	DISPEL( "Dispel", "Increases armor of the selected unit by 5 for 4 turns.", SpellElement.WATER, SpellType.BUFF, TargetType.ALLY, 5, 300 ),
	TELEPORT( "Rage", "Increases damage of the selected unit by 30% for 2 turns.", SpellElement.WATER, SpellType.BUFF, TargetType.ALLY, 15, 400 ),
	SLOW( "Exhaust", "Decreases damage of the selected unit by 50% for 3 turns.", SpellElement.EARTH, SpellType.DEBUFF, TargetType.ENEMY, 6, 700 ),
	FIRE_BALL( "Fire Ball", "Splash damage 3x3. Damage depends on the hero's power: heroPower * 10 + 15", SpellElement.FIRE, SpellType.DAMAGE, TargetType.SPLASH_MAP, 15, 500 ),
	IMPLOSION( "Implosion", "Damage spell. Damage depends on the hero's power: heroPower * 75 + 100", SpellElement.EARTH, SpellType.DAMAGE, TargetType.ENEMY, 10, 600 ),
	DEATH_RIPPLE( "Death Ripple", "Damage spell. Damage depends on the hero's power: heroPower * 5 + 10", SpellElement.EARTH, SpellType.DAMAGE, TargetType.ENEMY, 10, 800 ),
	MAGIC_ARROW( "Magic Arrow", "Damage spell. Damage depends on the hero's power: heroPower * 10 + 10", SpellElement.ALL, SpellType.DAMAGE, TargetType.ENEMY, 5, 900 ),
	;

	public enum SpellElement
	{
		AIR,
		FIRE,
		WATER,
		EARTH,
		ALL;
	}

	public enum SpellType
	{
		BUFF,
		DEBUFF,
		DAMAGE,
		SPECIAL,
		SUMMON,
		MAP_CHANGE;
	}

	public enum TargetType
	{
		ALLY,
		ALL_ALLIES,
		ENEMY,
		ALL_ENEMIES,
		MAP,
		SPLASH_MAP,
		ALL,
		SPECIAL;
	}

	SpellStatistic( String aName, String aDescription, SpellElement aElement, SpellType aSpellType, TargetType aTargetType, int aManaCost, int aGoldCost )
	{
		name = aName;
		description = aDescription;
		element = aElement;
		spellType = aSpellType;
		targetType = aTargetType;
		manaCost = aManaCost;
		goldCost = aGoldCost;
	}

	private final String name;
	private final String description;
	private final SpellElement element;
	private final SpellType spellType;
	private final TargetType targetType;
	private final int manaCost;
	private final int goldCost;

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public SpellElement getElement()
	{
		return element;
	}

	public SpellType getSpellType()
	{
		return spellType;
	}

	public TargetType getTargetType()
	{
		return targetType;
	}

	public int getManaCost()
	{
		return manaCost;
	}

	public int getGoldCost()
	{
		return goldCost;
	}

	@Override
	public String toString()
	{
		return "{\"spell\"={\"name\"=\"" + name + "\", \"element\"=\"" + element + "\", \"spellType\"=\"" + spellType + "\", \"targetType\"=\"" + targetType + "\", \"manaCost\"=" + manaCost + "}, \"goldCost\"=" + goldCost + '}';
	}
}
