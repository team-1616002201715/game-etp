/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.hero;

import artifacts.ArtifactSlot;
import artifacts.ArtifactStatistic;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import pl.etp.items.EconomyArtifact;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

class ArtifactList
{
	private static final String INVALID_NAME_MESSAGE = "Incorrect artifact name";
	private static final String FULL_SLOT_MESSAGE = "There is no empty slot for this artifact";
	private final Multimap<ArtifactSlot, EconomyArtifact> artifacts;

	public ArtifactList()
	{
		artifacts = ArrayListMultimap.create();
	}

	void add( EconomyArtifact aArtifact )
	{
		if ( hasEmptySlot( aArtifact.getName() ) )
		{
			artifacts.put( aArtifact.getArtifactStats()
			                        .getArtifactSlot(), aArtifact );
		}
		else
		{
			throw new IllegalStateException( FULL_SLOT_MESSAGE );
		}
	}

	boolean contains( EconomyArtifact aArtifact )
	{
		return artifacts.containsValue( aArtifact );
	}

	void remove( EconomyArtifact aArtifact )
	{
		artifacts.remove( aArtifact.getArtifactStats()
		                           .getArtifactSlot(), aArtifact );
	}

	List<EconomyArtifact> getArtifacts()
	{
		return new ArrayList<>( artifacts.values() );
	}

	boolean hasEmptySlot( String aArtifactName )
	{
		Optional<ArtifactSlot> slot = Stream.of( ArtifactStatistic.values() )
		                                    .filter( as -> as.getName()
		                                                     .equals( aArtifactName ) )
		                                    .map( ArtifactStatistic::getArtifactSlot )
		                                    .findFirst();

		if ( slot.isPresent() )
		{
			return hasEmptySlot( slot.get() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_NAME_MESSAGE );
		}
	}

	private boolean hasEmptySlot( ArtifactSlot aSlot )
	{
		if ( artifacts.containsKey( aSlot ) )
		{
			return artifacts.get( aSlot )
			                .size() < aSlot.getAmount();
		}
		return true;
	}
}
