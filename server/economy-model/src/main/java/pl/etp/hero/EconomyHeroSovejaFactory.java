/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.hero;

import pl.etp.creatures.hero.HeroStatistic;

import java.util.List;

import static pl.etp.creatures.hero.HeroStatistic.*;

class EconomyHeroSovejaFactory extends AbstractEconomyHeroFactory
{
	private static final String INVALID_HERO_NAME = "Invalid hero name";

	public EconomyHero create( String aName )
	{
		if ( aName.equals( ALEKSANDR_GOLOVIN.getName() ) )
		{
			return new EconomyHero( HeroStatistic.ALEKSANDR_GOLOVIN );
		}
		else if ( aName.equals( ARTEM_DZYUBA.getName() ) )
		{
			return new EconomyHero( ARTEM_DZYUBA );
		}
		else if ( aName.equals( STANISLAV_CHERCHESOV.getName() ) )
		{
			return new EconomyHero( STANISLAV_CHERCHESOV );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_HERO_NAME );
		}

	}

	public List<String> getAllHeroes()
	{
		List<String> heroesList;
		heroesList = List.of( ALEKSANDR_GOLOVIN.getName(), ARTEM_DZYUBA.getName(), STANISLAV_CHERCHESOV.getName() );
		return heroesList;
	}
}
