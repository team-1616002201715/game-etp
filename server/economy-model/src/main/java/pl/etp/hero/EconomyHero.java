/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.hero;

import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.HeroStatistic;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;

import java.util.ArrayList;
import java.util.List;

public class EconomyHero
{
	private final List<EconomyCreature> creatures;
	private final ArtifactList artifactList;
	private final List<EconomySpell> spellList;
	private final HeroStatistic heroStats;
	private static final String NO_EMPTY_SLOT = "Hero has no empty slot for creature";

	EconomyHero( HeroStatistic aHeroStats )
	{
		creatures = new ArrayList<>();
		artifactList = new ArtifactList();
		spellList = new ArrayList<>();
		heroStats = aHeroStats;
	}

	public void addCreature( EconomyCreature aEconomyCreature )
	{
		if ( creatures.size() == 5 )
		{
			throw new IllegalStateException( NO_EMPTY_SLOT );
		}

		if ( creatures.contains( aEconomyCreature ) )
		{
			int index = creatures.indexOf( aEconomyCreature );
			creatures.get( index ).addAmount(aEconomyCreature.getAmount());

		}
		else
		{
			creatures.add( aEconomyCreature );
		}
	}

	public void addSpell( EconomySpell aEconomySpell )
	{
		spellList.add( aEconomySpell );
	}

	public void addArtifact( EconomyArtifact aEconomyArtifact )
	{
		artifactList.add( aEconomyArtifact );
	}

	public void removeCreature( EconomyCreature aEconomyCreature )
	{
		creatures.remove( aEconomyCreature );
	}

	public void removeSpell( EconomySpell aEconomySpell )
	{
		spellList.remove( aEconomySpell );
	}

	public void removeArtifact( EconomyArtifact aEconomyArtifact )
	{
		artifactList.remove( aEconomyArtifact );
	}

	public int getPower()
	{
		return heroStats.getPower();
	}

	int getAttack()
	{
		return heroStats.getAttack();
	}

	int getDefence()
	{
		return heroStats.getDefence();
	}

	boolean hasEmptySlotForArtifact( String aName )
	{
		return artifactList.hasEmptySlot( aName );
	}

	public List<EconomyCreature> getCreatures()
	{
		return List.copyOf( creatures );
	}

	public List<EconomySpell> getSpells()
	{
		return List.copyOf( spellList );
	}

	public List<EconomyArtifact> getArtifacts()
	{
		return artifactList.getArtifacts();
	}

	public int getMana()
	{
		return heroStats.getMana();
	}
}
