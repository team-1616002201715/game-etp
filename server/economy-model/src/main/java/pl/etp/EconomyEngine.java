/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import pl.etp.creatures.EconomyCreature;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;
import pl.etp.player.Player;

import java.util.List;

public class EconomyEngine
{
	private final Player player;

	public EconomyEngine( Player aPlayer )
	{
		player = aPlayer;
	}

	public void buyCreature( EconomyCreature aEconomyCreature )
	{
		player.buyCreature( aEconomyCreature );
	}

	public void buyArtifact( EconomyArtifact aEconomyArtifact )
	{
		player.buyArtifact( aEconomyArtifact );
	}

	public void buySpell( EconomySpell aEconomySpell )
	{
		player.buySpell( aEconomySpell );
	}

	public void sellCreature( EconomyCreature aEconomyCreature )
	{
		player.sellCreature( aEconomyCreature );
	}

	public void sellArtifact( EconomyArtifact aEconomyArtifact )
	{
		player.sellArtifact( aEconomyArtifact );
	}

	public void sellSpell( EconomySpell aEconomySpell )
	{
		player.sellSpell( aEconomySpell );
	}

	public int getCurrentPopulation( int aTier )
	{
		return player.getCurrentPopulation( aTier );
	}

	public List<String> getCurrentCreaturePopulation()
	{
		return player.getCurrentCreaturepopulation();
	}

	public List<String> getCurrentSpellPopulation()
	{
		return player.getCurrentSpellPopulation();
	}

	public List<String> getCurrentArtifactPopulation()
	{
		return player.getCurrentArtifactPopulation();
	}

	public boolean hasSpell( String aName )
	{
		return player.hasSpell( aName );
	}

	public boolean hasArtifact( String aName )
	{
		return player.hasArtifact( aName );
	}

	public Player getPlayer()
	{
		return player;
	}

	public boolean canBuyArtifact( EconomyArtifact aEconomyArtifact )
	{
		return player.canBuyArtifact( aEconomyArtifact );
	}

	public boolean canBuySpell( EconomySpell aEconomySpell )
	{
		return player.canBuySpell( aEconomySpell );
	}

	public boolean canBuyCreature( EconomyCreature aEconomyCreature )
	{
		return player.canBuyCreature( aEconomyCreature );
	}

	public int calculateCreatureMaxAmount( String aCreatureName )
	{
		return player.calculateMaxAmount( aCreatureName );
	}
}
