/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.items;

import pl.etp.creatures.spells.SpellStatistic;

import java.util.ArrayList;
import java.util.List;

import static pl.etp.creatures.spells.SpellStatistic.*;

class EconomySpellFactory extends AbstractEconomyItemFactory<EconomySpell>
{

	@Override
	public EconomySpell create( String aName )
	{
		if ( aName.equals( HASTE.getName() ) )
		{
			return new EconomySpell( SpellStatistic.HASTE );
		}
		else if ( aName.equals( BLOODLUST.getName() ) )
		{
			return new EconomySpell( BLOODLUST );
		}
		else if ( aName.equals( DISPEL.getName() ) )
		{
			return new EconomySpell( SpellStatistic.DISPEL );
		}
		else if ( aName.equals( TELEPORT.getName() ) )
		{
			return new EconomySpell( TELEPORT );
		}
		else if ( aName.equals( FIRE_BALL.getName() ) )
		{
			return new EconomySpell( SpellStatistic.FIRE_BALL );
		}
		else if ( aName.equals( SLOW.getName() ) )
		{
			return new EconomySpell( SpellStatistic.SLOW );
		}
		else if ( aName.equals( IMPLOSION.getName() ) )
		{
			return new EconomySpell( SpellStatistic.IMPLOSION );
		}
		else if ( aName.equals( DEATH_RIPPLE.getName() ) )
		{
			return new EconomySpell( SpellStatistic.DEATH_RIPPLE );
		}
		else if ( aName.equals( MAGIC_ARROW.getName() ) )
		{
			return new EconomySpell( SpellStatistic.MAGIC_ARROW );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_ITEM_NAME );
		}

	}

	@Override
	public List<EconomySpell> getAllItems()
	{
		List<EconomySpell> spellList = new ArrayList<>();
		List.of( HASTE.getName(), MAGIC_ARROW.getName(), DEATH_RIPPLE.getName(), IMPLOSION.getName(), SLOW.getName(), FIRE_BALL.getName(), TELEPORT.getName(), DISPEL.getName(), BLOODLUST.getName() )
		    .forEach( spellName -> spellList.add( create( spellName ) ) );
		return spellList;
	}

}
