/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.items;

import java.util.ArrayList;
import java.util.List;

import static artifacts.ArtifactStatistic.*;

class EconomyArtifactFactory extends AbstractEconomyItemFactory<EconomyArtifact>
{
	@Override
	public EconomyArtifact create( String aName )
	{
		if ( aName.equals( RING_OF_VITALITY.getName() ) )
		{
			return new EconomyArtifact( RING_OF_VITALITY );
		}
		else if ( aName.equals( SURCOAT_OF_COUNTERPOISE.getName() ) )
		{
			return new EconomyArtifact( SURCOAT_OF_COUNTERPOISE );
		}
		else if ( aName.equals( ORB_OF_TEMPESTOUS_FIRE.getName() ) )
		{
			return new EconomyArtifact( ORB_OF_TEMPESTOUS_FIRE );
		}
		else if ( aName.equals( PENDANT_OF_LIFE.getName() ) )
		{
			return new EconomyArtifact( PENDANT_OF_LIFE );
		}
		else if ( aName.equals( ARMOR_OF_WONDER.getName() ) )
		{
			return new EconomyArtifact( ARMOR_OF_WONDER );
		}
		else if ( aName.equals( PENDANT_OF_COURAGE.getName() ) )
		{
			return new EconomyArtifact( PENDANT_OF_COURAGE );
		}
		else if ( aName.equals( BLACKSHARD_OF_THE_DEAD_KNIGHT.getName() ) )
		{
			return new EconomyArtifact( BLACKSHARD_OF_THE_DEAD_KNIGHT );
		}
		else if ( aName.equals( SHIELD_OF_THE_YAWNING_DEAD.getName() ) )
		{
			return new EconomyArtifact( SHIELD_OF_THE_YAWNING_DEAD );
		}
		else if ( aName.equals( GOLDEN_BOW.getName() ) )
		{
			return new EconomyArtifact( GOLDEN_BOW );
		}
		else if ( aName.equals( COLLAR_OF_CONJURING.getName() ) )
		{
			return new EconomyArtifact( COLLAR_OF_CONJURING );
		}
		else if ( aName.equals( STATEMANS_MEDAL.getName() ) )
		{
			return new EconomyArtifact( STATEMANS_MEDAL );
		}
		else if ( aName.equals( RING_OF_THE_WAYFARER.getName() ) )
		{
			return new EconomyArtifact( RING_OF_THE_WAYFARER );
		}
		else if ( aName.equals( TOME_OF_AIR_MAGIC.getName() ) )
		{
			return new EconomyArtifact( TOME_OF_AIR_MAGIC );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_ITEM_NAME );
		}
	}

	@Override
	public List<EconomyArtifact> getAllItems()
	{
		List<EconomyArtifact> artifacts = new ArrayList<>();
		List.of( RING_OF_VITALITY.getName(), SURCOAT_OF_COUNTERPOISE.getName(), ORB_OF_TEMPESTOUS_FIRE.getName(), PENDANT_OF_LIFE.getName(), BLACKSHARD_OF_THE_DEAD_KNIGHT.getName(), SHIELD_OF_THE_YAWNING_DEAD.getName(), ARMOR_OF_WONDER.getName(), PENDANT_OF_COURAGE.getName(), GOLDEN_BOW.getName(), COLLAR_OF_CONJURING.getName(), STATEMANS_MEDAL.getName(), RING_OF_THE_WAYFARER.getName(), TOME_OF_AIR_MAGIC.getName() )
		    .forEach( artifactName -> artifacts.add( create( artifactName ) ) );
		return artifacts;
	}
}
