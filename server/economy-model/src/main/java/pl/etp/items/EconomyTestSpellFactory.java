/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.items;

import pl.etp.creatures.spells.SpellStatistic;

import java.util.ArrayList;
import java.util.List;

import static pl.etp.creatures.spells.SpellStatistic.*;

class EconomyTestSpellFactory extends AbstractEconomyItemFactory<EconomySpell>
{
	@Override
	public EconomySpell create( String aName )
	{
		if ( aName.equals( TEST_HASTE.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_HASTE );
		}
		else if ( aName.equals( TEST_SUMMON_AIR_ELEMENTAL.getName() ) )
		{
			return new EconomySpell( TEST_SUMMON_AIR_ELEMENTAL );
		}
		else if ( aName.equals( TEST_DISPEL.getName() ) )
		{
			return new EconomySpell( TEST_DISPEL );
		}
		else if ( aName.equals( TEST_TELEPORT.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_TELEPORT );
		}
		else if ( aName.equals( TEST_FIRE_BALL.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_FIRE_BALL );
		}
		else if ( aName.equals( TEST_SLOW.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_SLOW );
		}
		else if ( aName.equals( TEST_IMPLOSION.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_IMPLOSION );
		}
		else if ( aName.equals( TEST_DEATH_RIPPLE.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_DEATH_RIPPLE );
		}
		else if ( aName.equals( TEST_MAGIC_ARROW.getName() ) )
		{
			return new EconomySpell( SpellStatistic.TEST_MAGIC_ARROW );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_ITEM_NAME );
		}

	}

	@Override
	public List<EconomySpell> getAllItems()
	{
		List<EconomySpell> spellList = new ArrayList<>();
		List.of( TEST_HASTE.getName(), TEST_MAGIC_ARROW.getName(), TEST_DEATH_RIPPLE.getName(), TEST_IMPLOSION.getName(), TEST_SLOW.getName(), TEST_FIRE_BALL.getName(), TEST_TELEPORT.getName(), TEST_DISPEL.getName(), TEST_SUMMON_AIR_ELEMENTAL.getName() )
		    .forEach( spellName -> spellList.add( create( spellName ) ) );
		return spellList;
	}

}
