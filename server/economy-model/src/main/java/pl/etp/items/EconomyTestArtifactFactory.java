/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.items;

import java.util.ArrayList;
import java.util.List;

import static artifacts.ArtifactStatistic.*;

public class EconomyTestArtifactFactory extends AbstractEconomyItemFactory<EconomyArtifact>
{
	@Override
	public EconomyArtifact create( String aName )
	{
		if ( aName.equals( TEST_RING_OF_VITALITY.getName() ) )
		{
			return new EconomyArtifact( TEST_RING_OF_VITALITY );
		}
		else if ( aName.equals( TEST_SURCOAT_OF_COUNTERPOISE.getName() ) )
		{
			return new EconomyArtifact( TEST_SURCOAT_OF_COUNTERPOISE );
		}
		else if ( aName.equals( TEST_ORB_OF_TEMPESTOUS_FIRE.getName() ) )
		{
			return new EconomyArtifact( TEST_ORB_OF_TEMPESTOUS_FIRE );
		}
		else if ( aName.equals( TEST_PENDANT_OF_LIFE.getName() ) )
		{
			return new EconomyArtifact( TEST_PENDANT_OF_LIFE );
		}
		else if ( aName.equals( TEST_ARMOR_OF_WONDER.getName() ) )
		{
			return new EconomyArtifact( TEST_ARMOR_OF_WONDER );
		}
		else if ( aName.equals( TEST_PENDANT_OF_COURAGE.getName() ) )
		{
			return new EconomyArtifact( TEST_PENDANT_OF_COURAGE );
		}
		else if ( aName.equals( TEST_BLACKSHARD_OF_THE_DEAD_KNIGHT.getName() ) )
		{
			return new EconomyArtifact( TEST_BLACKSHARD_OF_THE_DEAD_KNIGHT );
		}
		else if ( aName.equals( TEST_SHIELD_OF_THE_YAWNING_DEAD.getName() ) )
		{
			return new EconomyArtifact( TEST_SHIELD_OF_THE_YAWNING_DEAD );
		}
		else if ( aName.equals( TEST_GOLDEN_BOW.getName() ) )
		{
			return new EconomyArtifact( TEST_GOLDEN_BOW );
		}
		else if ( aName.equals( TEST_COLLAR_OF_CONJURING.getName() ) )
		{
			return new EconomyArtifact( TEST_COLLAR_OF_CONJURING );
		}
		else if ( aName.equals( TEST_STATEMANS_MEDAL.getName() ) )
		{
			return new EconomyArtifact( TEST_STATEMANS_MEDAL );
		}
		else if ( aName.equals( TEST_RING_OF_THE_WAYFARER.getName() ) )
		{
			return new EconomyArtifact( TEST_RING_OF_THE_WAYFARER );
		}
		else if ( aName.equals( TEST_TOME_OF_AIR_MAGIC.getName() ) )
		{
			return new EconomyArtifact( TEST_TOME_OF_AIR_MAGIC );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_ITEM_NAME );
		}
	}

	@Override
	public List<EconomyArtifact> getAllItems()
	{
		List<EconomyArtifact> artifacts = new ArrayList<>();
		List.of( TEST_RING_OF_VITALITY.getName(), TEST_SURCOAT_OF_COUNTERPOISE.getName(), TEST_ORB_OF_TEMPESTOUS_FIRE.getName(), TEST_PENDANT_OF_LIFE.getName(), TEST_BLACKSHARD_OF_THE_DEAD_KNIGHT.getName(), TEST_SHIELD_OF_THE_YAWNING_DEAD.getName(), TEST_ARMOR_OF_WONDER.getName(), TEST_PENDANT_OF_COURAGE.getName(), TEST_GOLDEN_BOW.getName(), TEST_COLLAR_OF_CONJURING.getName(), TEST_STATEMANS_MEDAL.getName(), TEST_RING_OF_THE_WAYFARER.getName(), TEST_TOME_OF_AIR_MAGIC.getName() )
		    .forEach( artifactName -> artifacts.add( create( artifactName ) ) );
		return artifacts;
	}
}
