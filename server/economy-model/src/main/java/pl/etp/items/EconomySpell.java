/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.items;

import pl.etp.creatures.spells.SpellStatistic;

import java.util.Objects;

public class EconomySpell implements EconomyItemIf
{
	public static final int SPELL_GROWTH = 1;
	private final SpellStatistic spellStats;

	EconomySpell( SpellStatistic aSpellStats )
	{
		spellStats = aSpellStats;
	}

	public String getName()
	{
		return spellStats.getName();
	}

	public String getDescription()
	{
		return spellStats.getDescription();
	}

	public SpellStatistic.SpellElement getElement()
	{
		return spellStats.getElement();
	}

	public SpellStatistic.SpellType getSpellType()
	{
		return spellStats.getSpellType();
	}

	public SpellStatistic.TargetType getTargetType()
	{
		return spellStats.getTargetType();
	}

	public int getManaCost()
	{
		return spellStats.getManaCost();
	}

	public SpellStatistic getSpellStatistic()
	{
		return spellStats;
	}

	public int getGoldCost()
	{
		return spellStats.getGoldCost();
	}

	public int getGrowth()
	{
		return SPELL_GROWTH;
	}

	@Override
	public String toString()
	{
		return spellStats.toString();
	}

	@Override
	public boolean equals( Object aO )
	{
		if ( this == aO )
		{
			return true;
		}
		if ( aO == null || getClass() != aO.getClass() || getName() == null )
		{
			return false;
		}
		EconomySpell that = ( EconomySpell ) aO;
		return getName().equals( that.getName() );
	}

	@Override
	public int hashCode()
	{
		return Objects.hash( getName() );
	}
}
