/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;
import pl.etp.hero.AbstractEconomyHeroFactory;
import pl.etp.hero.EconomyHero;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;

import java.util.List;
import java.util.stream.Collectors;

import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;

public class Player
{

	private final String LACK_OF_MONEY = "pl.etp.player.Player has not enough money";
	private int gold;
	private EconomyHero hero;
	private final CreatureShop creatureShop;
	private final SpellShop spellShop;
	private final ArtifactShop artifactShop;

	public Player( Fraction aFraction, int aGold )
	{
		this( aFraction, aGold, JOHN_TERRY.getName() );
	}

	public Player( Fraction aFraction, int aGold, String aHero )
	{
		this( aFraction, aGold, aHero, new CreatureShop( aFraction ) );
	}

	Player( Fraction aFraction, int aGold, String aHero, CreatureShop aCreatureShop )
	{
		this( aFraction, aGold, aHero, aCreatureShop, new SpellShop(), new ArtifactShop() );
	}

	Player( Fraction aFraction, int aGold, String aHero, SpellShop aSpellShop )
	{
		this( aFraction, aGold, aHero, new CreatureShop( aFraction ), aSpellShop, new ArtifactShop() );
	}

	Player( Fraction aFraction, int aGold, String aHero, ArtifactShop aArtifactShop )
	{
		this( aFraction, aGold, aHero, new CreatureShop( aFraction ), new SpellShop(), aArtifactShop );
	}

	public Player( Fraction aFraction, int aGold, String aHero, CreatureShop aCreatureShop, SpellShop aSpellShop, ArtifactShop aArtifactShop )
	{
		gold = aGold;
		hero = AbstractEconomyHeroFactory.getInstance( aFraction )
		                                 .create( aHero );
		creatureShop = aCreatureShop;
		spellShop = aSpellShop;
		artifactShop = aArtifactShop;
	}

	public void subtractGold( int aGoldToSubtract )
	{
		if ( aGoldToSubtract > gold )
		{
			throw new IllegalStateException( LACK_OF_MONEY );
		}
		gold -= aGoldToSubtract;
	}

	public int getGold()
	{
		return gold;
	}

	void addGold( int aGoldCost )
	{
		gold += aGoldCost;
	}

	public List<EconomyCreature> getCreatures()
	{
		return hero.getCreatures();
	}

	public int calculateMaxAmount( String aCreatureName )
	{
		return creatureShop.calculateMaxAmount( gold, aCreatureName );
	}

	public int getCurrentPopulation( int aTier )
	{
		return creatureShop.getCurrentPopulation( aTier );
	}

	void addCreature( EconomyCreature aEconomyCreature )
	{
		hero.addCreature( aEconomyCreature );
	}


	void removeCreature( EconomyCreature aEconomyCreature )
	{
		hero.removeCreature( aEconomyCreature );
	}

	void removeSpell( EconomySpell aEconomyCreature )
	{
		hero.removeSpell( aEconomyCreature );
	}

	void removeArtifact( EconomyArtifact aEconomyCreature )
	{
		hero.removeArtifact( aEconomyCreature );
	}

	void addSpell( EconomySpell aEconomySpell )
	{
		hero.addSpell( aEconomySpell );
	}

	void addArtifact( EconomyArtifact aEconomyArtifact )
	{
		hero.addArtifact( aEconomyArtifact );
	}

	public List<String> getCurrentCreaturepopulation()
	{
		return creatureShop.getCurrentCreaturePopulation();
	}

	public List<String> getCurrentSpellPopulation()
	{
		return spellShop.getCurrentSpellPopulation();
	}

	public List<String> getCurrentArtifactPopulation()
	{
		return artifactShop.getCurrentArtifactPopulation();
	}

	public void buyCreature( EconomyCreature aEconomyCreature )
	{
		creatureShop.buy( this, aEconomyCreature );
	}

	public void buySpell( EconomySpell aEconomySpell )
	{
		spellShop.buy( this, aEconomySpell );
	}

	public void buyArtifact( EconomyArtifact aEconomyArtifact )
	{
		artifactShop.buy( this, aEconomyArtifact );
	}

	public void sellCreature( EconomyCreature aEconomyCreature )
	{
		creatureShop.sell( this, aEconomyCreature );
	}

	public void sellArtifact( EconomyArtifact aEconomyArtifact )
	{
		artifactShop.sell( this, aEconomyArtifact );
	}

	public void sellSpell( EconomySpell aEconomySpell )
	{
		spellShop.sell( this, aEconomySpell );
	}

	public List<EconomySpell> getSpells()
	{
		return hero.getSpells();
	}

	public List<EconomyArtifact> getArtifacts()
	{
		return hero.getArtifacts();
	}

	public boolean hasSpell( String aName )
	{
		return getSpells().stream()
		                  .map( EconomySpell::getName )
		                  .collect( Collectors.toList() )
		                  .contains( aName );
	}

	public boolean hasArtifact( String aName )
	{
		return getArtifacts().stream()
		                     .map( EconomyArtifact::getName )
		                     .collect( Collectors.toList() )
		                     .contains( aName );
	}

	public int getPower()
	{
		return hero.getPower();
	}

	public int getMana()
	{
		return hero.getMana();
	}

	public boolean canBuySpell( EconomySpell aEconomySpell )
	{
		return spellShop.canBuySpell( gold, aEconomySpell );
	}

	public boolean canBuyArtifact( EconomyArtifact aEconomyArtifact )
	{
		return artifactShop.canBuyArtifact( gold, aEconomyArtifact );
	}

	public boolean canBuyCreature( EconomyCreature aCreature )
	{
		return creatureShop.canBuyCreature( gold, aCreature );
	}
}
