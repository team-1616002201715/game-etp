/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import pl.etp.calculator.DefaultShopCalculator;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ArtifactShop extends AbstractShop<EconomyArtifact>
{
	public static final String PLAYER_HAS_ALREADY_BOUGHT_THIS_ARTIFACT = "Player has already bought this artifact!";
	public static final String HERO_CANNOT_HOLD_MORE_ARTIFACTS = "Hero cannot hold more artifacts";
	private final AbstractEconomyItemFactory<EconomyArtifact> artifactFactory;
	private final Random rand;

	ArtifactShop()
	{
		super( new DefaultShopCalculator(), new ArrayList<>() );
		artifactFactory = AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT );
		rand = new Random();
		createPopulation();
	}

	public ArtifactShop( Random aRand, AbstractEconomyItemFactory<EconomyArtifact> aFactory )
	{
		super( new DefaultShopCalculator( aRand ), new ArrayList<>() );
		artifactFactory = aFactory;
		rand = aRand;
		createPopulation();
	}

	@Override
	protected void createPopulation()
	{
		List<EconomyArtifact> allArtifacts = artifactFactory.getAllItems();
		int artifactAmount = getArtifactAmount( allArtifacts );
		Collections.shuffle( allArtifacts );
		allArtifacts.subList( 0, artifactAmount )
		            .forEach( a -> getShopPopulation().add( a ) );
	}

	@Override
	protected void addItem( Player aPlayer, EconomyArtifact aShopItem )
	{
		aPlayer.addArtifact( aShopItem );
	}

	@Override
	protected String getSubtractPopulationErrorMessage()
	{
		return PLAYER_HAS_ALREADY_BOUGHT_THIS_ARTIFACT;
	}

	@Override
	protected void removeItem( Player aPlayer, EconomyArtifact aShopItem )
	{
		aPlayer.removeArtifact( aShopItem );
	}

	@Override
	protected String getBuyErrorMessage()
	{
		return HERO_CANNOT_HOLD_MORE_ARTIFACTS;
	}

	boolean canBuyArtifact( int aGold, EconomyArtifact aArtifact )
	{
		return getCalculator().calculateMaxAmount( aGold, aArtifact.getGrowth(), aArtifact.getGoldCost() ) == 1;
	}

	private int getArtifactAmount( List<EconomyArtifact> allArtifacts )
	{
		int amount = allArtifacts.size() / 2;
		return 1 + amount + rand.nextInt( amount / 2 );
	}

	List<String> getCurrentArtifactPopulation()
	{
		return List.copyOf( getShopPopulation().stream()
		                                       .map( EconomyArtifact::toString )
		                                       .collect( Collectors.toList() ) );
	}
}
