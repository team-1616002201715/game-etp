/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import pl.etp.calculator.AbstractShopCalculator;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomyItemIf;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractShop <T extends EconomyItemIf>
{
	private final AbstractShopCalculator calculator;
	private final List<T> shopPopulation;
	private static final String CANNOT_SELL_ITEM = "Cannot sell item";

	AbstractShop( AbstractShopCalculator aCalculator, List<T> aShopPopulation )
	{
		calculator = aCalculator;
		shopPopulation = aShopPopulation;
	}

	protected abstract void createPopulation();

	protected abstract void addItem( Player aActivePlayer, T aShopItem );

	protected abstract String getBuyErrorMessage();

	protected abstract String getSubtractPopulationErrorMessage();

	protected void buy( Player aActivePlayer, T aShopItem )
	{
		subtractGold( aActivePlayer, aShopItem );
		subtractPopulation( aShopItem );
		try
		{
			addItem( aActivePlayer, aShopItem );
		}
		catch ( Exception e )
		{
			restoreGold( aActivePlayer, aShopItem );
			restorePopulation( aShopItem );
			throw new IllegalStateException( getBuyErrorMessage() );
		}
	}

	protected void sell( Player aPlayer, T aShopItem )
	{
		restoreGold( aPlayer, aShopItem );
		restorePopulation( aShopItem );
		try
		{
			removeItem( aPlayer, aShopItem );
		}
		catch ( Exception e )
		{
			subtractGold( aPlayer, aShopItem );
			subtractPopulation( aShopItem );
			throw new IllegalStateException( CANNOT_SELL_ITEM );
		}
	}

	protected abstract void removeItem( Player aPlayer, T aShopItem );

	protected void subtractGold( Player aActivePlayer, T aShopItem )
	{
		aActivePlayer.subtractGold( aShopItem.getGoldCost() );
	}

	protected void subtractPopulation( T aShopItem )
	{
		if ( !shopPopulation.stream()
		                    .map( T::getName )
		                    .collect( Collectors.toList() )
		                    .contains( aShopItem.getName() ) )
		{
			throw new IllegalStateException( getSubtractPopulationErrorMessage() );
		}
		shopPopulation.removeIf( p -> p.getName()
		                               .equals( aShopItem.getName() ) );
	}

	protected void restoreGold( Player aActivePlayer, T aShopItem )
	{
		aActivePlayer.addGold( aShopItem.getGoldCost() );
	}

	protected void restorePopulation( T aShopItem )
	{
		shopPopulation.add( aShopItem );
	}

	AbstractShopCalculator getCalculator()
	{
		return calculator;
	}

	List<T> getShopPopulation()
	{
		return shopPopulation;
	}

}
