/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import pl.etp.calculator.DefaultShopCalculator;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomySpell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class SpellShop extends AbstractShop<EconomySpell>
{
	public static final String PLAYER_HAS_ALREADY_BOUGHT_THIS_SPELL = "Player has already bought this spell!";
	public static final String HERO_CANNOT_LEARN_MORE_SPELLS = "Hero cannot learn more spells";
	private final AbstractEconomyItemFactory<EconomySpell> spellFactory;
	private final Random rand;

	SpellShop()
	{
		super( new DefaultShopCalculator(), new ArrayList<>() );
		spellFactory = AbstractEconomyItemFactory.getInstance( FactoryType.SPELL );
		rand = new Random();
		createPopulation();
	}

	public SpellShop( Random aRand, AbstractEconomyItemFactory<EconomySpell> aFactory )
	{
		super( new DefaultShopCalculator( aRand ), new ArrayList<>() );
		spellFactory = aFactory;
		rand = aRand;
		createPopulation();
	}

	@Override
	protected void createPopulation()
	{
		List<EconomySpell> allSpells = spellFactory.getAllItems();
		int spellAmount = getSpellAmount( allSpells );
		Collections.shuffle( allSpells );
		allSpells.subList( 0, spellAmount )
		         .forEach( s -> getShopPopulation().add( s ) );
	}

	@Override
	protected void addItem( Player aActivePlayer, EconomySpell aShopItem )
	{
		aActivePlayer.addSpell( aShopItem );
	}

	@Override
	protected String getSubtractPopulationErrorMessage()
	{
		return PLAYER_HAS_ALREADY_BOUGHT_THIS_SPELL;
	}

	@Override
	protected void removeItem( Player aPlayer, EconomySpell aShopItem )
	{
		aPlayer.removeSpell( aShopItem );
	}

	@Override
	protected String getBuyErrorMessage()
	{
		return HERO_CANNOT_LEARN_MORE_SPELLS;
	}

	List<String> getCurrentSpellPopulation()
	{
		return List.copyOf( getShopPopulation().stream()
		                                       .map( EconomySpell::toString )
		                                       .collect( Collectors.toList() ) );
	}

	boolean canBuySpell( int aGold, EconomySpell aSpell )
	{
		return getCalculator().calculateMaxAmount( aGold, aSpell.getGrowth(), aSpell.getGoldCost() ) == 1;
	}

	private int getSpellAmount( List<EconomySpell> allSpells )
	{
		int amount = allSpells.size() / 2;
		return 1 + amount + rand.nextInt( amount );
	}
}
