/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import pl.etp.calculator.CreatureShopCalculator;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class CreatureShop extends AbstractShop<EconomyCreature>
{
	private final AbstractEconomyFractionFactory creatureFactory;
	public static final int TIER_AMOUNT = 8;
	public static final String HERO_CANNOT_BUY_MORE_CREATURES_THAN_POPULATION_IS = "Hero cannot buy more creatures than the current population allows";
	public static final String HERO_CANNOT_CONSUME_MORE_CREATURES = "Hero cannot control more creatures";
	public static final String CREATURE_DOES_NOT_EXIST = "Creature does not exist";

	public CreatureShop( Random aRand, Fraction aFraction )
	{
		super( new CreatureShopCalculator( aRand ), new ArrayList<>() );
		creatureFactory = AbstractEconomyFractionFactory.getInstance( aFraction );
		createPopulation();
	}

	CreatureShop( Fraction aFraction )
	{
		super( new CreatureShopCalculator(), new ArrayList<>() );
		creatureFactory = AbstractEconomyFractionFactory.getInstance( aFraction );
		createPopulation();
	}

	@Override
	protected void createPopulation()
	{
		for ( int i = 0; i < TIER_AMOUNT; i++ )
		{
			getShopPopulation().add( i, creatureFactory.create( i + 1, calculatePopulation( i + 1 ) ) );
		}
	}

	private int calculatePopulation( int aTier )
	{
		return getCalculator().randomize( creatureFactory.create( aTier, 1 )
		                                                 .getGrowth() );
	}


	public int calculateMaxAmount( int aGold, EconomyCreature aCreature )
	{
		return getCalculator().calculateMaxAmount( aGold, getShopPopulation().get( aCreature.getTier() - 1 )
		                                                                     .getAmount(), aCreature.getGoldCost() );
	}

	public int calculateMaxAmount( int aGold, String aCreatureName )
	{
		EconomyCreature creatureToCalculate;
		if ( getShopPopulation().stream()
		                        .anyMatch( creature -> creature.getName()
		                                                       .equals( aCreatureName ) ) )
		{
			creatureToCalculate = getShopPopulation().stream()
			                                         .filter( creature -> creature.getName()
			                                                                      .equals( aCreatureName ) )
			                                         .findFirst()
			                                         .get();
		}
		else
		{
			throw new IllegalArgumentException( CREATURE_DOES_NOT_EXIST );
		}
		return getCalculator().calculateMaxAmount( aGold, creatureToCalculate.getAmount(), creatureToCalculate.getGoldCost() );
	}

	boolean canBuyCreature( int aGold, EconomyCreature aCreature )
	{
		return calculateMaxAmount( aGold, aCreature ) >= 1;
	}

	@Override
	protected void addItem( Player aActivePlayer, EconomyCreature aShopItem )
	{
		aActivePlayer.addCreature( aShopItem );
	}

	@Override
	protected String getSubtractPopulationErrorMessage()
	{
		return HERO_CANNOT_BUY_MORE_CREATURES_THAN_POPULATION_IS;
	}

	@Override
	protected void removeItem( Player aPlayer, EconomyCreature aShopItem )
	{
		aPlayer.removeCreature( aShopItem );
	}

	@Override
	protected String getBuyErrorMessage()
	{
		return HERO_CANNOT_CONSUME_MORE_CREATURES;
	}

	@Override
	protected void subtractPopulation( EconomyCreature aShopItem )
	{
		int currentPopulation = getShopPopulation().get( aShopItem.getTier() - 1 )
		                                           .getAmount();
		int populationToSubtract = aShopItem.getAmount();

		if ( currentPopulation >= populationToSubtract )
		{
			getShopPopulation().remove( aShopItem.getTier() - 1 );
			getShopPopulation().add( aShopItem.getTier() - 1, creatureFactory.create( aShopItem.getTier(), currentPopulation - populationToSubtract ) );
		}
		else
		{
			throw new IllegalStateException( getSubtractPopulationErrorMessage() );
		}
	}

	@Override
	protected void subtractGold( Player aActivePlayer, EconomyCreature aShopItem )
	{
		aActivePlayer.subtractGold( aShopItem.getGoldCost() * aShopItem.getAmount() );
	}

	@Override
	protected void restoreGold( Player aActivePlayer, EconomyCreature aShopItem )
	{
		aActivePlayer.addGold( aShopItem.getAmount() * aShopItem.getGoldCost() );
	}

	int getCurrentPopulation( int aTier )
	{
		return getShopPopulation().get( aTier - 1 )
		                          .getAmount();
	}

	List<String> getCurrentCreaturePopulation()
	{
		return List.copyOf( getShopPopulation().stream()
		                                       .map( EconomyCreature::toString )
		                                       .collect( Collectors.toList() ) );
	}
}
