/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures;

import com.google.common.collect.Range;
import pl.etp.items.EconomyItemIf;
import pl.etp.creatures.statistic.CreatureStatistic;

import java.util.Objects;

public class EconomyCreature implements EconomyItemIf
{
	private final CreatureStatistic stats;
	private int amount;
	private final int goldCost;

	EconomyCreature( CreatureStatistic aStats, int aAmount, int aGoldCost )
	{
		stats = aStats;
		amount = aAmount;
		goldCost = aGoldCost;
	}

	public CreatureStatistic getStats()
	{
		return stats;
	}

	public int getAmount()
	{
		return amount;
	}

	public int getGoldCost()
	{
		return goldCost;
	}

	public String getName()
	{
		return stats.getName();
	}

	public int getArmor()
	{
		return stats.getArmor();
	}

	public int getMaxHp()
	{
		return stats.getMaxHp();
	}

	public int getMoveRange()
	{
		return stats.getMoveRange();
	}

	public Range<Integer> getDamage()
	{
		return stats.getDamage();
	}

	public String getDescription()
	{
		return stats.getDescription();
	}

	public int getGrowth()
	{
		return stats.getGrowth();
	}

	public int getTier()
	{
		return stats.getTier();
	}

	@Override
	public String toString()
	{
		return "{\"creature\"=" + stats.toStringEconomy() + ", \"goldCost\"=" + goldCost + ", \"amount\"=" + amount + '}';
	}

	@Override
	public boolean equals( Object aO )
	{
		if ( this == aO )
		{
			return true;
		}
		if ( aO == null || getClass() != aO.getClass() || getName() == null )
		{
			return false;
		}
		EconomyCreature that = ( EconomyCreature ) aO;
		return getName().equals( that.getName() );
	}

	@Override
	public int hashCode()
	{
		return Objects.hash( getName() );
	}

	public void addAmount( int aAmount )
	{
		amount += aAmount;
	}
}
