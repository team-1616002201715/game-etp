/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.creatures;

import pl.etp.creatures.statistic.CreatureStatistic;

public class EconomyFederalStateFactory extends AbstractEconomyFractionFactory
{
	@Override
	public EconomyCreature create( int aTier, int aAmount )
	{
		switch ( aTier )
		{
			case 1:
				return new EconomyCreature( CreatureStatistic.HEDGEHOG, aAmount, 60 );
			case 2:
				return new EconomyCreature( CreatureStatistic.RAILGUN, aAmount, 100 );
			case 3:
				return new EconomyCreature( CreatureStatistic.SPECTRE, aAmount, 200 );
			case 4:
				return new EconomyCreature( CreatureStatistic.NEW_YORKER, aAmount, 360 );
			case 5:
				return new EconomyCreature( CreatureStatistic.HANDYMAN, aAmount, 550 );
			case 6:
				return new EconomyCreature( CreatureStatistic.TROOP_CARRIER, aAmount, 1200 );
			case 7:
				return new EconomyCreature( CreatureStatistic.MARAUDER, aAmount, 1500 );
			case 8:
				return new EconomyCreature( CreatureStatistic.PATRIOT, aAmount, 1800 );
			default:
				throw new IllegalArgumentException( INVALID_TIER_MESSAGE );
		}
	}

	@Override
	public EconomyCreature creatureFromName( String aCreatureName, int aAmount )
	{
		switch ( aCreatureName )
		{
			case "Hedgehog":
				return create( 1, aAmount );
			case "Railgun":
				return create( 2, aAmount );
			case "Spectre":
				return create( 3, aAmount );
			case "New Yorker":
				return create( 4, aAmount );
			case "Handyman":
				return create( 5, aAmount );
			case "Troop Carrier":
				return create( 6, aAmount );
			case "Marauder":
				return create( 7, aAmount );
			case "Patriot":
				return create( 8, aAmount );
			default:
				throw new IllegalArgumentException( INVALID_TIER_MESSAGE );
		}
	}
}
