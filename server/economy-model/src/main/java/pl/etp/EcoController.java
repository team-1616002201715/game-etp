/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp;

import lombok.Getter;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.hero.Fraction;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.hero.AbstractEconomyHeroFactory;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;
import pl.etp.player.Player;

import java.util.UUID;

/**
 * In parallel, we will have to create an EconomyEngine for each player and react to the actions of each of them.
 * Then we have to collect the created heroes at the same time and convert them to GameEngine with the battle model
 */
@Getter
public class EcoController
{
	private EconomyEngine player1economyEngine;
	private EconomyEngine player2economyEngine;
	private Fraction player1Fraction;
	private Fraction player2Fraction;
	public final String gameId;
	private final String INVALID_PLAYER_SEAT = "Invalid player seat indicated";
	private final String INVALID_ITEM_TYPE = "Invalid item type indicated";

	public EcoController()
	{
		gameId = UUID.randomUUID()
		             .toString();
	}


	/**
	 * Creates the economy engine for given player which will be used for any future economy operations.
	 *
	 * @param aHeroName   hero selected by the player during hero selection
	 * @param aPlayerSeat number of the seat the player is set in (1 meaning host of the lobby, 2 meaning the host's oponnent)
	 */
	public void createPlayersEconomyEngine( String aPlayerSeat, String aHeroName )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			player1economyEngine = new EconomyEngine( new Player( player1Fraction, 10000, aHeroName ) );
			provideShopsAndGoldInformation( aPlayerSeat );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			player2economyEngine = new EconomyEngine( new Player( player2Fraction, 10000, aHeroName ) );
			provideShopsAndGoldInformation( aPlayerSeat );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void provideFractionInformation()
	{
		GameEventsService.broadcastFractions( gameId, Fraction.values() );
	}

	public void pickFraction( String aPlayerSeat, String aFractioName )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			player1Fraction = Fraction.valueOf( aFractioName );
			GameEventsService.broadcastOpponentsProgress( gameId, aPlayerSeat, 1);
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			player2Fraction = Fraction.valueOf( aFractioName );
			GameEventsService.broadcastOpponentsProgress( gameId, aPlayerSeat, 1);
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void provideAvailableHeroes( String aPlayerSeat )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			GameEventsService.broadcastHeroes( gameId, aPlayerSeat, AbstractEconomyHeroFactory.getInstance( player1Fraction )
			                                                                                  .getAllHeroes() );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			GameEventsService.broadcastHeroes( gameId, aPlayerSeat, AbstractEconomyHeroFactory.getInstance( player2Fraction )
			                                                                                  .getAllHeroes() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void pickHero( String aPlayerSeat, String aHeroName )
	{
		createPlayersEconomyEngine( aPlayerSeat, aHeroName );
		GameEventsService.broadcastOpponentsProgress( gameId, aPlayerSeat, 2);
	}


	private void provideShopsAndGoldInformation( String aPlayerSeat )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			GameEventsService.broadcastCreatureShop( gameId, aPlayerSeat, player1economyEngine.getCurrentCreaturePopulation() );
			GameEventsService.broadcastSpellShop( gameId, aPlayerSeat, player1economyEngine.getCurrentSpellPopulation() );
			GameEventsService.broadcastArtifactShop( gameId, aPlayerSeat, player1economyEngine.getCurrentArtifactPopulation() );
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player1economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			GameEventsService.broadcastCreatureShop( gameId, aPlayerSeat, player2economyEngine.getCurrentCreaturePopulation() );
			GameEventsService.broadcastSpellShop( gameId, aPlayerSeat, player2economyEngine.getCurrentSpellPopulation() );
			GameEventsService.broadcastArtifactShop( gameId, aPlayerSeat, player2economyEngine.getCurrentArtifactPopulation() );
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player2economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void buyItem( String aPlayerSeat, String aITEM_type, String aItemName, int aAmount )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			switch ( aITEM_type )
			{
				case "CREATURE":
					player1economyEngine.buyCreature( AbstractEconomyFractionFactory.getInstance( player1Fraction )
					                                                                .creatureFromName( aItemName, aAmount ) );
					break;
				case "SPELL":
					player1economyEngine.buySpell( ( EconomySpell ) AbstractEconomyItemFactory.getInstance( FactoryType.SPELL )
					                                                                          .create( aItemName ) );
					break;
				case "ARTIFACT":
					player1economyEngine.buyArtifact( ( EconomyArtifact ) AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT )
					                                                                                .create( aItemName ) );
					break;
				default:
					throw new IllegalArgumentException( INVALID_ITEM_TYPE );
			}
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player1economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			switch ( aITEM_type )
			{
				case "CREATURE":
					player2economyEngine.buyCreature( AbstractEconomyFractionFactory.getInstance( player2Fraction )
					                                                                .creatureFromName( aItemName, aAmount ) );
					break;
				case "SPELL":
					player2economyEngine.buySpell( ( EconomySpell ) AbstractEconomyItemFactory.getInstance( FactoryType.SPELL )
					                                                                          .create( aItemName ) );
					break;
				case "ARTIFACT":
					player2economyEngine.buyArtifact( ( EconomyArtifact ) AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT )
					                                                                                .create( aItemName ) );
					break;
				default:
					throw new IllegalArgumentException( INVALID_ITEM_TYPE );
			}
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player2economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void sellItem( String aPlayerSeat, String aITEM_type, String aItemName, int aAmount )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			switch ( aITEM_type )
			{
				case "CREATURE":
					player1economyEngine.sellCreature( AbstractEconomyFractionFactory.getInstance( player1Fraction )
					                                                                 .creatureFromName( aItemName, aAmount ) );
					break;
				case "SPELL":
					player1economyEngine.sellSpell( ( EconomySpell ) AbstractEconomyItemFactory.getInstance( FactoryType.SPELL )
					                                                                           .create( aItemName ) );
					break;
				case "ARTIFACT":
					player1economyEngine.sellArtifact( ( EconomyArtifact ) AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT )
					                                                                                 .create( aItemName ) );
					break;
				default:
					throw new IllegalArgumentException( INVALID_ITEM_TYPE );
			}
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player1economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			switch ( aITEM_type )
			{
				case "CREATURE":
					player2economyEngine.sellCreature( AbstractEconomyFractionFactory.getInstance( player2Fraction )
					                                                                 .creatureFromName( aItemName, aAmount ) );
					break;
				case "SPELL":
					player2economyEngine.sellSpell( ( EconomySpell ) AbstractEconomyItemFactory.getInstance( FactoryType.SPELL )
					                                                                           .create( aItemName ) );
					break;
				case "ARTIFACT":
					player2economyEngine.sellArtifact( ( EconomyArtifact ) AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT )
					                                                                                 .create( aItemName ) );
					break;
				default:
					throw new IllegalArgumentException( INVALID_ITEM_TYPE );
			}
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player2economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

	public void buyMaxCreatures( String aPlayerSeat, String aItemName )
	{
		if ( aPlayerSeat.equals( "1" ) )
		{
			player1economyEngine.buyCreature( AbstractEconomyFractionFactory.getInstance( player1Fraction )
			                                                                .creatureFromName( aItemName, player1economyEngine.calculateCreatureMaxAmount( aItemName ) ) );
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player1economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else if ( aPlayerSeat.equals( "2" ) )
		{
			player2economyEngine.buyCreature( AbstractEconomyFractionFactory.getInstance( player2Fraction )
			                                                                .creatureFromName( aItemName, player2economyEngine.calculateCreatureMaxAmount( aItemName ) ) );
			GameEventsService.broadcastPlayerGold( gameId, aPlayerSeat, player2economyEngine.getPlayer()
			                                                                                .getGold() );
		}
		else
		{
			throw new IllegalArgumentException( INVALID_PLAYER_SEAT );
		}
	}

}
