/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.hero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomyTestArtifactFactory;

import java.util.stream.Collectors;

import static artifacts.ArtifactStatistic.*;
import static org.junit.jupiter.api.Assertions.*;

class ArtifactListTest
{
	private ArtifactList artifactList;
	private final EconomyTestArtifactFactory factory = ( EconomyTestArtifactFactory ) AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT_TEST );
	private EconomyArtifact artifact;

	@BeforeEach
	void init()
	{
		artifactList = new ArtifactList();
		artifact = factory.create( STATEMANS_MEDAL.getName() );
	}

	@Test
	void shouldCorrectlyAddArtifact()
	{
		//when
		artifactList.add( artifact );

		//then
		assertTrue( artifactList.contains( artifact ) );
	}

	@Test
	void shouldCorrectlyRemoveArtifact()
	{
		//given
		artifactList.add( artifact );

		//when
		artifactList.remove( artifact );

		//then
		assertFalse( artifactList.contains( artifact ) );
	}

	@Test
	void shouldNotAddArtifactWhenSlotFull()
	{
		//given
		EconomyArtifact secondArtifactWithTheSameSlot = factory.create( COLLAR_OF_CONJURING.getName() );

		//when
		artifactList.add( artifact );

		//then
		assertThrows( IllegalStateException.class, () -> artifactList.add( secondArtifactWithTheSameSlot ) );
	}

	@Test
	void shouldReturnListOfAllArtifacts()
	{
		//given
		EconomyArtifact artifact2 = factory.create( GOLDEN_BOW.getName() );

		//when
		artifactList.add( artifact );
		artifactList.add( artifact2 );

		//then
		assertEquals( 2, artifactList.getArtifacts()
		                             .size() );
		assertTrue( artifactList.getArtifacts()
		                        .stream()
		                        .map( EconomyArtifact::getName )
		                        .collect( Collectors.toList() )
		                        .contains( artifact.getName() ) );
		assertTrue( artifactList.getArtifacts()
		                        .stream()
		                        .map( EconomyArtifact::getName )
		                        .collect( Collectors.toList() )
		                        .contains( artifact2.getName() ) );
	}
}