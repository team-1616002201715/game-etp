/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.player.Player;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreatureShopCalculatorTest
{
	private final AbstractEconomyFractionFactory creatureFactory = AbstractEconomyFractionFactory.getInstance( Fraction.FEDERAL_STATE );
	EconomyCreature creature;

	@BeforeEach
	void init()
	{
		creature = creatureFactory.create( 1, 1 );
	}

	@Test
	void shouldCorrectlyCalculateMaxAmountToBuyWhenGrowthIsSmallerThanPurchaseOpportunity()
	{
		Player player = new Player( Fraction.FEDERAL_STATE, 3000 );
		CreatureShopCalculator calculator = new CreatureShopCalculator();
		assertEquals( 12, calculator.calculateMaxAmount( player.getGold(), creature.getGrowth(), creature.getGoldCost() ) );
	}

	@Test
	void shouldCorrectlyCalculateMaxAmountToBuyWhenGrowthIsBiggerThanPurchaseOpportunity()
	{
		Player player = new Player( Fraction.FEDERAL_STATE, 600 );
		CreatureShopCalculator calculator = new CreatureShopCalculator();
		assertEquals( 10, calculator.calculateMaxAmount( player.getGold(), creature.getGrowth(), creature.getGoldCost() ) );
	}

	@Test
	void shouldCorrectlyCalculateMaxAmountToBuyWhenGrowthEqualsPurchaseOpportunity()
	{
		Player player = new Player( Fraction.FEDERAL_STATE, 720 );
		CreatureShopCalculator calculator = new CreatureShopCalculator();
		assertEquals( 12, calculator.calculateMaxAmount( player.getGold(), creature.getGrowth(), creature.getGoldCost() ) );
	}
}