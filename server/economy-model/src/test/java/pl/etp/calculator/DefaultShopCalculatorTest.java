/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.calculator;

import org.junit.jupiter.api.Test;
import pl.etp.player.Player;
import pl.etp.creatures.hero.Fraction;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;

import static artifacts.ArtifactStatistic.TEST_RING_OF_VITALITY;
import static org.junit.jupiter.api.Assertions.*;

class DefaultShopCalculatorTest
{

	private final AbstractEconomyItemFactory artifactFactory = AbstractEconomyItemFactory.getInstance( FactoryType.ARTIFACT_TEST );
	EconomyArtifact artifact;


	@Test
	void shouldReturn1IfPlayerCanAffordToFewArtifactsPurchase()
	{
		artifact = ( EconomyArtifact ) artifactFactory.create( TEST_RING_OF_VITALITY.getName() );
		Player aPlayer = new Player( Fraction.FEDERAL_STATE, 3000000 );
		DefaultShopCalculator aCalculator = new DefaultShopCalculator();
		assertEquals( 1, aCalculator.calculateMaxAmount( aPlayer.getGold(), artifact.getGrowth(), artifact.getGoldCost() ) );
	}

	@Test
	void shouldReturn1IfPlayerCanAffordToExactlyOneArtifactPurchase()
	{
		artifact = ( EconomyArtifact ) artifactFactory.create( TEST_RING_OF_VITALITY.getName() );
		Player aPlayer = new Player( Fraction.FEDERAL_STATE, 5000 );
		DefaultShopCalculator aCalculator = new DefaultShopCalculator();
		assertEquals( 1, aCalculator.calculateMaxAmount( aPlayer.getGold(), artifact.getGrowth(), artifact.getGoldCost() ) );
	}

	@Test
	void shouldReturn0IfPlayerCanNotAffordToArtifactPurchase()
	{
		artifact = ( EconomyArtifact ) artifactFactory.create( TEST_RING_OF_VITALITY.getName() );
		Player aPlayer = new Player( Fraction.FEDERAL_STATE, 4000 );
		DefaultShopCalculator aCalculator = new DefaultShopCalculator();
		assertEquals( 0, aCalculator.calculateMaxAmount( aPlayer.getGold(), artifact.getGrowth(), artifact.getGoldCost() ) );
	}
}