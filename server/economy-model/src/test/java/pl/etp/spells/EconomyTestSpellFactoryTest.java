/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.spells;

import org.junit.jupiter.api.Test;
import pl.etp.creatures.spells.FactoryType;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomySpell;

import static org.junit.jupiter.api.Assertions.*;
import static pl.etp.creatures.spells.SpellStatistic.*;

class EconomyTestSpellFactoryTest
{
	public static final String EXPECTED_NAME = "Haste";

	@Test
	void factoryShouldCreateSpellCorrectly()
	{
		AbstractEconomyItemFactory factory = AbstractEconomyItemFactory.getInstance( FactoryType.SPELL_TEST );
		EconomySpell spell = ( EconomySpell ) factory.create( TEST_HASTE.getName() );

		assertEquals( EXPECTED_NAME, spell.getName() );
	}
}