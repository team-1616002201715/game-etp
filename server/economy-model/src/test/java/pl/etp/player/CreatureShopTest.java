/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.EconomyEngine;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.creatures.hero.Fraction.FEDERAL_STATE;
import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;

class CreatureShopTest
{
	EconomyEngine economyEngine;
	private final Fraction fraction = FEDERAL_STATE;
	private final AbstractEconomyFractionFactory creatureFactory = AbstractEconomyFractionFactory.getInstance( fraction );

	@BeforeEach
	void init()
	{
		Random rand = mock( Random.class );
		when( rand.nextDouble() ).thenReturn( 1.0 );

		CreatureShop shop = new CreatureShop( rand, fraction );
		Player player = new Player( fraction, 1000, JOHN_TERRY.getName(), shop );
		economyEngine = new EconomyEngine( player );
	}

	@Test
	void toGameStartCurrentPopulationShouldBeEqualToCreatureGrowth()
	{
		assertEquals( 12, economyEngine.getCurrentPopulation( 1 ) );
		assertEquals( 8, economyEngine.getCurrentPopulation( 2 ) );
		assertEquals( 7, economyEngine.getCurrentPopulation( 3 ) );
		assertEquals( 4, economyEngine.getCurrentPopulation( 4 ) );
		assertEquals( 3, economyEngine.getCurrentPopulation( 5 ) );
	}


	@Test
	void afterPurchaseCreaturePopulationShouldBeDecreasedByBoughtAmount()
	{
		//when
		economyEngine.buyCreature( creatureFactory.create( 1, 10 ) );
		//then
		assertEquals( 2, economyEngine.getCurrentPopulation( 1 ) );
	}

	@Test
	void shouldThrowExceptionWhenTryToPurchaseMoreCreatureThanPopulationIs()
	{
		EconomyCreature creatureOverPopulation = creatureFactory.create( 1, 13 );
		assertThrows( IllegalStateException.class, () -> economyEngine.buyCreature( creatureOverPopulation ) );
		assertEquals( 12, economyEngine.getCurrentPopulation( 1 ) );
	}
}