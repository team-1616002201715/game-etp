/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.EconomyEngine;
import pl.etp.creatures.hero.Fraction;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;

import java.util.Random;

import static artifacts.ArtifactStatistic.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.creatures.hero.Fraction.FEDERAL_STATE;
import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;
import static pl.etp.creatures.spells.FactoryType.ARTIFACT_TEST;

class BuyingArtifactTest
{
	EconomyEngine economyEngine;
	private final Fraction fraction = FEDERAL_STATE;
	private final AbstractEconomyItemFactory artifactFactory = AbstractEconomyItemFactory.getInstance( ARTIFACT_TEST );
	Player player;
	EconomyArtifact economyArtifact;

	@BeforeEach
	void init()
	{
		Random rand = mock( Random.class );
		when( rand.nextInt( anyInt() ) ).thenReturn( 6 );
		ArtifactShop shop = new ArtifactShop( rand, artifactFactory );
		player = new Player( fraction, 15000, JOHN_TERRY.getName(), shop );
		economyEngine = new EconomyEngine( player );
		economyArtifact = ( EconomyArtifact ) artifactFactory.create( TEST_RING_OF_VITALITY.getName() );
	}

	@Test
	void shouldCorrectlyCreateArtifactPopulation()
	{
		assertEquals( 13, economyEngine.getCurrentArtifactPopulation()
		                               .size() );
	}

	@Test
	void heroShouldCanBuyArtifact()
	{
		//when
		economyEngine.buyArtifact( economyArtifact );
		//then
		assertEquals( 10000, player.getGold() );
		assertEquals( 1, player.getArtifacts()
		                       .size() );
	}

	@Test
	void afterPurchaseArtifactPopulationShouldBeDecreasedByBoughtOne()
	{
		//when
		economyEngine.buyArtifact( economyArtifact );
		//then
		assertEquals( 12, economyEngine.getCurrentArtifactPopulation()
		                               .size() );
	}

	@Test
	void shouldThrowExceptionWhenTryToPurchaseTheSameArtifactAgain()
	{
		//when
		economyEngine.buyArtifact( economyArtifact );
		//then
		assertThrows( IllegalStateException.class, () -> economyEngine.buyArtifact( economyArtifact ) );
		assertEquals( 12, economyEngine.getCurrentArtifactPopulation()
		                               .size() );
		assertEquals( 1, player.getArtifacts()
		                       .size() );
	}

	@Test
	void heroShouldBuyMoreThanOneArtifact()
	{
		//when
		economyEngine.buyArtifact( economyArtifact );
		economyEngine.buyArtifact( ( EconomyArtifact ) artifactFactory.create( TEST_ARMOR_OF_WONDER.getName() ) );
		//then
		assertEquals( 6000, player.getGold() );
		assertEquals( 2, player.getArtifacts()
		                       .size() );
	}

	@Test
	void heroCannotBuyArtifactWhenHasNotEnoughGold()
	{
		EconomyArtifact artifactOverGold = ( EconomyArtifact ) artifactFactory.create( TEST_TOME_OF_AIR_MAGIC.getName() );
		assertThrows( IllegalStateException.class, () -> economyEngine.buyArtifact( artifactOverGold ) );
		assertEquals( 15000, player.getGold() );
		assertEquals( 0, player.getArtifacts()
		                       .size() );
	}

	@Test
	void heroShouldNotBuyArtifactWhenHasNoEmptySlot()
	{
		economyEngine.buyArtifact( ( EconomyArtifact ) artifactFactory.create( TEST_SURCOAT_OF_COUNTERPOISE.getName() ) );

		assertThrows( IllegalStateException.class, () -> economyEngine.buyArtifact( economyArtifact ) );
		assertEquals( 11000, player.getGold() );
		assertEquals( 1, player.getArtifacts()
		                       .size() );
	}

	@Test
	void shouldCorrectlyCheckIfPlayerHasArtifact()
	{
		//when
		economyEngine.buyArtifact( economyArtifact );
		//then
		assertTrue( economyEngine.hasArtifact( TEST_RING_OF_VITALITY.getName() ) );
		assertFalse( economyEngine.hasArtifact( TEST_COLLAR_OF_CONJURING.getName() ) );
	}

	@Test
	void shouldReturnTrueWhenPlayerCanAffordPurchase()
	{
		assertTrue( economyEngine.canBuyArtifact( economyArtifact ) );
	}
}
