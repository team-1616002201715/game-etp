/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.EconomyEngine;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.creatures.hero.Fraction.FEDERAL_STATE;
import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;

class BuyingCreatureTest
{
	private final Fraction fraction = FEDERAL_STATE;
	private final AbstractEconomyFractionFactory creatureFactory = AbstractEconomyFractionFactory.getInstance( fraction );
	private EconomyEngine economyEngine;
	private Player player;

	@BeforeEach
	void init()
	{
		Random rand = mock( Random.class );
		when( rand.nextDouble() ).thenReturn( 1.0 );
		CreatureShop shop = new CreatureShop( rand, fraction );
		player = new Player( fraction, 6000, JOHN_TERRY.getName(), shop );
		economyEngine = new EconomyEngine( player );
	}

	@Test
	void heroShouldCanBuyCreature()
	{
		economyEngine.buyCreature( creatureFactory.create( 1, 1 ) );

		assertEquals( 5940, player.getGold() );
	}

	@Test
	void heroShouldCanBuyCreatureFromCastleFactory()
	{
		economyEngine.buyCreature( creatureFactory.create( 1, 1 ) );

		assertEquals( 5940, player.getGold() );
	}

	@Test
	void heroShouldCanBuyMoreThanOneCreatureInOneStack()
	{
		economyEngine.buyCreature( creatureFactory.create( 1, 2 ) );

		assertEquals( 5880, player.getGold() );
	}

	@Test
	void heroShouldCanBuyMoreThanOneCreatureInFewStack()
	{
		economyEngine.buyCreature( creatureFactory.create( 1, 2 ) );
		economyEngine.buyCreature( creatureFactory.create( 2, 2 ) );

		assertEquals( 5680, player.getGold() );
	}

	@Test
	void heroCannotBuyCreatureWhenHasNotEnoughGold()
	{
		EconomyCreature creatureOverGoldAmount = creatureFactory.create( 1, 500 );
		assertThrows( IllegalStateException.class, () -> economyEngine.buyCreature( creatureOverGoldAmount ) );
		assertEquals( 6000, player.getGold() );
		assertEquals( 0, player.getCreatures()
		                       .size() );
	}

	@Test
	void heroCannotBuyCreatureIfHeIsFull()
	{
		economyEngine.buyCreature( creatureFactory.create( 1, 1 ) );
		economyEngine.buyCreature( creatureFactory.create( 2, 1 ) );
		economyEngine.buyCreature( creatureFactory.create( 3, 1 ) );
		economyEngine.buyCreature( creatureFactory.create( 4, 1 ) );
		economyEngine.buyCreature( creatureFactory.create( 5, 1 ) );

		EconomyCreature creatureOverLimit = creatureFactory.create( 6, 1 );
		assertThrows( IllegalStateException.class, () -> economyEngine.buyCreature( creatureOverLimit ) );

		assertEquals( 4730, player.getGold() );
		assertEquals( 5, player.getCreatures()
		                       .size() );
	}

	@Test
	void shouldReturnTrueWhenPlayerCanAffordPurchase()
	{
		assertTrue( economyEngine.canBuyCreature( creatureFactory.create( 1, 1 ) ) );
	}
}
