/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.player;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.EconomyEngine;
import pl.etp.creatures.hero.Fraction;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomySpell;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.creatures.hero.Fraction.FEDERAL_STATE;
import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;
import static pl.etp.creatures.spells.FactoryType.SPELL_TEST;
import static pl.etp.creatures.spells.SpellStatistic.*;

class BuyingSpellsTest
{
	EconomyEngine economyEngine;
	private final Fraction fraction = FEDERAL_STATE;
	private final AbstractEconomyItemFactory spellFactory = AbstractEconomyItemFactory.getInstance( SPELL_TEST );
	Player player;
	private EconomySpell testSpell;

	@BeforeEach
	void init()
	{
		testSpell = ( EconomySpell ) spellFactory.create( TEST_HASTE.getName() );
		Random playerRand = mock( Random.class );
		when( playerRand.nextInt( anyInt() ) ).thenReturn( 4 );
		SpellShop shop = new SpellShop( playerRand, spellFactory );
		player = new Player( fraction, 1000, JOHN_TERRY.getName(), shop );
		economyEngine = new EconomyEngine( player );
	}

	@Test
	void shouldCorrectlyCreateSpellPopulation()
	{
		assertEquals( 9, economyEngine.getCurrentSpellPopulation()
		                              .size() );
	}

	@Test
	void playerShouldCanBuySpell()
	{
		//when
		economyEngine.buySpell( testSpell );

		//then
		assertEquals( 900, player.getGold() );
		assertEquals( 1, player.getSpells()
		                       .size() );
	}

	@Test
	void playerShouldCanBuyMoreThanOneSpell()
	{
		//when
		economyEngine.buySpell( testSpell );
		economyEngine.buySpell( ( EconomySpell ) spellFactory.create( TEST_MAGIC_ARROW.getName() ) );

		//then
		assertEquals( 700, player.getGold() );
		assertEquals( 2, player.getSpells()
		                       .size() );
	}

	@Test
	void heroCannotBuyCreatureWhenHasNotEnoughGold()
	{
		//given
		EconomySpell spellOverGold = ( EconomySpell ) spellFactory.create( TEST_TELEPORT.getName() );

		//when
		assertThrows( IllegalStateException.class, () -> economyEngine.buySpell( spellOverGold ) );

		//then
		assertEquals( 1000, player.getGold() );
		assertEquals( 0, player.getSpells()
		                       .size() );
	}

	@Test
	void afterPurchaseSpellPopulationShouldBeDecreasedByBought1()
	{
		//when
		economyEngine.buySpell( testSpell );
		//then
		assertEquals( 8, economyEngine.getCurrentSpellPopulation()
		                              .size() );
	}

	@Test
	void shouldThrowExceptionWhenTryToPurchaseTheSameSpellAgain()
	{
		//given
		economyEngine.buySpell( testSpell );

		//when
		assertThrows( IllegalStateException.class, () -> economyEngine.buySpell( testSpell ) );

		//then
		assertEquals( 8, economyEngine.getCurrentSpellPopulation()
		                              .size() );
		assertEquals( 1, player.getSpells()
		                       .size() );
	}

	@Test
	void shouldCorrectlyCheckIfPlayerHasSpell()
	{
		//when
		economyEngine.buySpell( testSpell );

		//then
		assertTrue( economyEngine.hasSpell( TEST_HASTE.getName() ) );
		assertFalse( economyEngine.hasSpell( TELEPORT.getName() ) );
	}

	@Test
	void shouldReturnTrueWhenPlayerCanAffordPurchase()
	{
		assertTrue( economyEngine.canBuySpell( testSpell ) );
	}
}
