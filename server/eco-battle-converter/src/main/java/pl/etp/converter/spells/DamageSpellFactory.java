/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.spells;

import pl.etp.creatures.spells.SpellStatistic;
import pl.etp.spells.AbstractSpell;
import pl.etp.items.EconomySpell;
import pl.etp.spells.DamageSpell;

import static pl.etp.creatures.spells.SpellStatistic.*;

class DamageSpellFactory extends AbstractSpellFactory
{

	@Override
	AbstractSpell createInner( EconomySpell aEconomySpell, int aHeroPower )
	{
		switch ( aEconomySpell.getSpellStatistic() )
		{
			case FIRE_BALL:
				return new DamageSpell( aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aHeroPower * 10 + 15, 3, aEconomySpell.getName() );
			case IMPLOSION:
				return new DamageSpell( aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), 75 * aHeroPower + 100, 0, aEconomySpell.getName() );
			case DEATH_RIPPLE:
				return new DamageSpell( aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), 5 * aHeroPower + 10, 0, aEconomySpell.getName() );
			case MAGIC_ARROW:
				return new DamageSpell( aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), 10 * aHeroPower + 10, 0, aEconomySpell.getName() );
			default:
				throw new UnsupportedOperationException( UNSUPPORTED_SPELL + aEconomySpell.getSpellType() );
		}

	}
}
