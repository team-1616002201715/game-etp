/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.spells;

import pl.etp.items.EconomySpell;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.EffectStats;
import pl.etp.spells.StatusEffectSpell;

class StatusEffectSpellFactory extends AbstractSpellFactory
{
	@Override
	AbstractSpell createInner( EconomySpell aEconomySpell, int aHeroPower )
	{
		switch ( aEconomySpell.getSpellStatistic() )
		{
			case HASTE:
				return new StatusEffectSpell( 3, EffectStats.builder()
				                                            .moveRange( 4 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			case BLOODLUST:
				return new StatusEffectSpell( 2, EffectStats.builder()
				                                            .armorPercentage( 0.2 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			case DISPEL:
				return new StatusEffectSpell( 4, EffectStats.builder()
				                                            .armor( 5 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			case TELEPORT:
				return new StatusEffectSpell( 2, EffectStats.builder()
				                                            .damagePercentage( 0.3 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			case SLOW:
				return new StatusEffectSpell( 3, EffectStats.builder()
				                                            .moveRangePercentage( -0.5 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			case TEST_HASTE:
				return new StatusEffectSpell( 2, EffectStats.builder()
				                                            .moveRange( 4 )
				                                            .build(), aEconomySpell.getManaCost(), aEconomySpell.getTargetType(), aEconomySpell.getElement(), aEconomySpell.getName() );
			default:
				throw new UnsupportedOperationException( UNSUPPORTED_SPELL + aEconomySpell.getSpellType() );
		}

	}
}
