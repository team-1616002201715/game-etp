/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.artifacts;

import pl.etp.items.EconomyArtifact;

public abstract class AbstractArtifactFactory
{

	private static final String INVALID_ARTIFACT_TYPE = "Cannot recognize artifact target type ";
	protected static final String CANNOT_RECOGNIZE_ARTIFACT = "Cannot recognize artifact";

	public static AbstractArtifact create( EconomyArtifact aEconomyArtifact )
	{
		switch ( aEconomyArtifact.getArtifactStats()
		                         .getTargetType() )
		{
			case CREATURE:
				return new CreatureArtifactFactory().createInner( aEconomyArtifact );
			case SPELL:
				return new SpellArtifactFactory().createInner( aEconomyArtifact );
			default:
				throw new UnsupportedOperationException( INVALID_ARTIFACT_TYPE + aEconomyArtifact.getArtifactStats()
				                                                                                 .getTargetType() );
		}
	}

	abstract AbstractArtifact createInner( EconomyArtifact aEconomyArtifact );
}
