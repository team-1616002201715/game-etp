/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.artifacts;

import pl.etp.items.EconomyArtifact;
import pl.etp.spells.EffectStats;

class SpellArtifactFactory extends AbstractArtifactFactory
{
	@Override
	AbstractArtifact createInner( EconomyArtifact aEconomyArtifact )
	{
		switch ( aEconomyArtifact.getArtifactStats() )
		{
			case COLLAR_OF_CONJURING:
				return new SpellArtifact( aEconomyArtifact.getArtifactStats()
				                                          .getName(), EffectStats.builder()
				                                                                 .damagePercentage( 0.25 )
				                                                                 .build() );
			case TOME_OF_AIR_MAGIC:
			case TEST_TOME_OF_AIR_MAGIC:
				return new SpellArtifact( aEconomyArtifact.getArtifactStats()
				                                          .getName(), EffectStats.builder()
				                                                                 .spellDuration( 1 )
				                                                                 .build() );
			case SHOES_OF_BLIGHTS:
				return new SpellArtifact( aEconomyArtifact.getArtifactStats()
				                                          .getName(), EffectStats.builder()
				                                                                 .damagePercentage( 0.2 )
				                                                                 .build() );
			case STATEMANS_MEDAL:
				return new SpellArtifact( aEconomyArtifact.getArtifactStats()
				                                          .getName(), EffectStats.builder()
				                                                                 .spellDuration( 2 )
				                                                                 .build() );
			case BLACKSHARD_OF_THE_DEAD_KNIGHT:
				return new SpellArtifact( aEconomyArtifact.getArtifactStats()
				                                          .getName(), EffectStats.builder()
				                                                                 .spellDamage( 3 )
				                                                                 .build() );
			default:
				throw new UnsupportedOperationException( CANNOT_RECOGNIZE_ARTIFACT );
		}
	}
}
