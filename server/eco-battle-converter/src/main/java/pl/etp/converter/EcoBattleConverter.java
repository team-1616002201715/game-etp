/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter;

import pl.etp.Hero;
import pl.etp.converter.artifacts.AbstractArtifactFactory;
import pl.etp.converter.spells.AbstractSpellFactory;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.player.Player;
import pl.etp.spells.AbstractSpell;
import pl.etp.spells.SpellBook;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EcoBattleConverter
{
	private EcoBattleConverter()
	{
	}

	public static Hero convert( Player aPlayer )
	{
		List<Creature> creatures = new ArrayList<>();
		aPlayer.getCreatures()
		       .forEach( aEconomyCreature -> creatures.add( CreatureFactory.create( aEconomyCreature.getStats(), aEconomyCreature.getAmount() ) ) );

		List<AbstractSpell> spells = aPlayer.getSpells()
		                                    .stream()
		                                    .map( aEconomySpell -> AbstractSpellFactory.create( aEconomySpell, aPlayer.getPower() ) )
		                                    .collect( Collectors.toList() );

		Hero hero = new Hero( creatures, new SpellBook( aPlayer.getMana(), spells ) );
		aPlayer.getArtifacts()
		       .stream()
		       .map( AbstractArtifactFactory::create )
		       .forEach( a -> a.apply( hero ) );

		return hero;
	}
}
