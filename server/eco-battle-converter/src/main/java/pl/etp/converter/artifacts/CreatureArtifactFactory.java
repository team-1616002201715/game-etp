/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.artifacts;

import pl.etp.items.EconomyArtifact;
import pl.etp.spells.EffectStats;

class CreatureArtifactFactory extends AbstractArtifactFactory
{
	@Override
	public AbstractArtifact createInner( EconomyArtifact aEconomyArtifact )
	{
		switch ( aEconomyArtifact.getArtifactStats() )
		{
			case GOLDEN_BOW:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .damagePercentage( 0.3 )
				                                                                    .build() );
			case PENDANT_OF_COURAGE:
			case ARMOR_OF_WONDER:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .armorPercentage( 0.2 )
				                                                                    .build() );
			case SHIELD_OF_THE_YAWNING_DEAD:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .armor( 2 )
				                                                                    .build() );
			case PENDANT_OF_LIFE:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .maxHpPercentage( 0.2 )
				                                                                    .build() );
			case ORB_OF_TEMPESTOUS_FIRE:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .attackPercentage( 0.15 )
				                                                                    .build() );
			case LIFES_BOOTS:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .maxHp( 2 )
				                                                                    .build() );
			case RING_OF_VITALITY:
			case TEST_RING_OF_VITALITY:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .maxHp( 1 )
				                                                                    .build() );
			case RING_OF_THE_WAYFARER:
			case TEST_RING_OF_THE_WAYFARER:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .moveRange( 1 )
				                                                                    .build() );
			case SURCOAT_OF_COUNTERPOISE:
			case TEST_SURCOAT_OF_COUNTERPOISE:
				return new CreatureArtifact( aEconomyArtifact.getArtifactStats()
				                                             .getName(), EffectStats.builder()
				                                                                    .magicResistancePercentage( 0.1 )
				                                                                    .build() );
			default:
				throw new UnsupportedOperationException( CANNOT_RECOGNIZE_ARTIFACT );
		}
	}
}