/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.artifacts;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.Hero;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.spells.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SpellArtifactTest
{

	private Hero hero;
	private Creature creature;

	@BeforeEach
	void init()
	{
		creature = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );
	}

	@Test
	void shouldIncreaseSpellDamageByOne()
	{
		//given
		AbstractSpell damageSpell = TestSpellFactory.createMagicArrow();
		hero = new Hero( List.of( creature ), new SpellBook( 10, List.of( damageSpell ) ) );
		EffectStats increaseSpellDamage = EffectStats.builder()
		                                             .spellDamage( 1 )
		                                             .build();
		SpellArtifact artifact = new SpellArtifact( "", increaseSpellDamage );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 11, ( ( DamageSpell ) damageSpell ).getDamage() );
	}

	@Test
	void shouldIncreaseSpellDurationByOne()
	{
		//given
		AbstractSpell statusSpell = TestSpellFactory.createBuff();
		hero = new Hero( List.of( creature ), new SpellBook( 10, List.of( statusSpell ) ) );
		EffectStats increaseSpellDamage = EffectStats.builder()
		                                             .spellDuration( 1 )
		                                             .build();
		SpellArtifact artifact = new SpellArtifact( "", increaseSpellDamage );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 3, ( ( StatusEffectSpell ) statusSpell ).getDuration() );
	}

	@Test
	void shouldIncreaseBothSpellDurationAndDamageByOne()
	{
		//given
		AbstractSpell statusSpell = TestSpellFactory.createBuff();
		AbstractSpell damageSpell = TestSpellFactory.createMagicArrow();
		hero = new Hero( List.of( creature ), new SpellBook( 10, List.of( statusSpell, damageSpell ) ) );
		EffectStats increaseSpellDamage = EffectStats.builder()
		                                             .spellDamage( 1 )
		                                             .spellDuration( 1 )
		                                             .build();
		SpellArtifact artifact = new SpellArtifact( "", increaseSpellDamage );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 3, ( ( StatusEffectSpell ) statusSpell ).getDuration() );
		assertEquals( 11, ( ( DamageSpell ) damageSpell ).getDamage() );
	}
}