/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter;

import pl.etp.EconomyEngine;
import pl.etp.creatures.AbstractEconomyFractionFactory;
import pl.etp.creatures.EconomyCreature;
import pl.etp.creatures.hero.Fraction;
import pl.etp.items.AbstractEconomyItemFactory;
import pl.etp.items.EconomyArtifact;
import pl.etp.items.EconomySpell;
import pl.etp.player.ArtifactShop;
import pl.etp.player.CreatureShop;
import pl.etp.player.Player;
import pl.etp.player.SpellShop;

import java.util.Random;

import static artifacts.ArtifactStatistic.TEST_RING_OF_VITALITY;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.etp.creatures.hero.Fraction.FEDERAL_STATE;
import static pl.etp.creatures.hero.HeroStatistic.JOHN_TERRY;
import static pl.etp.creatures.spells.FactoryType.ARTIFACT_TEST;
import static pl.etp.creatures.spells.FactoryType.SPELL_TEST;
import static pl.etp.creatures.spells.SpellStatistic.TEST_HASTE;

class EconomyEngineIT
{
	protected EconomyEngine economyEngine;
	protected final Fraction fraction = FEDERAL_STATE;
	protected final AbstractEconomyItemFactory artifactFactory = AbstractEconomyItemFactory.getInstance( ARTIFACT_TEST );
	protected final AbstractEconomyItemFactory spellFactory = AbstractEconomyItemFactory.getInstance( SPELL_TEST );
	protected final AbstractEconomyFractionFactory creatureFactory = AbstractEconomyFractionFactory.getInstance( fraction );
	protected EconomyArtifact economyArtifact;
	protected EconomySpell economySpell;
	protected EconomyCreature economyCreature;
	protected Player player;

	protected void setUp()
	{
		Random spellRand = mock( Random.class );
		when( spellRand.nextInt( anyInt() ) ).thenReturn( 4 );
		Random creatureRand = mock( Random.class );
		when( creatureRand.nextDouble() ).thenReturn( 1.0 );
		Random artifactRand = mock( Random.class );
		when( artifactRand.nextInt( anyInt() ) ).thenReturn( 6 );

		CreatureShop creatureShop = new CreatureShop( creatureRand, fraction );
		ArtifactShop artifactShop = new ArtifactShop( artifactRand, artifactFactory );
		SpellShop spellShop = new SpellShop( spellRand, spellFactory );

		player = new Player( fraction, 45000, JOHN_TERRY.getName(), creatureShop, spellShop, artifactShop );
		economyEngine = new EconomyEngine( player );

		economyArtifact = ( EconomyArtifact ) artifactFactory.create( TEST_RING_OF_VITALITY.getName() );
		economySpell = ( EconomySpell ) spellFactory.create( TEST_HASTE.getName() );
		economyCreature = creatureFactory.create( 1, 1 );

	}
}
