/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SellingItemsTest extends EconomyEngineIT
{
	@BeforeEach
	void init()
	{
		setUp();
	}

	@Test
	void shouldSellCreature()
	{
		//given
		economyEngine.buyCreature( economyCreature );
		assertEquals( 44940, player.getGold() );
		assertEquals( 1, player.getCreatures()
		                       .size() );

		//when
		economyEngine.sellCreature( economyCreature );

		//then
		assertEquals( 45000, player.getGold() );
		assertEquals( 0, player.getCreatures()
		                       .size() );
	}

	@Test
	void shouldSellSpell()
	{
		//given
		economyEngine.buySpell( economySpell );
		assertEquals( 44900, player.getGold() );
		assertEquals( 1, player.getSpells()
		                       .size() );

		//when
		economyEngine.sellSpell( economySpell );

		//then
		assertEquals( 45000, player.getGold() );
		assertEquals( 0, player.getSpells()
		                       .size() );
	}

	@Test
	void shouldSellArtifact()
	{
		//given
		economyEngine.buyArtifact( economyArtifact );
		assertEquals( 40000, player.getGold() );
		assertEquals( 1, player.getArtifacts()
		                       .size() );

		//when
		economyEngine.sellArtifact( economyArtifact );

		//then
		assertEquals( 45000, player.getGold() );
		assertEquals( 0, player.getArtifacts()
		                       .size() );
	}

	@Test
	void shouldAllowToBuyAfterSale()
	{
		//given
		economyEngine.buyCreature( economyCreature );
		economyEngine.buySpell( economySpell );
		economyEngine.buyArtifact( economyArtifact );

		economyEngine.sellArtifact( economyArtifact );
		economyEngine.sellSpell( economySpell );
		economyEngine.sellCreature( economyCreature );

		//when
		economyEngine.buyCreature( economyCreature );
		economyEngine.buySpell( economySpell );
		economyEngine.buyArtifact( economyArtifact );

		//then
		assertEquals( 39840, player.getGold() );
		assertEquals( 1, player.getArtifacts()
		                       .size() );
		assertEquals( 1, player.getSpells()
		                       .size() );
		assertEquals( 1, player.getCreatures()
		                       .size() );

	}

	@Test
	void shouldStackCreatures()
	{
		//first buy
		economyEngine.buyCreature( economyCreature );
		assertEquals( 44940, player.getGold() );
		assertEquals( 1, player.getCreatures()
		                       .size() );
		assertEquals( 1, player.getCreatures()
		                       .get( 0 )
		                       .getAmount() );

		//second buy
		economyEngine.buyCreature( economyCreature );
		assertEquals( 44880, player.getGold() );
		assertEquals( 1, player.getCreatures()
		                       .size() );
		assertEquals( 2, player.getCreatures()
		                       .get( 0 )
		                       .getAmount() );

		//sale
		economyEngine.sellCreature( economyCreature );
		assertEquals( 45000, player.getGold() );
		assertEquals( 0, player.getCreatures()
		                       .size() );
	}
}
