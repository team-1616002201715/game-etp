/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.Hero;
import pl.etp.items.EconomyArtifact;
import pl.etp.player.Player;
import pl.etp.spells.StatusEffectSpell;

import static artifacts.ArtifactStatistic.TEST_TOME_OF_AIR_MAGIC;
import static org.junit.jupiter.api.Assertions.*;

class EcoBattleConverterTest extends EconomyEngineIT
{
	@BeforeEach
	void init()
	{
		setUp();
	}

	@Test
	void shouldConvertEconomyHeroToBattleHero()
	{
		//given
		Player player = economyEngine.getPlayer();

		//when
		Hero battleHero = EcoBattleConverter.convert( player );

		//then
		assertEquals( 0, battleHero.getCreatures()
		                           .size() );
		assertEquals( 0, battleHero.getSpellBook()
		                           .getSpells()
		                           .size() );

	}

	@Test
	void shouldConvertHeroAndApplyArtifactsEffects()
	{
		//given
		economyEngine.buyCreature( economyCreature );
		economyEngine.buySpell( economySpell );
		economyEngine.buyArtifact( economyArtifact );
		economyEngine.buyArtifact( ( EconomyArtifact ) artifactFactory.create( TEST_TOME_OF_AIR_MAGIC.getName() ) );
		Player player = economyEngine.getPlayer();

		//when
		Hero battleHero = EcoBattleConverter.convert( player );

		//then
		assertEquals( 1, battleHero.getCreatures()
		                           .size() );
		assertEquals( 1, battleHero.getSpellBook()
		                           .getSpells()
		                           .size() );
		assertEquals( 11, battleHero.getCreatures()
		                            .get( 0 )
		                            .getCurrentHp() );
		StatusEffectSpell spell = ( StatusEffectSpell ) battleHero.getSpellBook()
		                                                          .getSpells()
		                                                          .get( 0 );
		assertEquals( 3, spell.getDuration() );


	}

}