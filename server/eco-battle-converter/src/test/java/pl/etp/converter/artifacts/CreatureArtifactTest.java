/*
 * Trifecta
 *
 * Copyright ©  2022 Marcel Szmeterowicz, Jakub Przybyła, Adam Toppmayer, Patryk Biesiada
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package pl.etp.converter.artifacts;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.etp.Hero;
import pl.etp.creatures.Creature;
import pl.etp.creatures.hero.CreatureFactory;
import pl.etp.creatures.statistic.CreatureStatistic;
import pl.etp.spells.EffectStats;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreatureArtifactTest
{
	private Creature creature;
	private Hero hero;

	@BeforeEach
	void init()
	{
		creature = CreatureFactory.create( CreatureStatistic.TEST_NORMAL_GROUND_UNIT, 1 );
		hero = new Hero( List.of( creature ) );
	}

	@Test
	void shouldIncreaseCreatureHealthByOne()
	{
		//given
		EffectStats increaseHealthStats = EffectStats.builder()
		                                             .maxHp( 1 )
		                                             .build();
		CreatureArtifact artifact = new CreatureArtifact( "", increaseHealthStats );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 16, creature.getCurrentHp() );
	}

	@Test
	void shouldIncreaseCreatureHealthByFiftyPercent()
	{
		//given
		EffectStats increaseHealthStats = EffectStats.builder()
		                                             .maxHpPercentage( 0.5 )
		                                             .build();
		CreatureArtifact artifact = new CreatureArtifact( "", increaseHealthStats );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 22, creature.getCurrentHp() );
	}

	@Test
	void shouldIncreaseCreatureMoveRangeByOne()
	{
		//given
		EffectStats increaseMoveRangeStats = EffectStats.builder()
		                                                .moveRange( 1 )
		                                                .build();
		CreatureArtifact artifact = new CreatureArtifact( "", increaseMoveRangeStats );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 8, creature.getMoveRange() );
	}

	@Test
	void shouldIncreaseCreatureMoveRangeByFiftyPercent()
	{
		//given
		EffectStats increaseMoveRangeStats = EffectStats.builder()
		                                                .moveRangePercentage( 0.5 )
		                                                .build();
		CreatureArtifact artifact = new CreatureArtifact( "", increaseMoveRangeStats );
		//when
		artifact.apply( hero );
		//then
		assertEquals( 10, creature.getMoveRange() );
	}

}