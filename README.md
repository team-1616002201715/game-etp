# Trifecta
Project developed by:
* Patryk Biesiada
* Jakub Przybyła
* Marcel Szmeterowicz
* Adam Toppmayer

This software is distributed under the MIT license (see LICENSE) and uses third party libraries that are distributed under their own terms (see THIRD-PARTY-LIST-OF-LICENSES and LICENSE-THIRD-PARTY). 
See [conflu guide](https://team-1616002201715.atlassian.net/wiki/spaces/ETP/pages/186580993/Trifecta+Licensing+101) for more details about licensing process.

## How to run the project

First install Docker Desktop https://www.docker.com/products/docker-desktop

Next thing you need to do is enter the server project folder and run

```sh
mvn clean install
```

In main product directory (where `docker-compose.yml` exists) run

```sh
docker-compose up
```

after running all containers correctly a success message is given:

```sh
client        | Compiled successfully!
client        |
client        | You can now view frontend in the browser.
client        |
client        |   Local:            http://localhost:3000
client        |   On Your Network:  http://192.168.112.3:3000
```

#### Finally, the application can be found here http://localhost:3000

#### Swagger UI is available under http://localhost:8080/swagger-ui/index.html

To remove containers, networks, volumes, and images created by "up" just use

```sh
docker-compose down -v
```

I recommend getting familiar with this wonderful technology https://docs.docker.com/
