import './App.css';
import React, {Component} from 'react'
import CanvasDisplay from "./Components/CanvasDisplay"
import SockJS from "sockjs-client"
import Stomp from "stompjs"
import axios from "axios"
import Message_Timeline from './Tools/Message_Timeline';
import Action_Timeline from './Tools/Action_Timeline';
import Unit from "./Components/Unit"
import Spellbook from './Components/Spellbook';
import ActionMenu from './Components/ActionMenu';
import ActiveCreature from './Components/ActiveCreature';
import PlayerNames from './Components/PlayerNames';
import VictoryScreen from './Components/VictoryScreen';
import Move_Timeline from './Tools/Move_Timeline';
import Asset_Timeline from './Tools/Asset_Timeline';

var socketsOpened = 0;
var socketsTotal = 14;
var game_started = false;

let init = 0;
var loggedIn = 0;
let Units = [];
//VARIABLES
//Stomp Clients
var stompClient;
var stompClientBoard;
var stompClientBoardTiles;
var stompClientMove;
var stompClientLastAction;
var stompClientActivePlayer;
var stompClientActiveCreature;
var stompClientAvailableMoves;
var stompClientAvailableAttacks;
var stompClientMana;

var stompClientPlayer1Setup;
var stompClientPlayer2Setup;

var stompClientAvailableSpells;
var stompClientCastRange;

var stompClientVictory;

//Board info

//Tools
var message = new Message_Timeline();
var action = new Action_Timeline();
var path = new Move_Timeline();
var asset = new Asset_Timeline();

class App extends Component {
  constructor(props) {
    //BOARD INICIALIZATION
    super(props)

    this.inputRef = React.createRef()
    let tempWidth = 0;
    let tempHeight = 0;

    //create tile structure in memory
    let boardData = [];
    for(let j=0; j<=15; j++) {
      let line = [];
      for(let i=0; i<=20; i++) {
        line.push({x: i, y: j, clickable: 0, attackable: 0, castable:0, item: {name: "empty", tile: "", anim: 0}});
      }
      boardData.push(line);
    }

    //Bind "this"
    this.update = this.update.bind(this)
    this.clearClickable = this.clearClickable.bind(this);
    this.clearAttackable = this.clearAttackable.bind(this)
    this.clearCastable = this.clearCastable.bind(this)
    this.cancelCasting = this.cancelCasting.bind(this)
    this.setActiveSpellbook = this.setActiveSpellbook.bind(this)

    //this.setClickable = this.setClickable.bind(this);
    this.CanvasClicked = this.CanvasClicked.bind(this);

    this.connectToSocket = this.connectToSocket.bind(this);
    this.connectToSocketConnect = this.connectToSocketConnect.bind(this);
    this.connectToSocketSubscribe = this.connectToSocketSubscribe.bind(this);

    this.connectToMoves = this.connectToMoves.bind(this);
    this.connectToMovesConnect = this.connectToMovesConnect.bind(this);
    this.connectToMovesSubscribe = this.connectToMovesSubscribe.bind(this);

    this.connectToLastAction = this.connectToLastAction.bind(this);
    this.connectToLastActionConnect = this.connectToLastActionConnect.bind(this);
    this.connectToLastActionSubscribe = this.connectToLastActionSubscribe.bind(this);

    this.connectToAvailableSpells = this.connectToAvailableSpells.bind(this);
    this.connectToAvailableSpellsConnect = this.connectToAvailableSpellsConnect.bind(this);
    this.connectToAvailableSpellsSubsribe = this.connectToAvailableSpellsSubsribe.bind(this);

    this.connectToCastRange = this.connectToCastRange.bind(this);
    this.connectToCastRangeConnect = this.connectToCastRangeConnect.bind(this);
    this.connectToCastRangeSubscribe = this.connectToCastRangeSubscribe.bind(this);

    this.connectToAvailableMoves = this.connectToAvailableMoves.bind(this);
    this.connectToAvailableMovesConnect = this.connectToAvailableMovesConnect.bind(this);
    this.connectToAvailableMovesSubscribe = this.connectToAvailableMovesSubscribe.bind(this);

    this.connectToAvailableAttacks = this.connectToAvailableAttacks.bind(this);
    this.connectToAvailableAttacksConnect = this.connectToAvailableAttacksConnect.bind(this);
    this.connectToAvailableAttacksSubscribe = this.connectToAvailableAttacksSubscribe.bind(this);

    this.connectToActiveCreature = this.connectToActiveCreature.bind(this);
    this.connectToActiveCreatureConnect = this.connectToActiveCreatureConnect.bind(this);
    this.connectToActiveCreatureSubscribe = this.connectToActiveCreatureSubscribe.bind(this);

    this.connectToActivePlayer = this.connectToActivePlayer.bind(this);
    this.connectToActivePlayerConnect = this.connectToActivePlayerConnect.bind(this);
    this.connectToActivePlayerSubscribe = this.connectToActivePlayerSubscribe.bind(this);

    this.connectToCurrentBoard = this.connectToCurrentBoard.bind(this);
    this.connectToCurrentBoardConnect = this.connectToCurrentBoardConnect.bind(this);
    this.connectToCurrentBoardSubscribe = this.connectToCurrentBoardSubscribe.bind(this);

    this.connectToCurrentBoardTiles = this.connectToCurrentBoardTiles.bind(this);
    this.connectToCurrentBoardTilesConnect = this.connectToCurrentBoardTilesConnect.bind(this);
    this.connectToCurrentBoardTilesSubscribe = this.connectToCurrentBoardTilesSubscribe.bind(this);

    this.connectToVictory = this.connectToVictory.bind(this);
    this.connectToVictoryConnect = this.connectToVictoryConnect.bind(this);
    this.connectToVictorySubscribe = this.connectToVictorySubscribe.bind(this);

    this.connectToMana = this.connectToMana.bind(this);
    this.connectToManaConnect = this.connectToManaConnect.bind(this);
    this.connectToManaSubscribe = this.connectToManaSubscribe.bind(this);

    this.connectToPlayer2SetupSubscribe = this.connectToPlayer2SetupSubscribe.bind(this);
    this.connectToPlayer2SetupConnect = this.connectToPlayer2SetupConnect.bind(this);
    this.connectToPlayer2Setup = this.connectToPlayer2Setup.bind(this);

    this.connectToPlayer1SetupSubscribe = this.connectToPlayer1SetupSubscribe.bind(this);
    this.connectToPlayer1SetupConnect = this.connectToPlayer1SetupConnect.bind(this);
    this.connectToPlayer1Setup = this.connectToPlayer1Setup.bind(this);

    this.connectToGame = this.connectToGame.bind(this);


    // this.create_game = this.create_game.bind(this)
    // this.start_game = this.start_game.bind(this);
    // this.connectToRandom = this.connectToRandom.bind(this);
    this.moveUnit = this.moveUnit.bind(this);
    this.attackUnit = this.attackUnit.bind(this);
    this.castRange = this.castRange.bind(this);
    this.passTurn = this.passTurn.bind(this);
    this.concludeGame = this.concludeGame.bind(this);
    this.startGameCheck = this.startGameCheck.bind(this);
    this.rematch = this.rematch.bind(this);

    // this.updateMoveQueue = this.updateMoveQueue.bind(this);
    this.calculateCouterAttack = this.calculateCouterAttack.bind(this);
    this.calculateSpellEffects = this.calculateSpellEffects.bind(this);
    this.unitStatsUpdateSet = this.unitStatsUpdateSet.bind(this);
    this.activeUnitSet = this.activeUnitSet.bind(this);
    this.sortUnits = this.sortUnits.bind(this);
    this.gameRematchReset = this.gameRematchReset.bind(this);

    //Set initial state
    this.state = {
      gameId: "game_id",
      playerId: 0,
      enemyId: 1,
      activePlayer: 1,
      endGameScreen: 0,

      width: tempWidth,
      height: tempHeight,
      cellSizeX: tempWidth*3593/100000,
      cellSizeY: tempHeight*4590/100000,
      offsetX: tempWidth*14062/100000,
      offsetY: tempHeight*10082/100000,
      boardSizeH: 20,
      boardSizeV: 15,


      activeUnit: {
          name: "Unit_Nazwa2",
          x: 0,
          y: 1,
          armor: 0,
          moveRange: 0,
          amount: 0,
      },
      Cells: boardData,

      spellBook:[],

      selectedSpellName: "",

      mana: 0,

      castingSpells: false,
      activeSpellbook: false,
      unitStatsUpdate: false,

      waitScreen: true,
    }

    
  }
  
  updateDimensions = () => {
    let tempWidth = this.inputRef.current.offsetWidth;
    let tempHeight = this.inputRef.current.offsetHeight;

    this.setState(prevState => {
      return {
        width: tempWidth,
        height: tempHeight,
        cellSizeX: tempWidth*3593/100000,
        cellSizeY: tempHeight*4590/100000,
        offsetX: tempWidth*14062/100000,
        offsetY: tempHeight*10082/100000
      }
    }, () => {
      console.log("W:" + this.state.cellSizeX + " H:" + this.state.cellSizeY)
    })

  };

  componentDidMount() {
    let tempWidth = this.inputRef.current.offsetWidth;
    let tempHeight = this.inputRef.current.offsetHeight;

    this.setState(prevState => {
      return {
        width: tempWidth,
        height: tempHeight,
        cellSizeX: tempWidth*3593/100000,
        cellSizeY: tempHeight*4590/100000,
        offsetX: tempWidth*14062/100000,
        offsetY: tempHeight*10082/100000
      }
    }, () => {
      console.log("W:" + this.state.cellSizeX + " H:" + this.state.cellSizeY)
    })
    window.addEventListener('resize', this.updateDimensions);
    // this.connectToSocket("rooms");
    this.connectToGame("gameId")
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
    socketsOpened = 0;
    Units = [];
    asset.resetSetupComplete();
    if (stompClient) { stompClient.disconnect(); }
    if (stompClientBoard) { stompClientBoard.disconnect(); }
    if (stompClientBoardTiles) { stompClientBoardTiles.disconnect(); }
    if (stompClientMove) { stompClientMove.disconnect(); }
    if (stompClientLastAction) { stompClientLastAction.disconnect(); }
    if (stompClientActivePlayer) { stompClientActivePlayer.disconnect(); }
    if (stompClientActiveCreature) { stompClientActiveCreature.disconnect(); }
    if (stompClientAvailableMoves) { stompClientAvailableMoves.disconnect(); }
    if (stompClientAvailableAttacks) { stompClientAvailableAttacks.disconnect(); }
    if (stompClientMana) { stompClientMana.disconnect(); }
    if (stompClientPlayer1Setup) { stompClientPlayer1Setup.disconnect(); }
    if (stompClientPlayer2Setup) { stompClientPlayer2Setup.disconnect(); }
    if (stompClientAvailableSpells) { stompClientAvailableSpells.disconnect(); }
    if (stompClientCastRange) { stompClientCastRange.disconnect(); }
    if (stompClientVictory) { stompClientVictory.disconnect(); }

    message.reset();
    action.reset();
    path.reset();
    asset.reset();
  }


  update(UpdatedState) {
    this.setState(UpdatedState,
      () => {
        //What happens after an update
      }
    )
  }
  
  sortUnits(a, b) {
    if(a.yFinalGoal < b.yFinalGoal) {
      return -1;
    } else if(a.yFinalGoal > b.yFinalGoal) {
      return 1;
    } else {
      return 0;
    }
  }

  clearClickable() {
    let tempBoard = this.state.Cells;
    for(let y=0; y<this.state.boardSizeV; y++) {
      for(let x=0; x<this.state.boardSizeH; x++) {
        tempBoard[y][x].clickable = 0;
      }
    }

    this.setState(prevState => {
      return {
        Cells: tempBoard
      }
    })
  }

  clearAttackable() {
    let tempBoard = this.state.Cells;
    for(let y=0; y<this.state.boardSizeV; y++) {
      for(let x=0; x<this.state.boardSizeH; x++) {
        tempBoard[y][x].attackable = 0;
      }
    }

    this.setState(prevState => {
      return {
        Cells: tempBoard
      }
    })
  }
 
  clearCastable() {
    let tempBoard = this.state.Cells;
    for(let y=0; y<this.state.boardSizeV; y++) {
      for(let x=0; x<this.state.boardSizeH; x++) {
        tempBoard[y][x].castable = 0;
      }
    }

    this.setState(prevState => {
      return {
        Cells: tempBoard
      }
    })
  }

  CanvasClicked(x, y) {
    if(true) {

      //x,y of the clicked tile
      let X= Math.floor((x-this.state.offsetX)/this.state.cellSizeX);
      let Y= Math.floor((y-this.state.offsetY)/this.state.cellSizeY);

      //Check if player clicked board or something outside
      if(X<this.state.boardSizeH && Y< this.state.boardSizeV && X>=0 && Y >=0)  {

        if(this.props.playerId === 1) {
          if(this.state.Cells[Y][X].castable === 1 && this.state.castingSpells) {
            // = Player casts a spell
            this.clearCastable();
            this.castSpell(X, Y, this.state.selectedSpellName);
          } else {
            //Decide what a given click means
            if(this.state.Cells[Y][X].clickable === 1 && !this.state.castingSpells) {
              // = Player moves a unit
              this.clearClickable();
              this.clearAttackable();
              this.clearCastable();
              this.moveUnit(X, Y);
            }
            if(this.state.Cells[Y][X].attackable === 1 && !this.state.castingSpells) {
              // = Player attacks
              this.clearClickable();
              this.clearCastable();
              this.clearAttackable();
              this.attackUnit(X, Y);
            }
          }
        } else {
          if(this.state.Cells[Y][X].castable === 2 && this.state.castingSpells) {
            // = Player casts a spell
            this.castSpell(X, Y, this.state.selectedSpellName);
          } else {
            //Decide what a given click means
            if(this.state.Cells[Y][X].clickable === 2 && !this.state.castingSpells) {
              // = Player moves a unit
              this.clearClickable();
              this.clearAttackable();
              this.clearCastable();
              this.moveUnit(X, Y);
            }
            if(this.state.Cells[Y][X].attackable === 2 && !this.state.castingSpells) {
              // = Player attacks
              this.clearClickable();
              this.clearCastable();
              this.clearAttackable();
              this.attackUnit(X, Y);
            }
          }
        }

      
      }

    }
    
  }

  calculateCouterAttack(result) {
    if(result.actionType === "ATTACK") {
      this.clearClickable();
      this.clearAttackable()
      this.clearCastable();
      if (Units.length > 0) {
        if(Units.length > result.data.length){ 
          //DEATH
          while(Units.length > result.data.length) {
            let index = Units.findIndex((elem) => {
              let ind = result.data.findIndex(elem2 => elem2.x === elem.xFinalGoal && elem2.y === elem.yFinalGoal)
              if(ind === -1) {
                return true;
              } else {
                return false;
              }
            })
            let index2 = Units.findIndex(elem => elem.xFinalGoal === this.state.activeUnit.x && elem.yFinalGoal === this.state.activeUnit.y)
            Units[index2].setToken({x: this.state.activeUnit.x, y: this.state.activeUnit.y, type:'attack'})
    
            Units.splice(index, 1);
          }
        } else {
          //DAMAGE
          //Set "Attack" token for the active creature
          let counterAttackCheck = false;
          let index = Units.findIndex(elem => elem.xFinalGoal === this.state.activeUnit.x && elem.yFinalGoal === this.state.activeUnit.y)
          if (index>=0) { Units[index].setToken({x: this.state.activeUnit.x, y: this.state.activeUnit.y, type:'attack'});}
          let index2 = result.data.findIndex(elem => elem.x === this.state.activeUnit.x && elem.y === this.state.activeUnit.y);
          if(index2>=0) {
            if(Units[index].hp !== result.data[index2].stats.currentHp) {
              counterAttackCheck = true;
            }
          }

          //Set "Hurt" and "Couterattack"
          Units.forEach(elem1 => {
            let ind = result.data.findIndex(elem2 => elem2.x === elem1.xFinalGoal && elem2.y === elem1.yFinalGoal && elem1.hp !== elem2.stats.currentHp);
            if(ind === -1) {
              console.log("Unit isn't hurt");
            } else {
              if(elem1.xFinalGoal !== this.state.activeUnit.x || elem1.yFinalGoal !== this.state.activeUnit.y) {
                //Set Hurt Token
                elem1.hp = result.data[ind].stats.currentHp;
                elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt'}); // hurt
                //Set Conterattack token
                if(counterAttackCheck) {
                  elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'attack'});
                }
              } else {
                //Set Hurt from counter Token
                elem1.hp = result.data[ind].stats.currentHp;
                elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt'}); // hurt
              }
            }
          })   
        }
      }
    }
  }

  calculateSpellEffects(result) {
    if(result.actionType === "SPELL") {
      this.clearCastable();
      if (Units.length > 0) {
        if(Units.length > result.data.length){ 
          //DEATH
          while(Units.length > result.data.length) {
            let index = Units.findIndex((elem) => {
              let ind = result.data.findIndex(elem2 => elem2.x === elem.xFinalGoal && elem2.y === elem.yFinalGoal)
              if(ind === -1) {
                return true;
              } else {
                return false;
              }
            })
            Units.splice(index, 1);
          }
          
        } else {
          //Set spell effects
          Units.forEach(elem1 => {
            let ind = result.data.findIndex(elem2 => elem2.x === elem1.xFinalGoal && elem2.y === elem1.yFinalGoal);
            if(ind === -1) {
              console.log("Spell error: unit not found");
            } else {
              //Damage Spell Effects
              if(elem1.hp > result.data[ind].stats.currentHp) {
                elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt_spell'});
                elem1.hp = result.data[ind].stats.currentHp;
              }

              //Healing Spell Effects
              // if(elem1.hp < result.data[ind].stats.currentHp) {
              //   elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt_spell'/*buff*/});
              //   elem1.hp = result.data[ind].stats.currentHp;
              // }
              //Buffing Spell Effects
              // if(elem1.moveRange < result.data[ind].stats.moveRange) {
              //   elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt_spell'/*buff*/});
              //   elem1.moveRange = result.data[ind].stats.moveRange;
              // }

              // if(elem1.armor < result.data[ind].stats.armor) {
              //   elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt_spell'/*buff*/});
              //   elem1.armor = result.data[ind].stats.armor;
              // }

              // if(elem1.amount < result.data[ind].stats.amount) {
              //   elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'hurt_spell'/*buff*/});
              //   elem1.amount = result.data[ind].stats.amount;
              // }

              if(Object.keys(elem1.buffContainer).length < Object.keys(result.data[ind].stats.buffContainer).length) {
                elem1.setToken({x:elem1.xFinalGoal, y:elem1.yFinalGoal, type:'buff_spell'});
                elem1.buffContainer = result.data[ind].stats.buffContainer;
              }

              if(Object.keys(elem1.buffContainer).length >= Object.keys(result.data[ind].stats.buffContainer).length) {
                elem1.buffContainer = result.data[ind].stats.buffContainer;
              }
            }
          }) 
        }
      }
    } else {
      Units.forEach(elem1 => {
        let ind = result.data.findIndex(elem2 => elem2.x === elem1.xFinalGoal && elem2.y === elem1.yFinalGoal);
        if(ind === -1) {
          console.log("Spell error: unit not found");
        } else {
          elem1.buffContainer = result.data[ind].stats.buffContainer;
        }
      }) 
    }
  }

  unitStatsUpdateSet(value) {
    this.setState(prevState => {
      return {
        unitStatsUpdate: value,
      }
    })
  }

  activeUnitSet(value) {
    this.setState(prevState => {
      return {
        activeUnit: {
          name: prevState.activeUnit.name,
          x: prevState.activeUnit.x,
          y: prevState.activeUnit.y,
          update: value,
        },
      }
    })
  }

  startGameCheck(id) {
    socketsOpened++;
    if(socketsOpened === socketsTotal) {
      console.log("Starting Game...")
      this.props.ready();
    }
  }

  gameRematchReset() {
    let boardData = [];
    for(let j=0; j<=15; j++) {
      let line = [];
      for(let i=0; i<=20; i++) {
        line.push({x: i, y: j, clickable: 0, attackable: 0, castable:0, item: {name: "empty", tile: "", anim: 0}});
      }
      boardData.push(line);
    }

    Units = [];
    
    this.setState(prevState => {
      return {
        Cells: boardData
      }
    }, () => {
    })
  }
//============================================================================================//
//                           vv           Otwieranie Socketów          vv                     //
//============================================================================================//

  connectToSocketSubscribe(response) {
    let data = JSON.parse(response.body);
    console.log("-----------> FROM: Room Socket");
    console.log(data);
  }

  connectToSocketConnect() {
    stompClient.subscribe("/topic/rooms/", this.connectToSocketSubscribe)
  }

  connectToSocket(endpoint) {
    let socket = new SockJS("http://localhost:8080/rooms/");
    stompClient = Stomp.over(socket);
    stompClient.connect({}, this.connectToSocketConnect);
  }



  connectToMovesSubscribe(response) {
    console.log("-----------> FROM: Move Socket");

    let data = JSON.parse(response.body);
    message.storePath(data);
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }

  async connectToMovesConnect() {
    await stompClientMove.subscribe("/topic/move/"+this.props.gameId, this.connectToMovesSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToAvailableAttacks, 1000);

  }

  connectToMoves() {
    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientMove = Stomp.over(socket);
    stompClientMove.connect({}, this.connectToMovesConnect);
  }

  connectToAvailableMovesSubscribe(response) {
    console.log("-----------> FROM: Available Moves");
    let data = JSON.parse(response.body);

    message.storeMoves(data);
    message.increaseCounter();
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }

  async connectToAvailableMovesConnect() {
    await stompClientAvailableMoves.subscribe("/topic/availableMoves/"+this.props.gameId, this.connectToAvailableMovesSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToMoves, 1000);
  }

  connectToAvailableMoves() {
    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientAvailableMoves = Stomp.over(socket);
      stompClientAvailableMoves.connect({}, this.connectToAvailableMovesConnect);
  }

  connectToAvailableAttacksSubscribe(response){
    console.log("-----------> FROM: Available Attacks");
    let data = JSON.parse(response.body);

    message.storeAttack(data);
    message.increaseCounter();
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }

  async connectToAvailableAttacksConnect() {
    await stompClientAvailableAttacks.subscribe("/topic/availableAttacks/"+this.props.gameId, this.connectToAvailableAttacksSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToActivePlayer, 1000);
  }

  connectToAvailableAttacks() {

    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientAvailableAttacks = Stomp.over(socket);
    stompClientAvailableAttacks.connect({}, this.connectToAvailableAttacksConnect);
  }

  
//
connectToLastActionSubscribe(response) {
  console.log("-----------> FROM: Last Action");
  let data = JSON.parse(response.body);
  if(data === "ATTACK" || data === "PASS" || data === "SPELL" || data === "MOVE") {
    action.storeActionType(data);
    action.increaseCounter();
    let result2 = action.Update();
    if(result2.update) {
      this.calculateSpellEffects(result2);
      this.calculateCouterAttack(result2);
      Units.sort(this.sortUnits);
      this.unitStatsUpdateSet(true);
    }
  }
  if (data === "MOVE" || data === "PASS") {
    message.storeLastAction(data);
    message.increaseCounter();
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }
}

async connectToLastActionConnect() {
  await stompClientLastAction.subscribe("/topic/lastAction/"+this.props.gameId, this.connectToLastActionSubscribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToAvailableSpells, 1000);
}

connectToLastAction() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientLastAction = Stomp.over(socket);
  stompClientLastAction.connect({}, this.connectToLastActionConnect);
}
//
  connectToActiveCreatureSubscribe(response) {
    console.log("-----------> FROM: Active Creature");
    let data = JSON.parse(response.body);

    // path.storeCreature(data);
    // path.increaseCounterCreature();
    // let result2 = path.Update();
    // if(result2 !== false) {
    //   //Update unit queue
      // Units.forEach(function(unit) {
      //   if(unit.xFinalGoal === result2.prev_x && unit.yFinalGoal === result2.prev_y) {
      //     console.log("added a point");
      //     unit.xFinalGoal = result2.x;
      //     unit.yFinalGoal = result2.y;
      //     result2.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
      //   }

      // })

      // Units.sort(this.sortUnits);
    // }

    message.storeCreature(data);
    message.increaseCounter();
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }

  async connectToActiveCreatureConnect() {
    await stompClientActiveCreature.subscribe("/topic/activeCreature/"+this.props.gameId, this.connectToActiveCreatureSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToCurrentBoard, 1000);
  }


  connectToActiveCreature() {

    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientActiveCreature = Stomp.over(socket);
      stompClientActiveCreature.connect({}, this.connectToActiveCreatureConnect);
  }

  /* !!! */
  connectToCurrentBoardTilesSubscribe(response) {
    console.log("-----------> FROM: Special Tiles");
    let data = JSON.parse(response.body);
    console.log(data);

    const unitData = data.map(element => {
      let temp = Object.keys(element);
      let str = temp[0];
      temp = Object.values(element);
      let stats = temp[0];
      let arr = str.split("=");
      let x = arr[1];
      x = x.substring(0, x.length - 3);
      let y = arr[2];
      y = y.substring(0, y.length - 1);
      return {x: +x, y: +y, stats: stats}
      

    });

    let cellData = this.state.Cells;
    for(const entry of Object.entries(unitData)) {
      cellData[entry[1].y][entry[1].x].item.tile = "A";
    }
    
    
    this.setState(prevState => {
      return {
        Cells: cellData,
      }
    })
  }

  async connectToCurrentBoardTilesConnect() {
    await stompClientBoardTiles.subscribe("/topic/currentBoard/specialTiles/"+this.props.gameId, this.connectToCurrentBoardTilesSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToLastAction, 1000);
  }

  connectToCurrentBoardTiles() {

    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientBoardTiles = Stomp.over(socket);
    stompClientBoardTiles.connect({}, this.connectToCurrentBoardTilesConnect);
  }
  /* !!! */

  connectToCurrentBoardSubscribe(response) {
    console.log("-----------> FROM: currentBoard");
    let data = JSON.parse(response.body);
    console.log("Board Refresh:");

    const unitData = data.map(element => {
      let temp = Object.keys(element);
      let str = temp[0];
      temp = Object.values(element);
      let stats = temp[0];
      let arr = str.split("=");
      let x = arr[1];
      x = x.substring(0, x.length - 3);
      let y = arr[2];
      y = y.substring(0, y.length - 1);
      return {x: +x, y: +y, stats: stats}
      

    });
    //Its the Init refresh
    if(Units.length === 0) {
      asset.storeUnits(unitData);
      asset.increaseCounter();
      let result = asset.Update();
      if(result.update !== false) {
        console.log("Board Init:");
        let p1Units = result.units.filter(u => u.x < 10);
        let p2Units = result.units.filter(u => u.x >= 10);

        p1Units.sort(this.sortUnits);
        p2Units.sort(this.sortUnits);

        
        p1Units.forEach((element, i) => {
          let unit = new Unit(element.x, element.y, result.assets1[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
          Units.push(unit);
          console.log("Saved Unit: ", result.assets1[0][i]);
        });
        p2Units.forEach((element, i) => {
          let unit = new Unit(element.x, element.y, result.assets2[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
          Units.push(unit);
          console.log("Saved Unit: ", result.assets2[0][i]);
        });
        this.unitStatsUpdateSet(true);
        this.setState(prevState => {
          return {
            waitScreen: false,
          }
        })
      }
    } else {
      action.storeResponse(unitData);
      action.increaseCounter();
      let result = action.Update();
      if(result.update) {
        this.calculateSpellEffects(result);
        this.calculateCouterAttack(result);
        Units.sort(this.sortUnits);
        console.log(Units);
        this.unitStatsUpdateSet(true);
      }
    }
  }

  async connectToCurrentBoardConnect() {
    await stompClientBoard.subscribe("/topic/currentBoard/creatures/"+this.props.gameId, this.connectToCurrentBoardSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToCurrentBoardTiles, 1000);
  }

  connectToCurrentBoard() {

    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientBoard = Stomp.over(socket);
    stompClientBoard.connect({}, this.connectToCurrentBoardConnect);
  }

  connectToActivePlayerSubscribe(response) {
    let data = JSON.parse(response.body); 
    console.log("-----------> FROM: Active Player");
    console.log(parseInt(data));
    message.storeActivePlayer(data);
    message.increaseCounter();
    let result = message.Update(this.state.Cells, this.state.Player1, this.state.Player2, this.state.boardSizeV, this.state.boardSizeH, this.state.activeUnit.x, this.state.activeUnit.y);
    if(result !== false) {
      if(result.path) {
        Units.forEach(function(unit) {
          if(unit.xFinalGoal === result.prev_x && unit.yFinalGoal === result.prev_y) {
            console.log("added a point");
            unit.xFinalGoal = result.x;
            unit.yFinalGoal = result.y;
            result.path.forEach(point => unit.setToken({x:point.x, y:point.y, type:'walk'}));
          }

        })

        Units.sort(this.sortUnits);
      }

      this.setState(prevState => {
        return {
          Cells: result.data,
          activePlayer: result.actvPlayer,
          activeUnit: {
            name: result.actvUnitName,
            x: result.actvUnitX,
            y: result.actvUnitY,
            update: true,
          },
        }
      })
    }
  }

  async connectToActivePlayerConnect() {
    await stompClientActivePlayer.subscribe("/topic/activePlayer/"+this.props.gameId, this.connectToActivePlayerSubscribe);
    this.startGameCheck(this.props.playerId);
    setTimeout(this.connectToActiveCreature, 1000);

  }

  connectToActivePlayer() {

    let socket = new SockJS('http://localhost:8080/gameplay/');
    stompClientActivePlayer = Stomp.over(socket);
      stompClientActivePlayer.connect({}, this.connectToActivePlayerConnect);
  }

connectToAvailableSpellsSubsribe(response) {
  console.log("-----------> FROM: Available Spells");
  let data = JSON.parse(response.body);
  this.setState(prevState => {
    return {
      spellBook: data
    }
  }, () => {
    console.log(this.state.spellBook);
  })
}

async connectToAvailableSpellsConnect() {
  await stompClientAvailableSpells.subscribe("/topic/availableSpells/"+this.props.gameId, this.connectToAvailableSpellsSubsribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToCastRange, 1000);
}

connectToAvailableSpells() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientAvailableSpells = Stomp.over(socket);
  stompClientAvailableSpells.connect({}, this.connectToAvailableSpellsConnect);
}

connectToCastRangeSubscribe(response) {
  console.log("-----------> FROM: Cast Range");
  let data = JSON.parse(response.body);
  this.clearCastable();
  let tempBoard = this.state.Cells;
  data.forEach(tile => tempBoard[tile.y][tile.x].castable = this.state.activePlayer)
  console.log(tempBoard)
  this.setState(prevState => {
    return {
      Cells: tempBoard
    }
  })
}

async connectToCastRangeConnect() {
  await stompClientCastRange.subscribe("/topic/castRange/"+this.props.gameId, this.connectToCastRangeSubscribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToVictory, 1000);
}

connectToCastRange() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientCastRange = Stomp.over(socket);
  stompClientCastRange.connect({}, this.connectToCastRangeConnect);
}

connectToVictorySubscribe(response) {
  console.log("-----------> FROM: Victory");
  let data = JSON.parse(response.body);
  this.concludeGame(data);
  this.gameRematchReset();
  this.setState(prevState => {
    return {
      endGameScreen: data
    }
  })
}

async connectToVictoryConnect() {
  await stompClientVictory.subscribe("/topic/victor/"+this.props.gameId, this.connectToVictorySubscribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToMana, 1000);
}

connectToVictory() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientVictory = Stomp.over(socket);
  stompClientVictory.connect({}, this.connectToVictoryConnect);
}

connectToManaSubscribe(response) {
  console.log("-----------> FROM: Mana");
  let data = JSON.parse(response.body);
  console.log(data);
  this.setState(prevState => {
    return {
      mana: data.currentMana
    }
  })
}

async connectToManaConnect() {
  await stompClientMana.subscribe("/topic/currentMana/"+this.props.gameId, this.connectToManaSubscribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToPlayer1Setup, 1000);
}

connectToMana() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientMana = Stomp.over(socket);
  stompClientMana.connect({}, this.connectToManaConnect);
}

connectToPlayer1SetupSubscribe(response) {
  console.log("-----------> FROM: Player 1 Setup");
  let data = JSON.parse(response.body);
  asset.storeAssets1(data);
  asset.increaseCounter();
  let result = asset.Update();
  if(result.update !== false) {
    console.log("Board Init:");
    let p1Units = result.units.filter(u => u.x < 10);
    let p2Units = result.units.filter(u => u.x >= 10);

    p1Units.sort(this.sortUnits);
    p2Units.sort(this.sortUnits);

    
    p1Units.forEach((element, i) => {
      let unit = new Unit(element.x, element.y, result.assets1[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
      Units.push(unit);
      console.log("Saved Unit: ", result.assets1[0][i]);
    });
    p2Units.forEach((element, i) => {
      let unit = new Unit(element.x, element.y, result.assets2[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
      Units.push(unit);
      console.log("Saved Unit: ", result.assets2[0][i]);
    });
    this.unitStatsUpdateSet(true);
    this.setState(prevState => {
      return {
        waitScreen: false,
      }
    })
  }
}

async connectToPlayer1SetupConnect() {
  await stompClientPlayer1Setup.subscribe("/topic/playerSetup/1/"+this.props.gameId, this.connectToPlayer1SetupSubscribe);
  this.startGameCheck(this.props.playerId);
  setTimeout(this.connectToPlayer2Setup, 1000);
}

connectToPlayer1Setup() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientPlayer1Setup = Stomp.over(socket);
  stompClientPlayer1Setup.connect({}, this.connectToPlayer1SetupConnect);
}

connectToPlayer2SetupSubscribe(response) {
  console.log("-----------> FROM: Player 2 Setup");
  let data = JSON.parse(response.body);
  asset.storeAssets2(data);
  asset.increaseCounter();
  let result = asset.Update();
  if(result.update !== false) {
    console.log("Board Init:");
    let p1Units = result.units.filter(u => u.x < 10);
    let p2Units = result.units.filter(u => u.x >= 10);

    p1Units.sort(this.sortUnits);
    p2Units.sort(this.sortUnits);

    
    p1Units.forEach((element, i) => {
      let unit = new Unit(element.x, element.y, result.assets1[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
      Units.push(unit);
      console.log("Saved Unit: ", result.assets1[0][i]);
    });
    p2Units.forEach((element, i) => {
      let unit = new Unit(element.x, element.y, result.assets2[0][i], element.stats); //./Animations/Img_Resting_Right.png, 92, 122, 22
      Units.push(unit);
      console.log("Saved Unit: ", result.assets2[0][i]);
    });
    this.unitStatsUpdateSet(true);
    this.setState(prevState => {
      return {
        waitScreen: false,
      }
    })
  }
}

async connectToPlayer2SetupConnect() {
  await stompClientPlayer2Setup.subscribe("/topic/playerSetup/2/"+this.props.gameId, this.connectToPlayer2SetupSubscribe);
  this.startGameCheck(this.props.playerId);
}

connectToPlayer2Setup() {
  let socket = new SockJS('http://localhost:8080/gameplay/');
  stompClientPlayer2Setup = Stomp.over(socket);
  stompClientPlayer2Setup.connect({}, this.connectToPlayer2SetupConnect);
}

  connectToGame(gameId) {
    this.connectToAvailableMoves();
  }

  connectToJoinGame(gameId) {
    this.connectToAvailableMoves();
    this.connectToMoves();  
    this.connectToAvailableAttacks();
    this.connectToActivePlayer();
    this.connectToActiveCreature();
    this.connectToCurrentBoard();
    this.connectToCurrentBoardTiles();
    this.connectToLastAction();
    this.connectToAvailableSpells();
    this.connectToCastRange();
    this.connectToVictory();
    this.connectToMana();
    this.connectToPlayer1Setup();
    this.connectToPlayer2Setup();
  }

  componentDidUpdate() {
    if(this.props.readyEcoP1 && this.props.readyEcoP2 && this.props.playerId === 1 && !game_started) {
      game_started = true;
      // this.start_game();
    }
  }
  //============================================================================================//
  //                               vv           REST API          vv                            //
  //============================================================================================//

  start_game() {
    axios.post("http://localhost:8080/lobby/"+this.props.gameId+"/start", {"id": "gracz-1"}).then(res => {
      console.log("-----------> FROM: Start Game");
    });
  }

  moveUnit(x, y) {

    axios.post("http://localhost:8080/game/"+this.props.gameId+"/move", {"gameId": this.props.gameId, "targetTile":{"x":x, "y":y}}).then(res => {

        console.log("-----------> FROM: Move Unit");
        
    });
  }

  attackUnit(x, y) {

    axios.post("http://localhost:8080/game/"+this.props.gameId+"/attack", {"gameId": this.props.gameId, "targetTile":{"x":x, "y":y}}).then(res => {

        console.log("-----------> FROM: Attack Unit");
        
    });
  }

  setActiveSpellbook() {
    this.setState(prevState => {
      return {
        activeSpellbook: !prevState.activeSpellbook,
      }
    })
  }

  castRange(spellname) {
    this.setState(prevState => {
      return {
        castingSpells: true,
        selectedSpellName: spellname,
      }
    }, () => {
      console.log("Selected Spell: "+this.state.selectedSpellName);
      console.log(this.state.castingSpells)
      axios.post("http://localhost:8080/game/"+this.props.gameId+"/castRange", spellname, { headers: {'Content-Type': 'text/plain'} }).then(res => {
        console.log("-----------> FROM: Cast Range");
      });
    })
  }

  castSpell(x, y, spellname) {
    this.setState(prevState => {
      return {
        castingSpells: false,
        selectedSpellName: "",
        
      }
    }, () => {
      console.log("Selected Spell Cleared");
      axios.post("http://localhost:8080/game/"+this.props.gameId+"/cast", {"targetTile": {"x":x, "y":y}, "spellName":spellname}).then(res => {
        console.log("-----------> FROM: Cast Spell");
      });
    })
    
  }

  passTurn() {
    if(this.state.activePlayer === this.props.playerId) {
      axios.post("http://localhost:8080/game/"+this.props.gameId+"/pass", {"gameId": this.props.gameId}).then(res => {

        console.log("-----------> FROM: Pass turn");
        console.log(res.data);
        
      });
    }

  }

  concludeGame(id) {
    if(this.props.playerId === id) {
      axios.post("http://localhost:8080/lobby/"+this.props.gameId+"/conclude", {"gameId": this.props.gameId}).then(res => {

        console.log("-----------> FROM: Conclude Game");
      });
    }

  }

  rematch() {
    axios.post("http://localhost:8080/lobby/"+this.props.gameId+"/rematch", {"gameId": this.props.gameId}).then(res => {
      console.log("-----------> FROM: Rematch");
    });
  }

  cancelCasting() {
    this.setState(prevState => {
      return {
        castingSpells: false,
      }
    }, () => {
      
    })
  }

  //============================================================================================//
  //                           vv               Render              vv                          //
  //============================================================================================//

  render() {
    let BW = this.state.width;
    let BH = this.state.height;

    return (
      
      <div className="scaleContainer">
        <div className="contnentWrapper" ref={this.inputRef}>             
          <div style={{width: "100%", height: "100%", position:"absolute"}} onMouseUp={(event) => this.CanvasClicked(event.pageX, event.pageY)}>
            <CanvasDisplay width={this.state.width} height={this.state.height} cellSizeX={this.state.cellSizeX} cellSizeY={this.state.cellSizeY} offsetX={this.state.offsetX} offsetY={this.state.offsetY} boardSizeH={this.state.boardSizeH} boardSizeV={this.state.boardSizeV} Cells={this.state.Cells} castingSpells={this.state.castingSpells} units={Units} activeCreature={this.state.activeUnit}/>
          </div>
            
          <Spellbook buttons={this.state.spellBook} castRange={this.castRange} cancelCasting={this.cancelCasting} active={this.props.playerId === this.state.activePlayer} activeSpellbook={this.state.activeSpellbook} castingSpells={this.state.castingSpells} setActiveSpellbook={this.setActiveSpellbook} mana={this.state.mana}/>
          <ActionMenu menuWidth={BW*0.177} menuHeight={BH*0.239} width={BW} height={BH} setActiveSpellbook={this.setActiveSpellbook} activeSpellbook={this.state.activeSpellbook} passTurn={this.passTurn} castingNow={this.props.playerId === this.state.activePlayer}/>
          <ActiveCreature width={BW} activeCreature={this.state.activeUnit} Units={Units} unitStatsUpdate={this.state.unitStatsUpdate} unitStatsUpdateSet={this.unitStatsUpdateSet} activeUnitSet={this.activeUnitSet}/>
          <VictoryScreen id={this.props.playerId} endGameScreen={this.state.endGameScreen} rematch={this.rematch} leaveLobby={this.props.leaveLobby}/>
        </div>
        <PlayerNames number={1} id={this.props.playerId}/>
        <PlayerNames number={2} id={this.state.enemyId}/>
        {this.state.waitScreen ? <div className='waitScreen'>Waiting for the enemy...</div> : null}
      </div>
    )
  }
}



export default App;

