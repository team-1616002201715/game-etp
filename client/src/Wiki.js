import Sidebar from './Components/Sidebar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import About from './pages/Wiki/About'
import {Units, Hedgehog, Spectre, Handyman, Marauder, NewYorker, Patriot, Railgun, TroopCarrier, Ivaylo, Officer, Undead, Werewolf, Troglodyte, Minigunner, BabaYaga, Matushka} from './pages/Wiki/Units';
import Howto from './pages/Wiki/Howto';
import {Items, Blackshard, Collar, Orb, Ring, Shield, Stateman, Surcoat, Tome} from './pages/Wiki/Items';
import { Spells, Haste, Bloodlust, Dispell, Teleport, Slow, Fireball, Implosion, DeathRipple, MagicArrow}  from './pages/Wiki/Spells';

function Wiki() {
    return(
        <Router>
            <Sidebar />
            <Switch>
                <Route path='/wiki' exact component={About} />
                <Route path='/about' exact component={About} />
                <Route path='/units' exact component={Units} />
                <Route path='/units/hedgehog' exact component={Hedgehog} />
                <Route path='/units/spectre' exact component={Spectre} />
                <Route path='/units/handyman' exact component={Handyman} />
                <Route path='/units/marauder' exact component={Marauder} />
                <Route path='/units/newyorker' exact component={NewYorker} />
                <Route path='/units/patriot' exact component={Patriot} />
                <Route path='/units/railgun' exact component={Railgun} />
                <Route path='/units/troopcarrier' exact component={TroopCarrier} />
                <Route path='/units/ivaylo' exact component={Ivaylo} />
                <Route path='/units/officer' exact component={Officer} />
                <Route path='/units/undead' exact component={Undead} />
                <Route path='/units/werewolf' exact component={Werewolf} />
                <Route path='/units/troglodyte' exact component={Troglodyte} />
                <Route path='/units/minigunner' exact component={Minigunner} />
                <Route path='/units/babayaga' exact component={BabaYaga} />
                <Route path='/units/matushka' exact component={Matushka} />
                <Route path='/howto' exact component={Howto} />
                <Route path='/items' exact component={Items} />
                <Route path='/items/blackshard' exact component={Blackshard} />
                <Route path='/items/collar' exact component={Collar} />
                <Route path='/items/orb' exact component={Orb} />
                <Route path='/items/ring' exact component={Ring} />
                <Route path='/items/shield' exact component={Shield} />
                <Route path='/items/stateman' exact component={Stateman} />
                <Route path='/items/surcoat' exact component={Surcoat} />
                <Route path='/items/tome' exact component={Tome} />
                <Route path='/spells' exact component={Spells} />
                <Route path='/spells/haste' exact component={Haste} />
                <Route path='/spells/bloodlust' exact component={Bloodlust} />
                <Route path='/spells/dispell' exact component={Dispell} />
                <Route path='/spells/rage' exact component={Teleport} />
                <Route path='/spells/slow' exact component={Slow} />
                <Route path='/spells/fireball' exact component={Fireball} />
                <Route path='/spells/ripple' exact component={DeathRipple} />
                <Route path='/spells/implosion' exact component={Implosion} />
                <Route path='/spells/arrow' exact component={MagicArrow} />
                <img id="layoutFront" style={{position: 'absolute', width:"100%", height:"100%"}} src="background.png" alt="Layout_front"></img>
            </Switch>
        </Router>

    );
}

export default Wiki;