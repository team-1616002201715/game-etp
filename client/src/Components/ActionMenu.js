import React from 'react'
import "./styles/ActionMenu.css"
const ActionMenu = ({width, height, menuWidth, menuHeight, setActiveSpellbook, activeSpellbook, passTurn, castingNow}) => {
    let spellBookColor = activeSpellbook && castingNow? `actionMenu__spellbook--active`:`actionMenu__spellbook`

    return (
        <div className="actionMenu">
            <div className="svg-actions">ACTIONS</div>
            <button className={`${spellBookColor}`} onClick={() => setActiveSpellbook()}>Spellbook</button>
            <button className="actionMenu__pass" onMouseUp={() => passTurn()}>Pass</button>
        </div>
    )
}

export default ActionMenu
