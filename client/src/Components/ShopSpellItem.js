import React from 'react'
import { useState } from 'react/cjs/react.development'
import Spellbook from './Spellbook';

const ShopUnitItem = ({spellName, buyItem, sellItem, spell}) => {
    const [toggle, setToggle] = useState(false);

    const sell = (e) => {
        e.stopPropagation();
        sellItem(spellName, 1, "SPELL");
    }

    const buy = (e) => {
        e.stopPropagation();
        buyItem(spellName, 1, "SPELL");
    }

    let tog = toggle? "" : "shop__unit__info--hidden";
    return (
        <div onClick={() => setToggle(!toggle)} style={{color: "white", backgroundSize: '64px', backgroundImage: `url(/ShopAssets/${spellName.toLowerCase().replace(/ /g, '-').replace(/'/g, '')}.png)`, backgroundRepeat: 'no-repeat', backgroundPosition: '95%, 50%'}} className="shop__window__item">
            <div>{spellName}</div>
            <div className='shop__item__controls'>
                <button onClick={(e) => buy(e)}>Buy</button>
                <button onClick={(e) => sell(e)}>Sell</button>
            </div>
            <div className={`shop__unit__info ${tog}`}>
                Element:      {spell.spell.element}<br/><br/>
                Type:         {spell.spell.spellType}<br/><br/>
                Target:       {spell.spell.targetType}<br/><br/>
                Mana Cost:    {spell.spell.manaCost}<br/><br/>
                Cost:    {spell.goldCost}$<br/><br/>
            </div>
        </div>
    )
}

export default ShopUnitItem
