import React from 'react'
import { useEffect } from 'react/cjs/react.development'
import "./styles/Buffdisplay.css"
const Buffdisplay = ({buffs}) => {

    useEffect(() => {
        console.log(Object.entries(buffs));
    }, [buffs]);

    return (
        <div className="buff_container">
            {Object.values(buffs).map((buff, index) => 
            <div key={index} className="buff">
                <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 27.8223V16.108H43.9286V27.8223H0Z" fill="#D74949"/>
                    <path d="M16.1072 0H27.8215V43.9286H16.1072V0Z" fill="#D74949"/>
                </svg>
            </div>)}
        </div>
 
 )
}

export default Buffdisplay
