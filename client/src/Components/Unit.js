import Queue from "../Tools/Queue"


class Unit {

    constructor(_x, _y, _unitType, _stats) {

        let spriteWidth = 0;
        let spriteHeight = 0;
        let spriteName;
        switch(_unitType) {
            case "Ivaylo":
                spriteName = "Ivaylo/";
                break;
            case "Baba Yaga":
                spriteName = "Baba Yaga/";
                break;
            case "Handyman":
                spriteName = "Handyman/";
                break;
            case "Hedgehog":
                spriteName = "Hedgehog/";
                break;
            case "Marauder":
                spriteName = "Marauder/";
                break;
            case "Matushka":
                spriteName = "Matushka/";
                break;
            case "Minigunner":
                spriteName = "Minigunner/";
                break;
            case "New Yorker":
                spriteName = "New Yorker/";
                break;
            case "Officer":
                spriteName = "Officer/";
                break;
            case "Patriot":
                spriteName = "Patriot/";
                break;
            case "Railgun":
                spriteName = "Railgun/";
                break;
            case "Spectre":
                spriteName = "Spectre/";
                break;
            case "Troglodyte":
                spriteName = "Troglodyte/";
                break;
            case "Troop Carrier":
                spriteName = "Troop Carrier/";
                break;
            case "Undead":
                spriteName = "Undead/";
                break;
            case "Werewolf":
                spriteName = "Werewolf/";
                break;
            default:
                spriteName = "Handyman/";
                break;
        }
        //Idle
        spriteWidth = 32;
        spriteHeight = 64;
        this.idleRightFrames = 1;
        this.spriteIdleRight = new Image(5*spriteWidth, Math.ceil(this.idleRightFrames/5)*spriteHeight);
        this.spriteIdleRight.src = `./Animations/${spriteName}spriteSheetIdleRight.png`;

        //Idle
        spriteWidth = 32;
        spriteHeight = 64;
        this.idleLeftFrames = 1;
        this.spriteIdleLeft = new Image(5*spriteWidth, Math.ceil(this.idleLeftFrames/5)*spriteHeight);
        this.spriteIdleLeft.src = `./Animations/${spriteName}spriteSheetIdleLeft.png`;

        //Attack
        spriteWidth = 32;
        spriteHeight = 64;
        this.attackFrames = 10;
        this.spriteAttack = new Image(5*spriteWidth, Math.ceil(this.attackFrames/5)*spriteHeight);
        this.spriteAttack.src = `./Animations/${spriteName}spriteSheetAttack.png`;

        //Hurt
        spriteWidth = 32;
        spriteHeight = 64;
        this.hurtFrames = 10;
        this.spriteHurt = new Image(5*spriteWidth, Math.ceil(this.hurtFrames/5)*spriteHeight);
        this.spriteHurt.src = `./Animations/${spriteName}spriteSheetHurt.png`;

        //Hurt Spell
        spriteWidth = 32;
        spriteHeight = 64;
        this.hurtSpellFrames = 10;
        this.spriteHurtSpell = new Image(5*spriteWidth, Math.ceil(this.hurtSpellFrames/5)*spriteHeight);
        this.spriteHurtSpell.src = `./Animations/${spriteName}spriteSheetHurtSpell.png`;

        //Buff Spell
        spriteWidth = 32;
        spriteHeight = 64;
        this.buffSpellFrames = 10;
        this.spriteBuffSpell = new Image(5*spriteWidth, Math.ceil(this.buffSpellFrames/5)*spriteHeight);
        this.spriteBuffSpell.src = `./Animations/${spriteName}spriteSheetBuffSpell.png`;

        //Walk
        spriteWidth = 32;
        spriteHeight = 64;
        this.walkFrames = 10;
        this.spriteWalkRigth = new Image(5*spriteWidth, Math.ceil(this.walkFrames/5)*spriteHeight);
        this.spriteWalkRigth.src = `./Animations/${spriteName}spriteSheetWalkRight.png`;

        this.spriteWalkLeft = new Image(5*spriteWidth, Math.ceil(this.walkFrames/5)*spriteHeight);
        this.spriteWalkLeft.src = `./Animations/${spriteName}spriteSheetWalkLeft.png`;

        this.spriteWalkUp = new Image(5*spriteWidth, Math.ceil(this.walkFrames/5)*spriteHeight);
        this.spriteWalkUp.src = `./Animations/${spriteName}spriteSheetWalkUp.png`;

        this.spriteWalkDown = new Image(5*spriteWidth, Math.ceil(this.walkFrames/5)*spriteHeight);
        this.spriteWalkDown.src = `./Animations/${spriteName}spriteSheetWalkDown.png`;

        this.unitName = _unitType;
        this.x = +_x;
        this.y = +_y;
        this.armor = _stats.armor;
        this.hp = _stats.currentHp;
        this.amount = _stats.amount;
        this.moveRange = _stats.moveRange;
        this.buffContainer = _stats.buffContainer;

        this.axis = 1;
        this.xGoal = +_x;
        this.yGoal = +_y;
        this.xFinalGoal = +_x;
        this.yFinalGoal = +_y;

        this.unitState = 'idle';
        this.queue = new Queue();
        this.sprite = this.spriteIdleRight;

        this.animFrame = 0;
        this.animX = 0;
        this.animY = 0;
        this.animWidth = 32;
        this.animHeight = 64;
        
        this.step = 0.03;
        this.frameCount = 0;
    }

    updateQueue(point) {
        this.queue.enqueue(point);
    }

    idleAnimation(timestamp) {
         this.animFrame = 0
        // if(timestamp - this.frameCount  >= 500) {
        //     if(this.animFrame >= this.idleFrames-1) {
        //         this.animY = Math.floor(this.animFrame/5);
        //         this.animX = this.animFrame-this.animY*5;
        //         this.animFrame = 0;
        //         this.frameCount = timestamp;
        //     } else {
        //         this.animY = Math.floor(this.animFrame/5);
        //         this.animX = this.animFrame-this.animY*5;
        //         this.animFrame += 1;
        //         this.frameCount = timestamp;
        //     }
        // }
    }

    walkAnimation(timestamp) {
        if(timestamp - this.frameCount >= 100) {
            if(this.animFrame >= this.walkFrames-1) {
                this.animY = Math.floor(this.animFrame/5);
                this.animX = this.animFrame-this.animY*5;
                this.animFrame = 0;
                this.frameCount = timestamp;
            } else {
                this.animY = Math.floor(this.animFrame/5);
                this.animX = this.animFrame-this.animY*5;
                this.animFrame += 1;
                this.frameCount = timestamp;
            }
        }
        if(timestamp - this.frameCount >= 10) {
            if(this.x < this.xGoal) {
                if(this.x+this.step >= this.xGoal) {
                    this.x = this.xGoal;
                } else {
                    this.x = this.x+this.step;
                }
            } else if(this.x > this.xGoal) {
                if(this.x-this.step <= this.xGoal) {
                    this.x = this.xGoal;
                } else {
                    this.x = this.x-this.step;
                }
            }
            if(this.y < this.yGoal) {
                if(this.y+this.step >= this.yGoal) {
                    this.y = this.yGoal;
                } else {
                    this.y = this.y+this.step;
                }
            } else if(this.y > this.yGoal) {
                if(this.y-this.step <= this.yGoal) {
                    this.y = this.yGoal;
                } else {
                    this.y = this.y-this.step;
                }
            }
        }
    }

    attackAnimation(timestamp) {
        if(timestamp - this.frameCount  >= 100) {
            this.animY = Math.floor(this.animFrame/5);
            this.animX = this.animFrame-this.animY*5;
            this.animFrame += 1;
            this.frameCount = timestamp;
        }
    }
    
    hurtAnimation(timestamp) {
        if(timestamp - this.frameCount  >= 100) {
            this.animY = Math.floor(this.animFrame/5);
            this.animX = this.animFrame-this.animY*5;
            this.animFrame += 1;
            this.frameCount = timestamp;
        }
    }

    hurtSpellAnimation(timestamp) {
        if(timestamp - this.frameCount  >= 100) {
            this.animY = Math.floor(this.animFrame/5);
            this.animX = this.animFrame-this.animY*5;
            this.animFrame += 1;
            this.frameCount = timestamp;
        }
    }

    buffSpellAnimation(timestamp) {
        if(timestamp - this.frameCount  >= 100) {
            this.animY = Math.floor(this.animFrame/5);
            this.animX = this.animFrame-this.animY*5;
            this.animFrame += 1;
            this.frameCount = timestamp;
        }
    }

    setToken(token) {
        this.queue.enqueue(token);
    }

    setAxis() {
        if(this.x < this.xGoal) {
            this.axis = 1;
            this.sprite = this.spriteWalkRigth;

        } else if(this.x > this.xGoal) {
            this.axis = 3;
            this.sprite = this.spriteWalkLeft;
        } else if (this.y > this.xGoal || this.y < this.xGoal) {
            if (this.axis === 1) {
                this.sprite = this.spriteWalkRigth;
            } else if (this.axis === 3){
                this.sprite = this.spriteWalkLeft;
            }
        }
        // if(this.y < this.yGoal) {
        //     this.axis = 2;
        //     this.sprite = this.spriteWalkDown;
        // } else if(this.y > this.yGoal) {
        //     this.axis = 0;
        //     this.sprite = this.spriteWalkUp;
        // }
    }

    Update(timestamp) {
        if(this.newToken) {
            if(this.queue.getSize()!==0) {
                //Set the new state based on token
                let token = this.queue.dequeue();
                if(token.type === 'walk') {
                    switch(this.unitState) {
                        case 'idle':
                            this.unitState = "walk";
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();

                            this.animFrame=0;
                            this.frameCount=0;
                            this.animX = 0;
                            this.animY = 0;
                            
                        break;
                        case 'walk':
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();
                        break;
                        case 'attack':
                            this.unitState = "walk";
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();

                            this.animFrame=0;
                            this.frameCount=0;
                            this.animX = 0;
                            this.animY = 0;

                        break;
                        case 'hurt':
                            this.unitState = "walk";
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();

                            this.animFrame=0;
                            this.frameCount=0;
                            this.animX = 0;
                            this.animY = 0;

                        break;
                        case 'hurt_spell':
                            this.unitState = "walk";
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();

                            this.animFrame=0;
                            this.frameCount=0;
                            this.animX = 0;
                            this.animY = 0;

                        break;
                        case 'buff_spell':
                            this.unitState = "walk";
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.setAxis();

                            this.animFrame=0;
                            this.frameCount=0;
                            this.animX = 0;
                            this.animY = 0;

                        break;
                        default:
                            console.error('Bad token');
                            break;
                    } 
                } else if(token.type === 'attack') {
                    switch(this.unitState) {
                        case 'idle':
                            this.unitState = "attack";
                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;

                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'walk':

                            this.unitState = "attack";
                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'attack':

                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt':

                            this.unitState = "attack";
                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt_spell':

                            this.unitState = "attack";
                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'buff_spell':

                            this.unitState = "attack";
                            this.sprite = this.spriteAttack;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        default:
                            console.error('Bad token');
                            break;
                    } 
                }else if(token.type === 'hurt') {
                    switch(this.unitState) {
                        case 'idle':
                            this.unitState = "hurt";
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;

                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'walk':

                            this.unitState = "hurt";
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'attack':
                           
                            this.unitState = "hurt";
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt':
                           
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt_spell':
                            this.unitState = "hurt";
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'buff_spell':
                            this.unitState = "hurt";
                            this.sprite = this.spriteHurt;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        default:
                            console.error('Bad token');
                            break;
                    } 
                }else if(token.type === 'hurt_spell') {
                    switch(this.unitState) {
                        case 'idle':
                            this.unitState = "hurt_spell";
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;

                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'walk':

                            this.unitState = "hurt_spell";
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'attack':
                           
                            this.unitState = "hurt_spell";
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt':
                            this.unitState = "hurt_spell";
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt_spell':
                           
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'buff_spell':
                            this.unitState = "hurt_spell";
                            this.sprite = this.spriteHurtSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        default:
                            console.error('Bad token');
                            break;
                    } 
                }else if(token.type === 'buff_spell') {
                    switch(this.unitState) {
                        case 'idle':
                            this.unitState = "buff_spell";
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'walk':
                            this.unitState = "buff_spell";
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'attack':
                            this.unitState = "buff_spell";
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt':
                            this.unitState = "buff_spell";
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'hurt_spell':
                            this.unitState = "buff_spell";
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        case 'buff_spell':
                            this.sprite = this.spriteBuffSpell;
                            this.xGoal = +token.x;
                            this.yGoal = +token.y;
                            this.animFrame=0;
                            this.frameCount=0;
                        break;
                        default:
                            console.error('Bad token');
                            break;
                    } 
                }
            } else {
                this.unitState = 'idle';
                let spr;
                if (this.axis === 1){
                    spr = this.spriteIdleRight;
                } else {
                    spr = this.spriteIdleLeft;
                }
                this.sprite = spr;
                this.animFrame=0;
                this.frameCount=0;
                this.animX = 0;
                this.animY = 0;
            }
            this.newToken = false;
        }
        

        if(this.unitState==='idle') {
            if(this.queue.getSize()!==0) {
                this.newToken = true
            } else {
                this.idleAnimation(timestamp);
            }
        } else if(this.unitState==='walk') {
            if(this.x === this.xGoal && this.y === this.yGoal) {
                this.newToken = true;
            } else {
                this.walkAnimation(timestamp);
            }
        } else if(this.unitState==='attack') {
            if(this.animFrame > this.attackFrames) {
                this.newToken = true;
            } else {
                this.attackAnimation(timestamp);
            }
        } else if(this.unitState==='hurt') {
            if(this.animFrame > this.hurtFrames) {
                this.newToken = true;
            } else {
                this.hurtAnimation(timestamp);
            }
        } else if(this.unitState==='hurt_spell') {
            if(this.animFrame > this.hurtSpellFrames) {
                this.newToken = true;
            } else {
                this.hurtSpellAnimation(timestamp);
            }
        } else if(this.unitState==='buff_spell') {
            if(this.animFrame > this.hurtSpellFrames) {
                this.newToken = true;
            } else {
                this.buffSpellAnimation(timestamp);
            }
        } else {
            console.error("Unit has an invalid state");
        }

    }

}

export default Unit;
