import React, { useEffect, useState } from 'react'
import {BrowserRouter as Router, Switch, NavLink, Route} from 'react-router-dom'
import App from "../App"
import Stomp from "stompjs"
import SockJS from "sockjs-client"
import Start from '../pages/Economy/Economy'
import Economy1 from '../pages/Economy/Economy_Nation'
import Economy2 from '../pages/Economy/Economy_Hero'
import Economy3 from '../pages/Economy/Economy_Shop'
import "../pages/styles/AppContainer.css"
import axios from "axios"
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import Wiki from "../Wiki"
import Aboutus from "../pages/Wiki/Aboutus"

var stompClientFractions;
var stompClientHero;
var stompClientSpellShop;
var stompClientCreatureShop;
var stompClientArtifactShop;
var stompClientCurrentGold;
var stompClientOpponentsEcoPhase;
var stompClientReady;
var stompClientReadyEco;
var stompClientPlayer1Setup;
var stompClientPlayer2Setup;

var loggedIn = 0;

const AppContainer = () => {
    const history = useHistory();
    const [gameId, setGameId] = useState(null);
    const [playerId, setPlayerId] = useState(null);
    const [heroes, setHeroes] = useState(null);
    const [units, setUnits] = useState(null);
    const [spells, setSpells] = useState(null);
    const [items, setItems] = useState(null);
    const [goToShop, setGoToShop] = useState(false);
    const [money, setMoney] = useState(null);
    const [readyP1, setReadyP1] = useState(false);
    const [readyP2, setReadyP2] = useState(false);
    const [inLobby, setInLobby] = useState(false);

    const [readyEcoP1, setReadyEcoP1] = useState(false);
    const [readyEcoP2, setReadyEcoP2] = useState(false);
    const [allSocketsOpened, setAllSocketsOpened] = useState(false);
    const [waitScreen, setWaitScreen] = useState(true);

    
    const startEconomy = () => {
        if(readyP1 && readyP2) {
            axios.post("http://localhost:8080/economy/"+gameId+"/startEconomy").then(res => {
                console.log("-----------> FROM: Request");
                console.log(res)
            });
        }
    }

    const startEconomyTemp = () => {
        axios.post("http://localhost:8080/economy/"+gameId+"/startEconomy").then(res => {
            console.log("-----------> FROM: Request");
            console.log(res)
        });
    }

    const create_game = () => {
        axios.post("http://localhost:8080/lobby/create", {"id": "gracz-1"}).then(res => {
            console.log("-----------> FROM: Create Game");
            loggedIn = 1;
            setPlayerId(1);
            setInLobby(true);
            setGameId(res.data);
        });
    }

    const connectToRandom = (id) => {
        axios.post("http://localhost:8080/lobby/connect/"+id, {"id": "gracz-2"}).then(res => {
            console.log("-----------> FROM: Connect Random");
            loggedIn = 1;
            setPlayerId(2);
            setInLobby(true);
            setGameId(res.data);
        });      
    }

    const ready = () => {
        if(allSocketsOpened) {
            axios.post("http://localhost:8080/lobby/"+gameId+"/readyForEco/"+playerId).then(res => {
                console.log("-----------> FROM: Ready");
            });
        }
    }

    const readyBattle = () => {
        axios.post("http://localhost:8080/economy/"+gameId+"/readyForBattle/"+playerId).then(res => {
            console.log("-----------> FROM: Ready Battle");
        });
    }

    const connectToFractionsSubscribe = (response) => {
        console.log("-----------> FROM: Fractions");
        console.log(response);
        history.push("/economy/1");
      }
    
    const connectToFractionsConnect = () => {
        stompClientFractions.subscribe("/topic/fractions/"+gameId, connectToFractionsSubscribe);
    }
    
    const connectToFractions = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientFractions = Stomp.over(socket);
        stompClientFractions.connect({}, connectToFractionsConnect);
    }

    //HERO
    const connectToHeroSubscribe = (response) => {
        console.log("-----------> FROM: Hero");
        console.log(response);
        let data = JSON.parse(response.body);
        setHeroes(data)
      }
    
    const connectToHeroConnect = () => {
        stompClientHero.subscribe("/topic/heroes/"+playerId+"/"+gameId, connectToHeroSubscribe);
        setTimeout( connectToCreatureShop, 300 );
    }
    
    const connectToHero = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientHero = Stomp.over(socket);
        stompClientHero.connect({}, connectToHeroConnect);
    }

    //creatureShop
    const connectToCreatureShopSubscribe = (response) => {
        console.log("-----------> FROM: creatureShop");
        let data = JSON.parse(response.body);
        let unitData = [];
        data.forEach(a => {
            unitData.push(JSON.parse(a.replaceAll('=', ":")));
        })
        setUnits(unitData);
      }
    
    const connectToCreatureShopConnect = () => {
        stompClientCreatureShop.subscribe("/topic/creatureShop/"+playerId+"/"+gameId, connectToCreatureShopSubscribe);
        setTimeout( connectToSpellShop, 300 );
    }
    
    const connectToCreatureShop = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientCreatureShop = Stomp.over(socket);
        stompClientCreatureShop.connect({}, connectToCreatureShopConnect);
    }
    
    //Spell Shop
    const connectToSpellShopSubscribe = (response) => {
        console.log("-----------> FROM: Spell Shop");
        let data = JSON.parse(response.body);
        let spellData = [];
        data.forEach(a => {
            spellData.push(JSON.parse(a.replaceAll('=', ":")));
        })
        setSpells(spellData);
      }
    
    const connectToSpellShopConnect = () => {
        stompClientSpellShop.subscribe("/topic/spellShop/"+playerId+"/"+gameId, connectToSpellShopSubscribe);
        setTimeout( connectToArtifactShop, 300 );
    }
    
    const connectToSpellShop = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientSpellShop = Stomp.over(socket);
        stompClientSpellShop.connect({}, connectToSpellShopConnect);
    }

    //artifact Shop
    const connectToArtifactShopSubscribe = (response) => {
        console.log("-----------> FROM: Artifact Shop");
        let data = JSON.parse(response.body);
        let artifactData = [];
        data.forEach(a => {
            artifactData.push(JSON.parse(a.replaceAll('=', ":")));
        })
        setItems(artifactData);
      }
    
    const connectToArtifactShopConnect = () => {
        stompClientArtifactShop.subscribe("/topic/artifactShop/"+playerId+"/"+gameId, connectToArtifactShopSubscribe);
        setTimeout( connectToCurrentGold, 300 );
    }
    
    const connectToArtifactShop = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientArtifactShop = Stomp.over(socket);
        stompClientArtifactShop.connect({}, connectToArtifactShopConnect);
    }

    //Current Gold
    const connectToCurrentGoldSubscribe = (response) => {
        console.log("-----------> FROM: Currency");
        console.log(response);
        let data = JSON.parse(response.body)
        setMoney(data)
      }
    
    const connectToCurrentGoldConnect = () => {
        stompClientCurrentGold.subscribe("/topic/currentGold/"+playerId+"/"+gameId, connectToCurrentGoldSubscribe);
        setTimeout( connectToOpponentsEcoPhase, 300 );
    }
    
    const connectToCurrentGold = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientCurrentGold = Stomp.over(socket);
        stompClientCurrentGold.connect({}, connectToCurrentGoldConnect);
    }

    //Opponents Eco Phase
    const connectToOpponentsEcoPhaseSubscribe = (response) => {
        console.log("-----------> FROM: Eco Phase");
        console.log(response);
      }
    
    const connectToOpponentsEcoPhaseConnect = () => {
        let id = playerId === 1 ? 2 : 1;
        stompClientOpponentsEcoPhase.subscribe("/topic/opponentsEcoPhase/"+id+"/"+gameId, connectToOpponentsEcoPhaseSubscribe);
        setTimeout( connectToReady, 300 );
    }
    
    const connectToOpponentsEcoPhase = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientOpponentsEcoPhase = Stomp.over(socket);
        stompClientOpponentsEcoPhase.connect({}, connectToOpponentsEcoPhaseConnect);
    }

    //Ready
    const connectToReadySubscribe = (response) => {
        console.log("-----------> FROM: Ready");
        let data = JSON.parse(response.body);
        let readyOne = JSON.parse(data[0]);
        let readyTwo = JSON.parse(data[1]);
        readyOne.readyForEcoStatus === 'true' ? setReadyP1(true) : setReadyP1(false);
        readyTwo.readyForEcoStatus === 'true' ? setReadyP2(true) : setReadyP2(false);

        readyOne.readyForBattleStatus === 'true' ? setReadyEcoP1(true) : setReadyEcoP1(false);
        readyTwo.readyForBattleStatus === 'true' ? setReadyEcoP2(true) : setReadyEcoP2(false);
    }
    
    const connectToReadyConnect = () => {
        stompClientReady.subscribe("/topic/readyForEco/"+gameId, connectToReadySubscribe);
        setAllSocketsOpened(true);
    }
    
    const connectToReady = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientReady = Stomp.over(socket);
        stompClientReady.connect({}, connectToReadyConnect);
    }

    //Player 1 Setup
    const connectToPlayer1SetupSubscribe = (response) => {
        console.log("-----------> FROM: Player Setup");
        let data = JSON.parse(response.body);
        console.log(data);
    }
    
    const connectToPlayer1SetupConnect = () => {
        stompClientPlayer1Setup.subscribe("/topic/playerSetup/1/"+gameId, connectToPlayer1SetupSubscribe);
    }
    
    const connectToPlayer1Setup = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientPlayer1Setup = Stomp.over(socket);
        stompClientPlayer1Setup.connect({}, connectToPlayer1SetupConnect);
    }
    
    //Player 2 Setup
    const connectToPlayer2SetupSubscribe = (response) => {
        console.log("-----------> FROM: Player Setup");
        let data = JSON.parse(response.body);
        console.log(data);
    }
    
    const connectToPlayer2SetupConnect = () => {
        stompClientPlayer2Setup.subscribe("/topic/playerSetup/2/"+gameId, connectToPlayer2SetupSubscribe);
    }
    
    const connectToPlayer2Setup = () => {
        let socket = new SockJS('http://localhost:8080/gameplay/');
        stompClientPlayer2Setup = Stomp.over(socket);
        stompClientPlayer2Setup.connect({}, connectToPlayer2SetupConnect);
    }

    const leaveLobby = () => {
        //Reset Data
        setGameId(null);
        setPlayerId(null);
        setHeroes(null);
        setUnits(null);
        setSpells(null);
        setItems(null);
        setGoToShop(false);
        setMoney(null);
        setReadyP1(false);
        setReadyP2(false);
        setInLobby(false);

        if(stompClientFractions?.connected) {
            stompClientFractions.disconnect();
        }
        if(stompClientHero?.connected) { stompClientHero.disconnect(); }
        if(stompClientSpellShop?.connected) { stompClientSpellShop.disconnect(); }
        if(stompClientCreatureShop?.connected) { stompClientCreatureShop.disconnect(); }
        if(stompClientArtifactShop?.connected) { stompClientArtifactShop.disconnect(); }
        if(stompClientCurrentGold?.connected) { stompClientCurrentGold.disconnect(); }
        if(stompClientOpponentsEcoPhase?.connected) { stompClientOpponentsEcoPhase.disconnect(); }
        if(stompClientReady?.connected) { stompClientReady.disconnect(); }
        if(stompClientReadyEco?.connected) { stompClientReadyEco.disconnect(); }
        if(stompClientPlayer1Setup?.connected) { stompClientPlayer1Setup.disconnect(); }
        if(stompClientPlayer2Setup?.connected) { stompClientPlayer2Setup.disconnect(); }
        history.push("/");
    }

    useEffect(() => {
        if(gameId){
            connectToFractions();
        }
    }, [gameId])

    useEffect(()=>{
        if(gameId && playerId) {
            connectToHero();
            // connectToReadyEco();
        }
    }, [gameId, playerId])

    useEffect(()=>{
        if(heroes) {
            console.log("Adam");
            console.log(heroes);
            history.push("/economy/2");
        }
    }, [heroes])

    const resetShopInfo = () => {
        setGoToShop(false);
        setUnits(null);
        setSpells(null);
        setItems(null);
    }

    useEffect(() => {
        if(units && spells && items) {
            if(!goToShop) {
                history.push("/economy/3");
                setGoToShop(true);
            }
        }
    }, [units, spells, items, goToShop])

    useEffect(() => {
        if(readyEcoP2 && readyEcoP1) {
            setWaitScreen(false);
        };
    }, [ readyEcoP1, readyEcoP2])

    const goToGame = () => {
        history.push("/game");
    }

    const goToAbout = () => {
        history.push("/about");
    }

    const goToWiki = () => {
        history.push("/wiki");
    }

    return (
        <div className="scaleContainer">
            <div className="contnentWrapper">
                <img id="background" src="/background.png" style={{width: "100%", height: "100%", position: "absolute"}} alt="background"/>
                <Switch>
                    <Route path="/" exact render={() => {
                        return <Start goToWiki={goToWiki} goToAbout={goToAbout} leaveLobby={leaveLobby} create_game={create_game} connectToRandom={connectToRandom} startEconomy={startEconomy} gameId={gameId} ready={ready} unlockStart={readyP1 && readyP2} playerId={playerId} inLobby={inLobby} readyP1={readyP1} readyP2={readyP2}/>
                    }
                    }/>

                    <Route path="/game" exact render={() => {
                        if (gameId) {
                            return <App setWaitScreen={setWaitScreen} leaveLobby={leaveLobby} gameId={gameId} playerId={playerId} ready={readyBattle} playerId={playerId} readyEcoP1={readyEcoP1} readyEcoP2={readyEcoP2} waitScreen={waitScreen}/>
                        } else {
                            return <Redirect to="/"/>
                        }
                    }
                    }/>

                    <Route path="/economy/1" exact render={() => {
                        if (gameId) {
                            return <Economy1 leaveLobby={leaveLobby} gameId={gameId} playerId={playerId}/>
                        } else {
                            return <Redirect to="/"/>
                        }
                    }
                    }/>

                    <Route path="/economy/2" exact render={() => {
                        if (gameId) {
                            return <Economy2 leaveLobby={leaveLobby} gameId={gameId} playerId={playerId} heroes={heroes}/>
                        } else {
                            return <Redirect to="/"/>
                        }  
                    }
                    }/>

                    <Route path="/economy/3" exact render={() => {
                        if (gameId) {
                            return <Economy3 goToGame={goToGame} leaveLobby={leaveLobby} gameId={gameId} playerId={playerId} units={units} spells={spells} items={items} money={money} resetShopInfo={resetShopInfo}/>
                        } else {
                            return <Redirect to="/"/>
                        }
                    }
                    }/>

                    <Route path="/wiki" exact render={() => {
                        return <div style={{justifySelf:"flex-start", alignSelf:"flex-start", position:"absolute", left:"0"}}><Wiki/></div>
                    }
                    }/>

                    <Route path="/about" exact render={() => {
                        return <div style={{justifySelf:"flex-start", alignSelf:"flex-start", position:"absolute", left:"0"}}><Aboutus/></div>
                    }
                    }/>
                </Switch>
            </div>
        </div>
    )
}

export default AppContainer
