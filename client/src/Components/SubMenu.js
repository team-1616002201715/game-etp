import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const SidebarLink = styled(Link)`
    display: flex;
    color: rgba(220,228,244,0.5);
    justify-content: space-between;
    align-items:center;
    font-size: 20px;
    font-weight: bold;
    font-family: 'Courier New', monospace;
    padding: 7%;
    height: 2%;
    text-decoration: none;
    list-style:none;

    &:hover{
        background: rgba(27,99,206,0.3);
        border-radius:25px;
        border-left: 10px solid rgba(39,76,132,0.7);
        curser: pointer;

    }
`;

const SidebarLabel = styled.span`
  margin-left: 5px;
`;

const DropdownLink = styled(Link)`
  margin-top:2px;
  margin-left: 10%;
  background: rgba(25,25,50,0.35);
  height: 60px;
  padding-left: 3rem;
  border-radius: 25px;
  display: flex;
  align-items: center;
  font-family: 'Courier New', monospace;
  text-decoration: none;
  color: rgba(220,228,244,0.5);
  font-size: 17px;
  font-weight: bold;
  &:hover {
    background: rgba(27,99,206,0.3);
    cursor: pointer;
    border-left: 10px solid rgba(39,76,132,0.7);
    border-right: 10px solid rgba(39,76,132,0.7);
  }
`;

const SubMenu = ({ item }) => {
  const [subnav, setSubnav] = useState(false);

  const showSubnav = () => setSubnav(!subnav);

  return (
    <>
      <SidebarLink to={item.path} onClick={item.subNav && showSubnav}>
        <div>
          {item.icon}
          <SidebarLabel>{item.title}</SidebarLabel>
        </div>
        <div>
          {item.subNav && subnav
            ? item.iconOpened
            : item.subNav
            ? item.iconClosed
            : null}
        </div>
      </SidebarLink>
      {subnav &&
        item.subNav.map((item, index) => {
          return (
            <DropdownLink to={item.path} key={index}>
              {item.icon}
              <SidebarLabel>{item.title}</SidebarLabel>
            </DropdownLink>
          );
        })}
    </>
  );
};

export default SubMenu;
