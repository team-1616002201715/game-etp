import React, {useEffect} from 'react'
import "./styles/Spellbook.css"

const Spellbook = ({buttons, castRange, cancelCasting, active, activeSpellbook, castingSpells, setActiveSpellbook, mana}) => {

    const spellCast = (name) => {
        setActiveSpellbook();
        castRange(name);
    }

    useEffect(() => {
        console.log(buttons)
    }, [buttons])

    let cancelBtn = castingSpells ? "spellCancel" : "spellCancel--deactivated"
    let spellBookClose = activeSpellbook ? "spellbook" : "spellbook--deactivated"
    let manaAmount = mana ? mana : 0;
    
    if(active) {
        return (
            <div className="container">
                <div className={`${spellBookClose}`}>
                    <div className="svg-spells">SPELLS</div>
                    <div className="svg-mana">{manaAmount} Mana</div>
                    <div className="spellWindow">
                        <ul>
                            {buttons.map(elem => <li className="spell" key={elem.name}>
                                <img src="./fireBall.png" alt={elem.name}/>
                                <div className="spellName">{elem.name}</div>
                                <div className='spellCast'>
                                    <button onClick={() => spellCast(elem.name)}>Cast</button>
                                    <div className='spellCost'>Cost:{elem.manaCost}</div>
                                </div>
                            </li>)}
                        </ul>
                    </div>
                </div>
                <button className={`${cancelBtn}`} onClick={() => cancelCasting()}>Cancel Casting</button>
            </div>
        )
    } else {
        return (
            <>
            </>
        )
    }
    
}

export default Spellbook
