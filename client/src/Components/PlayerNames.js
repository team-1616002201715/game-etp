import React from 'react'
import "./styles/PlayerNames.css"
const PlayerNames = ({number, id}) => {
    return (
        <div className={`player${number}`}>
            
            <p>{number===1 ? "Player1" : "Player2"}</p>
        </div>
    )
}

export default PlayerNames
