import React from 'react'
import { useEffect, useState } from 'react/cjs/react.development'
import Buffdisplay from './Buffdisplay';
import "./styles/ActiveCreature.css"

const ActiveCreature = ({width, activeCreature, Units, unitStatsUpdate, unitStatsUpdateSet, activeUnitSet}) => {
    const [index, setIndex] = useState(-1);
    const [hp, setHp] = useState(0);
    const [name, setName] = useState("");
    const [armor, setArmor] = useState(0);
    const [amount, setAmount] = useState(0);
    const [moveRange, setMoveRange] = useState(0);
    const [buffs, setBuffs] = useState({});

    useEffect(() => {
        if(activeCreature.update && Units.length > 0) {
            let index2 = Units.findIndex(elem => elem.xFinalGoal === activeCreature.x && elem.yFinalGoal === activeCreature.y)
            if(unitStatsUpdate) {
                if(index2 < Units.length && index2>-1) {
    
                    setHp(Units[index2].hp);
                    setName(Units[index2].unitName);
                    setArmor(Units[index2].armor)
                    setAmount(Units[index2].amount)
                    setMoveRange(Units[index2].moveRange)
                    setBuffs(Units[index2].buffContainer)
                    console.log("DIONIZY - info update");
                }
                unitStatsUpdateSet(false);
                activeUnitSet(false);
            }
        }
    }, [unitStatsUpdate,unitStatsUpdateSet, Units.length, Units, activeCreature.x, activeCreature.y, activeCreature.update])

    return (
        <>
            <img className="uiBackground" src="./uiBackground.png" alt="transparent background"/>
            <div className="creaturePortrait" style={{borderWidth:width*0.003}}>
                {name ? <img className="unitPortrait" src={`/UnitPortrait/${name}.png`}/> : null}
                <div className="unitCount" style={{borderWidth:width*0.003}}>{amount}</div>
            </div>
            <Buffdisplay buffs={buffs}/>
            <div className="creatureInfo" style={{borderWidth:width*0.003, color:"white"}}>
                <div className="stat">HP: {hp}</div>
                <div className="stat">DEF: {armor}</div>
                <div className="stat">MOV: {moveRange}</div>
                
            </div>
        </>
        
    )
}

export default ActiveCreature
