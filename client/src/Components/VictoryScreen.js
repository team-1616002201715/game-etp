import React from 'react'
import { useEffect, useState } from 'react/cjs/react.development'

const VictoryScreen = ({id, endGameScreen, rematch, leaveLobby}) => {
    const [message, setMessage] = useState("");
    useEffect(() => {
        if(id) {
            if(endGameScreen === id) {
                setMessage("Victory")
            } else {
                setMessage("Defeat")
            }
        }
    }, [id, endGameScreen])

    if(endGameScreen !== 0) {
        return (
                <div className="victoryScreen">
                    {`${message}`}
                    <div className='victoryScreen__buttons'>
                        <button onClick={() => rematch()}>Rematch</button>
                        <button onClick={() => leaveLobby()}>Leave</button>
                    </div>
                </div>
        )
    } else {
        return (
            <>
                
            </>
        )
    }
    
}

export default VictoryScreen
