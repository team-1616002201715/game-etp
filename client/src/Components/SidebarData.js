import React from 'react'
import * as RiIcons from 'react-icons/ri'

export const SidebarData = [
    //about game menu
    {
        title: 'ABOUT GAME',
        path: '/about'
    },
    {
        title: 'HOW TO PLAY',
        path: '/howto'
    },
    //units menu and submenu
    {
        title: 'UNITS',
        path: '/units',
        iconClosed: <RiIcons.RiArrowDownSFill />,
        iconOpened: <RiIcons.RiArrowUpSFill />,
        subNav: [
            {
                title: 'HANDYMAN',
                path: '/units/handyman'
            },
            {
                title: 'HEDGEHOG',
                path: '/units/hedgehog'
            },
            {
                title: 'MARAUDER',
                path: '/units/marauder'
            },
            {
                title: 'NEW YORKER',
                path: '/units/newyorker'
            },
            {
                title: 'PATRIOT',
                path: '/units/patriot'
            },
            {
                title: 'RAILGUN',
                path: '/units/railgun'
            },
            {
                title: 'SPECTRE',
                path: '/units/spectre'
            },
            {
                title: 'TROOP CARRIER',
                path: '/units/troopcarrier'
            },
            {
                title: 'IVAYLO',
                path: '/units/ivaylo'
            },
            {
                title: 'OFFICER',
                path: '/units/officer'
            },
            {
                title: 'UNDEAD',
                path: '/units/undead'
            },
            {
                title: 'WEREWOLF',
                path: '/units/werewolf'
            },
            {
                title: 'TROGLODYTE',
                path: '/units/troglodyte'
            },
            {
                title: 'MINIGUNNER',
                path: '/units/minigunner'
            },
            {
                title: 'BABA YAGA',
                path: '/units/babayaga'
            },
            {
                title: 'MATUSHKA',
                path: '/units/matushka'
            }
        ]
    },
    // items menu and submenu
    {
        title: 'ITEMS',
        path: '/items',
        iconClosed: <RiIcons.RiArrowDownSFill />,
        iconOpened: <RiIcons.RiArrowUpSFill />,
        subNav: [
            {
                title: 'BLACKSHARD OF THE DEAD KNIGHT',
                path: '/items/blackshard'
            },
            {
                title: 'COLLAR OF CONJURING',
                path: '/items/collar'
            },
            {
                title: 'ORB OF TEMPESTUOUS FIRE',
                path: '/items/orb'
            },
            {
                title: 'RING OF VITALITY',
                path: '/items/ring'
            },
            {
                title: 'SHIELD OF THE YAWNING DEAD',
                path: '/items/shield'
            },
            {
                title: "STATEMAN'S MEDAL",
                path: '/items/stateman'
            },
            {
                title: 'SURCOAT OF COUNTERPOISE',
                path: '/items/surcoat'
            },
            {
                title: 'TOME OF AIR MAGIC',
                path: '/items/tome'
            }
        ]
    },
    {
        title: 'SPELLS',
        path: '/spells',
        iconClosed: <RiIcons.RiArrowDownSFill />,
        iconOpened: <RiIcons.RiArrowUpSFill />,
        subNav: [
            {
                title: 'HASTE',
                path: '/spells/haste'
            },
            {
                title: 'BLOODLUST',
                path: '/spells/bloodlust'
            },
            {
                title: 'DISPELL',
                path: '/spells/dispell'
            },
            {
                title: 'RAGE',
                path: '/spells/rage'
            },
            {
                title: 'SLOW',
                path: '/spells/slow'
            },
            {
                title: "FIREBALL",
                path: '/spells/fireball'
            },
            {
                title: 'IMPLOSION',
                path: '/spells/implosion'
            },
            {
                title: 'DEATH RIPPLE',
                path: '/spells/ripple'
            },
            {
                title: 'MAGIC ARROW',
                path: '/spells/arrow'
            }
        ]
    },
]