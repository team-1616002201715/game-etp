import React from 'react'
import { useState } from 'react/cjs/react.development'

const ShopUnitItem = ({unitName, buyItem, buyMaxCreature, sellItem, unit}) => {
    const [sellAmount, setSellAmount] = useState(0);
    const [buyAmount, setBuyAmount] = useState(1);
    const [toggle, setToggle] = useState(false);

    const sell = (e) => {
        e.stopPropagation();
        sellItem(unitName, sellAmount, "CREATURE");
    }

    const buy = (e) => {
        e.stopPropagation();
        buyItem(unitName, buyAmount, "CREATURE");
    }

    const InputBuy = (e) => {
        e.preventDefault();
        e.stopPropagation();
    }

    const InputSell = (e) => {
        e.preventDefault();
        e.stopPropagation();
    }

    let tog = toggle? "" : "shop__unit__info--hidden";
    return (
        <div onClick={() => setToggle(!toggle)} style={{color: "white"}} className="shop__window__item">
            <div>{unitName} Cost: {unit.goldCost}</div>
            <div className='shop__item__controls'>
                <input type="text" value={buyAmount} onClick={(e) => InputBuy(e)} onChange={(e) => setBuyAmount(e.target.value)}/>
                <button onClick={(e) => buy(e)}>Buy</button>

                {/* <input type="text" value={sellAmount} onClick={(e) => InputSell(e)} onChange={(e) =>  setSellAmount(e.target.value)}/>
                <button onClick={(e) => sell(e)}>Sell</button> */}

                <button onClick={() => buyMaxCreature(unitName, unit.amount)}>Max</button>
            </div>
            <div className={`shop__unit__info ${tog}`}>
                Amount - {unit.amount}<br/><br/>
                DEF: {unit.creature.armor}<br/><br/>
                HP: {unit.creature.maxHp}<br/><br/>
                MOV: {unit.creature.moveRange}<br/><br/>
                DMG: {unit.creature.damage}<br/><br/>
                MR: {unit.creature.magicResistance}<br/><br/>
                TYPE: {unit.creature.movementType}<br/><br/>
                Tier: {unit.creature.tier}<br/><br/>
                Cost: {unit.goldCost}$<br/><br/>
            </div>
        </div>
    )
}

export default ShopUnitItem
