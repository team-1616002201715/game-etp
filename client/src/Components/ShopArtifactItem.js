import React from 'react'
import { useState } from 'react/cjs/react.development'

const ShopUnitItem = ({artifactName, buyItem, sellItem, artifact}) => {
    const [toggle, setToggle] = useState(false);

    const sell = (e) => {
        e.stopPropagation();
        sellItem(artifactName, 1, "ARTIFACT");
    }

    const buy = (e) => {
        e.stopPropagation();
        buyItem(artifactName, 1, "ARTIFACT");
    }

    let tog = toggle? "" : "shop__unit__info--hidden";
    return (
        <div onClick={() => setToggle(!toggle)} style={{color: "white", backgroundSize: '64px', backgroundImage: `url(/ShopAssets/${artifactName.toLowerCase().replace(/ /g, '-').replace(/'/g, '')}.png)`, backgroundRepeat: 'no-repeat', backgroundPosition: '95%, 50%'}} className="shop__window__item">
            <div>{artifactName}</div>
            <div className='shop__item__controls'>
                <button onClick={(e) => buy(e)}>Buy</button>
                <button onClick={(e) => sell(e)}>Sell</button>
            </div>
            <div className={`shop__unit__info ${tog}`}>
                Slot:   {artifact.item.artifactSlot}<br/><br/>
                Cost:   {artifact.goldCost}$<br/><br/>
            </div>
        </div>
    )
}

export default ShopUnitItem
