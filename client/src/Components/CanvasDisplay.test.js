import { render, screen } from '@testing-library/react';
import CanvasDisplay from './CanvasDisplay';


test('renders without crashing', () => {
  render(<CanvasDisplay state={{
    testingProps: 0,
    gameId: "game_id",
    playerId: 0,
    activePlayer: 1,

    width: 1920,
    height: 1080,
    cellSizeX: 1920*3593/100000,
    cellSizeY: 1080*4590/100000,
    offsetX: 1920*14062/100000,
    offsetY: 1080*10082/100000,
    boardSizeH: 20,
    boardSizeV: 15,


    activeUnit: {
        name: "Unit_Nazwa2",
        x: 0,
        y: 1,
        prev_x: 0,
        prev_y: 1
    },
    Cells: [{x: 0, y: 0, clickable: 0, attackable: 0, item: {name: "empty"}}, {x: 1, y: 0, clickable: 0, attackable: 0, item: {name: "empty"}}],

    Player1: ["Unit_Nazwa2", "Unit_Nazwa3", "Unit_Nazwa4"],

    Player2: ["Unit_Nazwa", "Unit_Nazwa5"],

    UnitInfo: [],

    spellBook:[]
  }}></CanvasDisplay>)
  screen.debug();
});
