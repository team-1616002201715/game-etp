import React, {useEffect, useRef, useLayoutEffect, useState, useCallback } from 'react';

//Board Assets
const img_cell = new Image();
img_cell.src = "Cell.png";

const img_cell_hit = new Image();
img_cell_hit.src = "Cell_Hit.png";

const img_cell_attack = new Image();
img_cell_attack.src = "Cell_Attack.png";

const img_cell_clickable = new Image();
img_cell_clickable.src = "Cell_Clickable.png";

const img_cell_active = new Image();
img_cell_active.src = "Cell_Active.png";

const img_cell_wall = new Image();
img_cell_wall.src = "Cell_Wall.png";

function useWindowSize(width, height) {
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([width, height]);
    }
    updateSize();
  }, [width, height]);
  return size;
}

let stopId;


function DrawClickable1(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgb(200,200,220)";
    ctx.drawImage(img_cell_clickable,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawClickable2(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgb(220,200,200)";
    ctx.drawImage(img_cell_clickable,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawAttackable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgb(220,70,70)";
    ctx.drawImage(img_cell_attack,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawCastable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgb(220,70,70)";
    ctx.drawImage(img_cell_attack,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawActive(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgb(220,70,70)";
    ctx.drawImage(img_cell_active,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawSpecialTile(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.drawImage(img_cell_wall,x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawTest(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y) {
    ctx.fillStyle = "rgba(200,0,200, 0.3)";
    ctx.fillRect(x*cellSizeX+offsetX,y*cellSizeY+offsetY, cellSizeX, cellSizeY);
}

function DrawBoard(ctx, cellSizeX,cellSizeY, offsetX, offsetY, boardSizeH, boardSizeV) {
    ctx.strokeStyle = "rgba(0,180,0)";
    for(var i=0; i<boardSizeH; i++) {
        for(var j=0; j<boardSizeV; j++) {
            ctx.drawImage(img_cell, i*cellSizeX+offsetX,j*cellSizeY+offsetY,cellSizeX,cellSizeY);
        }
    }
}


export default function CanvasDisplay({width,height,cellSizeX,cellSizeY,offsetX,offsetY,boardSizeH,boardSizeV,Cells, castingSpells, units, activeCreature}) {
    const canvasRef = useRef(null);

    useWindowSize(width, height);
    var test = 0;
    const render = useCallback((timestamp) => {

        //Prepare canvas 
        const canvas = canvasRef.current;
        if(canvas) {
            const ctx = canvas.getContext("2d");
            ctx.canvas.width  = width;
            ctx.canvas.height = height;
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            //Display here:
            //----->
            DrawBoard(ctx, cellSizeX,cellSizeY, offsetX, offsetY, boardSizeH, boardSizeV);
            for(let y=0; y<=boardSizeV; y++) {
                for(let x=0; x<=boardSizeH; x++) {
                    if(Cells[y][x].clickable === 1 && !castingSpells) {
                        DrawClickable1(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y); 
                    } else if(Cells[y][x].clickable === 2 && !castingSpells) {
                        DrawClickable2(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    }
                    if(Cells[y][x].attackable === 1 && !castingSpells) {
                        DrawAttackable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    } else if(Cells[y][x].attackable === 2 && !castingSpells) {
                        DrawAttackable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    }
                    if(Cells[y][x].castable === 1 && castingSpells) {
                        DrawCastable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    } else if(Cells[y][x].castable === 2 && castingSpells) {
                        DrawCastable(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    } else if(Cells[y][x].item.tile === "A") {
                        DrawSpecialTile(ctx, cellSizeX,cellSizeY, offsetX, offsetY, x, y);
                    }
                }
            }

            units.forEach(unit => {
                let dx = unit.x*cellSizeX+offsetX;
                let dy = unit.y*cellSizeY+offsetY;
                let dWidth = cellSizeX
                let dHeight = cellSizeY*2.6

                let sx = unit.animX;
                let sy = unit.animY;
                let sWidth = unit.animWidth;
                let sHeight = unit.animHeight;

                if(Cells[Math.floor(unit.y)][Math.floor(unit.x)].item.tile === "A" && Cells[Math.floor(unit.y)][Math.floor(unit.x)].item.anim === 0) {
                    Cells[Math.floor(unit.y)][Math.floor(unit.x)].item.anim = timestamp
                }

                // if(timestamp - test >= 200) {
                //     ctx.fillStyle = "rgb(200,0,0)";
                //     ctx.fillRect(dx,dy, cellSizeX/2, cellSizeY/2);
                // }   
                if(activeCreature.x === unit.x && activeCreature.y === unit.y) {
                    DrawActive(ctx, cellSizeX,cellSizeY, offsetX, offsetY, unit.x, unit.y)
                }

                ctx.drawImage(unit.sprite, sx*32, sy*64, sWidth, sHeight, dx, dy-dHeight*0.5, dWidth, dHeight);
                unit.Update(timestamp);
                test = timestamp;
            });
        }
        //<-----
        stopId = requestAnimationFrame(render);
        
    }, [width,height,cellSizeX,cellSizeY,offsetX,offsetY,boardSizeH,boardSizeV,Cells, castingSpells, units, activeCreature]); 

    useEffect(() => {
        cancelAnimationFrame(stopId);
        stopId = requestAnimationFrame(render);
        return () => {
            cancelAnimationFrame(stopId);
        }
    }, [render]);

    return (
        <>
            <img id="layout" src="Layout.png" style={{width: "100%", height: "100%"}} alt="Layout"></img>
            <canvas id="canvas" ref={canvasRef} style={{width: "100%", height: "100%"}}/>
        </>
        
    );
}