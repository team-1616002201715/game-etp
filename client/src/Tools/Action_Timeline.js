export default class Action_Timeline {
    constructor() {
        this.response = [];
        this.actionType = "";
        this.counter = 0;
    }

    storeResponse(data) {
        this.response = data;
    }

    storeActionType(data) {
        this.actionType = data;
    }

    checkState() {
        return this.counter;
    }

    increaseCounter() {
        this.counter += 1;
    }

    reset() {
        this.response = [];
        this.actionType = "";
        this.counter = 0;
    }

    Update() {
        if(this.counter === 2) {
            this.counter = 0;
            return { update: true,actionType: this.actionType, data: this.response};
        } else {
            return { update: false,actionType: "", data: []};
        }
        
    }




}