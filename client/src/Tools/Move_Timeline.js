export default class Move_Timeline {
    constructor() {
        this.dataPath = [];
        this.dataCreature = {};
        this.counterPath = 0;
        this.counterCreature = 0;
    }

    storePath(data) {
        this.dataPath = data;
    }

    storeCreature(data) {
        this.dataCreature = data;
    }

    checkState() {
        return this.counter;
    }

    increaseCounterPath() {
        this.counterPath = 1;
    }

    reset() {
        this.dataPath = [];
        this.dataCreature = {};
        this.counterPath = 0;
        this.counterCreature = 0;
    }

    increaseCounterCreature() {
        this.counterCreature = 1;
    }

    resetCounterPath() {
        this.counterPath = 0;
    }

    resetCounterCreature() {
        this.counterCreature = 0;
    }

    Update() {
        if(this.counterCreature === 1 && this.counterPath === 1) {
            //Change data in good order
            //Active Creature
            let actvUnitX = this.dataCreature.x;
            let actvUnitY = this.dataCreature.y;
            let prev_x;
            let prev_y;
            let x;
            let y;
            let path;

            if(this.dataPath === "no move") {

                console.log("Noone moved");
          
            } else {
                //Analyze which unit makes a move
                prev_x = actvUnitX;
                prev_y = actvUnitY;
          
                //Set new position
                const entry = Object.entries(this.dataPath).pop()
                x = entry[1].x;
                y = entry[1].y;
          
                path = this.dataPath;
            }

            this.dataPath = [];
            this.dataCreature = {};
            this.counterCreature = 0;
            this.counterPath = 0;

            return {
                path, prev_x, prev_y, x, y
            };
        } else {
            return false;
        }
        
    }




}