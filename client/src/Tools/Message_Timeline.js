export default class Message_Timeline {
    constructor() {
        this.dataMoves = [];
        this.dataAttack = [];
        this.dataCreature = {};
        this.counter = 0;
        this.activePlayer = 0;
        this.init = 4;

        this.path = null;
        this.lastAction = "";
    }

    storeMoves(data) {
        this.dataMoves = data;
    }

    storeAttack(data) {
        this.dataAttack = data;
    }

    storeCreature(data) {
        this.dataCreature = data;
    }

    storeActivePlayer(data) {
        this.activePlayer = data;
    }

    reset() {
        this.dataMoves = [];
        this.dataAttack = [];
        this.dataCreature = {};
        this.counter = 0;
        this.activePlayer = 0;
        this.init = 4;

        this.path = null;
        this.lastAction = "";
    }


    storeLastAction(data) {
        this.lastAction = data;
    }

    storePath(data) {
        this.path = data;
    }



    checkState() {
        return this.counter;
    }

    increaseCounter() {
        this.counter += 1;
    }

    Update(data, Player1, Player2, boardSizeV, boardSizeH, prev_x, prev_y) {
        if(this.counter === this.init) {
            if(this.lastAction === "MOVE" && this.path){
                //Change data in good order
                //Active Creature
                let actvUnitX = this.dataCreature.x;
                let actvUnitY = this.dataCreature.y;
                let actvUnitName = "";
                let actvPlayer = this.activePlayer;
                let path = this.path;
                let x = actvUnitX;
                let y = actvUnitY;

                //Available Moves
                for(let y=0; y<boardSizeV; y++) {
                    for(let x=0; x<boardSizeH; x++) {
                    data[y][x].clickable = 0;
                    data[y][x].attackable = 0;
                    }
                }
                for(const entry of Object.entries(this.dataMoves)) {
                    data[entry[1].y][entry[1].x].clickable = this.activePlayer;
                }
                for(const entry of Object.entries(this.dataAttack)) {
                    data[entry[1].y][entry[1].x].attackable = this.activePlayer;
                }

                this.dataMoves = [];
                this.dataAttack = [];
                this.dataCreature = {};
                this.counter = 0;
                this.activePlayer = 0;

                this.path = null;
                this.lastAction = "";
                this.init = 5;

                return {
                    actvUnitX, actvUnitY, actvUnitName, actvPlayer, data, path, prev_x, prev_y, x, y
                };
            } else if((this.lastAction !== "MOVE" || this.init === 4)&& !this.path) {
                //Change data in good order
                //Active Creature
                let actvUnitX = this.dataCreature.x;
                let actvUnitY = this.dataCreature.y;
                let actvUnitName = ""
                let actvPlayer = this.activePlayer

                //Available Moves
                for(let y=0; y<boardSizeV; y++) {
                    for(let x=0; x<boardSizeH; x++) {
                    data[y][x].clickable = 0;
                    data[y][x].attackable = 0;
                    }
                }
                for(const entry of Object.entries(this.dataMoves)) {
                    data[entry[1].y][entry[1].x].clickable = this.activePlayer;
                }
                for(const entry of Object.entries(this.dataAttack)) {
                    data[entry[1].y][entry[1].x].attackable = this.activePlayer;
                }

                this.dataMoves = [];
                this.dataAttack = [];
                this.dataCreature = {};
                this.counter = 0;
                this.activePlayer = 0;
                
                this.init = 5;
                this.path = null;
                this.lastAction = "";

                return {
                    actvUnitX, actvUnitY, actvUnitName, actvPlayer, data
                };
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }




}