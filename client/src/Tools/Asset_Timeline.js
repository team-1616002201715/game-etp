export default class Asset_Timeline {
    constructor() {
        this.units1 = [];
        this.units2 = [];
        this.assets = [];
        this.counter = 0;
        this.setupComplete = false;
    }

    storeUnits(data) {
        this.units = data;
    }

    storeAssets1(data) {
        this.assets1 = data;
    }

    storeAssets2(data) {
        this.assets2 = data;
    }

    checkState() {
        return this.counter;
    }

    reset() {
        this.units1 = [];
        this.units2 = [];
        this.assets = [];
        this.counter = 0;
        this.setupComplete = false;
    }

    increaseCounter() {
        this.counter += 1;
    }

    resetSetupComplete() {
        this.setupComplete = false;
        this.counter = 0;
    }

    Update() {
        if(this.counter === 3 && !this.setupComplete) {
            this.counter = 0;
            this.setupComplete = true;
            return { update: true, assets1: this.assets1, assets2: this.assets2, units: this.units};
        } else {
            return { update: false, assets1: [], assets2: [], units: []};
        }  
    }
}