import React, { useEffect, useState } from 'react'
import {BrowserRouter as Router, Switch, Link, Route, NavLink} from 'react-router-dom'
import axios from 'axios';


const descriptions2 = {
    ADEBAYO_AKINFENWA: "Adebayo Akinfenwa is one of many immigrants that came to the Federal State. Starting from the bottom of society he decided to enlist in the military. Now he is one of the Heroes of The Great War that led his soldeirs to victory.",
    EMILE_HESKEY:"Emile Heskey didn't want to become part of the military, however her thirst for scientific knowledge led her to the western front and captured German technology",
    JOHN_TERRY: "John became one of the best agents of the CIA. Now whenever the situation is dire there is only one person for the job - John Terry.",

    ALEKSANDR_GOLOVIN: "This young Revolutionary always looks to make his country proud. ",
    ARTEM_DZYUBA: "Artem Dzyuba is a 104 year old general in the Red Army. Noone knows the reason for his longevity... except himself.",
    STANISLAV_CHERCHESOV: "Comming from the harsh conditions of northern Siberia there are many rumours about Stanislav Cherchesov. One of them says he once battled 10 polar bears.",
}

const Economy_Hero = ({leaveLobby, gameId, playerId, heroes}) => {
    const [selectedHero, setSelectedHero] = useState(0);

    let heroSelect = [selectedHero === 0? `hero__portrait--selected` : ``,
                selectedHero === 1? `hero__portrait--selected` : ``,
                selectedHero === 2? `hero__portrait--selected` : ``,
                selectedHero === 3? `hero__portrait--selected` : ``,
                selectedHero === 4? `hero__portrait--selected` : ``,
                selectedHero === 5? `hero__portrait--selected` : ``,
                selectedHero === 6? `hero__portrait--selected` : ``,
                selectedHero === 7? `hero__portrait--selected` : ``,
                selectedHero === 8? `hero__portrait--selected` : ``]

    const pickHero = () => {
        axios.post("http://localhost:8080/economy/"+gameId+"/pickHero/"+playerId, heroes[selectedHero], {headers: {"Content-Type": "text/plain"}}).then(res => {
            console.log("-----------> FROM: pickFraction");
            console.log(res)
        });
    }

    useEffect(() => {
        console.log(heroes);
    }, [])
    return (
        <div className="hero__container">
            <nav className="economy__navbar">
                <img id="navbar__logo" src="/40k-Logo.png" alt="Logo"></img>
                <span onClick={leaveLobby} className="navbar__leave">LEAVE</span>
            </nav>
            <div className="hero__button">
                {heroes.map((hero, i) => (
                    <div key={i} className={`hero__portrait ${heroSelect[i]}`} onClick={() => setSelectedHero(i)}>
                        <img className="hero__img" src={`/Heroes/${hero}.png`}/>
                    </div>
                ))}
            </div>
            <div className="hero__desription">
                <svg viewBox="0 0 263 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 5V55H250.327L201.166 5H5Z" fill="#8FEDE7" stroke="#8FEDE7" strokeWidth="10"/>
                </svg>
                <div className="hero__svg__desciption__name">Description</div>
                {descriptions2[heroes[selectedHero]] ? descriptions2[heroes[selectedHero]] : null}
            </div>
            <button onClick={() => pickHero()} className="hero__next">Next</button>
        </div>
    )
}

export default Economy_Hero
