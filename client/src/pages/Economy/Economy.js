import React from 'react'
import { useState } from 'react/cjs/react.development'
import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"


const Economy = ({goToWiki, goToAbout, leaveLobby, create_game, connectToRandom, startEconomy, gameId, ready, unlockStart, playerId, inLobby, readyP1, readyP2}) => {
    const [inputGameId, setInputGameId] = useState("");

    let lobbyBg = inLobby ? "economy_lobby--inGame" : "economy_lobby";
    let readyClicked = (playerId===1 && readyP1) || (playerId===2 && readyP2) ? "economy__ready__btn--clicked" : "economy__ready__btn";
    let startUnlocked = unlockStart ? "" : "economy__ready__start--locked";

    return (
        <div className="economy__container">
            <nav className="economy__navbar">
                <img id="navbar__logo" src="/40k-Logo.png" alt="Logo"></img>
                {inLobby && <span onClick={leaveLobby} className="navbar__leave">LEAVE</span>}
                {gameId? <div className='economy_gameId' title="Copy to clipboard" onClick={(evt) => {
                        navigator.clipboard.writeText(evt.target.innerText.slice(4));
                        Toastify({
                            text: 'Game ID copied',
                            duration: 2000,
                        }).showToast();
                    }}>
                    {"ID: " + gameId}
                </div> : null}
                <span className="navbar__buttons" onClick={() => goToAbout()}>About<br/>us</span>
                <span className="navbar__buttons" onClick={() => goToWiki()}>Game<br/>Wiki</span>
            </nav>
            <div className={lobbyBg}>
                <img className="economy__logo" src="/40k-Logo.png" alt="Logo"></img>
                <div className="economy__link">
                    {!inLobby ? <div className='economy__create'>
                        <button onClick={() => create_game()}>Create</button>
                    </div>: <></>}
                    {!inLobby ? <div className='economy__input'>
                        <input type="text" placeholder='Game ID' value={inputGameId} onChange={(e) => setInputGameId(e.target.value)}/>
                        <button onClick={() => connectToRandom(inputGameId)}>Join</button>
                    </div>: <></>}

                    {/* <button onClick={() => startEconomyTemp()}>Start Economy Temporary</button> */}
                    {inLobby ? <div className={`economy__ready`}>
                        {playerId === 1 ? <button id={`${startUnlocked}`} onClick={() => startEconomy()}>Start</button> : <></>}
                        <button id={`${readyClicked}`} onClick={() => ready()}>Ready</button>
                    </div>: <></>}
                </div> 
            </div>
            
        </div>
    )
}

export default Economy
