import React, {useState} from 'react'
import {BrowserRouter as Router, Switch, Link, Route, NavLink} from 'react-router-dom'
import axios from 'axios';
import ShopUnitItem from '../../Components/ShopUnitItem';
import ShopArtifactItem from '../../Components/ShopArtifactItem';
import ShopSpellItem from "../../Components/ShopSpellItem";
import { useEffect } from 'react/cjs/react.development';

const Economy_Shop = ({goToGame, leaveLobby, gameId, playerId, units, spells, items, money, resetShopInfo}) => {
    const [tabSelected, setTabSelected] = useState(0);
    const [boughtUnits, setBoughtUnits] = useState([]);
    const [boughtItems, setBoughtItems] = useState([]);
    const [boughtSpells, setBoughtSpells] = useState([]);

    // const boughtUnits = [];
    // const boughtItems = [];
    // const boughtSpells = [];

    let unitsTab = tabSelected === 0? `shop__tabs__tab--active`: `shop__tabs__tab`;
    let itemsTab = tabSelected === 1? `shop__tabs__tab--active`: `shop__tabs__tab`;
    let spellsTab = tabSelected === 2? `shop__tabs__tab--active`: `shop__tabs__tab`;

    const buyItem = (name, amount, type) => {
        if(type === "ARTIFACT") {
            // boughtItems.push({name: name});
            let index = boughtItems.findIndex(u => u.name === name);
            if(index < 0) {
                axios.post("http://localhost:8080/economy/"+gameId+"/buyItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                    console.log("-----------> FROM: Buy");
                    setBoughtItems([...boughtItems, {name: name}]);
                })
            }
        }
        if(type === "SPELL") {
            // boughtItems.push({name: name});
            let index = boughtSpells.findIndex(u => u.name === name);
            if(index < 0) {
                axios.post("http://localhost:8080/economy/"+gameId+"/buyItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                    console.log("-----------> FROM: Buy");
                    setBoughtSpells([...boughtSpells, {name: name}]);
                })
            }
        }
        if(type === "CREATURE") {
            axios.post("http://localhost:8080/economy/"+gameId+"/buyItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                console.log("-----------> FROM: Buy");
                console.log(res)
                let index = boughtUnits.findIndex(u => u.name === name);
                if(index>-1) {
                    let temp = boughtUnits.filter(u => u.name !== name);
                    setBoughtUnits([...temp, {name: name, amount: parseInt(boughtUnits[index].amount)+parseInt(amount)}]);
                } else {
                    setBoughtUnits([...boughtUnits, {name: name, amount: parseInt(amount)}]);
                }
            });
        } 
    }

    const buyMaxCreature = (name, maxAmount) => {
        axios.post("http://localhost:8080/economy/"+gameId+"/buyMaxCreature/"+playerId, name, { headers: {'Content-Type': 'text/plain'} }).then(res => {
            console.log("-----------> FROM: Buy Max");
            console.log(res)
            //Look if you have bought this creature?
            let index = boughtUnits.findIndex(u => u.name === name);
            if(index>-1) {
                //Yes - add amount
                let temp = boughtUnits.filter(u => u.name !== name);
                setBoughtUnits([...temp, {name: name, amount: maxAmount}]);
            } else {
                //No - push to array
                setBoughtUnits([...boughtUnits, {name: name, amount: maxAmount}]);
            }
        });
    }

    const sellItem = (name, amount, type) => {
        if(type === "CREATURE") {
            console.log("Test 1");
            if(name) {
                console.log("Test 2");
                //Look if you have bought this creature?
                let index = boughtUnits.findIndex(u => u.name === name);
                if(index>-1 && parseInt(boughtUnits[index].amount) >= parseInt(amount)) {
                    axios.post("http://localhost:8080/economy/"+gameId+"/sellItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                        console.log("-----------> FROM: Sell");
                        console.log(res);
                        if(parseInt(boughtUnits[index].amount) > parseInt(amount)) {
                            let temp = boughtUnits.filter(u => u.name !== name);
                            setBoughtUnits([...temp, {name: name, amount: parseInt(boughtUnits[index].amount) - parseInt(amount)}]);
                        } else if(parseInt(boughtUnits[index].amount) === parseInt(amount)) {
                            let temp = boughtUnits.filter(u => u.name !== name);
                            setBoughtUnits([...temp]);
                        }
                    });
                }
            }
        }
        if(type === "ARTIFACT") {
            let index = boughtItems.findIndex(u => u.name === name);
            if(index>-1) {
                axios.post("http://localhost:8080/economy/"+gameId+"/sellItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                    console.log("-----------> FROM: Sell");
                    console.log(res);
                    let temp = boughtItems.filter(u => u.name !== name);
                    setBoughtItems([...temp]);
                });
            }
        }
        if(type === "SPELL") {
            let index = boughtSpells.findIndex(u => u.name === name);
            if(index>-1) {
                axios.post("http://localhost:8080/economy/"+gameId+"/sellItem/"+playerId, {"ITEM_TYPE":type, "itemName":name, "amount":amount}).then(res => {
                    console.log("-----------> FROM: Sell");
                    console.log(res);
                    let temp = boughtSpells.filter(u => u.name !== name);
                    setBoughtSpells([...temp]);
                });
            }
        }
    }

    useEffect(() => {
        return () => {
            resetShopInfo();
        };
    }, [])

    useEffect(() => {
        console.log(boughtItems)
    }, [ boughtItems ])

    return (
        <div className="shop__container">
            <nav className="economy__navbar">
                <img id="navbar__logo" src="/40k-Logo.png" alt="Logo"></img>
                <span onClick={leaveLobby} className="navbar__leave">LEAVE</span>
            </nav>
            <div className="shop__main">
                <div className="shop__tabs">
                    <div className={unitsTab} onClick={() => setTabSelected(0)}>Units</div>
                    <div className={itemsTab} onClick={() => setTabSelected(1)}>Items</div>
                    <div className={spellsTab} onClick={() => setTabSelected(2)}>Spells</div>
                </div>
                <div className="shop__window">
                    {tabSelected===0 ? units?.map((unit, i) => (
                        <ShopUnitItem key={i} unitName={unit.creature.name} buyItem={buyItem} buyMaxCreature={buyMaxCreature} sellItem={sellItem} unit={unit}/>
                    )) : tabSelected===1 ? items?.map((item, i) => (
                        <ShopArtifactItem key={i} artifactName={item.item.name} buyItem={buyItem} buyMaxCreature={buyMaxCreature} sellItem={sellItem} artifact={item}/>
                    )) : spells?.map((spell, i) => (
                        <ShopSpellItem key={i} spellName={spell.spell.name} buyItem={buyItem} buyMaxCreature={buyMaxCreature} sellItem={sellItem} spell={spell}/>

                    ))}
                </div>
                {/* <div className="shop__spells">
                    <div onClick={() => sellItem(boughtSpells.length >= 1 ? boughtSpells[0].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 1 ? `${boughtSpells[0].name}` : ""}</div>
                    <div onClick={() => sellItem(boughtSpells.length >= 2 ? boughtSpells[1].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 2 ? `${boughtSpells[1].name}` : ""}</div>
                    <div onClick={() => sellItem(boughtSpells.length >= 3 ? boughtSpells[2].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 3 ? `${boughtSpells[2].name}` : ""}</div>
                    <div onClick={() => sellItem(boughtSpells.length >= 4 ? boughtSpells[3].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 4 ? `${boughtSpells[3].name}` : ""}</div>
                    <div onClick={() => sellItem(boughtSpells.length >= 5 ? boughtSpells[4].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 5 ? `${boughtSpells[4].name}` : ""}</div>
                    <div onClick={() => sellItem(boughtSpells.length >= 6 ? boughtSpells[5].name : "NULL", 1, "SPELL")} className="shop__spells__spell">{boughtSpells.length >= 6 ? `${boughtSpells[5].name}` : ""}</div>
                </div> */}
                <div className="shop__spells">
                    {boughtSpells.map((item, i) => {
                        return <div key={i} onClick={() => sellItem(item.name, 1, "SPELL")} className="shop__spells__spell"><div style={{flexGrow: 1}}>{`${item.name}`}</div><img style={{height: '70%'}} src={`/ShopAssets/${item.name.toLowerCase().replace(/ /g, '-').replace(/'/g, '')}.png`} /></div>
                    })}
                </div>
                <div className="shop__items">
                    {boughtItems.map((item, i) => {
                        return <div key={i} onClick={() => sellItem(item.name, 1, "ARTIFACT")} className="shop__items__item"><div style={{flexGrow: 1}}>{`${item.name}`}</div><img style={{height: '70%'}} src={`/ShopAssets/${item.name.toLowerCase().replace(/ /g, '-').replace(/'/g, '')}.png`} /></div>
                    })}
                </div>
                <div className="shop__units">
                    <div onClick={() => sellItem(boughtUnits.length >= 1 ? boughtUnits[0].name : null, boughtUnits.length >= 1 ? boughtUnits[0].amount : null, "CREATURE")} className="shop__units__unit">{boughtUnits.length >= 1 ? <><img src={`/ShopAssets/${boughtUnits[0].name}.png`}/><div className='unit__amount'>{boughtUnits[0].amount}</div></> : ""}</div>
                    <div onClick={() => sellItem(boughtUnits.length >= 2 ? boughtUnits[1].name : null, boughtUnits.length >= 2 ? boughtUnits[1].amount : null, "CREATURE")} className="shop__units__unit">{boughtUnits.length >= 2 ? <><img src={`/ShopAssets/${boughtUnits[1].name}.png`}/><div className='unit__amount'>{boughtUnits[1].amount}</div></> : ""}</div>
                    <div onClick={() => sellItem(boughtUnits.length >= 3 ? boughtUnits[2].name : null, boughtUnits.length >= 3 ? boughtUnits[2].amount : null, "CREATURE")} className="shop__units__unit">{boughtUnits.length >= 3 ? <><img src={`/ShopAssets/${boughtUnits[2].name}.png`}/><div className='unit__amount'>{boughtUnits[2].amount}</div></> : ""}</div>
                    <div onClick={() => sellItem(boughtUnits.length >= 4 ? boughtUnits[3].name : null, boughtUnits.length >= 4 ? boughtUnits[3].amount : null, "CREATURE")} className="shop__units__unit">{boughtUnits.length >= 4 ? <><img src={`/ShopAssets/${boughtUnits[3].name}.png`}/><div className='unit__amount'>{boughtUnits[3].amount}</div></> : ""}</div>
                    <div onClick={() => sellItem(boughtUnits.length >= 5 ? boughtUnits[4].name : null, boughtUnits.length >= 5 ? boughtUnits[4].amount : null, "CREATURE")} className="shop__units__unit">{boughtUnits.length >= 5 ? <><img src={`/ShopAssets/${boughtUnits[4].name}.png`}/><div className='unit__amount'>{boughtUnits[4].amount}</div></> : ""}</div>
                </div>
            </div>
            <div className='shop__money'>
                {money}$
            </div>
            <button onClick={() => {
                if(boughtUnits.length > 0) {
                    goToGame()
                }
            }} className="shop__next">Next</button> 
        </div>
    )
}

export default Economy_Shop
