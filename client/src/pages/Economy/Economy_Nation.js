import React, {useState, useEffect} from 'react'
import {BrowserRouter as Router, Switch, Link, Route, NavLink} from 'react-router-dom'
import axios from 'axios';

const descriptionOne = "A Federation of North American states came to be after United States conquered all of North America. After winning II World War they incorporated German scientists to create powerful machines of war. Their army consits of highly advanced mechs and augmented soldiers.";
const descriptionTwo = "Nothing would stop the Sovejan people. Hardened by the harsh winds of north Siberia they marched to Berlin crushing everything on their way. Death is no excuse for your soldiers - Soveja uses their necromancy and numbers to overwhelm their opponents.";

const Economy_Nation = ({leaveLobby, gameId, playerId}) => {
    const [description, setDescription] = useState(descriptionOne);
    const [selected, setSelected] = useState("FEDERAL_STATE");

    const handleChooseNAtion = (id) => {
        if(id === 0) {
            setSelected("FEDERAL_STATE")
        } else {
            setSelected("SOVEJA")
            // pickFraction()
        }
    }

    const pickFraction = () => {
        axios.post("http://localhost:8080/economy/"+gameId+"/pickFraction/"+playerId, selected, {headers: {"Content-Type": "text/plain"}}).then(res => {
            console.log("-----------> FROM: pickFraction");
            console.log(res)
        });
    }

    useEffect(() => {
        if (selected === "FEDERAL_STATE") {
            setDescription(descriptionOne);
        } else {
            setDescription(descriptionTwo);
        }
    }, [selected]);

    let nationButtonLeft = selected === "FEDERAL_STATE"? `nation__left--active`:`nation__left`
    let nationButtonRight = selected === "SOVEJA"? `nation__right--active`:`nation__right`

    return (
        <div className="nation__container">
            <nav className="economy__navbar">
                <img id="navbar__logo" src="/40k-Logo.png" alt="Logo"></img>
                <span onClick={leaveLobby} className="navbar__leave">LEAVE</span>
            </nav>
            <button className={`nation__button ${nationButtonLeft}`} onClick={() => handleChooseNAtion(0)}>
                <svg viewBox="0 0 263 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 5V55H250.327L201.166 5H5Z" fill="#8FEDE7" stroke="#8FEDE7" strokeWidth="10"/>
                </svg>
                <div className="nation__svg__name">FED STATE</div>
                <img className='nation__img' src={`/Fractions/FEDERAL_STATE.png`}/>
            </button>
            <button className={`nation__button ${nationButtonRight}`} onClick={() => handleChooseNAtion(1)}>
                <svg viewBox="0 0 263 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 5V55H250.327L201.166 5H5Z" fill="#8FEDE7" stroke="#8FEDE7" strokeWidth="10"/>
                </svg>
                <div className="nation__svg__name">SOVEJA</div>
                <img className='nation__img' src={`/Fractions/SOVEJA.png`}/>
            </button>
            <div className="nation__desription">
                <svg viewBox="0 0 263 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 5V55H250.327L201.166 5H5Z" fill="#8FEDE7" stroke="#8FEDE7" strokeWidth="10"/>
                </svg>
                <div className="nation__svg__desciption__name">Description</div>
                {description}
            </div>
            <button className="nation__next" onClick={() => pickFraction()}>Next</button>
        </div>
        
    )
}

export default Economy_Nation
