import React from "react";

export const Items = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="background.png" alt="Layout_front"></img>
                    <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                </div>
        </div>
     );
}

export const Blackshard = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>BLACKSHARD OF THE DEAD KNIGHT</h2>
                        </div>
                        <div className="itemsdesc">
                            GIVES +3 ATTACK SKILL
                        </div>
                        <div className="itemsstats">
                                SLOT: RIGHT HAND
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: SPELL
                            
                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Collar = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>COLLAR OF CONJURING</h2>
                        </div>
                        <div className="itemsdesc">
                            Increases duration of all your spells by 1
                        </div>
                        <div className="itemsstats">

                                SLOT: NECK
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: SPELL
                        
                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Orb = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>ORB OF TEMPESTUOUS FIRE</h2>
                        </div>
                        <div className="itemsdesc">
                            Your hero's fire spells deal extra 50% damage
                        </div>
                        <div className="itemsstats">

                                SLOT: NECK
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: CREATURE
                            
                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Ring = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>RING OF VITALITY</h2>
                        </div>
                        <div className="itemsdesc">
                            Increases health of all your units by 1
                        </div>
                        <div className="itemsstats">

                                SLOT: FINGER
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: CREATURE
                            
                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Shield = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>SHIELD OF THE YAWNING DEAD</h2>
                        </div>
                        <div className="itemsdesc">
                            Gives +3 defence skill
                        </div>
                        <div className="itemsstats">

                                SLOT: LEFT HAND
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: CREATURE

                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Stateman = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>STATEMAN'S MEDAL</h2>
                        </div>
                        <div className="itemsdesc">
                            Reduces the cost of surrendering
                        </div>
                        <div className="itemsstats">

                                SLOT: NECK
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: SPELL

                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}

export const Surcoat = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>SURCOAT OF COUNTERPOISE</h2>
                        </div>
                        <div className="itemsdesc">
                            Gives +10% to magic resistance
                        </div>
                        <div className="itemsstats">

                                SLOT: SHOULDERS
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: CREATURE
                            </div>
                        </div>
                    </div>     

        </div>
     );  

     
}

export const Tome = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>TOME OF AIR MAGIC</h2>
                        </div>
                        <div className="itemsdesc">
                            All air spells are available to hero when equipped
                        </div>
                        <div className="itemsstats">

                                SLOT: RIGHT HAND
                                <br></br>
                                COST: 2000 
                                <br></br>
                                TYPE: SPELL

                        </div>
                    </div>     
                </div>
        </div>
     );  

     
}
 