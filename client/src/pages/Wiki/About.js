import React from "react";

const About = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="background.png" alt="Layout_front"></img>
                </div>
                <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
            <h1 className='about'>
                Welcome to Trifecta. This is a browser turn-based strategy game where the player can choose between two factions: Federal State or Soveja. With numerous and varied creatures, items, spells and mechanics you can provide yourself and your friends with hours of great fun. Choose your creatures, buy the right equipment in the economy layer, develop tactics and set out to conquer the world of Trifecta!
                <br></br><br></br><a href="https://trifecta.azurewebsites.net/" >GO TO THE TRIFECTA</a>
                </h1>  
        </div>
     );
}
 
export default About;