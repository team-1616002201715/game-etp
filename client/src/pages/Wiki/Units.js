import React from "react";

export const Units = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="background.png" alt="Layout_front"></img>
                    <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                </div>
        </div>
     );
}

export const Handyman = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>HANDYMAN</h2>
                        </div>
                        <div className="unitsdesc">
                        Has a 50% chance of his attack dealing twice as much damage
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Handyman.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 20
                                <br></br>
                                MIN DAMAGE: 5
                                <br></br>
                                MAX DAMAGE: 5
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0
                                <br></br>
                                GROWTH: 3
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}
 
export const Spectre = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>SPECTRE</h2>
                        </div>
                        <div className="unitsdesc">
                        Shooting creature. Ignores the opponent's armor
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Spectre.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 14
                                <br></br>
                                MIN DAMAGE: 2
                                <br></br>
                                MAX DAMAGE: 4
                                <br></br>
                                MOVE RANGE: 3
                                RESISTANCE TO MAGIC: 0
                                <br></br>
                                GROWTH: 7
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Hedgehog = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>HEDGEHOG</h2>
                        </div>
                        <div className="unitsdesc">
                        Blocks counter-attack
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Hedgehog.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 10
                                <br></br>
                                MIN DAMAGE: 2
                                <br></br>
                                MAX DAMAGE: 2
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 12
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Marauder = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>MARAUDER</h2>
                        </div>
                        <div className="unitsdesc">
                        Without special powers
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Marauder.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 3
                                <br></br>
                                MAX HP: 30
                                <br></br>
                                MIN DAMAGE: 7
                                <br></br>
                                MAX DAMAGE: 7
                                <br></br>
                                MOVE RANGE: 8
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 1
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const NewYorker = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>NEW YORKER</h2>
                        </div>
                        <div className="unitsdesc">
                        Attacks twice in a row
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/New Yorker.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 14
                                <br></br>
                                MIN DAMAGE: 3
                                <br></br>
                                MAX DAMAGE: 5
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 4
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Patriot = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>PATRIOT</h2>
                        </div>
                        <div className="unitsdesc">
                        Shooting creature
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Patriot.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 4
                                <br></br>
                                MAX HP: 30
                                <br></br>
                                MIN DAMAGE: 5
                                <br></br>
                                MAX DAMAGE: 12
                                <br></br>
                                MOVE RANGE: 3
                                RESISTANCE TO MAGIC: 0
                                <br></br>
                                GROWTH: 1
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Railgun = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>RAILGUN</h2>
                        </div>
                        <div className="unitsdesc">
                        Shooting creature. Splash damage 3x3
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Railgun.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 8
                                <br></br>
                                MIN DAMAGE: 2
                                <br></br>
                                MAX DAMAGE: 3
                                <br></br>
                                MOVE RANGE: 0
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 8
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const TroopCarrier = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>TROOP CARRIER</h2>
                        </div>
                        <div className="unitsdesc">
                        Without special powers
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Troop Carrier.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: FEDERAL STATE
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 2
                                <br></br>
                                MAX HP: 30
                                <br></br>
                                MIN DAMAGE: 3
                                <br></br>
                                MAX DAMAGE: 4
                                <br></br>
                                MOVE RANGE: 7
                                RESISTANCE TO MAGIC: 30
                                <br></br>
                                GROWTH: 2
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Ivaylo = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>IVAYLO</h2>
                        </div>
                        <div className="unitsdesc">
                        Has a 50% chance of his attack dealing twice as much damage
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Ivaylo.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 0
                                <br></br>
                                MAX HP: 12
                                <br></br>
                                MIN DAMAGE: 2
                                <br></br>
                                MAX DAMAGE: 4
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 12
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Officer = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>OFFICER</h2>
                        </div>
                        <div className="unitsdesc">
                        Blocks counter-attack
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Officer.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 0
                                <br></br>
                                MAX HP: 12
                                <br></br>
                                MIN DAMAGE: 2
                                <br></br>
                                MAX DAMAGE: 3
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 8
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Undead = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>UNDEAD</h2>
                        </div>
                        <div className="unitsdesc">
                        After being attacked it heals 15% of its current HP
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Undead.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 0
                                <br></br>
                                MAX HP: 20
                                <br></br>
                                MIN DAMAGE: 3
                                <br></br>
                                MAX DAMAGE: 5
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 7
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Werewolf = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>WEREWOLF</h2>
                        </div>
                        <div className="unitsdesc">
                        After being attacked it heals 10% of its current HP"
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Werewolf.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 0
                                <br></br>
                                MAX HP: 20
                                <br></br>
                                MIN DAMAGE: 4
                                <br></br>
                                MAX DAMAGE: 5
                                <br></br>
                                MOVE RANGE: 9
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 4
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Troglodyte = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>TROGLODYTE</h2>
                        </div>
                        <div className="unitsdesc">
                        Without special powers
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Troglodyte.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 0
                                <br></br>
                                MAX HP: 30
                                <br></br>
                                MIN DAMAGE: 3
                                <br></br>
                                MAX DAMAGE: 7
                                <br></br>
                                MOVE RANGE: 7
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 3
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Minigunner = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>Minigunner</h2>
                        </div>
                        <div className="unitsdesc">
                        Shooting creature
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Minigunner.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 1
                                <br></br>
                                MAX HP: 25
                                <br></br>
                                MIN DAMAGE: 7
                                <br></br>
                                MAX DAMAGE: 7
                                <br></br>
                                MOVE RANGE: 3
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 2
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const BabaYaga = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>BABA YAGA</h2>
                        </div>
                        <div className="unitsdesc">
                        Can move twice in a turn
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Baba Yaga.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: FLYING
                                ARMOR: 2
                                <br></br>
                                MAX HP: 25
                                <br></br>
                                MIN DAMAGE: 6
                                <br></br>
                                MAX DAMAGE: 9
                                <br></br>
                                MOVE RANGE: 8
                                RESISTANCE TO MAGIC: 0 
                                <br></br>
                                GROWTH: 1
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Matushka = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='units'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='unitstitle'>
                            <h2>MATUSHKA</h2>
                        </div>
                        <div className="unitsdesc">
                        Without special powers
                        </div>
                        <div className="unitsstats">
                            <img id="layoutFront" style={{position:'fixed'}} src="/ShopAssets/Matushka.png" alt="Layout_front"></img>
                            <div className="unitsstatstext">
                                NATION: SOVEJA
                                <br></br>
                                TYPE: GROUND
                                ARMOR: 3
                                <br></br>
                                MAX HP: 40
                                <br></br>
                                MIN DAMAGE: 7
                                <br></br>
                                MAX DAMAGE: 12
                                <br></br>
                                MOVE RANGE: 5
                                RESISTANCE TO MAGIC: 3
                                <br></br>
                                GROWTH: 1
                            </div>
                        </div>
                    </div>     
                </div>
        </div>
     );  
}