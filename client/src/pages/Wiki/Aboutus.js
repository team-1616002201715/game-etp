import React from "react";

const Aboutus = () => {
    return ( 
        
        <div className='aboutus'>
            <img id="layoutFront" src="backgroundabout.png" style={{position:'fixed' }} alt="Layout_front"></img>
        
            <div className="title">
                <h1>ABOUT TRIFECTA PROJECT</h1>
            </div>

            <div className='description'>
                <h1>We are 4th year students of Computer Science at the Adam&nbsp;Mickiewicz&nbsp;University&nbsp;in Poznan. This project was created for the purpose of obtaining a Bachelor&nbsp;degree in Computer&nbsp;Science. The goal of our project was to create a browser turn-based strategy game that you and your friends can play with no download or installation required. Don't wait and join the Trifecta! </h1>
                
                <a href="https://trifecta.azurewebsites.net/" >GO TO THE TRIFECTA</a>
            </div>
            <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>

            <div className="title two">
                <h1>TRIFECTA TEAM</h1>
            </div>

            <div className="row">
                <div className="column">
                    <img id="jakubPic" src="jakub.png" style={{position:'fixed' }} alt="jakubPic"></img>
                    <p>Jakub Przybyła</p>
                    <p2>Team Leader</p2>
                </div>
                <div className="column">
                    <img id="marcelPic" src="marcel.png" style={{position:'fixed' }} alt="marcelPic"></img>
                    <p>Marcel&nbsp;Szmeterowicz</p>
                    <p2>Developer</p2>
                </div>
                <div className="column">
                    <img id="patrykPic" src="patryk.png" style={{position:'fixed' }} alt="patrykPic"></img>
                    <p>Patryk&nbsp;Biesiada</p>
                    <p2>Developer</p2>
                </div>
                <div className="column">
                    <img id="adamPic" src="adam.png" style={{position:'fixed' }} alt="adamPic"></img>
                    <p>Adam Toppmayer</p>
                    <p2>Developer</p2>
                </div>           
            </div>
        </div>

     );
}
 
export default Aboutus;