import React from "react";

export const Spells = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="background.png" alt="Layout_front"></img>
                    <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                </div>
        </div>
     );
}

export const Haste = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>HASTE</h2>
                        </div>
                        <div className="itemsdesc">
                        Increases the speed of the selected unit
                        </div>
                        <div className="itemsstats">
                                MANA COST: 6
                                <br></br>
                                GOLD COST: 100
                                <br></br>
                                SPELL TYPE: BUFF
                                <br></br>
                                TARGET TYPE: ALLY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Bloodlust = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>BLOODLUST</h2>
                        </div>
                        <div className="itemsdesc">
                        Increases armor of the selected unit by 20% for 2 turns
                        </div>
                        <div className="itemsstats">
                                MANA COST: 6
                                <br></br>
                                GOLD COST: 200
                                <br></br>
                                SPELL TYPE: BUFF
                                <br></br>
                                TARGET TYPE: ALLY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Dispell = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>DISPELL</h2>
                        </div>
                        <div className="itemsdesc">
                        Increases armor of the selected unit by 5 for 4 turns
                        </div>
                        <div className="itemsstats">
                                MANA COST: 5
                                <br></br>
                                GOLD COST: 300
                                <br></br>
                                SPELL TYPE: BUFF
                                <br></br>
                                TARGET TYPE: ALLY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Teleport = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>RAGE</h2>
                        </div>
                        <div className="itemsdesc">
                        Increases damage of the selected unit by 30% for 2 turns
                        </div>
                        <div className="itemsstats">
                                MANA COST: 15
                                <br></br>
                                GOLD COST: 400
                                <br></br>
                                SPELL TYPE: BUFF
                                <br></br>
                                TARGET TYPE: ALLY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Slow = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>SLOW</h2>
                        </div>
                        <div className="itemsdesc">
                        Decreases damage of the selected unit by 50% for 3 turns
                        </div>
                        <div className="itemsstats">
                                MANA COST: 6
                                <br></br>
                                GOLD COST: 700
                                <br></br>
                                SPELL TYPE: DEBUFF
                                <br></br>
                                TARGET TYPE: ENEMY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Fireball = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>FIREBALL</h2>
                        </div>
                        <div className="itemsdesc">
                        Splash damage 3x3. Damage depends on the hero's power: heroPower * 10 + 15
                        </div>
                        <div className="itemsstats">
                                MANA COST: 15
                                <br></br>
                                GOLD COST: 500
                                <br></br>
                                SPELL TYPE: DAMAGE
                                <br></br>
                                TARGET TYPE: MAP       
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const Implosion = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>IMPLOSION</h2>
                        </div>
                        <div className="itemsdesc">
                        Damage spell. Damage depends on the hero's power: heroPower * 75 + 100
                        </div>
                        <div className="itemsstats">
                                MANA COST: 30
                                <br></br>
                                GOLD COST: 600
                                <br></br>
                                SPELL TYPE: DAMAGE
                                <br></br>
                                TARGET TYPE: ENEMY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const DeathRipple = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>DEATH RIPPLE</h2>
                        </div>
                        <div className="itemsdesc">
                        Damage spell. Damage depends on the hero's power: heroPower * 5 + 10
                        </div>
                        <div className="itemsstats">
                                MANA COST: 10
                                <br></br>
                                GOLD COST: 800
                                <br></br>
                                SPELL TYPE: DAMAGE
                                <br></br>
                                TARGET TYPE: ENEMY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

export const MagicArrow = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="/background.png" alt="Layout_front"></img>
                    <div className='items'>
                    <div className="togame">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
                        <div className='itemstitle'>
                            <h2>MAGIC ARROW</h2>
                        </div>
                        <div className="itemsdesc">
                        Damage spell. Damage depends on the hero's power: heroPower * 10 + 10
                        </div>
                        <div className="itemsstats">
                                MANA COST: 5
                                <br></br>
                                GOLD COST: 900
                                <br></br>
                                SPELL TYPE: DAMAGE
                                <br></br>
                                TARGET TYPE: ENEMY        
                        </div>
                    </div>     
                </div>
        </div>
     );  
}

