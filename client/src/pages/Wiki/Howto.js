import React from "react";

const Howto = () => {
    return ( 
        
        <div className='back'>
                <div className="bckgrnd>">
                    <img id="layoutFront" style={{position:'fixed'}} src="background.png" alt="Layout_front"></img>
                </div>
                    <div className="togamemain">
                                    <h1><a href="https://trifecta.azurewebsites.net/" >TO GAME</a></h1>
                    </div>
            <div className='howto'>
                After pressing the "Create" button, a room ID will appear in the upper right corner - copy it, paste it in the guest browser and join the game by pasting the copied room ID into the box under the "Create" button, then click "Join".            
            
                <div className='howtoimg'>
                    <br></br>
                    <img id="layouthow" style={{position:'static'}} src="joinhow.png" alt="Layout_front"></img>
                </div>

                <br></br><br></br>
                After pressing this button, both the room host and his guest will see the "Ready" button on the screen. Once both players are ready to play, the host will have the option to start the game using the "Start" button.
                

                <div className='howtoimg'>
                    <br></br>
                    <img id="layouthow" style={{position:'static'}} src="readyhow.png" alt="Layout_front"></img>
                </div>

                <br></br><br></br>
                After the host starts the game, players will be moved to the economy phase. First, players will have a choice of fraction and hero, after selecting which they will be able to purchase any chosen creatures, artifacts or skills (of course, if their funds allow it). 

                <div className='howtoimgbig'>
                    <br></br>
                    <img id="layouthow" style={{position:'static'}} src="economyhow.png" alt="Layout_front"></img>
                </div>

                <br></br><br></br>
                Once both players press the "Next" button, the game will move to the battle phase. 

                <div className='howtoimgbig'>
                    <br></br>
                    <img id="layouthow" style={{position:'static'}} src="gameplayhow.png" alt="Layout_front"></img>
                </div>

                <br></br><br></br>
                In the battle phase, the active player can move his unit, attack enemy units and use previously purchased skills. When the player has finished moving, he must press the "Pass" button in the lower right corner.
                <br></br><br></br>
                At the end of the battle, after one of the players has lost all his units, the player who won sees "Victory" on his screen, while the loser sees "Defeat". The players have the option to have a rematch or exit the game.

                <div className='victory'>
                    <br></br>
                    <img id="layouthow" style={{position:'static'}} src="victoryhow.png" alt="Layout_front"></img>
                </div>


                <br></br><br></br><a href="https://trifecta.azurewebsites.net/" >GO TO THE TRIFECTA</a>
            </div>  
        </div>
     );
}
 
export default Howto;