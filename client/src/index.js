import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Wiki from './Wiki';
import Aboutus from './pages/Wiki/Aboutus'
import AppContainer from "./Components/AppContainer"
import { BrowserRouter as Router } from 'react-router-dom';


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <AppContainer />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);